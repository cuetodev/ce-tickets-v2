<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
  'prefix' => LaravelLocalization::setLocale(),
  'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
], function()
{
  Route::get('/', 'IndexController@index');
  Route::get('events', 'EventController@index');
  Route::get('events/{slug}', 'EventController@show');

  Route::get('categories', 'CategoryController@index');

  Route::get('categories/{slug}', 'CategoryController@show');

  Route::get('countries', 'CountryController@index');
  Route::get('countries/{slug}', 'CountryController@show');

  Route::get('cities/{slug}', 'CityController@show');

  Route::get('search', 'SearchController@index');

  Route::get('/auth/{provider}', 'Auth\SocialController@getSocialRedirect');
  Route::get('/auth/handle/{provider}', 'Auth\SocialController@getSocialHandle');

  Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard', 'ProfileController@index')->name('home');

    Route::get('organization/become', 'OrganizationRequestController@index');
    Route::post('organization/make_request', 'OrganizationRequestController@makeRequest');
    Route::get('organization/is_organizer', 'OrganizationRequestController@is_organizer')->name('organization.is_organizer');

    Route::post('dashboard/update_basic_info', 'ProfileController@basicInfo');
    Route::post('dashboard/change_password', 'ProfileController@changePassword');
    Route::post('dashboard/update_avatar', 'ProfileController@updateAvatar');

    Route::get('/dashboard/gift', 'GiftsController@index');

    Route::post('gift/{ticket}/generate', 'GiftsController@generateGift');

    Route::post('gift/{gift}/accept', 'GiftsController@acceptGift');
    Route::post('gift/{gift}/reject', 'GiftsController@rejectGift');

    Route::get('tickets', 'TicketsControllers@all');
    Route::get('tickets/{ticket_id}', 'TicketsControllers@single');

    Route::get('check-out', 'OrderController@makeOrder');
    Route::get('check-out/pay', 'OrderController@checkOut');

    Route::get('order/payment', 'OrderController@payment');

    Route::get('invoice/{invoice_id}', 'OrderController@invoice');

    Route::get('order/cancel', 'OrderController@cancel');
    Route::get('payment/success', 'OrderController@success');
    Route::get('payment/f/success', 'OrderController@successAlt');

    Route::get('/payment/paypal/result', 'PaypalController@getPaymentStatus');
    Route::post('events/{event}/reviews', 'ReviewController@store');


    Route::post('/order/stripe/payment', 'OrderController@stripePayment');
    Route::get('/order/stripe/success', 'OrderController@stripeSuccess');

  });

  Route::get('cart', 'CartController@index');
  Route::post('cart/add/event/{event}/', 'CartController@addItem');
  Route::post('cart/add/event/{event}/m', 'CartController@addMultipleItems');

  Route::get('cart/remove/{item}', 'CartController@removeItem');
  Route::post('cart/update/{item}/price', 'CartController@updatePrice');
  Route::get('cart/clear', 'CartController@clearCart');

  Auth::routes();

});
