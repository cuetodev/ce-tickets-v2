<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Starting auth for api_token
Route::post('auth', 'Auth\ApiAuthenticationController@login');

Route::post('auth/register', 'Auth\ApiAuthenticationController@register');

Route::post('auth/social/{provider}', 'Auth\ApiAuthenticationController@socialAuth');

// Need Autentication
Route::group(['middleware' => 'auth:api'], function()
{
  // Users
  Route::get('users', 'Api\UserController@index');
  Route::get('users/{user}', 'Api\UserController@show');
  Route::put('users/{user}', 'Api\UserController@update');
  Route::delete('users/{user}', 'Api\UserController@destroy');
  Route::get('users/{user}/assign_role/{role}', 'Api\UserController@assignRole');
  Route::get('users/{user}/remove_role/{role}', 'Api\UserController@removeRole');

  //roles
  Route::get('roles', 'Api\RoleController@index');
  Route::post('roles', 'Api\RoleController@store');
  Route::get('roles/{role}', 'Api\RoleController@show');
  Route::put('roles/{role}', 'Api\RoleController@update');
  Route::delete('roles/{role}', 'Api\RoleController@destroy');

  //events
  Route::post('events', 'Api\EventController@store');
  Route::put('events/{event}', 'Api\EventController@update');
  Route::delete('events/{event}', 'Api\EventController@destroy');

  Route::get('events/{event}/assign/category/{category}', 'Api\EventController@assignCategory');

  //categories
  Route::post('categories', 'Api\CategoryController@store');
  Route::put('categories/{category}', 'Api\CategoryController@update');
  Route::delete('categories/{category}', 'Api\CategoryController@destroy');

  //Reviews
  Route::post('reviews', 'Api\ReviewController@store');
  Route::put('reviews/{review}', 'Api\ReviewController@update');
  Route::delete('reviews/{review}', 'Api\ReviewController@destroy');

  //Entrance
  Route::get('entrances', 'Api\EntranceController@index');
  Route::get('entrances/{entrance}', 'Api\EntranceController@show');
  Route::post('entrances', 'Api\EntranceController@store');
  Route::put('entrances/{entrance}', 'Api\EntranceController@update');
  Route::delete('entrances/{entrance}', 'Api\EntranceController@destroy');

  //Organizations
  Route::get('organizations', 'Api\OrganizationController@index');
  Route::get('organizations/{organization}', 'Api\OrganizationController@show');
  Route::post('organizations', 'Api\OrganizationController@store');
  Route::put('organizations/{organization}', 'Api\OrganizationController@update');
  Route::delete('organizations/{organization}', 'Api\OrganizationController@destroy');

  Route::get('organizations/request/all', 'Api\OrganizationRequestController@index');

  //Organization Request - Organization actions
  Route::get('organization/request/{organizationRequest}/aprove', 'Api\OrganizationRequestController@aprove');
  Route::get('organization/request/{organizationRequest}/reject', 'Api\OrganizationRequestController@reject');

  //Tickets
  Route::get('tickets', 'Api\TicketController@index');
  Route::get('tickets/event/{event}', 'Api\TicketController@ticketsByEvent');
  Route::post('tickets/check', 'Api\TicketController@checkTicket');
  Route::post('tickets/verify', 'Api\TicketController@verifyTicket');

  //Cities
  Route::post('cities', 'Api\CitiesController@store');
  Route::put('cities/{city}', 'Api\CitiesController@update');
  Route::delete('cities/{city}', 'Api\CitiesController@destroy');

  //Countries
  Route::post('countries', 'Api\CountriesController@store');
  Route::put('countries/{country}', 'Api\CountriesController@update');
  Route::delete('countries/{country}', 'Api\countriesController@destroy');

  //Orders
  Route::get('orders', 'Api\OrderController@index');
  Route::post('orders/pay/confirmation', 'Api\OrderController@pay_confirmation');
  Route::post('orders/make/order', 'Api\OrderController@make_order');
  Route::get('orders/{order}/cancel', 'Api\OrderController@cancel');

  //Finances
  Route::get('finances/all', 'Api\FinancesController@general');
  Route::get('finances/all/details', 'Api\FinancesController@byEventsGeneral');

  Route::get('finances/by_organization/{organization_id}', 'Api\FinancesController@byOrganization');
  Route::get('finances/by_organization/{organization_id}/events', 'Api\FinancesController@byEventsOfOrganizator');

  Route::get('finances/event/{event}', 'Api\FinancesController@byEvent');
  Route::get('finances/organization/all', 'Api\FinancesController@byOrganizationAll');
  Route::get('finances/organization/{organization}/events/', 'Api\FinancesController@byEventsAll');
  Route::post('finances/pay_event', 'Api\FinancesController@payEvent');
  Route::get('finances/by_organization_paid/{organization_id}', 'Api\FinancesController@byOrganizationPaid');



  //Staff
  Route::get('staff', 'Api\StaffController@index');
  Route::get('staff/by/event/{event}', 'Api\StaffController@byEvent');
  Route::get('staff/{staff}/events', 'Api\StaffController@allEvents');
  Route::post('staff', 'Api\StaffController@store');
  Route::put('staff/{staff}/reset_access', 'Api\StaffController@regenerateCode');
  Route::delete('staff/{staff}', 'Api\StaffController@destroy');

  Route::get('staff/{staff}/assign/event/{event}', 'Api\StaffController@assignEvent');
  Route::get('staff/{staff}/remove/event/{event}', 'Api\StaffController@removeEvent');

  //Gifts ----- Need Authentication, Only return the data of the current user authenticated... For reason of security...
  // No everybody need to have all the power. ;-)

  Route::get('gifts/to/me', 'Api\GiftController@getGiftsReceived');
  Route::get('gifts/given', 'Api\GiftController@getGiftsGifted');

  Route::post('gifts/generate/{ticket}', 'Api\GiftController@generateGift');
  Route::post('gifts/accept/{gift}', 'Api\GiftController@acceptGift');
  Route::post('gifts/reject/{gift}', 'Api\GiftController@rejectGift');

});


//Not need Authentication

//Reviews
Route::get('reviews', 'Api\ReviewController@index');
Route::get('reviews/{review}', 'Api\ReviewController@show');

//Categories
Route::get('categories', 'Api\CategoryController@index');
Route::get('categories/{category}', 'Api\CategoryController@show');

//Events
Route::get('events', 'Api\EventController@index');
Route::get('events/most-visited', 'Api\EventController@mostVisited');
Route::post('events/recurrently', 'Api\EventController@recurrentEvents'); // FOR TESTING PURPOSE
Route::get('events/{event}', 'Api\EventController@show');

//Cities
Route::get('cities', 'Api\CitiesController@index');
Route::get('cities/{city}', 'Api\CitiesController@show');

//Countries
Route::get('countries', 'Api\CountriesController@index');
Route::get('countries/{country}', 'Api\CountriesController@show');

//Search
Route::get('search', 'Api\SearchController@index');

//Uploads
Route::post('uploads', 'Api\UploadsController@load');
Route::get('uploads', 'Api\UploadsController@index');

//Auth Staff
Route::post('staff/auth', 'Api\StaffController@auth');
