<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Event::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->name,
        'category_id' => 1,
        'picture' => $faker->imageUrl($width = 640, $height = 480),
        'organization_id' => 1,
        'privacy' => 'none',
        'start_date' => $faker->dateTimeBetween('-30 days', '+50 days')->format('Y-m-d'),
        'end_date' => $faker->dateTimeBetween('-20 days', '+40 days')->format('Y-m-d'),
        'start_time' => $faker->time($format = 'H:i:s', $max = 'now'),
        'end_time' => $faker->time($format = 'H:i:s', $max = 'now'),
        'share_code' => 'CFSF'
    ];
});