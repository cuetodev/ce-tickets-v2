<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaidToEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function($table) {
            $table->boolean('paid')->default(0);
            $table->date('paid_date')->nullable();
            $table->string('reference', 20);
        });
      

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('events', function($table) {
            $table->dropColumn('paid');
            $table->dropColumn('paid_date');
            $table->dropColumn('reference');
        });
    }
}
