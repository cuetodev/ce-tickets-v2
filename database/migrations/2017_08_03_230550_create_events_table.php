<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->text('description');
            $table->integer('category_id');
            $table->string('picture');
            $table->integer('organization_id');

            $table->string('privacy');

            $table->date('start_date');
            $table->date('end_date');

            $table->time('start_time');
            $table->time('end_time');

            // $table->integer('quantity_tickets')->default(0);

            $table->string('status')->default('available');
            // $table->decimal('price', 15, 2)->default(0.00);

            //if location is null or '', assume that envent is online;
            $table->string('location')->nullable();

            $table->string('share_code');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
