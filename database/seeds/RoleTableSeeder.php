<?php

use Illuminate\Database\Seeder;
use App\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_user_client = new Role();
        $role_user_client->name = 'client';
        $role_user_client->description = 'Just A Client User';
        $role_user_client->save();

        $role_user_organizer = new Role();
        $role_user_organizer->name = 'organizer';
        $role_user_organizer->description = 'Just A Organizer User';
        $role_user_organizer->save();

        $role_user_manager = new Role();
        $role_user_manager->name = 'manager';
        $role_user_manager->description = 'Just A Manager User';
        $role_user_manager->save();
    }
}
