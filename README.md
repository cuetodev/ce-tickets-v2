# CE-TICKETS V2 #

This is the repository for the new system improved and rewrited on Laravel 5.4.

### Getting Started ###

First, you must clone this repository, into your directory app.

```
$ git clone
```
Go to the folder of app, then run the follow command, this will regenerate the vendor folder and download all necessary dependencies.

```
$ composer install
```
Configure your .env file.

```
$ cp .env.server .env
```

* Set the name of app.
* Set the url of app, by default is http://localhost
* Configure your Database.
* Config with yours credentials of socials app.

Run the migrations with the Parameter `--seed`. With the parameter `-seed` automatically will create the preset of roles (client, organizer, manager). **This is important**

```
$ php artisan migrate --seed
```


For run in Local, run the follow command:

```
$ php artisan serve
```

and enjoy your development.

### Api Specifications ###

[Api Guide](documentation/api-guide.md)

### Contribution guidelines ###

Please follow the following advices

* Create a new branch for development
* Test it!
* When is ready, merge with the `master` branch.
* And Enjoy the development.
