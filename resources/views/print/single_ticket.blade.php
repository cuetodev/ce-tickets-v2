<!doctype html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/invoice.css') }}">

        <title>EventToCity</title>

        <!-- Styles -->
        <style>

        /*.invoice-title h2, .invoice-title h3 {
          display: inline-block;
        }

        .table > tbody > tr > .no-line {
          border-top: none;
        }

        .table > thead > tr > .no-line {
          border-bottom: none;
        }

        .table > tbody > tr > .thick-line {
          border-top: 2px solid;
        }*/
        </style>
    </head>
    <body onload="window.print()">
      <div class="container" id="invoice">
        <div class="row">
          <div class="col-xs-12">
            <hr>
            <div class="row">
              <div class="col-xs-3">
                <img src="https://chart.googleapis.com/chart?cht=qr&chs=170x170&chl={{ $ticket->code }}" alt="{{ $ticket->code }}">
              </div>
              <div class="col-xs-6">
                <h3  style="font-weight: bold;">{{ $ticket->event->name }} - {{ $ticket->entrance->name }} </h3>
                <span>{{ $ticket->event->location }}</span>
                <div class="">
                  @lang("Start") {{ Carbon\Carbon::parse($ticket->event->start_date)->toFormattedDateString() }} - {{ Carbon\Carbon::parse($ticket->event->start_time)->toTimeString() }}
                </div>
                <div class="">
                  @lang("code"): {{ $ticket->code }}
                </div>
                <button onclick="print();" class="hidden-print btn btn-default">@lang("Print!")</button>
              </div>
              <div class="col-xs-3">
                <img src="{{ $ticket->event->picture }}" alt="{{ $ticket->event->picture }}" width="205"></br>
                <img src="{{ asset("images/logo.png") }}" alt="" width="159" style="padding-top: 20px;">
              </div>
            </div>
            <hr>
          </div>
        </div>
      </div>
    </body>
</html>
