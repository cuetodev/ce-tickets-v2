<!doctype html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('/css/invoice.css') }}">

        <title>EventToCity</title>

        <!-- Styles -->
        <style>

        /*.invoice-title h2, .invoice-title h3 {
          display: inline-block;
        }

        .table > tbody > tr > .no-line {
          border-top: none;
        }

        .table > thead > tr > .no-line {
          border-bottom: none;
        }

        .table > tbody > tr > .thick-line {
          border-top: 2px solid;
        }*/
        </style>
    </head>
    <body>
      <div class="container" id="invoice">
        <div class="row">
          <div class="col-xs-12">
            <div class="invoice-title">
              <img id="#logo" src="{{ asset('images/logo.png') }}" alt=""><h3 class="pull-right">@lang("Order") #{{ $order->id }}</h3>
            </div>
            <hr>
            <div class="row">
              <div class="col-xs-6">
                <address>
                  <strong>@lang("Client Name"):</strong><br>
                  {{$order->user->first_name}} {{ $order->user->last_name }}<br>
                  {{ $order->user->email }}
                </address>
              </div>
              <div class="col-xs-6 text-right">
                <address>
                  <strong>@lang("Order Date"):</strong><br>
                  {{ Carbon\Carbon::parse($order->updated_at)->toFormattedDateString() }}<br>
                  <strong>@lang("Status"):</strong>
                  <span>{{$order->status}}</span><br>
                </address>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-xs-12">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">@lang("Order Summary")</h3>
              </div>
              <div class="panel-body">
                <div class="table-responsive">
                  <table class="table table-condensed">
                    <thead>
                      <tr>
                        <td>@lang("Item")</td>
                        <td class="text-center">@lang("Quantity")</td>
                        <td class="text-center">@lang("Tax")</td>
                        <td class="text-right">@lang("Subtotal")</td>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach ($order->items as $item)
                        <tr>
                          <td>{{ $item->event_name }} <strong>{{ $item->entrance_type }}</strong></td>
                          <td class="text-center">{{ $item->quantity }}</td>
                          <td class="text-center">{{ $item->tax }}</td>
                          <td class="text-right">{{ $item->subtotal }}</td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row" id="footer">
          <div class="col-xs-12">
            <button onclick="print();" class="print-button">@lang("Print")</button>
          </div>
        </div>
      </div>
    </body>
</html>
