@extends('layouts.app')

@section('content')

  <!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h2>@lang("Cart")</h2>
        <!-- Breadcrumbs -->
        <nav id="breadcrumbs">
          <ul>
            <li><a href="/">@lang("Home")</a></li>
            <li>@lang("Cart")</li>
          </ul>
        </nav>

      </div>
    </div>
  </div>
</div>

<div class="container padding-bottom-70">

  <div class="row">
    <div class="col-md-8">
      <!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-25">

				<div class="col-md-6 col-xs-6">
					<!-- Layout Switcher -->
					{{-- <div class="layout-switcher">
						<a href="@" class="grid"><i class="fa fa-th"></i></a>
						<a href="#" class="list active"><i class="fa fa-align-justify"></i></a>
					</div> --}}
				</div>

				<div class="col-md-6 col-xs-6">
					<!-- Sort by -->
					<div class="sort-by">
						<div class="sort-by-select">
							<a href="{{ url('cart') }}/clear" class="button border"><i class="sl sl-icon-trash"></i> @lang("Empty the cart")</a>
						</div>
					</div>
				</div>
			</div>
			<!-- Sorting / Layout Switcher / End -->
      <table class="table table-striped cart-list">
        <thead>
          <tr>
            <th>@lang("ITEM")</th>
            <th>@lang("QUANTITY")</th>
            <th>@lang("TAX")</th>
            <th>@lang("SUBTOTAL")</th>
            <th>@lang("TOTAL")</th>
            <th>@lang("ACTIONS")</th>
          </tr>
        </thead>
        <tbody>
          @foreach ( \Cart::content() as $item)
            <tr>
              <td style="vertical-align: middle;">
                <span class="">
                  {{ $item->name }}
                </span> -
                <strong> {{ $item->options->type_ticket['name'] }} </strong>
              </td>
              <td style="vertical-align: middle;">
                <form id="update-{{ $item->rowId }}" action="/cart/update/{{ $item->rowId }}/price" method="POST">
                  {{ csrf_field() }}
                  <input type="number" name="item_quantity" value="{{ $item->qty }}" min="0" max="{{ \App\Entrance::find($item->options->type_ticket['id'])->quantity_availables }}">
                </form>
              </td>
              <td style="vertical-align: middle;"> ${{ $item->tax() }} </td>
              <td style="vertical-align: middle;"> ${{ $item->subtotal() }} </td>
              <td style="vertical-align: middle;"> ${{ $item->total() }} </td>
              <td style="vertical-align: middle;">
                <a href="{{ url('cart') }}/remove/{{ $item->rowId }}" data-toggle="tooltip" data-placement="bottom" title="Remove Item">
                  <i class="sl sl-icon-trash"></i>
                </a>
                <a href="#" onclick="event.preventDefault(); document.getElementById('update-{{ $item->rowId }}').submit();">
                  <i class="sl sl-icon-arrow-up-circle" style="margin-left: 9px;"></i>
                </a>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

    <div class="col-md-4 col-lg-4 sticky">
      <div class="boxed-widget">

        <div class="box_style_1 expose">
          <h3 class="inner">- @lang("Summary") -</h3>

          <table class="table table_summary">
            <tbody>
              @foreach ( \Cart::content() as $item)
                <tr>
                  <td>
                    {{ $item->name }} - {{ $item->options->type_ticket['name'] }}
                  </td>
                  <td class="text-right">
                    {{ $item->qty }}x ${{ $item->price }}
                  </td>
                </tr>
              @endforeach
              <tr>
                <td>
                  @lang("Total amount")
                </td>
                <td class="text-right">
                  ${{ \Cart::subtotal() }}
                </td>
              </tr>
              <tr class="total">
                <td>
                  @lang("Total cost")
                </td>
                <td class="text-right">
                  ${{ \Cart::total() }}
                </td>
              </tr>
            </tbody>
          </table>
          @if (!\Cart::content()->count() == 0)
            <a href="{{ url('check-out') }}" class="button fullwidth margin-top-5"><span>@lang("Check Out")</span></a>
          @endif
          <a href="{{ url('events') }}" class="button border fullwidth margin-top-5"><i class="sl sl-icon-bag" ></i><span>@lang("Continue Shopping")</span></a>

        </div>
        <!--/box_style_1 -->
      </div>
    </div>
  </div>
</div>
@endsection
