@extends('layouts.app')

@section('content')
  <!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h2>@lang("Thank you!")</h2>
        <!-- Breadcrumbs -->
        <nav id="breadcrumbs">
          <ul>
            <li><a href="/">@lang("Home")</a></li>
            <li>@lang("Thank you")</li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container padding-bottom-70">
  <div class="row">
    <div class="col-md-8">
      <h1><i class="sl sl-icon-check"></i> @lang("Thank you")</h1>
      <p>@lang("Your tickets has been generate, you can view your tickets in your profile, in the tab") <a  href="{{ url('/dashboard') }}" style="color: #ff5f06; font-weight: bold;"> @lang("My Tickets")</a>.</p>
    </div>
  </div>
</div>
@endsection
