@extends('layouts.app')

@section('content')
  <!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h2>@lang("Dashboard")</h2>
        <!-- Breadcrumbs -->
        <nav id="breadcrumbs">
          <ul>
            <li><a href="/">@lang("Home")</a></li>
            <li>@lang("Dashboard")</li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container padding-bottom-70">
  <div class="row">
    <div class="col-md-8">
      <div class="style-2">
        <div>
        <!-- Nav tabs -->
          <ul class="tabs-nav">
            <li class="active"><a href="#my-tickets"><i class="sl sl-icon-tag"></i> @lang("My Tickets")</a></li>
            <li><a href="#my-orders"><i class="sl sl-icon-docs"></i> @lang("My Orders")</a></li>
            <li><a href="#settings"><i class="sl sl-icon-settings"></i> @lang("Settings")</a></li>
          </ul>
          <!-- Tab panes -->
          <div class="tabs-container">
            <div class="tab-content" id="my-tickets">
              <a class="button border" href="{{url('tickets')}}" target="_blank">@lang("Print all")</a>
              <div class="row">
                @forelse (auth()->user()->tickets->where('checked', 0)->sortByDesc('updated_at')->groupBy('event_id') as $eventGroup)

                  <div class="toggle-wrap">
                    <span class="trigger">
                      <a href="#">
                        <i class="sl sl-icon-tag"></i>
                        {{ \App\Event::find($eventGroup->first()->event_id)->name }} <span class="badge">{{ $eventGroup->count() }}</span>
                        <i class="sl sl-icon-plus"></i>
                      </a>
                    </span>
                    <div class="toggle-container">
                      <div class="row">
                        @foreach ($eventGroup as $ticket)
                          <div class="col-lg-12 col-xs-12">
                            <div class="listing-item-container list-layout">
                              <span class="listing-item" style="height: auto;">

                                <!-- Image -->
                                <div class="listing-item-image" style="min-height: auto;">
                                  <img src="https://chart.googleapis.com/chart?cht=qr&chs=170x170&chl={{ $ticket->code }}" alt="">
                                </div>

                                <!-- Content -->
                                <div class="listing-item-content">
                                  <div class="listing-item-inner">
                                    <h3>{{ $ticket->event->name }} - {{ $ticket->entrance->name }}</h3>
                                    <span>{{ $ticket->event->location }}</span>
                                    <div>
                                        @lang("Start"): {{Carbon\Carbon::parse($ticket->event->start_date)->toFormattedDateString()}} -
                                        {{Carbon\Carbon::parse($ticket->event->start_time)->toTimeString()}}
                                    </div>
                                    <div class="hidden-print">
                                      <a class="btn btn-default" href="{{url('tickets')}}/{{$ticket->id}}" target="_blank">@lang("Print")</a>
                                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#gift-{{ $ticket->id }}">@lang("Gift a Ticket")</button>
                                    </div>
                                  </div>
                                </div>
                              </span>
                            </div>

                            <!-- Modal -->
                            <div class="modal fade" id="gift-{{ $ticket->id }}" role="dialog" aria-labelledby="gift-{{ $ticket->id }}Label" data-backdrop="false">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <form class="" action="{{ url('/gift').'/'.$ticket->id.'/generate' }}" method="POST">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title" id="gift-{{ $ticket->id }}Label">@lang("Gift Ticket") #{{ $ticket->id }}</h4>
                                    </div>
                                    <div class="modal-body">
                                      {{ csrf_field() }}
                                      <input type="email" required name="email_receiver" placeholder="@lang("Enter the email of the people who you whant to gift")">
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">@lang("Close")</button>
                                      <button type="submit" class="btn btn-primary">@lang("Gift Ticket")</button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        @endforeach
                      </div>
                    </div>
                  </div>

                @empty
                  <div class="center-block">
                    <h4 class="text-center">@lang("You don't have any ticket yet!")</h4>
                  </div>

                @endforelse
              </div>
            </div>
            <div class="tab-content" id="my-orders">
              @forelse (auth()->user()->orders->sortByDesc('updated_at')->values()->all() as $order)
                <hr>
                <div class="row">
                  <div class="col-md-6 col-sm-6 text-left">
                    <h3><strong>@lang("Order")</strong> #{{ $order->id }}</h3>
                  </div>

                  <div class="col-md-6 col-sm-6 text-right">
                    <address>
                      <strong>@lang("Order Date"):</strong>
                      <br/>
                      {{ Carbon\Carbon::parse($order->updated_at)->toFormattedDateString() }}
                      <br>
                      <strong>@lang("Status"): </strong><span>{{$order->status}}</span>
                    </address>
                  </div>
                </div>

                <div class="row">

                  <div class="col-md-12">
                    @if ($order->status == 'pending')
                      <a href="{{url('/check-out/pay')}}" class="button">@lang("Pay Invoice")</a>
                    @else
                      <a href="{{ url('invoice/'.$order->id) }}" class="btn"><i class="sl sl-icon-printer"></i> @lang("Print Invoice")</a>
                    @endif

                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h3 class="panel-title">@lang("Order Summary")</h3>
                      </div>
                      <div class="panel-body">
                        <div class="table-responsive">
                          <table class="table table-condensed">
                            <thead>
                              <tr>
                                <td>@lang("Item")</td>
                                <td class="text-center">@lang("Quantity")</td>
                                <td class="text-center">@lang("Tax")</td>
                                <td class="text-right">@lang("Subtotal")</td>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($order->items as $item)
                                <tr>
                                  <td>{{ $item->event_name }} <strong>{{ $item->entrance_type }}</strong></td>
                                  <td class="text-center">{{ $item->quantity }}</td>
                                  <td class="text-center">{{ $item->tax }}</td>
                                  <td class="text-right">{{ $item->subtotal }}</td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                      </div>
                    </div>

                  </div>

                </div>
              @empty
                <div class="center-block">
                  <h4 class="text-center">@lang("You dont have any invoices.")</h4>
                </div>

              @endforelse
            </div>
            <div class="tab-content" id="settings">
              <div class="toggle-wrap">
                <span class="trigger">
                  <a href="#"><i class="sl sl-icon-people"></i>@lang("Edit Profile") <i class="sl sl-icon-plus"></i></a>
                </span>
                <div class="toggle-container">
                  <form class="" action="{{ url("/dashboard/update_basic_info") }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="first_name" class="control-label">@lang("First Name")</label>
                          <input id="first_name" required class="form-control" type="text" name="first_name" value="{{ auth()->user()->first_name }}">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="last_name" class="control-label">@lang("Last Name")</label>
                          <input id="last_name" required class="form-control" type="text" name="last_name" value="{{ auth()->user()->last_name }}">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="phone_number" class="control-label">@lang("Phone Number")</label>
                          <input id="phone_number" required class="form-control" type="text" name="phone_number" value="{{ auth()->user()->phone_number }}">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="email" class="control-label">@lang("E-Mail Address")</label>
                          <input id="email" required class="form-control" type="text" name="email" value="{{ auth()->user()->email }}">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="gender" class="control-label">@lang("Gender")</label>
                          <select class="form-control" required name="gender">
                            @if (auth()->user()->gender == "male")
                              <option value="male" selected>@lang("Male")</option>
                              <option value="female">@lang("Female")</option>
                            @else
                              <option value="male">@lang("Male")</option>
                              <option value="female" selected>@lang("Female")</option>
                            @endif
                          </select>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="birth_date" class="control-label">@lang("Birth Date")</label>
                          <input id="birth_date" required class="form-control" type="date" name="birth_date" value="{{ Carbon\Carbon::parse(auth()->user()->birth_date)->toDateString() }}">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <button type="submit" class="button">@lang("Update Profile")</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

              <div class="toggle-wrap">
                <span class="trigger">
                  <a href="#">
                    <i class="sl sl-icon-key"></i>
                    @lang("Change Password")
                    <i class="sl sl-icon-plus"></i>
                  </a>
                </span>
                <div class="toggle-container">
                  <form class="" action="{{ url('dashboard/change_password') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group {{ $errors->has('current_password') ? 'has-error' : '' }}">
                          <label for="current_password" class="control-label">@lang("Current Password")</label>
                          <input id="current_password" class="form-control" type="password" name="current_password">

                          @if ($errors->has('current_password'))
                            <span class="help-block">
                              <strong>{{ $errors->first('current_password') }}</strong>
                            </span>

                          @endif
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group {{ $errors->has('new_password') ? 'has-error' : '' }}">
                          <label for="new_password" class="control-label">@lang("New Password")</label>
                          <input id="new_password" required class="form-control" type="password" name="new_password">

                          @if ($errors->has('new_password'))
                            <span class="help-block">
                              <strong>{{ $errors->first('new_password') }}</strong>
                            </span>
                          @endif
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label for="new_password_confirmation" class="control-label">@lang("Confirm Password")</label>
                          <input id="new_password_confirmation" required class="form-control" type="password" name="new_password_confirmation">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <button type="submit" class="button">@lang("Change Password")</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>



              <div class="toggle-wrap">
                <span class="trigger">
                  <a href="#">
                    <i class="sl sl-icon-picture"></i>
                    @lang("Change Profile Picture")
                    <i class="sl sl-icon-plus"></i>
                  </a>
                </span>
                <div class="toggle-container">
                  <form class="" action="{{url('dashboard/update_avatar')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                      <div class="col-md-12">
                        @if ($errors->has('avatar'))
                          <span class="help-block text-danger">
                            <strong>{{ $errors->first('avatar') }}</strong>
                          </span>
                        @endif

                        <div class="edit-profile-photo">
                          <img src="{{ auth()->user()->avatar }}">
                          <div class="change-photo-btn">
                            <div class="photoUpload">
                              <span>
                                <i class="fa fa-upload"></i>
                                @lang("Select a Picture")
                              </span>
                              <input type="file" name="avatar" class="upload" onchange="onFileSelected(event)">
                            </div>
                          </div>
                        </div>
                        <button type="submit" class="button">@lang("Update Picture")</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      @if (session('organizer_request'))
            <div class="alert alert-success">
                {{ session('organizer_request') }}
            </div>
        @endif
        <div class="boxed-widget margin-top-60 margin-bottom-50">
          <p class="text-center"><img src="{{ auth()->user()->avatar }}" alt="" class="img-circle" width="100"/></p>
                <h3 class="text-center">
                  <strong>{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}</strong>
                  <em>{{ auth()->user()->username }}</em>
                </h3>

          <ul class="listing-details-sidebar">
            <li class=""><i class="sl sl-icon-directions"></i> {{ auth()->user()->email }} </li>
            <li class=""><i class="sl sl-icon-phone"></i> {{ auth()->user()->phone_number }} </li>
            <li class=""><i class="sl sl-icon-people"></i> {{ auth()->user()->gender }} </li>
            <li class=""><i class="sl sl-icon-chart"></i> {{ auth()->user()->birth_date }} </li>
          </ul>

        </div>
        <div class="boxed-widget margin-top-60 margin-bottom-50">
            <h3>@lang("Organization")</h3>
            @if ($is_organizer > 0)
              <a href="{{url('admin/#!/login')}}">@lang("Link to organization")</a>
            @elseif($organizer_request > 0)
              <div class="alert alert-success">
                @lang("You have an organization request in proccess!")
              </div>
            @else
              <a href="{{route('organization.is_organizer')}}">@lang("Become on Organizer")</a>
            @endif

        </div>
    </div>

    <div class="col-md-12">
      <h3 class="listing-desc-headline margin-top-60 margin-bottom-30" style="text-align: center;">@lang("Other Events You May Like")</h3>
      <div class="simple-slick-carousel dots-nav">

        @foreach (App\Event::where('privacy', 'public')->where('status', '!=', 'ended')->whereHas('reviews', function ($query) {
        $query->orderBy('rating', 'DESC');})->has('type_entrances')->take(12)->get()->chunk(2) as $chunks)
          <!-- Listing Item -->
          <div class="carousel-item">
            @foreach ($chunks as $popular)
              <a href="/events/{{ $popular->slug }}" class="listing-item-container">
                <div class="listing-item">
                  <img src="{{ $popular->picture }}" alt="">

                  @if ($popular->status == 'sold-out')
                    <div class="listing-badge now-closed">@lang("Sold Out!")</div>
                  @endif

                  <div class="listing-item-content">
                    <span class="tag">{{ $popular->category->name_translated[LaravelLocalization::getCurrentLocale()] }}</span>
                    <h3>{{ $popular->name }}</h3>
                    <span>{{ $popular->location }}</span>
                  </div>
                  {{-- <span class="like-icon"></span> --}}
                </div>
                <div class="star-rating" data-rating="{{ $popular->averageReview() }}">
                  <div class="rating-counter">({{ $popular->reviews->count() }} @lang("reviews"))</div>
                  <div class="price">USD${{ $popular->type_entrances->first()->price }}</div>
                </div>
              </a>
            @endforeach
          </div>
          <!-- Listing Item / End -->
        @endforeach
      </div>

    </div>
  </div>
</div>
@endsection

@push('specific-style')
  <style>
    .modal.in .modal-dialog {
      position: relative;
      top: 50%;
      transform: translateY(-50%);
    }
  </style>
@endpush
