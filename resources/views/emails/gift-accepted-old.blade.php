@component('mail::message')
# Gift Accepted

{{ App\User::where('email', $gift->receiver)->first()->first_name }} accepted your gift.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
