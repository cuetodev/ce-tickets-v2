@component('mail::message')
# Gift Rejected

A Ticket you've gifted was rejected.
We've regenerated the ticket back, you can see again in your dashboard or clicking the button.

@component('mail::button', ['url' => url('tickets').'/'.$ticket->id ])
View The Ticket
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
