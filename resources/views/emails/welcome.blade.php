<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width">
  <meta name="format-detection" content="telephone=no">
  <!--[if !mso]>
      <!-->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300&subset=latin" rel="stylesheet" type="text/css">
  <!--<![endif]-->
  <title>Event to City - Welcome</title>
  <style type="text/css">
    *{
      			margin:0;
      			padding:0;
      			font-family:'OpenSans-Light', "Helvetica Neue", "Helvetica",Calibri, Arial, sans-serif;
      			font-size:100%;
      			line-height:1.6;
      		}
      		body{
      			-webkit-font-smoothing:antialiased;
      			-webkit-text-size-adjust:none;
      			width:100%!important;
      			height:100%;
      		}
      		a{
      			color:#348eda;
      		}
      		.btn-primary{
      			text-decoration:none;
      			color:#FFF;
      			background-color:#a55bff;
      			border:solid #a55bff;
      			border-width:10px 20px;
      			line-height:2;
      			font-weight:bold;
      			margin-right:10px;
      			text-align:center;
      			cursor:pointer;
      			display:inline-block;
      		}
      		.last{
      			margin-bottom:0;
      		}
      		.first{
      			margin-top:0;
      		}
      		.padding{
      			padding:10px 0;
      		}
      		table.body-wrap{
      			width:100%;
      			padding:0px;
      			padding-top:20px;
      			margin:0px;
      		}
      		table.body-wrap .container{
      			border:1px solid #f0f0f0;
      		}
      		table.footer-wrap{
      			width:100%;
      			clear:both!important;
      		}
      		.footer-wrap .container p{
      			font-size:12px;
      			color:#666;
      		}
      		table.footer-wrap a{
      			color:#999;
      		}
      		.footer-content{
      			margin:0px;
      			padding:0px;
      		}
      		h1,h2,h3{
      			color:#660099;
      			font-family:'OpenSans-Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
      			line-height:1.2;
      			margin-bottom:15px;
      			margin:40px 0 10px;
      			font-weight:200;
      		}
      		h1{
      			font-family:'Open Sans Light';
      			font-size:45px;
      		}
      		h2{
      			font-size:28px;
      		}
      		h3{
      			font-size:22px;
      		}
      		p,ul,ol{
      			margin-bottom:10px;
      			font-weight:normal;
      			font-size:14px;
      		}
      		ul li,ol li{
      			margin-left:5px;
      			list-style-position:inside;
      		}
      		.container{
      			display:block!important;
      			max-width:600px!important;
      			margin:0 auto!important;
      			clear:both!important;
      		}
      		.body-wrap .container{
      			padding:0px;
      		}
      		.content,.footer-wrapper{
      			max-width:600px;
      			margin:0 auto;
      			padding:20px 33px 20px 37px;
      			display:block;
      		}
      		.content table{
      			width:100%;
      		}
      		.content-message p{
      			margin:20px 0px 20px 0px;
      			padding:0px;
      			font-size:22px;
      			line-height:38px;
      			font-family:'OpenSans-Light',Calibri, Arial, sans-serif;
      		}
      		.preheader{
      			display:none !important;
      			visibility:hidden;
      			opacity:0;
      			color:transparent;
      			height:0;
      			width:0;
      		}
  </style>
</head>

<body bgcolor="#f6f6f6">
  <span class="preheader" style="display: none !important; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
</span>

  <!-- body -->
  <table class="body-wrap" width="600">
    <tr>
      <td class="container" bgcolor="#FFFFFF">
        <!-- content -->
        <table border="0" cellpadding="0" cellspacing="0" class="contentwrapper" width="600">
          <tr>
            <td style="height:25px;">
              <img src="{{secure_asset('images/96288204-f67c-4ba2-9981-1be77c9fa18b.png')}}" border="0" width="600">
            </td>
          </tr>
          <tr>
            <td>
              <div class="content">
                <table class="content-message">
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left">
                      <a href="https://eventstocity.com/">
                        <img src="https://eventstocity.com/images/logo.png" width="200">
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td class="content-message" style="font-family: 'Open Sans', 'Helvetica Neue', 'Helvetica', Calibri, Arial, sans-serif; color: #595959;">
                      <p>&nbsp;</p>
                      <h1 style="font-family:'Open Sans', 'Helvetica Neue', 'Helvetica', Calibri, Arial, sans-serif;">Welcome</h1>

                      <p style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;">Thank for join to us, {{ $user->first_name }}.</p>
                      <p style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;">Now you can enjoy the best events and activities of your interest in your city.                      </p>
                      <p style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;">Now, are yo ready to explore?</p>
                      <p style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;">&nbsp;</p>
                      <table width="325" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="325" height="60" bgcolor="#FF5F06" style="text-align:center;">
                            <a href="https://eventstocity.com/events"
                              align="center" style="display:block; font-family:'Open Sans', 'Helvetica Neue', 'Helvetica', Calibri, Arial, sans-serif; font-size:20px; color:#ffffff; text-align: center; line-height:60px; display:block; text-decoration:none;">Browse all events</a>
                          </td>
                          <td>&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                      </table>
                      <p style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;">&nbsp;</p>
                      <p style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;">&nbsp;</p>
                      <p style="font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif; font-size:18px; line-height:26px;">Thanks,<br />
                      EventsToCity</p>
                    </td>
                  </tr>
                </table>


              </div>
             <table>
<tbody>
<tr style="padding:0;text-align:left;vertical-align:top">
<td class="m_1168913025692755088app-banner-container" style="background:#fff;border-collapse:collapse!important;color:#222;font-family:'Helvetica','Arial',sans-serif;font-size:14px;font-weight:400;line-height:19px;margin:0;padding:15px;text-align:left;vertical-align:top;word-break:break-word">
<table class="m_1168913025692755088app-banner-image" style="border-collapse:collapse;border-spacing:0;float:left;padding:0;text-align:left;vertical-align:top;width:97px">
<tbody>
<tr style="padding:0;text-align:left;vertical-align:top">
<td style="border-collapse:collapse!important;color:#222;font-family:'Helvetica','Arial',sans-serif;font-size:14px;font-weight:400;line-height:19px;margin:0;padding:0;text-align:left;vertical-align:top;word-break:break-word">
<img src="{{secure_asset('images/icon_app.png')}}" alt="gyg logo icon" width="97" height="97" class="CToWUd" style="clear:both;display:block;float:left;max-width:100%;outline:none;text-decoration:none;width:auto"></td>
</tr>
</tbody>
</table>
<table class="m_1168913025692755088app-banner-text" style="border-collapse:collapse;border-spacing:0;float:left;margin-left:20px;margin-top:5px;padding:0;text-align:left;vertical-align:top;width:430px">
<tbody>
<tr style="padding:0;text-align:left;vertical-align:top">
<td class="m_1168913025692755088app-banner-title" style="border-collapse:collapse!important;color:#ff3d00;font-family:'Helvetica','Arial',sans-serif;font-size:18px;font-weight:700;line-height:18px;margin:0;padding:3px 0 2px;text-align:left;vertical-align:top;word-break:break-word">
¡Explore all the events on your mobile!</td>
</tr>
<tr style="padding:0;text-align:left;vertical-align:top">
<td class="m_1168913025692755088app-banner-message" style="border-collapse:collapse!important;color:#606159;font-family:'Helvetica','Arial',sans-serif;font-size:14px;font-weight:400;line-height:14px;margin:0;padding:3px 0 8px;text-align:left;vertical-align:top;word-break:break-word">Take your ticket where you go and enjoy the events that you most like.</td>
</tr>
<tr class="m_1168913025692755088app-banner-links" style="padding:0;text-align:left;vertical-align:top">
  <td class="m_1168913025692755088app-banner-links-wrapper" style="border-collapse:collapse!important;color:#222;font-family:'Helvetica','Arial',sans-serif;font-size:14px;font-weight:400;line-height:19px;margin:0;padding:0;text-align:left;vertical-align:top;word-break:break-word">
  <a href="https://play.google.com/store/apps/details?id=events.to.city" style="color:#2ba6cb;display:block;float:left;height:45px;text-decoration:none;width:150px" target="_blank">
    <img src="{{secure_asset('images/561fb21ee1aca.png')}}" style="border:none;clear:both;display:block;float:left;height:40px;max-width:100%;outline:none;text-decoration:none;width:128px" class="CToWUd"></a>
    <a href="https://itunes.apple.com/us/app/events-to-city/id1366304944?l=es&ls=1&mt=8" style="color:#2ba6cb;display:block;float:left;height:45px;text-decoration:none;width:150px" target="_blank">
      <img src="{{secure_asset('images/56a27ced1967e.png')}}" style="border:none;clear:both;display:block;float:left;height:40px;max-width:100%;outline:none;text-decoration:none;width:128px" class="CToWUd"></a>
  </td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#F7F7F7" style="max-width:600px; margin:0 auto; padding:20px 33px 20px 37px; display:block;">
              <table cellspacing="0" cellpadding="10" width="100%">
                <tr>
                  <td colspan="3" height="18" style="font-size:1px; line-height:1px;">&nbsp;</td>
                </tr>
                <tr>
                  <td align="left" colspan="3">
                    <table cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td>
                          <table>
                            <tr>
                              <td>
                                <div style="float:left; margin:5px;">
                                  <a href="https://www.facebook.com/jet" title="Like us on Facebook" target="new">
                                    <img src="{{secure_asset('images/3f540d56-178e-4d73-83e5-8ccaaf70f2cd.png')}}" width="29" height="29" border="0" alt="Like us on Facebook">
                                  </a>
                                </div>
                              </td>
                              <td>
                                <div style="float:left; margin:5px;">
                                  <a href="https://twitter.com/jet" title="Jet on Twitter" target="new">
                                    <img src="{{secure_asset('images/e78ba6bd-5b0f-4d69-8fcf-acc064cbf7ea.png')}}" width="29" height="29" border="0" alt="Jet on Twitter">
                                  </a>
                                </div>
                              </td>
                              <td>
                                <div style="float:left; margin:5px;"></div>
                              </td>
                              <td>
                                <div style="float:left; margin:5px;"></div>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td align="right" style="text-align:right;">
                          <a href="mailto:help@eventstocity.com" style="text-decoration: none; font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;" title="help@eventstocity.com">help@eventstocity.com</a>                          <span style="text-decoration: none; font-size:9px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;">|</span>

                          <a style="text-decoration: none; font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;" href="tel:18555384323">1 (855) (538 4323</a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="3" height="18" style="font-size:1px; line-height:1px;">&nbsp;</td>
                </tr>
                <tr>
                  <td style="font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141; text-align:left;" align="left">
                    <a href="https://jet.com/privacy-policy" style="text-decoration:none;font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141;">Privacy Policy</a>
                  </td>
                  <td colspan="2" style="font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141; text-align:right;" align="right">221 River Street, 8th Floor, Hoboken, NJ 07030 &copy; 2016</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="height:25px;">
              <img width="600" src="{{ secure_asset("images/4c1b3727-e048-4e80-815b-a9197acc62fe.png") }}">
            </td>
          </tr>
          <tr>
            <td>
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                  <td width="25"></td>
                  <td style="font-size:8px; line-height:12px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #535352;">&nbsp;</td>
                  <td width="25"></td>
                </tr>
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <!-- /content -->
      </td>
      <td></td>
    </tr>
  </table>
  <!-- /body -->
</body>

</html>
