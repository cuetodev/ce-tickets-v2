@component('mail::message')
# {{ $user->first_name }} Gifts you a Ticket.

{{$user->first_name}} {{ $user->last_name }} gifts you a ticket for the event {{ $event->name }}.

@component('mail::button', ['url' => url('dashboard/gift')])
View your Ticket Gift
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
