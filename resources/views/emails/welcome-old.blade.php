@component('mail::message')
# Welcome

Thank for join to us, {{ $user->first_name }}.

@component('mail::button', ['url' => url('events') ])
Browse Events
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
