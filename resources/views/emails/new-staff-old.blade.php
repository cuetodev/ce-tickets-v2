@component('mail::message')
# Welcome, Staff {{ $staff->name }}

Your Access code is: {{ $staff->code }}.

you can access by scanning with the app also.

@component('mail::panel')
  <img src="https://chart.googleapis.com/chart?cht=qr&chs=170x170&chl={{ $staff->code }}" alt="{{ $staff->code }}">
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
