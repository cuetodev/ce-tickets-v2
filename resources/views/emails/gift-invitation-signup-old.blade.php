@component('mail::message')
# {{ $user->first_name }} Gifts you a Ticket.

{{$user->first_name}} {{ $user->last_name }} gifts you a ticket for the event {{ $event->name }}.

We've notice you don't have any account with us.

@component('mail::button', ['url' => url('register')])
Sing Up to Claim your Gift
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
