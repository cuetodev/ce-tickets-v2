@component('mail::message')
# Your Access Code was reset.

Your Access Code is: {{ $staff->code }}.

you can access by scanning this code with the app also.

@component('mail::panel')
  <img src="https://chart.googleapis.com/chart?cht=qr&chs=170x170&chl={{ $staff->code }}" alt="{{ $staff->code }}">
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
