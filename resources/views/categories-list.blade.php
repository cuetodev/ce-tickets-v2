@extends('layouts.app')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>@lang("Categories")</h2><span>@lang("Explore all Categories")</span>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{ url('/') }}">@lang("Home")</a></li>
            <li><a href="{{ url('categories') }}">@lang("Categories")</a></li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->


<div class="container">

	<div class="row">

		<div class="col-lg-9 col-md-8 padding-right-30">

			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-25">

				<div class="col-md-6 col-xs-6">
					<!-- Layout Switcher -->
					<div class="layout-switcher">
						<a href="#" class="grid active"><i class="fa fa-th"></i></a>
						{{-- <a href="category-list.html" class="list"><i class="fa fa-align-justify"></i></a> --}}
					</div>
				</div>
			</div>
			<!-- Sorting / Layout Switcher / End -->

			<div class="row">

				@forelse ($categories as $category)
					<!-- Listing Item -->
  				<div class="col-lg-6 col-md-12">
  					<a href="{{ url('categories') }}/{{ $category->slug }}" class="listing-item-container">
  						<div class="listing-item">
  							<img src="{{ $category->picture }}" alt="">
  							<div class="listing-item-content">
  								<h3>{{ $category->name_translated[LaravelLocalization::getCurrentLocale()] }}</h3>
  								<span>{{ $category->events()->where('privacy', 'public')->where('status', '!=', 'ended')->count() }} @lang("events")</span>
  							</div>
  						</div>
  					</a>
  				</div>
  				<!-- Listing Item / End -->
				@empty
          <p>@lang("There are not Categories")</p>
				@endforelse

			</div>

			<!-- Pagination -->
			<div class="clearfix"></div>

      {{-- {{ $category->links('pagination.page-event') }} --}}
			<!-- Pagination / End -->

		</div>

    @include('filter-sidebar')

	</div>
</div>
@endsection
