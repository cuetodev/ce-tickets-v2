@extends('layouts.app')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>{{ $country->name }}</h2><span>@lang("Find the attractions of :Country", ['Country' => $country->name])</span>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{ url('/') }}">@lang("Home")</a></li>
            <li><a href="{{ url('countries') }}">@lang("Countries")</a></li>
						<li>{{ $country->name }}</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->


<div class="container">

	<div class="row">

		<div class="col-lg-9 col-md-8 padding-right-30">

			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-25">

				<div class="col-md-6 col-xs-6">
					<!-- Layout Switcher -->
					<div class="layout-switcher" role="tablist">
						<a aria-controls="Events" role="tab" data-toggle="tab" href="#Events" class="grid active"><i class="fa fa-film"></i></a>
						<a aria-controls="Cities" role="tab" data-toggle="tab" href="#Cities" class="list"><i class="fa fa-map-pin"></i></a>
					</div>
				</div>

			</div>
			<!-- Sorting / Layout Switcher / End -->


			<div class="row tab-content">

				<div id="Events" role="tabpanel" class="tab-pane active">
					@forelse ( $country->events->where('privacy', 'public')->where('status', '!=', 'ended') as $events)
	          <!-- Listing Item -->
	  				<div class="col-lg-6 col-md-12">
	  					<a href="{{ url('events') }}/{{ $events->slug }}/" class="listing-item-container">
	  						<div class="listing-item">
	  							<img src="{{ $events->picture }}" alt="">

									@if ($events->status == 'sold-out')
	                  <div class="listing-badge now-closed">@lang("Sold Out!")</div>
	                @endif

	  							<div class="listing-item-details">
	  								<ul>
	  									<li>{{ $events->start_date->toFormattedDateString() }}</li>
	  								</ul>
	  							</div>
	  							<div class="listing-item-content">
	  								<span class="tag">{{ $events->category->name_translated[LaravelLocalization::getCurrentLocale()] }}</span>
	  								<h3>{{ $events->name }}</h3>
	  								<span>{{ $events->location }}</span>
	  							</div>
	  							{{-- <span class="like-icon"></span> --}}
	  						</div>
	  						<div class="star-rating" data-rating="{{ $events->averageReview() }}">
	  							<div class="rating-counter">({{ $events->reviews->count() }} @lang("reviews"))</div>
	  						</div>
	  					</a>
	  				</div>
	  				<!-- Listing Item / End -->
					@empty
	          <p>@lang("There are not events for this country")</p>
					@endforelse
				</div>

				<div id="Cities" role="tabpanel" class="tab-pane">
					@forelse ($country->cities as $city)
						<!-- Listing Item -->
	  				<div class="col-lg-6 col-md-12">
	  					<a href="{{ url('cities') }}/{{ $city->slug }}/" class="listing-item-container">
	  						<div class="listing-item">
	  							<img src="{{ $city->picture }}" alt="">
	  							<div class="listing-item-content">
	  								<h3>{{ $city->name }}</h3>
	  								<span>{{ $city->events()->where('privacy', 'public')->where('status', '!=', 'ended')->count() }} @lang("events")</span>
	  							</div>
	  						</div>
	  					</a>
	  				</div>
	  				<!-- Listing Item / End -->
					@empty
	          <p>@lang("There are not cities")</p>
					@endforelse
				</div>

			</div>

			<!-- Pagination -->
			<div class="clearfix"></div>

      {{-- {{ $category->links('pagination.page-event') }} --}}
			<!-- Pagination / End -->

		</div>

    @include('filter-sidebar')

	</div>
</div>
@endsection
