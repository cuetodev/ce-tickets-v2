@extends('layouts.app')


@section('content')
  <!-- Banner
  ================================================== -->
  <div class="main-search-container" data-background-image="{{ asset('images/main-slider.jpg') }}">
  	<div class="main-search-inner">

  		<div class="container">
  			<div class="row">
  				<div class="col-md-12">
  					<h2>@lang("Find Nearby Attractions")</h2>
  					<h4>@lang("Explore top-rated attractions, activities and more")</h4>

            <form class="" action="/search" method="GET">
  					   <div class="main-search-input">
                <div class="main-search-input-item">
                  <input type="text" placeholder="@lang("What are you looking for?")" value="" name="keyword"/>
                </div>

                <div class="main-search-input-item location">
                  <input type="text" placeholder="@lang("Location")" value="" name="location"/>
                  <a href="#"><i class="fa fa-dot-circle-o"></i></a>
                </div>

                <div class="main-search-input-item">
                  <select data-placeholder="@lang("All Dates")" class="chosen-select" name="date">
                    <option value="all day">@lang("All Dates")</option>
                    <option value="today">@lang("Today")</option>
                    <option value="tomorrow">@lang("Tomorrow")</option>
                    <option value="this-week">@lang("This Week")</option>
                    <option value="next-week">@lang("Next Week")</option>
                    <option value="this-month">@lang("This Month")</option>
                    <option value="next-month">@lang("Next Month")</option>
                  </select>
                </div>

                <button class="button" type="submit">@lang("Search")</button>
  					</div>
          </form>
  				</div>
  			</div>
  		</div>

  	</div>
  </div>

  <section class="section-todays-schedule">
			<div class="container">
				<div class="row">
					<div class="section-header">
						<h2>@lang("Today's Activities")</h2>
						<span class="todays-date"><i class="fa fa-calendar" aria-hidden="true"></i> <strong>{{ Carbon\Carbon::parse('today')->format('d') }}</strong> {{ Carbon\Carbon::parse('today')->format('F Y') }} </span>
					</div>
					<div class="section-content">
						<ul class="clearfix">
							@forelse ($todays as $today)
                <li class="event">
  								<span class="event-time">{{ Carbon\Carbon::parse($today->start_time)->format('h:i') }} <strong>{{ Carbon\Carbon::parse($today->start_time)->format('A') }}</strong> </span>
  								<strong class="event-name">{{ $today->name }}</strong>
                  @if ($today->status == "sold-out")
                    <a href="#" class="sold-ticket">@lang("Sold Out!")</a>
                  @else
                    <a href="/events/{{ $today->slug }}" class="get-ticket">@lang("Get Ticket")</a>
                  @endif
  							</li>
              @empty
                <p>@lang("Today is a good day to stay home, there are not events today.")</p>
							@endforelse

						</ul>
						{{-- <strong class="event-list-label">Full Event <span>Schedules</span></strong> --}}
					</div>
				</div>
			</div>
		</section>


    <div class="container">
	<div class="row">

		<div class="col-md-12">
			<h3 class="headline centered margin-top-75">
				@lang("Popular Categories")
				<span>@lang("Browse <i>the most desirable</i> categories")</span>
			</h3>
		</div>

	</div>
</div>
<!-- Categories Carousel -->
<div class="fullwidth-carousel-container margin-top-25">
	<div class="fullwidth-slick-carousel category-carousel">

    @foreach ($categories as $category)
      <!-- Item -->
  		<div class="fw-carousel-item">
  			<div class="category-box-container">
  				<a href="/categories/{{ $category->slug }}" class="category-box" data-background-image="{{ $category->picture }}">
  					<div class="category-box-content">
  						<h3>{{ $category->name_translated[LaravelLocalization::getCurrentLocale()] }}</h3>
  						<span>{{ $category->events()->where('privacy', 'public')->where('status', '!=', 'ended')->count() }} @lang("events")</span>
  					</div>
  					<span class="category-box-btn">@lang("Browse")</span>
  				</a>
  			</div>
  		</div>
    @endforeach

	</div>
</div>
<!-- Categories Carousel / End -->

<!-- Fullwidth Section -->
<section class="fullwidth margin-top-65 padding-top-75 padding-bottom-70" data-background-color="#f8f8f8">

	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<h3 class="headline centered margin-bottom-45">
					@lang("Popular Activities")
					<span>@lang("Discover attractions, activities and more")</span>
				</h3>
			</div>

			<div class="col-md-12">
				<div class="simple-slick-carousel dots-nav">

          @foreach ($populars->chunk(2) as $chunks)
            <!-- Listing Item -->
    				<div class="carousel-item">
    					@foreach ($chunks as $popular)
                <a href="/events/{{ $popular->slug }}" class="listing-item-container">
      						<div class="listing-item">
      							<img src="{{ $popular->picture }}" alt="">

                    @if ($popular->status == 'sold-out')
                      <div class="listing-badge now-closed">@lang("Sold Out!")</div>
                    @endif

      							<div class="listing-item-content">
      								<span class="tag">{{ $popular->category->name_translated[LaravelLocalization::getCurrentLocale()] }}</span>
      								<h3>{{ $popular->name }}</h3>
      								<span>{{ $popular->location }}</span>
      							</div>
      							{{-- <span class="like-icon"></span> --}}
      						</div>
      						<div class="star-rating" data-rating="{{ $popular->averageReview() }}">
      							<div class="rating-counter">({{ $popular->reviews->count() }} @lang("reviews"))</div>
      							<div class="price">USD${{ $popular->type_entrances->first()->price }}</div>
      						</div>
      					</a>
    					@endforeach
    				</div>
    				<!-- Listing Item / End -->
          @endforeach
				</div>

			</div>

		</div>
	</div>

</section>
<!-- Fullwidth Section / End -->

<!-- City Section -->
<!-- Container -->
<div class="container">
  @foreach ($popularCities->chunk(4) as $chunk)
    <div class="row">

  		<div class="col-md-12">
  			<h3 class="headline centered margin-bottom-35 margin-top-70">@lang("Popular Cities") <span>@lang("Browse events in popular places")</span></h3>
  		</div>

  		<div class="col-md-4">

  			<!-- Image Box -->
  			<a href="{{ url('cities'.'/'.$chunk[0]->slug).'/' }}" class="img-box" data-background-image="{{ $chunk[0]->picture }}">
  				<div class="img-box-content visible">
  					<h4>{{ $chunk[0]->name }}</h4>
  					<span>{{ $chunk[0]->events()->where('privacy', 'public')->where('status', '!=', 'ended')->count() }} @lang("Events")</span>
  				</div>
  			</a>

  		</div>

  		<div class="col-md-8">

  			<!-- Image Box -->
  			<a href="{{ url('cities'.'/'.$chunk[1]->slug).'/' }}" class="img-box" data-background-image="{{ $chunk[1]->picture }}">
  				<div class="img-box-content visible">
            <h4>{{ $chunk[1]->name }}</h4>
  					<span>{{ $chunk[1]->events()->where('privacy', 'public')->where('status', '!=', 'ended')->count() }} @lang("Events")</span>
  				</div>
  			</a>

  		</div>

  		<div class="col-md-8">

  			<!-- Image Box -->
  			<a href="{{ url('cities'.'/'.$chunk[2]->slug).'/' }}" class="img-box" data-background-image="{{ $chunk[2]->picture }}">
  				<div class="img-box-content visible">
            <h4>{{ $chunk[2]->name }}</h4>
  					<span>{{ $chunk[2]->events()->where('privacy', 'public')->where('status', '!=', 'ended')->count() }} @lang("Events")</span>
  				</div>
  			</a>

  		</div>

  		<div class="col-md-4">

  			<!-- Image Box -->
  			<a href="{{ url('cities'.'/'.$chunk[3]->slug).'/' }}" class="img-box" data-background-image="{{ $chunk[3]->picture }}">
  				<div class="img-box-content visible">
            <h4>{{ $chunk[3]->name }}</h4>
  					<span>{{ $chunk[3]->events()->where('privacy', 'public')->where('status', '!=', 'ended')->count() }} @lang("Events")</span>
  				</div>
  			</a>

  		</div>

  	</div>
  @endforeach
</div>
<!-- Container / End -->
<!-- City Section / End -->
@endsection
