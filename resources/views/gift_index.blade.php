@extends('layouts.app')

@section('content')
  <!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h2>@lang("Gift Tickets")</h2>
        <!-- Breadcrumbs -->
        <nav id="breadcrumbs">
          <ul>
            <li><a href="/">@lang("Home")</a></li>
            <li>@lang("Gift Tickets")</li>
          </ul>
        </nav>
      </div>
    </div>
  </div>
</div>

<div class="container style-2 padding-bottom-70">
  <!-- Nav tabs -->
  <ul class="tabs-nav">
    <li class="active"><a href="#gift">@lang("Gift")</a></li>
    <li><a href="#gifted">@lang("Gifted")</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tabs-container">
    <div class="tab-content" id="gift" style="display: inline-block;">
      <div class="row">
        @forelse ($gift_to_me as $gift)
        <div class="col-lg-12 col-xs-12">
          <div class="listing-item-container list-layout">
            <span class="listing-item" style="height: auto;">

              <!-- Image -->
              <div class="listing-item-image" style="min-height: auto;">
                <img src="{{ App\Event::find($gift->event_id)->picture }}" alt="">
              </div>

              <!-- Content -->
              <div class="listing-item-content">
                <div class="listing-item-inner">
                  <h4>{{ App\Event::find($gift->event_id)->name }}</h4>
                  <span>{{ App\Entrance::find($gift->entrance_id)->name }}</span></br>
                  <span>{{ App\Entrance::find($gift->entrance_id)->start_date }} {{ App\Entrance::find($gift->entrance_id)->start_time }}</span><br>
                  <span>{{ App\Entrance::find($gift->entrance_id)->location }}</span>
                  <div class="hidden-print">
                    <span>
                      <a class="btn btn-danger" href="{{ url('/gift').'/'.$gift->id.'/reject' }}"
                      onclick="event.preventDefault();
                      document.getElementById('reject-form').submit();">@lang("Decline")</a>

                      <a class="btn btn-primary" href="{{ url('/gift').'/'.$gift->id.'/accept' }}"
                      onclick="event.preventDefault();
                      document.getElementById('accept-form').submit();">@lang("Accept")</a>
                    </span>

                    <form id="accept-form" class="form-horizontal" action="{{ url('/gift').'/'.$gift->id.'/accept' }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                      <input type="hidden" name="accept" value="{{ str_random(9) }}">
                    </form>

                    <form id="reject-form" class="form-horizontal" action="{{ url('/gift').'/'.$gift->id.'/reject' }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                      <input type="hidden" name="reject" value="{{ str_random(8) }}">
                    </form>
                  </div>
                </div>
              </div>
            </span>
          </div>
        </div>
        @empty
          <h4>@lang("Sorry, You don't have any gift yet!")</h4>
        @endforelse
      </div>
    </div>
    <div class="tab-content" id="gifted">
      <div class="row">
        @forelse ($gifted as $given)
        <div class="col-lg-12 col-xs-12">
          <div class="listing-item-container list-layout">
            <span class="listing-item" style="height: auto;">

              <!-- Image -->
              <div class="listing-item-image" style="min-height: auto;">
                <img src="{{ App\Event::find($given->event_id)->picture }}" alt="">
              </div>

              <!-- Content -->
              <div class="listing-item-content">
                <div class="listing-item-inner">
                  <h4>{{ App\Event::find($given->event_id)->name }}</h4>
                  <span>{{ App\Entrance::find($given->entrance_id)->name }}</span>
                  <span>{{ App\Entrance::find($given->entrance_id)->receiver }}</span>
                </div>
              </div>
            </span>
          </div>
        </div>
        @empty
          <h4>@lang("You have not given any ticket yet.")</h4>
        @endforelse
      </div>
    </div>
  </div>
</div>

@endsection
