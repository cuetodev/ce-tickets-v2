@extends('layouts.app')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>@lang("Countries")</h2><span>@lang("Explore all Countries")</span>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{ url('/') }}">@lang("Home")</a></li>
            <li><a href="{{ url('countries') }}">@lang("Countries")</a></li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->


<div class="container">

	<div class="row">

		<div class="col-lg-9 col-md-8 padding-right-30">

			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-25">

				<div class="col-md-6 col-xs-6">
					<!-- Layout Switcher -->
					<div class="layout-switcher">
						<a href="#" class="grid active"><i class="fa fa-th"></i></a>
						{{-- <a href="category-list.html" class="list"><i class="fa fa-align-justify"></i></a> --}}
					</div>
				</div>
			</div>
			<!-- Sorting / Layout Switcher / End -->

			<div class="row">

				@forelse ($countries as $country)
					<!-- Listing Item -->
  				<div class="col-lg-6 col-md-12">
  					<a href="{{ url('countries') }}/{{ $country->slug }}/" class="listing-item-container">
  						<div class="listing-item">
  							<img src="{{ $country->picture }}" alt="">
  							<div class="listing-item-content">
  								<h3>{{ $country->name }}</h3>
  								<span>{{ $country->events()->where('privacy', 'public')->where('status', '!=', 'ended')->count() }} @lang("events")</span>
  							</div>
  						</div>
  					</a>
  				</div>
  				<!-- Listing Item / End -->
				@empty
          <p>@lang("There are not Countries")</p>
				@endforelse

			</div>

			<!-- Pagination -->
			<div class="clearfix"></div>

      {{-- {{ $category->links('pagination.page-event') }} --}}
			<!-- Pagination / End -->

		</div>

    @include('filter-sidebar')

	</div>
</div>
@endsection
