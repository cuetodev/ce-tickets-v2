<!-- Footer
================================================== -->
<div id="footer" class="sticky-footer" style="background-color: #3b2367; padding: 30px 0 0 0;">
	<!-- Main -->
	<div class="container">
		<div class="row">
			<div class="col-md-5 col-sm-6">
				<img class="footer-logo" src="{{ asset("images/logo-white.png") }}" alt="">
				<br>

			    <ul class="social-icons margin-top-20">
					<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
					<li><a class="vimeo" href="#"><i class="icon-vimeo"></i></a></li>
				</ul>
			</div>

			<div class="col-md-4 col-sm-6 ">

				<div class="clearfix"></div>
			</div>

			<div class="col-md-3  col-sm-12">

			<h5 style="color: #fff;"><strong>@lang("Download our App")</strong></h5>
				<a href="https://itunes.apple.com/us/app/events-to-city/id1366304944?l=es&ls=1&mt=8">
					<img class="footer-logo" src="{{asset("images/appstore.png")}}" alt="">
				</a>
				<a href="https://play.google.com/store/apps/details?id=events.to.city">
					<img class="footer-logo" src="{{asset("images/playstore.png")}}" alt="">
				</a>

			</div>

		</div>

		<!-- Copyright -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyrights" >© 2017 Events2City. @lang("All Rights Reserved").
				<a href="/legal/" id="copyright-policies" title="@lang("User Agreement, Privacy Notice and Cookie Notice")">
				@lang("User Agreement, Privacy Notice and Cookie Notice")</a>
				</div>
			</div>
		</div>

	</div>

</div>
<!-- Footer / End -->

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>
