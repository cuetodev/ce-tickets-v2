<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
  <head>

  <!-- Basic Page Needs
  ================================================== -->
    <title>Events2City</title>

    @if ( Auth::check() )
      <meta name=":api-token:" content="{{ Auth::user()->api_token }}">
    @endif

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="description" content="@lang('Events in your city')">
    <meta name="keywords" content="Tickets,City,Events,Buy">
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{asset( 'images/icon_app.png' )}}" />
    <meta property="og:image:secure_url" content="{{asset( 'images/icon_app.png' )}}" />
    @stack('meta-tags-image')

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset( 'css/app.css' ) }}">
    <link rel="stylesheet" href="{{ asset( "css/style.css" ) }}">
    <link rel="stylesheet" href="{{ asset("css/colors/main.css") }}" id="colors">
    {{-- <link rel="stylesheet" href="{{ asset('css/all-fontello.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset("css/jquery.scrolling-tabs.min.css") }}">

    @stack('specific-style')

    <script>
    toggleCart = function() {
      var x = document.getElementById("CartNav");
      x.style.display = (x.style.display === "none") ? "block" : "none" ;
    }
</script>

    <style type="text/css">
    .cart-nav {
    height: 100vh;
    width: 25%;
    right: 0px;
    display: block;
    position: fixed;
    background-color: #2a2a2a;
    z-index: 99999;
    box-shadow: 0px 0px 12px 0px rgba(0,0,0,0.12);
}
    </style>

    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
    <script>
      window.addEventListener("load", function(){
      window.cookieconsent.initialise({
        "palette": {
        "popup": {
          "background": "#000"
        },
        "button": {
          "background": "#ff5f06",
          "text": "#ffffff"
        }
      },
      "theme": "classic",
      "position": "bottom-right"
    })});
  </script>
</head>

<body>

  <!-- Wrapper -->
  <div id="wrapper">

    <div class="cart-nav" id="CartNav" style="display: none;">
      <div class="cart-nav-inner" style="width: 100%; margin: 2px;">
        <div class="cart-nav-title" style="padding: 0px 0 8px 0;margin: 0 auto;position: relative;">
          <h1 style="color: #fff;" class="">
            <div style="margin: 0px 16px; float: left;">
              <a href="/cart" ><i class="sl sl-icon-close" style="color: #fff;"></i></a>
            </div>
            <div style="margin: 0 19%;" class="text-center">Cart</div>
          </h1>
        </div>

        <div class="cart-nav-body" style="margin: 9px 14px;">
          @if (\Cart::content()->count() != 0)
            <table class="table table_summary">
              <tbody>
                @forelse ( \Cart::content() as $item)
                  <tr>
                    <td>
                      {{ $item->name }} - {{ $item->options->type_ticket['name'] }}
                    </td>
                    <td class="text-right">
                      {{ $item->qty }}x ${{ $item->price }}
                    </td>
                  </tr>
                @endforeach
                <tr>
                  <td>
                    Total amount
                  </td>
                  <td class="text-right">
                    ${{ \Cart::subtotal() }}
                  </td>
                </tr>
                <tr class="total">
                  <td>
                    Total cost
                  </td>
                  <td class="text-right">
                    ${{ \Cart::total() }}
                  </td>
                </tr>
              </tbody>
            </table>
          @else
            <p class="text-center" style="color: #fff; top: 0px; left: 0px; position: relative;">The Cart is Empty.</p>
          @endif
        </div>
        <div class="cart-nav-footer" style="position: absolute; bottom: 0; text-align: center; margin: 0px 33%; display: inline;">
          <a href="{{ url("check-out") }}" class="button fullwidth" >Checkout</a>
          <a href="{{ url("cart") }}" class="button border fullwidth">Go to Cart</a>
        </div>
      </div>
    </div>

    @include('layouts.header')
    @yield('content')
    @include('layouts.footer')

  </div>
  <!-- Wrapper / End -->

  <!-- Scripts
  ================================================== -->
  <script src="{{ asset('js/app.js') }}"></script>
  <script type="text/javascript" src="{{asset("scripts/jquery-2.2.0.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("scripts/jpanelmenu.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("scripts/chosen.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("scripts/slick.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("scripts/rangeslider.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("scripts/magnific-popup.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("scripts/waypoints.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("scripts/counterup.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("scripts/jquery-ui.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("scripts/tooltips.min.js")}}"></script>
  <script type="text/javascript" src="{{asset("scripts/custom.js")}}"></script>
  <script type="text/javascript" src="https://checkout.stripe.com/checkout.js"></script>
  <script>
     var stripe = StripeCheckout.configure({
        "key": "{{ config('services.stripe.key') }}",
        "image": "{{ asset("images/logo.png") }}",
        "locale": "auto",
        token: function (token) {
          $("#stripeToken").val(token.id);
          $("#stripeEmail").val(token.email);


          $("#stripeForm").submit();
        }
    });

     $("#stripeCheckoutBtn").click(function (e) {
        e.preventDefault();

        var amount = $(this).data('amount') * 100;
        stripe.open({
            "name": "Buy tickets",
            "description": "Buy your tickets for the event",
            "amount": amount
        });
     });
  </script>
  <script type="text/javascript">
  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

  $('.image-popup-vertical-fit').magnificPopup({
		type: 'image',
		closeOnContentClick: true,
		mainClass: 'mfp-img-mobile',
		image: {
			verticalFit: true
		}

	});

  $('.popup-modal').magnificPopup({
    type: 'inline',
    preloader: false,
    focus: '#username',
    modal: false,
    closeBtnInside:true
  });

  $(document).on('click', '.popup-modal-dismiss', function (e) {
    e.preventDefault();
    $.magnificPopup.close();
  });

  function onFileSelected(event) {
    var selectedFile = event.target.files[0];
    var reader = new FileReader();

    var imgtag = document.querySelector(".edit-profile-photo img");
    imgtag.title = selectedFile.name;
    reader.onload = function(event) {

      imgtag.src = event.target.result;
    };

    reader.readAsDataURL(selectedFile);
  }
  </script>

  <!-- Maps -->
  <script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyDs4wqHQuBeN_eRJjXZbyJAThVdTSLnRmA&language=es"></script>
  <script type="text/javascript" src="{{ asset('scripts/infobox.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('scripts/markerclusterer.js') }}"></script>
  <script type="text/javascript" src="{{ asset('scripts/maps.js') }}"></script>

  </body>
  </html>
