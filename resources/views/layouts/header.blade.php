<!-- Header Container
================================================== -->
<header id="header-container">

	<!-- Header -->
	<div id="header">
		<div class="container">

			<!-- Left Side Content -->
			<div class="left-side" style="width: 40%;">

				<!-- Logo -->
				<div id="logo">
					<a href="/"><img src="{{ asset("images/logo.png") }}" alt=""></a>
				</div>

				<!-- Mobile Navigation -->
				{{-- <div class="menu-responsive">
					<i class="fa fa-reorder menu-trigger"></i>
				</div> --}}

				<!-- Main Navigation -->

				{{-- <div class="clearfix"></div> --}}
				<!-- Main Navigation / End -->

			</div>
			<!-- Left Side Content / End -->


			<!-- Right Side Content / End -->
			<div class="right-side" style="width: 60%;">
				<div class="header-widget hidden-xs hidden-sm">
					@if (Auth::guest())
						<a href="{{url("register")}}" class="button border sign-in"> @lang("Sign up")</a>
						<a href="{{url("login")}}" class="sign-in"> @lang("Sign In")</a>

					@else
						<div class="user-menu">
							<div class="user-name"><span><img src="{{ Auth::user()->avatar }}" alt="">
							</span>{{ Auth::user()->first_name.' '.Auth::user()->last_name }}</div>
							<ul>
								<li>
									<a href="{{ url('/dashboard') }}"><i class="sl sl-icon-settings"></i> @lang("Dashboard")</a>
								</li>
								<li>
									<a
									  href="{{ route('logout') }}"
									  onclick="event.preventDefault();
										document.getElementById('logout-form').submit();">
										<i class="sl sl-icon-power"></i> @lang("Logout")
									</a>
									<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
											{{ csrf_field() }}
									</form>
								</li>
							</ul>
						</div>
						<a href="{{ url('/dashboard/gift') }}" class="sign-in"><i class="sl sl-icon-present"></i> @lang("Gifts") <span class="badge">{{ App\Gift::where('receiver', auth()->user()->email)->where('accepted', 0)->count() }}</span></a>
					@endif
					<a href="{{url('search')}}" class="sign-in"><i class="sl sl-icon-magnifier"></i></a>
					<a href="/cart" class="sign-in"><i class="sl {{ \Cart::content()->count() == 0 ? 'sl-icon-basket' : 'sl-icon-basket-loaded'}}"></i><span class="badge">{{ \Cart::content()->count() }}</span></a>

					<div class="dropdown sign-in">
						<a href="#" class="dropdown-toggle" type="button" id="lang-menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<svg class="flag-icon" aria-hidden="true" focusable="false">
								<use xlink:href="{{ asset('fonts/flag-localize.svg') }}#{{ LaravelLocalization::getCurrentLocale() }}"></use>
							</svg>
							{{ strtoupper(LaravelLocalization::getCurrentLocale()) }}
							<span class="caret"></span>
						</a>
						<ul class="dropdown-menu" aria-labelledby="lang-menu">
							@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
								<li>
									<a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
										<svg class="flag-icon" aria-hidden="true" focusable="false">
											<use xlink:href="{{ asset('fonts/flag-localize.svg') }}#{{ $localeCode }}"></use>
										</svg>
										{{ $properties['native'] }}
									</a>
								</li>
							@endforeach
						</ul>
					</div>
				</div>

				<div class="header-widget hidden-md hidden-lg pull-right">
					<nav id="navigation" class="style-1" style="display: inline;">
						<ul>
							<li>
								<a href="{{url('search')}}" style="padding: 5px 5px;">
									<i class="sl sl-icon-magnifier"></i>
								</a>
							</li>
							<li>
								<a href="{{url('cart')}}" style="padding: 5px 5px;">
									<i class="sl {{ \Cart::content()->count() == 0 ? 'sl-icon-basket' : 'sl-icon-basket-loaded'}}"></i>
								</a>
							</li>
							<li>
								<a href="{{ url('/dashboard/gift') }}" style="padding: 5px 5px;">
									<i class="sl sl-icon-present"></i>
								</a>
							</li>
							<li>
								<a href="{{ url('/dashboard') }}" style="padding: 5px 5px;">
									<i class="sl sl-icon-user"></i>
								</a>
							</li>
						</ul>
					</nav>
				</div>



			</div>
			<!-- Right Side Content / End -->
{{--
			<!-- Sign In Popup -->
			<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">

				<div class="small-dialog-header">
					<h3>Sign In</h3>

				</div>

				<!--Tabs -->
				<div class="sign-in-form style-1">
					<ul class="social-icons margin-top-20">
						<li> <h5>Connect with: </h5></li>
					  <li><a class="facebook" href="/auth/facebook"><i class="icon-facebook"></i></a></li>
					  <li><a class="twitter" href="/auth/twitter"><i class="icon-twitter"></i></a></li>
					  <li><a class="gplus" href="/auth/google"><i class="icon-gplus"></i></a></li>
					</ul>
			<br>

					<ul class="tabs-nav" style="padding-top: 60px;">
						<li class=""><a href="#tab1">Log In</a></li>
						<li><a href="#tab2">Register</a></li>
					</ul>

					<div class="tabs-container alt">

						<!-- Login -->
						<div class="tab-content" id="tab1" style="display: none;">
							<form method="POST" class="login" action="{{ route('login') }}">
								{{ csrf_field() }}

								<p class="form-row form-row-wide">
									<label for="email">
										<i class="im im-icon-Male"></i>
										<input type="email" class="input-text" name="email" id="email" value="{{ old('email') }}" placeholder="Email" required autofocus/>
									</label>
								</p>

								<p class="form-row form-row-wide">
									<label for="password">
										<i class="im im-icon-Lock-2"></i>
										<input class="input-text" type="password" name="password" id="password" placeholder="Password" />
									</label>
									<div class="checkboxes margin-top-10">
										<input id="remember-me" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
										<label for="remember-me">Remember Me</label>
									</div>
								</p>

								<div class="form-row">
									<input type="submit" class="button border margin-top-5" name="login" value="Login" />

									<span class="lost_password">
										<a href="{{ route('password.request') }}" >Forgot Your Password?</a>
									</span>
								</div>

							</form>
						</div>

						<!-- Register -->
						<div class="tab-content" id="tab2" style="display: none;">

							<form method="POST" class="register" action="{{ route('register') }}">
								{{ csrf_field() }}

							<p class="form-row form-row-wide">
								<label for="first_name">
									First Name
									<input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>
									@if ($errors->has('first_name'))
									<span class="help-block">
										<strong>{{ $errors->first('first_name') }}</strong>
                  </span>
                  @endif
								</label>
							</p>

							<p class="form-row form-row-wide">

								<label for="last_name">
									Last Name
									<input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required >
									@if ($errors->has('last_name'))
                  <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                  </span>
									@endif
								</label>
							</p>

							<p class="form-row form-row-wide">

								<label for="username2">
									UserName
									<input id="username2" type="text" class="form-control" name="username" value="{{ old('username') }}" required >
									@if ($errors->has('username'))
                  <span class="help-block">
                    <strong>{{ $errors->first('username') }}</strong>
                  </span>
									@endif
								</label>
							</p>

							<p class="form-row form-row-wide">

								<label for="email2">
									Email
									<input id="email2" type="text" class="form-control" name="email" value="{{ old('email') }}" required >
									@if ($errors->has('email'))
                  <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                  </span>
									@endif
								</label>
							</p>

							<p class="form-row form-row-wide">

								<label for="phone_number">
									Phone
									<input id="phone_number" type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}" required >
								</label>
							</p>

							<p class="form-row form-row-wide">

								<label for="password3">
									Password
									<input id="password3" type="password" class="form-control" name="password" required>
									@if ($errors->has('password'))
                  <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                  </span>
									@endif
								</label>
							</p>

							<p class="form-row form-row-wide">

								<label for="password-confirm3">
									Password Confirmation
									<input id="password-confirm3" type="password" class="form-control" name="password_confirmation" required>
								</label>
							</p>

							<p class="form-row form-row-wide">
								<label for="gender2">
									Gender
									<select id="gender2" name="gender" class="form-control">
										<option value="male">Male</option>
                    <option value="female">Female</option>
                  </select>
								</label>
							</p>

							<p class="form-row form-row-wide">
								<label for="birthday-0">
									Birth date
									<input id="birthday-0" type="date" class="form-control" name="birth_date" required>
								</label>
							</p>

							<input type="submit" class="button border fw margin-top-10" name="register" value="Register" />

							</form>
						</div>

					</div>
				</div>
			</div>
			<!-- Sign In Popup / End --> --}}

		</div>
	</div>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->
