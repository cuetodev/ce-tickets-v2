@extends('layouts.app')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h2>@lang("Organization Request")</h2>
        <span>@lang("Become on a Organizator and Organize Events")</span>
      </div>
    </div>
  </div>
</div>

<div class="container margin-bottom-35">

  <div class="row">

    <form class="" action="{{ url('organization/make_request') }}" enctype="multipart/form-data" method="POST">

      {{ csrf_field() }}

      <div class="col-md-9">
        <div class="row">

          <div class="col-md-6">
            <div class="form-group">
              <label for="name_organization" class="control-label">@lang("Organization's Name")</label>
              <input id="name_organization" type="text" name="name_organization" value="{{ old('name_organization') }}" required>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="email_organization" class="control-label">@lang("E-Mail Address")</label>
              <input id="email_organization" type="email" name="email_organization" value="{{ old('email_organization') }}" required>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label for="description_organization" class="control-label">@lang("Description")</label>
              <textarea id="description_organization" name="description_organization" rows="8" cols="80" value="{{ old('description_organization') }}" required></textarea>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="phone_organization" class="control-label">@lang("Phone Number")</label>
              <input id="phone_organization" type="tel" name="phone_organization" value="{{ old('phone_organization') }}" required>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="website" class="control-label">@lang("Website")</label>
              <input id="website" type="text" name="website" value="{{ old('website', "https://") }}">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label for="facebook_link" class="control-label">@lang("Facebook Link")</label>
              <input id="facebook_link" type="text" name="facebook_link" value="{{ old('facebook_link', "https://") }}">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label for="twitter_link" class="control-label">@lang("Twitter Link")</label>
              <input id="twitter_link" type="text" name="twitter_link" value="{{ old('twitter_link', "https://") }}">
            </div>
          </div>

          <div class="col-md-4">
            <div class="form-group">
              <label for="instagram_link" class="control-label">@lang("Instagram Link")</label>
              <input id="instagram_link" type="text" name="instagram_link" value="{{ old('instagram_link', "https://") }}">
            </div>
          </div>

        </div>

      </div>

      <div class="col-md-3">

        @if ($errors->any())
          <div class="alert alert-danger">
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
        @endif

        <div class="row">
          <div class="col-md-12">
            <div class="edit-profile-photo">
              <img src="{{ old('upload', asset("images/user-avatar.jpg")) }}">
              <div class="change-photo-btn">
                <div class="photoUpload">
                  <span>
                    <i class="fa fa-upload"></i>
                    @lang("Select a Picture")
                  </span>
                  <input type="file" name="picture_organization" class="upload" onchange="onFileSelected(event)" value="{{ old('upload') }}">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-12">
        <button class="button" type="submit">@lang("Create Organization")</button>
      </div>
    </form>

  </div>

</div>

@endsection
