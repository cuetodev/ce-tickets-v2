
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width">
  <meta name="format-detection" content="telephone=no">
  <!--[if !mso]>
      <!-->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300&subset=latin" rel="stylesheet" type="text/css">
  <!--<![endif]-->
  <title>Event to City - Order Confirmation</title>
  <style type="text/css">
    *{
      			margin:0;
      			padding:0;
      			font-family:'OpenSans-Light', "Helvetica Neue", "Helvetica",Calibri, Arial, sans-serif;
      			font-size:100%;
      			line-height:1.6;
      		}
      		body{
      			-webkit-font-smoothing:antialiased;
      			-webkit-text-size-adjust:none;
      			width:100%!important;
      			height:100%;
      		}
      		a{
      			color:#348eda;
      		}
      		.btn-primary{
      			text-decoration:none;
      			color:#FFF;
      			background-color:#a55bff;
      			border:solid #a55bff;
      			border-width:10px 20px;
      			line-height:2;
      			font-weight:bold;
      			margin-right:10px;
      			text-align:center;
      			cursor:pointer;
      			display:inline-block;
      		}
      		.last{
      			margin-bottom:0;
      		}
      		.first{
      			margin-top:0;
      		}
      		.padding{
      			padding:10px 0;
      		}
      		table.body-wrap{
      			width:100%;
      			padding:0px;
      			padding-top:20px;
      			margin:0px;
      		}
      		table.body-wrap .container{
      			border:1px solid #f0f0f0;
      		}
      		table.footer-wrap{
      			width:100%;
      			clear:both!important;
      		}
      		.footer-wrap .container p{
      			font-size:12px;
      			color:#666;
      		}
      		table.footer-wrap a{
      			color:#999;
      		}
      		.footer-content{
      			margin:0px;
      			padding:0px;
      		}
      		h1,h2,h3{
      			color:#660099;
      			font-family:'OpenSans-Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
      			line-height:1.2;
      			margin-bottom:15px;
      			margin:40px 0 10px;
      			font-weight:200;
      		}
      		h1{
      			font-family:'Open Sans Light';
      			font-size:45px;
      		}
      		h2{
      			font-size:28px;
      		}
      		h3{
      			font-size:22px;
      		}
      		p,ul,ol{
      			margin-bottom:10px;
      			font-weight:normal;
      			font-size:14px;
      		}
      		ul li,ol li{
      			margin-left:5px;
      			list-style-position:inside;
      		}
      		.container{
      			display:block!important;
      			max-width:600px!important;
      			margin:0 auto!important;
      			clear:both!important;
      		}
      		.body-wrap .container{
      			padding:0px;
      		}
      		.content,.footer-wrapper{
      			max-width:600px;
      			margin:0 auto;
      			padding:20px 33px 20px 37px;
      			display:block;
      		}
      		.content table{
      			width:100%;
      		}
      		.content-message p{
      			margin:20px 0px 20px 0px;
      			padding:0px;
      			font-size:22px;
      			line-height:38px;
      			font-family:'OpenSans-Light',Calibri, Arial, sans-serif;
      		}
      		.preheader{
      			display:none !important;
      			visibility:hidden;
      			opacity:0;
      			color:transparent;
      			height:0;
      			width:0;
      		}
  </style>
</head>

<body bgcolor="#f6f6f6">
  <span class="preheader" style="display: none !important; visibility: hidden; opacity: 0; color: transparent; height: 0; width: 0;">
</span>

  <!-- body -->
<table class="body-wrap" width="600">
    <tr>
      <td class="container" bgcolor="#FFFFFF">
        <!-- content -->
        <table border="0" cellpadding="0" cellspacing="0" class="contentwrapper" width="600">
          <tr>
            <td style="height:25px;">
              <img src="{{secure_asset('images/96288204-f67c-4ba2-9981-1be77c9fa18b.png')}}" border="0" width="600">
            </td>
          </tr>
          <tr>
            <td>
              <div class="content">

                <table class="content-message">
                  <tr>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td align="left">
                      <a href="https://eventstocity.com/">
                        <img src="https://eventstocity.com/images/logo.png" width="200">
                      </a>
                    </td>
                  </tr>

                </table>

              </div>


              <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" bgcolor="#F7F7F7">
    <tr>
      <td style="padding-right: 10px; padding-left: 10px;">
        <!-- Outlook Hack (doesn't support max-width property until 2013) -->
        <!--[if (gte mso 9)|(IE)]>
            <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#F7F7F7">
              <tr>
                <td>
                <![endif]-->

        <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
            </table>
          <![endif]-->
      </td>
    </tr>
    <tr>
      <td>
        <!--[if (gte mso 9)|(IE)]>
            <table width="600" align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
              <tr>
                <td>
                <![endif]-->
        <table   cellpadding="0" cellspacing="0" border="0" bgcolor="#F7F7F7" style="width: 600px; max-width: 600px;">
          <tr>
            <td colspan="2" style="background: #fff; border-radius: 8px;">
              <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                  <td style="font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;">
                    <style>
                      @media (max-device-width: 640px) {
                        .organized_by {
                          font-size: 14px !important;
                        }

                        .ticket-section__or {
                          padding: 12px 0 14px 0 !important;
                        }

                        .your-account__logo {
                          margin-top: 8px;
                        }
                      }

                      @media screen and (min-device-height: 480px) and (max-device-height: 568px) {
                        h2 {
                          font-weight: 600 !important;
                        }

                        .header_defer {
                          font-size: 12px;
                        }
                      }
                    </style>
                    <tr class="">
                      <td class="grid__col" style="font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; padding: 32px 40px; border-radius:6px 6px 0 0;" align="">

                        <h2 style="color: #404040; font-weight: 300; margin: 0 0 12px 0; font-size: 24px; line-height: 30px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " class="">
                          Hi {{ $order->user->first_name }} {{ $order->user->last_name }}, this is your order confirmation for <a href='{{ url('events').'/'.App\Event::find($order->items->first()->event_id)->slug }}' style='text-decoration: none; color: #1090ba; font-weight:normal;'>{{ App\Event::find($order->items->first()->event_id)->name }}</a>
                        </h2>
{{--
                        <div style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; font-weight: 300; " class="organized_by">Organized by
                          <a href="#" style="color:#999;">The Hustle</a>
                        </div> --}}
                      </td>
                    </tr>
                    <tr class="">
                      <td class="grid__col" style="font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; padding: 32px 40px; border-radius:0 0 6px 6px;" align="">
                        <table cellpadding="0" cellspacing="0" border="0" align="left" style="width:260px; line-height:1.67; font-size:13px;" class="small_full_width ">
                          <tr>
                            <td>

                              <h2 style="color: #404040; font-weight: 300; margin: 0 0 12px 0; font-size: 24px; line-height: 30px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " class="">
About this event
</h2>

                              <table cellspacing="0" cellpadding="0" width='100%' style='' align='' class="">
                                <tr style='' class=''>
                                  <td width='20' height='' style='vertical-align:top; padding-right:10px;' align='' valign='' class='' colspan='1'>
                                    <img src="{{secure_asset('images/date-iconx2.png')}}" title='date' alt='date' style='width:20px; height:20px; vertical-align:-2px;' border="0" width='20' height='20' class="" />
                                  </td>
                                  <td width='' height='' style='' align='' valign='' class='' colspan='1'>
                                    <div style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " class="">
                                      {{ Carbon\Carbon::parse(App\Event::find($order->items->first()->event_id)->start_date)->format('l jS \\of F Y') }} from {{ Carbon\Carbon::parse(App\Event::find($order->items->first()->event_id)->start_time)->format('h:i:s A') }} to {{ Carbon\Carbon::parse(App\Event::find($order->items->first()->event_id)->end_time)->format('h:i:s A') }}
                                    </div>
                                  </td>
                                </tr>
                                <tr style='' class=''>
                                  <td width='20' height='' style='vertical-align:top; padding-right:10px;' align='' valign='' class='' colspan='1'>
                                    <img src="{{secure_asset('images/location-pin-iconx2.png')}}" title='date' alt='date' style='width:20px; height:20px; vertical-align:-4px;' border="0" width='20' height='20' class="" />
                                  </td>
                                  <td width='' height='' style='padding-bottom:10px;' align='' valign='' class='' colspan='1'>{{ App\Event::find($order->items->first()->event_id)->location }}</td>
                                </tr>

                              </table>
                            </td>
                          </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" align="right" style="width:240px; text-align: center; margin-top:16px;" class="small_full_width ">
                          <tr>
                            <td>
                              <a style="text-decoration: none; color: #0f90ba; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " href="#" class="">
                                <img src='https://maps.googleapis.com/maps/api/staticmap?center={{ App\Event::find($order->items->first()->event_id)->latitude }},{{ App\Event::find($order->items->first()->event_id)->longitude }}&zoom=15&size=240x160&key=AIzaSyCbtvrHBbOxLtIaM4I3s0T05YKL1a7F-AU' title='map' alt='map' style='width:240px; height:160px'
                                  border="0" width='240' height='160' class="hide-for-small" />
                              </a>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding: 0 40px;">
                        <table cellspacing="0" cellpadding="0" width="100%" style="width: 100%; min-width: 100%;" class="">
                          <tr>
                            <td style="background-color: #dedede; width: 100%; min-width: 100%; font-size: 1px; height: 1px; line-height: 1px; " class="">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr class="">
                      <td class="grid__col" style="font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; padding: 32px 40px; " align="">

                        <h2 style="color: #404040; font-weight: 300; margin: 0 0 12px 0; font-size: 24px; line-height: 30px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; margin-bottom:18px;" class="">

            Here are your tickets

</h2>

                        <table cellpadding="0" cellspacing="0" border="0" align="left" style="width:215px; text-align:center; overflow:hidden;" class="small_full_width ">
                          <tr>
                            <td>
                              <a style="text-decoration: none; color: #0f90ba; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " href="#" class="">

                                <table width=100% cellspacing="0" cellpadding="0" style="font-weight:bold; margin-bottom:12px;" class="">
                                  <tr width="100%">
                                    <td width="100%" style="text-align:center;" class="">Mobile Tickets</td>
                                  </tr>
                                </table>
                              </a>
                              <table cellspacing="0" cellpadding="0" width='100%' style='' align='' class="">
                                <tr style='' class=''>
                                  <td width='' height='' style='vertical-align:top;' align='' valign='' class='' colspan='1'>
                                    <table cellspacing="0" cellpadding="0" width='' style='' align='right' class="no_text_resize">
                                      <tr style='
    display: block;
    background-color: #000;
    border-radius: 4px;
    color: #fff !important;
    text-align: left;
    height: 22px;
    mso-line-height-rule: exactly;
    line-height: 1;
    padding: 3px 6px 3px 6px;
    margin-top: 1px;
    font-weight: 500;
    font-size: 9px;
    font-family: &#39;Myriad Pro&#39;, &#39;Helvetica Neue&#39;, Helvetica, Arial, sans-serif;
    background: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(0, 0, 0, 0.1)), to(black)), -webkit-gradient(linear, right top, left bottom, color-stop(0, rgba(255, 255, 255, 0.65)), color-stop(0.5, rgba(255, 255, 255, 0.4)), color-stop(0.501, rgba(255, 255, 255, 0.15)), color-stop(1, rgba(255, 255, 255, 0))), black;

    -moz-text-size-adjust: none;
    -webkit-text-size-adjust: none;
    -ms-text-size-adjust: none;
    text-size-adjust: none;
' class=''>
                                        <td width='20' height='22' style='' align='' valign='' class='' colspan='1'>
                                          <a style="text-decoration: none; color: #0f90ba; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " href="#" class="">
                                            <img src='{{secure_asset('images/apple-icon.png')}}' title='' alt='' style='width:14px; height:16px; ' border="0" width='14' height='16' class="" />
                                          </a>
                                        </td>
                                        <td width='' height='' style='min-width:52px;' align='' valign='' class='' colspan='1'>
                                          <a style="text-decoration: none; color: #0f90ba; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; color: #fff !important;" href="#" class=""> <span style="color: #fff !important;">Available on</span>
                                            <br /> <span style="font-size: 1.2em; color: #fff !important;">App Store</span>

                                          </a>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                  <td width='' height='' style='text-align:left; padding-left:4px;' align='' valign='' class='' colspan='1'>
                                    <table width=100% cellspacing="0" cellpadding="0" style="" class="">
                                      <tr width="100%">
                                        <td width="100%" style="float:left;" class="">
                                          <a style="text-decoration: none; color: #0f90ba; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " href="#" class="hide_for_iphone">
                                            <img src='{{secure_asset('images/en_badge_web_generic.png')}}' title='Google Play' alt='Google Play' style='widht:86px;height:30px;' border="0" width='86' height='30' class="" />
                                          </a>
                                        </td>
                                      </tr>
                                    </table>
                                    <table width=100% cellspacing="0" cellpadding="0" style="" class="">
                                      <tr width="100%">
                                        <td width="100%" style="
    overflow: hidden;
    float: left;
    display: none;
    mso-hide: all;
    font-size: 0;
    height: 0;
    line-height: 0;
    max-height: 0;
    width: 0;
    text-indent: -9999px;
    visibility: hidden;
    margin: 0;
    padding: 0;
" class="passbook">
                                          <table cellspacing="0" cellpadding="0" width='' style='
    display: inline-block;
    background-color: #f9f9f9;
    border-radius: 4px;
    border: 1px solid #e5e5e5;
    color: #77787b !important;

    padding: 3px 6px 3px 6px;
    margin-left: 4px;
    text-align: left;
    line-height: 1;
    font-size: 9px;

    background: -webkit-gradient(linear, left top, left bottom, from(#fdfdfd), to(#bfc1c2)); -webkit-linear-gradient(#fdfdfd, #bfc1c2); linear-gradient(#fdfdfd, #bfc1c2);
 mso-hide:all;' align='' class="no_text_resize">
                                            <tr style='' class=''>
                                              <td width='22' height='22' style='' align='' valign='' class='' colspan='1'>
                                                <a style="text-decoration: none; color: #0f90ba; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " href="#" class="">
                                                  <img src='{{secure_asset('images/passbook-icon.png')}}' title='' alt='' style='width:17px; height:18px; ' border="0" width='17' height='18' class="" />
                                                </a>
                                              </td>
                                              <td width='' height='' style='' align='' valign='' class='' colspan='1'>
                                                <a style="text-decoration: none; color: #0f90ba; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " href="#" class=""> <span style="color:#77787b;">Add to</span>
                                                  <br /><span style="color:#221f1f; font-weight:500; font-size:1.2em;">Wallet</span>

                                                </a>
                                              </td>
                                            </tr>
                                          </table>
                                        </td>
                                      </tr>
                                    </table>
                                  </td>
                                </tr>
                              </table>
                            </td>
                          </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" align="left" style="text-align:center; padding: 30px 0;" class="small_full_width ticket-section__or">
                          <tr>
                            <td>
                              <img src='{{secure_asset('images/lottering-divider-leftx2.png')}}' title='' alt='' style='width:26px; height:4px; ' border="0" width='26' height='4' class="" />
                              <span style='
    font-size: 22px;
    font-weight: 600;
    font-style: italic;
    color: #ccc;
    padding: 0 2px;
    vertical-align: middle;
' class=''>or</span>

                              <img src='{{secure_asset('images/lottering-divider-rightx2.png')}}' title='' alt='' style='width:26px; height:4px; ' border="0" width='26' height='4' class="" />
                            </td>
                          </tr>
                        </table>
                        <table cellpadding="0" cellspacing="0" border="0" align="right" style="width:215px; text-align:center; overflow:hidden;" class="small_full_width ">
                          <tr>
                            <td>
                                <table width=100% cellspacing="0" cellpadding="0" style="font-weight:bold; margin-bottom:4px;" class="">

                                  <tr width="100%">
                                    <td width="100%" style="text-align:center;" class="">
                                      <a style="text-decoration: none; color: #0f90ba; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " href="{{ url('tickets').'/' }}">Print Your Tickets</a>
                                      </td>
                                  </tr>
                                </table>
                              <div style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; margin-top:0;margin-bottom:0;" class=""><a href="{{ url('login') }}" style="text-decoration: none; color: #1090ba; font-weight:normal;">Log in</a> to access tickets and manage your orders.

                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td style="padding: 0 40px;">
                        <table cellspacing="0" cellpadding="0" width="100%" style="width: 100%; min-width: 100%;" class="">
                          <tr>
                            <td style="background-color: #dedede; width: 100%; min-width: 100%; font-size: 1px; height: 1px; line-height: 1px; " class="">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>

                    <tr>

                    </tr>
                    <tr>
                      <td class="grid__col" style="font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; padding: 32px 40px; background-color: #ededed;">
                        <table cellpadding="0" cellspacing="0" border="0" style="width: 100%; margin-bottom: 12px;" class="no_text_resize">
                        <tr>
                         <td colspan="2" style="text-align: right; ">
                              <div style="color: #ff650f; font-weight: 700; font-size: 14px; line-height: 30px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " class="">
                              <a href="{{ url('invoice').'/'.$order->id }}">Print Invoice</a></div>
                            </td></tr>
                          <tr>
                            <td style="border-bottom: 1px dashed #d3d3d3;">

                              <h2 style="color: #404040; font-weight: 300; margin: 0 0 12px 0; font-size: 24px; line-height: 30px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " class="">

                        Order Summary

</h2>

                            </td>
                            <td colspan="2" style="text-align: right; border-bottom: 1px dashed #d3d3d3;">
                              <div style="color: #666666; font-weight: 400; font-size: 13px; line-height: 18px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " class="">{{ Carbon\Carbon::parse($order->updated_at)->format('jS \o\f F, Y') }}</div>
                            </td>
                          </tr>
                          <tr>
                            <td colspan="3">
                              <p style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; margin-bottom: 18px;" class="">Order #: {{ $order->id }}</p>
                              <table cellpadding="0" cellspacing="0" border="0" style="width: 100%;">
                                <thead>
                                  <tr>
                                    <th style="border-bottom: 1px dashed #d3d3d3; text-align: left; padding-bottom: 12px; padding-right: 12px">
                                      <div style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; font-weight: 500; " class="">Item</div>
                                    </th>
                                    <th style="border-bottom: 1px dashed #d3d3d3; text-align: left; padding-bottom: 12px; padding-right: 12px">
                                      <div style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; font-weight: 500; " class="">Quantity</div>
                                    </th>
                                    <th style="border-bottom: 1px dashed #d3d3d3; text-align: right; padding-bottom: 12px; padding-right: 0">
                                      <div style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; font-weight: 500; " class="">Tax</div>
                                    </th>
                                    <th style="border-bottom: 1px dashed #d3d3d3; text-align: right; padding-bottom: 12px; padding-right: 0">
                                      <div style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; font-weight: 500; " class="">Sub-total</div>
                                    </th>
                                  </tr>
                                </thead>
                                <tbody>
                                @foreach ($order->items as $item)
                                  <tr>
                                    <td style=" padding: 12px 0; padding-right:3px">
                                      <div style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " class="">{{ $item->event_name }} {{ $item->entrance_type }}</div>
                                    </td>
                                    <td style=" padding: 12px 0; padding-right:3px">
                                      <div style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " class="">{{ $item->quantity }}</div>
                                    </td>
                                    <td style="text-align: right; padding: 12px 0; ">
                                      <div style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " class="">${{ $item->tax }}</div>
                                    </td>
                                    <td style="text-align: right; padding: 12px 0; ">
                                      <div style="color: #666666; font-weight: 400; font-size: 15px; line-height: 21px; font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; " class="">${{ $item->subtotal }}</div>
                                    </td>
                                  </tr>
                                @endforeach
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </table>
                        <p style="text-align: center; color: #999; margin-bottom: 0;" class="no_text_resize">This order is subject to EventstoCity
                          <a href="http://eventbrite.force.com/cust_maint/site_down/maintenance.html" style="text-decoration: none; color: #0f90ba;">Terms of Service</a>,
                          <a href="http://eventbrite.force.com/cust_maint/site_down/maintenance.html" style="text-decoration: none; color: #0f90ba;">Privacy Policy</a>, and
                          <a href="https://www.eventbrite.com/support/articleredirect?anum=7504" style="text-decoration: none; color: #0f90ba;">Cookie Policy</a>
                        </p>
                      </td>
                    </tr>
                    <tr>

                    </tr>

                    <tr>
                      <td style="padding: 0 40px;">
                        <table cellspacing="0" cellpadding="0" width="100%" style="width: 100%; min-width: 100%;" class="">
                          <tr>
                            <td style="background-color: #dedede; width: 100%; min-width: 100%; font-size: 1px; height: 1px; line-height: 1px; " class="">&nbsp;</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr class="">
                      <td class="grid__col" style="font-family: 'Benton Sans', -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica neue', Helvetica, Tahoma, Arial, sans-serif; padding: 32px 40px; border-radius:0 0 6px 6px;" align="">

                        <table cellpadding="0" cellspacing="0" border="0" align="right" style="width:340px;" class="small_full_width ">

                        </table>
                      </td>
                    </tr>

                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>


      </td>
    </tr>
  </table>
  <table align="center" bgcolor="#f4f4f4" border="0" cellpadding="0" cellspacing="0" class="container-main" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;line-height:0px;" width="600">
            <tbody>

              <tr>
                <td align="center">
                    <!--Start Events -->
                 @foreach (App\Event::where('privacy', 'public')->where('status', '!=', 'ended')->whereHas('reviews', function ($query) {
          $query->orderBy('rating', 'DESC');})->has('type_entrances')->take(6)->get()->chunk(2) as $chunks)

                    <table border="0" width="575" align="center" cellpadding="0" cellspacing="0" class="">
                   <tbody><tr>
                       @foreach ($chunks as $popular)
                       <!-- Start Second Event-->
                     <td>
                     <table border="0" width="270" align="left" cellpadding="0" cellspacing="0" bgcolor="ffffff" style="mso-table-lspace:0pt; mso-table-rspace:0pt;border-right:#d9d9d9 solid 1px;border-bottom:#d9d9d9 solid 1px;border-left:#d9d9d9 solid 1px;border-top:#d9d9d9 solid 1px;border-radius:3px;line-height:0;" class="feature">
                         <tbody><tr>
                           <td align="center">
                             <table border="0" width="100%" align="center" cellpadding="0" cellspacing="0" class="feature-middle">

                               <tbody><tr>
                                 <td align="center">
                                   <a href="{{ url('events').'/'.$popular->slug }}" style="text-decoration:none;"><img alt="{{ $popular->slug }}" border="0" class="feature-img" height="auto" src="{{ $popular->picture }}" style="font-family: proxima_nova_regular, 'Proxima Nova', Helvetica, Arial, sans-serif; color: rgb(122, 122, 122); font-size: 14pt; width: 270px; height:auto;" width="270"></a>
                                 </td>
                               </tr>
                               <tr>
                                 <td height="15"></td>
                               </tr>
                               <tr>
                                 <td>

                                   <table align="center" bgcolor="ffffff" border="0" cellpadding="0" cellspacing="0" class="feature-text" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" width="230">
                                     <tbody>
                                       <tr>
                                         <td align="center">
                                           <table align="center" bgcolor="ffffff" border="0" cellpadding="0" cellspacing="0" class="feature-middle2" width="250">
                                             <tbody>
                                               <tr>
                                                 <td align="left" style="color: #7a7a7a; font-size: 19px; font-family: 'proxima_nova_light', Proxima Nova Light, Helvetica, Arial, sans-serif;-webkit-text-size-adjust:none;line-height: 23px;">{{ $popular->name }}</td>
                                               </tr>
                                               <tr>
                                                 <td align="left" style="color:#7a7a7a;font-size: 14px;font-family: 'proxima_nova_light', Proxima Nova Light, Helvetica, Arial, sans-serif;-webkit-text-size-adjust:none;line-height:16px;">{{ $popular->location }}</td>
                                               </tr>
                                               <tr>
                                                 <td height="10"><img alt="" border="0" height="10" src="https://gozengo-emails.s3.amazonaws.com/newsletter/mod-images/spacer.png" style="border: 0px; width: 1px; height: 10px;" width="1"></td>
                                               </tr>

                                               <tr>
                                                 <td height="15"></td>
                                               </tr>
                                               <tr>
                                                 <td align="left">
                                                   <table align="left" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;line-height:0;">
                                                     <tbody>
                                                       <tr>
                                                         <td align="left" style="color: #7a7a7a; font-size: 16px; font-family: 'proxima_nova_light', Proxima Nova Light, Helvetica, Arial, sans-serif;-webkit-text-size-adjust:none;line-height: 24px;margin: 0;">from<br>
                                                           <span style="font-size: 18px; font-family: 'proxima_nova_regular', Proxima Nova Regular, Helvetica, Arial, sans-serif;-webkit-text-size-adjust:none; font-weight: 800;
   color: #7cb342;">USD${{ $popular->type_entrances->first()->price }}</span></td>
                                                       </tr>
                                                     </tbody>
                                                   </table>

                                                   <table align="right" border="0" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;line-height:0;">
                                                     <tbody>
                                                       <tr>
                                                         <td align="center">
                                                           <div>
                                                             <!--[if mso]>
 <v:roundrect xmlns:v="urn:schemas-microsoft-com:vml" xmlns:w="urn:schemas-microsoft-com:office:word" href="https://gozengo.com/hotels/38/bluebay-grand-esmeralda-all-inclusive?utm_medium=email&utm_term=1891499&amp;utm_source=Newsletter&amp;utm_medium=email&amp;utm_campaign=EM_3-31-2016_Spring_Savings&amp;utm_content=Feature_1_CTA" style="height:43px;v-text-anchor:middle;width:115px;" arcsize="10%" fillcolor="#0387bf">
   <w:anchorlock/>
   <center style="color:#ffffff;font-family: 'proxima_nova_regular', Proxima Nova, Helvetica, Arial, sans-serif;font-size:16px;font-weight:bold;">Book now</center>
 </v:roundrect>
<![endif]--><a href="{{ url('events').'/'.$popular->slug }}" style="background-color:#0387bf;border-radius:4px;color:#ffffff;display:inline-block;font-family: 'proxima_nova_regular', Proxima Nova, Helvetica, Arial, sans-serif;font-size:18px;line-height:40px;text-align:center;text-decoration:none;width:115px;-webkit-text-size-adjust:none;mso-hide:all;">Buy Now</a></div>
                                                         </td>
                                                       </tr>
                                                     </tbody>
                                                   </table>
                                                 </td>
                                               </tr>
                                             </tbody>
                                           </table>
                                         </td>
                                       </tr>
                                       <tr>
                                         <td height="15"></td>
                                       </tr>
                                     </tbody>
                                   </table>
                                 </td>
                               </tr>
                             </tbody></table>
                           </td>
                         </tr>
                       </tbody></table>
                     </td>
                       <!-- End Second Event -->
                       @endforeach
                   </tr>

                 </tbody></table>

                @endforeach
                    <!-- End Events -->
                </td>
              </tr>


              <tr>
                <td height="20"><img alt="" border="0" height="20" src="https://gozengo-emails.s3.amazonaws.com/newsletter/mod-images/spacer.png" style="border: 0px; width: 1px; height: 20px;" width="1"></td>
              </tr>

              <tr>
                <td height="15"></td>
              </tr>


            </tbody>
          </table>
                <table align="center" bgcolor="#f4f4f4" border="0" cellpadding="0" cellspacing="0" class="container-main" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;line-height:0px;" width="600">
            <tbody>

              <tr>
                <td align="center">

                </td>
              </tr>


              <tr>
                <td height="20"><img alt="" border="0" height="20" src="https://gozengo-emails.s3.amazonaws.com/newsletter/mod-images/spacer.png" style="border: 0px; width: 1px; height: 20px;" width="1"></td>
              </tr>

              <tr>
                <td height="5"></td>
              </tr>


            </tbody>
          </table>

             <table>
                 <tbody>
                     <tr style="padding:0;text-align:left;vertical-align:top">
                         <td class="m_1168913025692755088app-banner-container" style="background:#fff;border-collapse:collapse!important;color:#222;font-family:'Helvetica','Arial',sans-serif;font-size:14px;font-weight:400;line-height:19px;margin:0;padding:15px;text-align:left;vertical-align:top;word-break:break-word">
                             <table class="m_1168913025692755088app-banner-image" style="border-collapse:collapse;border-spacing:0;float:left;padding:0;text-align:left;vertical-align:top;width:97px">
                                 <tbody>
                                     <tr style="padding:0;text-align:left;vertical-align:top">
                                         <td style="border-collapse:collapse!important;color:#222;font-family:'Helvetica','Arial',sans-serif;font-size:14px;font-weight:400;line-height:19px;margin:0;padding:0;text-align:left;vertical-align:top;word-break:break-word">
                                             <img src="{{secure_asset('images/icon_app.png')}}" alt="gyg logo icon" width="97" height="97" class="CToWUd" style="clear:both;display:block;float:left;max-width:100%;outline:none;text-decoration:none;width:auto"></td>
                                     </tr>
                                 </tbody>
                             </table>
                             <table class="m_1168913025692755088app-banner-text" style="border-collapse:collapse;border-spacing:0;float:left;margin-left:20px;margin-top:5px;padding:0;text-align:left;vertical-align:top;width:430px">
<tbody>
<tr style="padding:0;text-align:left;vertical-align:top">
<td class="m_1168913025692755088app-banner-title" style="border-collapse:collapse!important;color:#ff3d00;font-family:'Helvetica','Arial',sans-serif;font-size:18px;font-weight:700;line-height:18px;margin:0;padding:3px 0 2px;text-align:left;vertical-align:top;word-break:break-word">
¡Explore all the events on your mobile!</td>
</tr>
<tr style="padding:0;text-align:left;vertical-align:top">
<td class="m_1168913025692755088app-banner-message" style="border-collapse:collapse!important;color:#606159;font-family:'Helvetica','Arial',sans-serif;font-size:14px;font-weight:400;line-height:14px;margin:0;padding:3px 0 8px;text-align:left;vertical-align:top;word-break:break-word">Take your ticket where you go and enjoy the events that you most like.</td>
</tr>
<tr class="m_1168913025692755088app-banner-links" style="padding:0;text-align:left;vertical-align:top">
  <td class="m_1168913025692755088app-banner-links-wrapper" style="border-collapse:collapse!important;color:#222;font-family:'Helvetica','Arial',sans-serif;font-size:14px;font-weight:400;line-height:19px;margin:0;padding:0;text-align:left;vertical-align:top;word-break:break-word">
  <a href="https://play.google.com/store/apps/details?id=events.to.city" style="color:#2ba6cb;display:block;float:left;height:45px;text-decoration:none;width:150px" target="_blank">
    <img src="{{secure_asset('images/561fb21ee1aca.png')}}" style="border:none;clear:both;display:block;float:left;height:40px;max-width:100%;outline:none;text-decoration:none;width:128px" class="CToWUd"></a>
    <a href="https://itunes.apple.com/us/app/events-to-city/id1366304944?l=es&ls=1&mt=8" style="color:#2ba6cb;display:block;float:left;height:45px;text-decoration:none;width:150px" target="_blank">
      <img src="{{secure_asset('images/56a27ced1967e.png')}}" style="border:none;clear:both;display:block;float:left;height:40px;max-width:100%;outline:none;text-decoration:none;width:128px" class="CToWUd"></a>
  </td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
            </td>
          </tr>
          <tr>
            <td bgcolor="#F7F7F7" style="max-width:600px; margin:0 auto; padding:20px 33px 20px 37px; display:block;">
              <table cellspacing="0" cellpadding="10" width="100%">
                <tr>
                  <td colspan="3" height="18" style="font-size:1px; line-height:1px;">&nbsp;</td>
                </tr>
                <tr>
                  <td align="left" colspan="3">
                    <table cellpadding="0" cellspacing="0" width="100%">
                      <tr>
                        <td>
                          <table>
                            <tr>
                              <td>
                                <div style="float:left; margin:5px;">
                                  <a href="https://www.facebook.com/jet" title="Like us on Facebook" target="new">
                                    <img src="{{secure_asset('images/3f540d56-178e-4d73-83e5-8ccaaf70f2cd.png')}}" width="29" height="29" border="0" alt="Like us on Facebook">
                                  </a>
                                </div>
                              </td>
                              <td>
                                <div style="float:left; margin:5px;">
                                  <a href="https://twitter.com/jet" title="Jet on Twitter" target="new">
                                    <img src="{{secure_asset('images/e78ba6bd-5b0f-4d69-8fcf-acc064cbf7ea.png')}}" width="29" height="29" border="0" alt="Jet on Twitter">
                                  </a>
                                </div>
                              </td>
                              <td>
                                <div style="float:left; margin:5px;"></div>
                              </td>
                              <td>
                                <div style="float:left; margin:5px;"></div>
                              </td>
                            </tr>
                          </table>
                        </td>
                        <td align="right" style="text-align:right;">
                          <a href="mailto:help@jet.com" style="text-decoration: none; font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;" title="help@jet.com">help@eventstocity.com</a>                          <span style="text-decoration: none; font-size:9px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;">|</span>

                          <a style="text-decoration: none; font-size:10px; font-weight:bold; font-family: 'Open Sans','Helvetica Neue', 'Helvetica',Calibri, Arial, sans-serif;color: #414141;" href="tel:18555384323">1 (855) (538 4323</a>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td colspan="3" height="18" style="font-size:1px; line-height:1px;">&nbsp;</td>
                </tr>
                <tr>
                  <td style="font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141; text-align:left;" align="left">
                    <a href="https://jet.com/privacy-policy" style="text-decoration:none;font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141;">Privacy Policy</a>
                  </td>
                  <td colspan="2" style="font-size:10px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #414141; text-align:right;" align="right">221 River Street, 8th Floor, Hoboken, NJ 07030 &copy; 2016 Jet</td>
                </tr>
              </table>
            </td>
          </tr>
          <tr>
            <td style="height:25px;">
              <img width="600" src="{{secure_asset('images/4c1b3727-e048-4e80-815b-a9197acc62fe.png')}}">
            </td>
          </tr>
          <tr>
            <td>
              <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                  <td width="25"></td>

                  <td style="font-size:8px; line-height:12px; font-weight:normal; font-family: 'OpenSans', helvetica, sans-serif; color: #535352;">&nbsp;</td>
                  <td width="25"></td>
                </tr>
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                  <td colspan="3">&nbsp;</td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <!-- /content -->
      </td>
      <td></td>
    </tr>
  </table>
  <!-- /body -->
</body>

</html>
