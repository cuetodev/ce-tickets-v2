@component('mail::message')
# Invoice Order #{{ $order->id }} Paid.

@component('mail::table')
| Item          | Quantity      | Tax      | SubTotal  |
| ------------- | ------------- | -------- | --------- |
@foreach ($order->items as $item)
  | {{ $item->event_name }} {{ $item->entrance_type }} | {{ $item->quantity }} | ${{ $item->tax }} | ${{ $item->subtotal }} |
@endforeach
@endcomponent

@component('mail::button', ['url' => url('invoice').'/'.$order->id])
Print Invoice
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
