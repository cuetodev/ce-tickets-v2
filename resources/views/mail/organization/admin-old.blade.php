@component('mail::message')
# A Organization Request needs your attention.

A user has made an request to have an Organization, please check it!

Thanks,<br>
{{ config('app.name') }}
@endcomponent
