<!-- Sidebar
================================================== -->
<div class="col-lg-3 col-md-4">
  <div class="sidebar">
    <form action="{{url('search')}}" method="GET">
      <!-- Widget -->
      <div class="widget margin-bottom-40">
        <h3 class="margin-top-0 margin-bottom-30">@lang("Filters")</h3>

        <!-- Row -->
        <div class="row with-forms">
          <!-- Cities -->
          <div class="col-md-12">
            <input type="text" name="keyword"  placeholder="@lang("What are you looking for?")" value="{{ request('keyword') }}"/>
          </div>
        </div>
        <!-- Row / End -->

        <!-- Row -->
        <div class="row with-forms">
          <!-- Date -->
          <div class="col-md-12">
            <select data-placeholder="All Dates" class="chosen-select" name="date" >
              <option {{(request('date') == "all day" ? 'selected' : '')}} value="all day">@lang("All Dates")</option>
              <option {{(request('date') == "today" ? 'selected' : '')}} value="today">@lang("Today")</option>
              <option {{(request('date') == "tomorrow" ? 'selected' : '')}} value="tomorrow">@lang("Tomorrow")</option>
              <option {{(request('date') == "this-week" ? 'selected' : '')}} value="this-week">@lang("This Week")</option>
              <option {{(request('date') == "next-week" ? 'selected' : '')}} value="next-week">@lang("Next Week")</option>
              <option {{(request('date') == "this-month" ? 'selected' : '')}} value="this-month">@lang("This Month")</option>
              <option {{(request('date') == "next-month" ? 'selected' : '')}} value="next-month">@lang("Next Month")</option>
            </select>
          </div>
        </div>
        <!-- Row / End -->

        <!-- Row -->
        <div class="row with-forms">
          <!-- Type -->
          <div class="col-md-12">
            <select data-placeholder="All Categories" class="chosen-select" name="category">
              <option value="">@lang("All Categories")</option>
              @foreach (\App\Category::all() as $category)
                <option {{(request('category') == $category->name ? 'selected' : '')}} value="{{ $category->name }}">{{ $category->name_translated[LaravelLocalization::getCurrentLocale()] }}</option>
              @endforeach
            </select>
          </div>
        </div>
        <!-- Row / End -->

        <div class="row with-forms">
          <!-- Type -->
          <div class="col-md-12">
            <select data-placeholder="All Stars" class="chosen-select" name="stars">
              <option value="">@lang("All Stars")</option>
              <option value="1">1 @lang("star")</option>
              <option value="2">2 @lang("stars")</option>
              <option value="3">3 @lang("stars")</option>
              <option value="4">4 @lang("stars")</option>
              <option value="5">5 @lang("stars")</option>
            </select>
          </div>
        </div>


        <!-- Row -->
        <div class="row with-forms">
          <!-- Cities -->
          <div class="col-md-12">

            <div class="input-with-icon location">
              <input type="text" placeholder="@lang("Destination, city, address")" name="location" value="{{ request('location') }}"/>
              {{-- <a href="#"><i class="fa fa-dot-circle-o"></i></a> --}}
            </div>

          </div>
        </div>
        <!-- Row / End -->
        <br>

        <!-- Price Range -->
        <div class="range-slider2">
          <input class="price-radius" name="price" type="range" min="1" max="10000" step="1" value="{{ (request()->has('price') ? request('price') : 1 ) }}" data-title="@lang("Select the price range")">
        </div>

        {{-- <div class="row with-forms">
          <div class="col-md-12">
            <span>Rating</span>
            <div class="">
              <label>
                <input type="radio" name="stars" value="1" style="display: inline">
                <span>
                  <i class="sl sl-icon-star"></i>
                </span>
              </label>

              <label>
                <input type="radio" name="stars" value="2" style="display: inline">
                <span>
                  <i class="sl sl-icon-star"></i>
                  <i class="sl sl-icon-star"></i>
                </span>
              </label>

              <label>
                <input type="radio" name="stars" value="3" style="display: inline">
                <span>
                  <i class="sl sl-icon-star"></i>
                  <i class="sl sl-icon-star"></i>
                  <i class="sl sl-icon-star"></i>
                </span>
              </label>

              <label>
                <input type="radio" name="stars" value="4" style="display: inline">
                <span>
                  <i class="sl sl-icon-star"></i>
                  <i class="sl sl-icon-star"></i>
                  <i class="sl sl-icon-star"></i>
                  <i class="sl sl-icon-star"></i>
                </span>
              </label>

              <label>
                <input type="radio" name="stars" value="5" style="display: inline">
                <span>
                  <i class="sl sl-icon-star"></i>
                  <i class="sl sl-icon-star"></i>
                  <i class="sl sl-icon-star"></i>
                  <i class="sl sl-icon-star"></i>
                  <i class="sl sl-icon-star"></i>
                </span>
              </label>
            </div>
          </div>

        </div> --}}

        <button class="button fullwidth margin-top-25">@lang("Update")</button>

      </div>
      <!-- Widget / End -->
    </form>
  </div>
</div>
<!-- Sidebar / End -->
