@extends('layouts.app')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>{{ $category->name_translated[LaravelLocalization::getCurrentLocale()] }}</h2><span>@lang("Find the :category attractions of your city", ['category' => $category->name_translated[LaravelLocalization::getCurrentLocale()]])</span>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="{{ url('/') }}">@lang("Home")</a></li>
            <li><a href="{{ url('categories') }}">@lang("Categories")</a></li>
						<li>{{ $category->name_translated[LaravelLocalization::getCurrentLocale()] }}</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->


<div class="container">

	<div class="row">

		<div class="col-lg-9 col-md-8 padding-right-30">

			<!-- Sorting / Layout Switcher -->
			<div class="row margin-bottom-25">

				<div class="col-md-6 col-xs-6">
					<!-- Layout Switcher -->
					<div class="layout-switcher">
						<a href="#" class="grid active"><i class="fa fa-th"></i></a>
						{{-- <a href="category-list.html" class="list"><i class="fa fa-align-justify"></i></a> --}}
					</div>
				</div>

			</div>
			<!-- Sorting / Layout Switcher / End -->


			<div class="row">

				@forelse ( $category->events->where('privacy', 'public')->where('status', '!=', 'ended') as $events)
          <!-- Listing Item -->
  				<div class="col-lg-6 col-md-12">
  					<a href="{{ url('events') }}/{{ $events->slug }}" class="listing-item-container">
  						<div class="listing-item">
  							<img src="{{ $events->picture }}" alt="">

								@if ($events->status == 'sold-out')
                  <div class="listing-badge now-closed">@lang("Sold Out!")</div>
                @endif

  							<div class="listing-item-details">
  								<ul>
  									<li>{{ $events->start_date->toFormattedDateString() }}</li>
  								</ul>
  							</div>
  							<div class="listing-item-content">
  								<span class="tag">{{ $events->category->name_translated[LaravelLocalization::getCurrentLocale()] }}</span>
  								<h3>{{ $events->name }}</h3>
  								<span>{{ $events->location }}</span>
  							</div>
  							{{-- <span class="like-icon"></span> --}}
  						</div>
  						<div class="star-rating" data-rating="{{ $events->averageReview() }}">
  							<div class="rating-counter">({{ $events->reviews->count() }} @lang("reviews"))</div>
  						</div>
  					</a>
  				</div>
  				<!-- Listing Item / End -->
				@empty
          <p>@lang("There are not Events for this category")</p>
				@endforelse

			</div>

			<!-- Pagination -->
			<div class="clearfix"></div>

      {{-- {{ $category->links('pagination.page-event') }} --}}
			<!-- Pagination / End -->

		</div>

    @include('filter-sidebar')

	</div>
</div>
@endsection
