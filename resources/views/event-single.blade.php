@extends('layouts.app')

@push('meta-tags-image')
  <meta property="og:title" content="{{ $event->name }}" />
  <meta name="og:description" content="{{ str_limit($event->description, 50) }}">
@endpush


@section('content')
  <!-- Slider
  ================================================== -->
  <div class="event-main-container" style="background-image: url({{ asset( $event->picture ) }}); background-color: #f9f9f9; background-position: center; background-size: cover; height: 60vh;">
  	{{-- <a href="{{ asset( $event->picture ) }}"  class="item mfp-gallery" title="Title 1"></a> --}}
  </div>

  {{-- <div style="text-align: center; width: 100%;">
    <img src="{{ asset($event->picture) }}" alt=""/>
  </div> --}}


  <!-- Content
  ================================================== -->
  <div class="container">
  	<div class="row sticky-wrapper">
  		<div class="col-lg-8 col-md-8 padding-right-30">

  			<!-- Titlebar -->
  			<div id="titlebar" class="listing-titlebar">
  				<div class="listing-titlebar-title">
  					<h2>{{ $event->name }} <span class="listing-tag">
              {{ $event->category->name_translated[LaravelLocalization::getCurrentLocale()] }}
            </span></h2>
  					<div>
              <span>
  							<i class="fa fa-calendar"></i>
  							{{ $event->start_date->toFormattedDateString() }} - {{ $event->end_date->toFormattedDateString() }} |
  							<i class="fa fa-clock-o"></i> {{ date('h:m A', strtotime($event->start_time) ) }} - {{ date('h:m A', strtotime($event->end_time) ) }}
              </span>
  					</div>
  					<span>
  						<a href="#listing-location" class="listing-address">
  							<i class="fa fa-map-marker"></i>
  							{{ $event->location }}
  						</a>
  					</span>

            <!-- Share Buttons -->
  					<ul class="share-buttons">
  						<li><a class="fb-share" href="http://www.facebook.com/share.php?u={{ urlencode(url('events/').'/'.$event->slug) }}"><i class="fa fa-facebook"></i> @lang("Share")</a></li>

  						<li><a class="twitter-share" href="https://twitter.com/intent/tweet?url={{ urlencode(url('events/').'/'.$event->slug) }}"><i class="fa fa-twitter"></i> Tweet</a></li>
  						<li><a class="gplus-share" href="https://plus.google.com/share?url={{ urlencode(url('events/').'/'.$event->slug) }}"><i class="fa fa-google-plus"></i> @lang("Share")</a></li>
  						<!-- <li><a class="pinterest-share" href="#"><i class="fa fa-pinterest-p"></i> Pin</a></li> -->
  					</ul>

  					<div class="star-rating" data-rating="{{ $event->averageReview() }}">
  						<div class="rating-counter"><a href="#listing-reviews">({{ $event->reviews()->count() }} @lang("reviews"))</a></div>
  					</div>
  				</div>
  			</div>

  			<!-- Listing Nav -->
  			<div id="listing-nav" class="listing-nav-container">
  				<ul class="listing-nav">
  					<li><a href="#listing-overview" class="active">@lang("Overview")</a></li>
            <li class="hidden-md hidden-lg"><a href="#buy-tickets">@lang("Buy Tickets")</a></li>
            @if (!is_null($event->stage_map))
              <li class="hidden-md hidden-lg"><a href="#stage-map">@lang("Stage")</a></li>
            @endif
  					<li><a href="#listing-location">@lang("Location")</a></li>
            {{-- <li><a href="#listing-reviews">Reviews</a></li> --}}
  				</ul>
  			</div>

  			<!-- Overview -->
  			<div id="listing-overview" class="listing-section">

  				<!-- Description -->

  				<p>
            {!! $event->description !!}
          </p>

  			</div>

        <!-- Buy Tickets -->

  			<div id="buy-tickets" class="listing-section hidden-md hidden-lg">
  				<h3 class="listing-desc-headline margin-top-60 margin-bottom-30">@lang("Buy Tickets")</h3>
          <div class="">
            <form class="form-inline" action="/cart/add/event/{{ $event->id }}/m" method="POST">
              {{ csrf_field() }}
            <table class="table table-striped table-dark">
              <tbody>
                @foreach ($event->type_entrances as $key => $entrance)
                  <tr>
                    <td>{{ $entrance->name }}</td>
                    <td>USD$ {{ $entrance->price }}</td>
                    <td>
                      <input type="hidden" name="ticket[{{$key}}][id_type]" value="{{ $entrance->id }}">
                      <div class="form-group" style="display: inline-flex;"><input type="number" class="form-control" id="quantity_tickets" name="ticket[{{$key}}][quantity]" min="0" max="{{ $entrance->quantity_availables }}" value="0"> <span> | {{ $entrance->quantity_availables }}</span></div>
                    </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <div class="text-center"><button type="submit" class="button"><i class="sl sl-icon-bag"></i> @lang("add to cart")</button></div>
            </form>
          </div>
  			</div>

        @if (!is_null($event->stage_map))
          <!-- Stage Maps -->
    			<div id="stage-map" class="listing-section hidden-md hidden-lg">
    				<h3 class="listing-desc-headline margin-top-60 margin-bottom-30">@lang("Stage")</h3>
            <p>
              <img style="width: 100%;" src="{{ $event->stage_map }}" alt="{{ $event->stage_map }}">
            </p>
    			</div>
        @endif

  			<!-- Location -->
  			<div id="listing-location" class="listing-section">
  				<h3 class="listing-desc-headline margin-top-60 margin-bottom-30">@lang("Location")</h3>

  				<div id="singleListingMap-container">
  					<div id="singleListingMap" data-latitude="{{ $event->latitude }}" data-longitude="{{ $event->longitude }}" data-map-icon="im im-icon-pin"></div>
  					{{-- <a href="#" id="streetView">Street View</a> --}}
  				</div>
  			</div>

  			<!-- Reviews -->
  			{{-- <div id="listing-reviews" class="listing-section">
  				<h3 class="listing-desc-headline margin-top-75 margin-bottom-20">Reviews <span>({{ $event->reviews()->count() }})</span></h3>

  				<div class="clearfix"></div> --}}
{{--
  				<!-- Reviews -->
  				@if ($event->reviews()->first())
            <section class="comments listing-reviews" style="overflow-y: scroll; max-height: 40em;">

    					<ul>
                @foreach ($event->reviews as $review)
                  <li>
      							<div class="avatar">
                      <img src="{{ !is_null('$review->user()->first()->avatar') ? $review->user()->first()->avatar : 'http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;s=70' }}" alt="" />
                    </div>

      							<div class="comment-content"><div class="arrow-comment"></div>
      								<div class="comment-by">{{ $review->user()->first()->first_name }}<span class="date">{{ $review->updated_at->diffForHumans() }}</span>
      									<div class="star-rating" data-rating="{{ $review->rating }}"></div>
      								</div>
      								{{-- <p>{{ $review->comment }}</p> --}}

      								{{-- <div class="review-images mfp-gallery-container">
      									<a href="{{ $review->picture }}" class="mfp-gallery"><img src="{{ $review->picture }}" alt=""></a>
      								</div> --}}
      								{{-- <a href="#" class="rate-review"><i class="sl sl-icon-like"></i> Helpful Review <span>12</span></a> --}}
      							{{-- </div>
      						</li>
                @endforeach
    					 </ul>
    				</section>
  				@endif  --}}

  				<!-- Pagination -->
  				<div class="clearfix"></div>
  				<!-- Pagination / End -->
  			{{-- </div> --}}


  			<!-- Add Review Box -->
  			<div id="add-review" class="add-review-box hidden-lg hidden-md">
          <form id="add-comment" class="add-comment" method="POST" action="/events/{{ $event->id }}/reviews" enctype="multipart/form-data">

            {{ csrf_field() }}

            <!-- Add Review -->
            <h3 class="listing-desc-headline margin-bottom-20">@lang("Add Review")</h3>

            <span class="leave-rating-title">@lang("Your rating for this event")</span>

            <!-- Rating / Upload Button -->
            <div class="row">
              <div class="col-md-8">
                <!-- Leave Rating -->
                <div class="clearfix"></div>
                <div class="leave-rating margin-bottom-30">
                  <input type="radio" name="rating" id="rating-5" value="5"/>
                  <label for="rating-5" class="fa fa-star"></label>
                  <input type="radio" name="rating" id="rating-4" value="4"/>
                  <label for="rating-4" class="fa fa-star"></label>
                  <input type="radio" name="rating" id="rating-3" value="3"/>
                  <label for="rating-3" class="fa fa-star"></label>
                  <input type="radio" name="rating" id="rating-2" value="2"/>
                  <label for="rating-2" class="fa fa-star"></label>
                  <input type="radio" name="rating" id="rating-1" value="1"/>
                  <label for="rating-1" class="fa fa-star"></label>
                </div>
                <div class="clearfix"></div>
              </div>


              {{-- <div class="col-md-6">
                <!-- Uplaod Photos -->
                <div class="add-review-photos margin-bottom-30">
                  <div class="photoUpload">
                    <span><i class="sl sl-icon-arrow-up-circle"></i> Upload Photos</span>
  							    <input type="file" class="upload" name="picture"/>
  							</div>
  						</div>
  					</div> --}}
  				</div>

  				<!-- Review Comment -->
  					{{-- <fieldset>

  						<div>
  							<label>Review:</label>
  							<textarea cols="40" rows="3" name="comment"></textarea>
  						</div>

  					</fieldset> --}}

  					<button class="button" type="submit">@lang("Submit Review")</button>
  					<div class="clearfix"></div>
  				</form>

  			</div>
  			<!-- Add Review Box / End -->

  		</div>

  		<!-- Sidebar
  		================================================== -->
  		<div class="col-lg-4 col-md-4 margin-top-75 sticky">

        @if ($event->status == 'sold-out')
          <div class="notification error" role="alert">@lang("Sold Out!")</div>
        @else
          <a class="popup-modal button  hidden-xs hidden-sm fullwidth" href="#buy-ticket">@lang("Buy Tickets")</a>

          <div id="buy-ticket" class="white-popup-block mfp-hide" style="padding: 20px; background-color: #FFF; border-radius: 5px;">

            <h1><a class="popup-modal-dismiss" href="#"><i class="sl sl-icon-close"></i></a> @lang("Buy Tickets")</h1>

            <br><br><br>

            <div style="background-color: #fff">
                <form class="form-inline" action="/cart/add/event/{{ $event->id }}/m" method="POST">
                  {{ csrf_field() }}
                <table class="table table-striped table-dark">
                  <tbody>
                    @foreach ($event->type_entrances as $key => $entrance)
                      <tr>
                        <td>{{ $entrance->name }}</td>
                        <td>USD$ {{ $entrance->price }}</td>
                        <td>
                          <input type="hidden" name="ticket[{{$key}}][id_type]" value="{{ $entrance->id }}">
                          <div class="form-group" style="display: inline-flex;"><input type="number" class="form-control" id="quantity_tickets" name="ticket[{{$key}}][quantity]" min="0" max="{{ $entrance->quantity_availables }}" value="0"> <span> | {{ $entrance->quantity_availables }}</span></div>
                        </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="text-center"><button type="submit" class="button"><i class="sl sl-icon-bag"></i> @lang("add to cart")</button></div>
                </form>
            </div>
            </div>
        @endif

        <div class="boxed-widget hidden-xs hidden-sm">

  					<div class="box_style_1 expose">
  						{{-- <h3 class="inner">- Buy Tickets -</h3> --}}
              @if ($event->type_entrances()->first())
                {{-- <form class="" action="/cart/add/event/{{ $event->id }}" method="POST">
                  {{ csrf_field() }}
                  <div class="form-group">
                    <label for="select-ticket">Select A Ticket</label>
                    <select class="form-control" name="id_type" required>
                      @foreach ($event->type_entrances as $entrance)
                        <option value="{{ $entrance->id }}">{{ $entrance->name . ' - ' . 'USD$' . $entrance->price }}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="quantity_tickets">Quantity</label>
                    <input type="number" class="form-control" id="quantity_tickets" name="quantity" min="1" value="1" required>
                    Only left {{ $event->type_entrances()->sum('quantity_availables') }} tickets in total.
                  </div>

                  <button type="submit" class="button fullwidth margin-top-5"><span>Add To Cart</span></button>

                </form> --}}

                <h3 class="inner">@lang("Stage")</h3>
                <p>
                  <a class="image-popup-vertical-fit" href="{{ $event->stage_map }}"" title="{{ $event->stage_map }}"">
                    <img src="{{ $event->stage_map }}"" style="width: 100%;">
                  </a>
                </p>
				<p style="text-align: center; font-size: 13px;  color: #ff5f06;"> Click to zoom </p>
              @else
                <p style="text-align: center;">@lang("We are Sorry nobody can buy tickets for this events. Report this problem to the administrator.")</p>
              @endif

  					</div>
  					<!--/box_style_1 -->

  				</div>
          <br>

        {{-- <div class="boxed-widget">

					<div class="box_style_1 expose">
						<h3 class="inner">- On the Cart -</h3>

						<table class="table table_summary">
							<tbody>
								@foreach ( \Cart::content() as $item)
                  <tr>
  									<td>
  										{{ $item->name }} - {{ $item->options->type_ticket['name'] }}
  									</td>
  									<td class="text-right">
  										{{ $item->qty }}x ${{ $item->price }}
  									</td>
  								</tr>
								@endforeach
								<tr>
									<td>
										Total amount
									</td>
									<td class="text-right">
										${{ \Cart::subtotal() }}
									</td>
								</tr>
								<tr class="total">
									<td>
										Total cost
									</td>
									<td class="text-right">
										${{ \Cart::total() }}
									</td>
								</tr>
							</tbody>
						</table>
						<a href="{{ url('cart') }}" class="button fullwidth margin-top-5"><span>Go to Cart</span></a>

					</div>
					<!--/box_style_1 -->


				</div> --}}

  			<!-- Contact -->
  			<div class="boxed-widget margin-top-35 hidden-xs hidden-sm">
  				<h3><i class="sl sl-icon-pin"></i> @lang("Organizer")</h3>
          <p class="text-center"><img src="{{ $event->organization->picture }}" alt="" class="img-circle" width="100"/></p>
                <h4 class="text-center">
                  <strong>{{ $event->organization->name }}</strong>
                </h4>
  				<ul class="listing-details-sidebar">
  					<li><i class="sl sl-icon-phone"></i> {{ $event->organization->phone }}</li>
  					<li><i class="sl sl-icon-globe"></i> <a href="{{ $event->organization->website }}">{{ $event->organization->website }}</a></li>
  					<li><i class="fa fa-envelope-o"></i> <a href="mailto:{{ $event->organization->email }}">{{ $event->organization->email }}</a></li>
  				</ul>

  				<ul class="listing-details-sidebar social-profiles">
  					<li><a href="{{ $event->organization->facebook_link }}" class="facebook-profile"><i class="fa fa-facebook-square"></i> Facebook</a></li>
  					<li><a href="{{ $event->organization->twitter_link }}" class="twitter-profile"><i class="fa fa-twitter"></i> Twitter</a></li>
            <li><a href="{{ $event->organization->instagram_link }}" class="instagram-profile"><i class="fa fa-instagram"></i>Instagram</a></li>
  					<!-- <li><a href="#" class="gplus-profile"><i class="fa fa-google-plus"></i> Google Plus</a></li> -->
  				</ul>
  			</div>
  			<!-- Contact / End-->

  			{{-- <!-- Share / Like -->
  			<div class="listing-share margin-top-40 margin-bottom-40 no-border"> --}}
  				{{-- <button class="like-button"><span class="like-icon"></span> Bookmark this listing</button>
  				<span>159 people bookmarked this place</span> --}}

  					<!-- Share Buttons -->
  					{{-- <ul class="share-buttons margin-top-40 margin-bottom-0">
  						<li><a class="fb-share" href="http://www.facebook.com/share.php?u={{ urlencode(url('events/').'/'.$event->slug) }}"><i class="fa fa-facebook"></i> Share</a></li>

  						<li><a class="twitter-share" href="https://twitter.com/intent/tweet?url={{ urlencode(url('events/').'/'.$event->slug) }}"><i class="fa fa-twitter"></i> Tweet</a></li>
  						<li><a class="gplus-share" href="https://plus.google.com/share?url={{ urlencode(url('events/').'/'.$event->slug) }}"><i class="fa fa-google-plus"></i> Share</a></li>
  						<!-- <li><a class="pinterest-share" href="#"><i class="fa fa-pinterest-p"></i> Pin</a></li> -->
  					</ul>
  					<div class="clearfix"></div>
  			</div> --}}

        <!-- Add Review Box -->
  			<div id="add-review" class="add-review-box hidden-xs hidden-sm">
          <form id="add-comment" class="add-comment" method="POST" action="/events/{{ $event->id }}/reviews" enctype="multipart/form-data">

            {{ csrf_field() }}

            <!-- Add Review -->
            <h3 class="listing-desc-headline margin-bottom-10">@lang("Add Review")</h3>

            <span class="leave-rating-title">@lang("Your rating for this event")</span>

            <!-- Rating / Upload Button -->
            <div class="row">
              <div class="col-md-12">
                <!-- Leave Rating -->
                <div class="clearfix"></div>
                <div class="leave-rating margin-bottom-30">
                  <input type="radio" name="rating" id="rating-5-5" value="5"/>
                  <label for="rating-5-5" class="fa fa-star"></label>
                  <input type="radio" name="rating" id="rating-4-4" value="4"/>
                  <label for="rating-4-4" class="fa fa-star"></label>
                  <input type="radio" name="rating" id="rating-3-3" value="3"/>
                  <label for="rating-3-3" class="fa fa-star"></label>
                  <input type="radio" name="rating" id="rating-2-2" value="2"/>
                  <label for="rating-2-2" class="fa fa-star"></label>
                  <input type="radio" name="rating" id="rating-1-1" value="1"/>
                  <label for="rating-1-1" class="fa fa-star"></label>
                </div>
                <div class="clearfix"></div>
              </div>


              {{-- <div class="col-md-6">
                <!-- Uplaod Photos -->
                <div class="add-review-photos margin-bottom-30">
                  <div class="photoUpload">
                    <span><i class="sl sl-icon-arrow-up-circle"></i> Upload Photos</span>
  							    <input type="file" class="upload" name="picture"/>
  							</div>
  						</div>
  					</div> --}}
  				</div>

  				<!-- Review Comment -->
  					{{-- <fieldset>

  						<div>
  							<label>Review:</label>
  							<textarea cols="40" rows="3" name="comment"></textarea>
  						</div>

  					</fieldset> --}}

  					<button class="button" type="submit">@lang("Submit Review")</button>
  					<div class="clearfix"></div>
  				</form>

  			</div>
  			<!-- Add Review Box / End -->

  		</div>
  		<!-- Sidebar / End -->

      <div class="col-md-12">
        <h3 class="listing-desc-headline margin-top-60 margin-bottom-30" style="text-align: center;">@lang("Other Events You May Like")</h3>
        <div class="simple-slick-carousel dots-nav">

          @foreach (App\Event::where('privacy', 'public')->where('status', '!=', 'ended')->whereHas('reviews', function ($query) {
          $query->orderBy('rating', 'DESC');})->has('type_entrances')->take(12)->get()->chunk(2) as $chunks)
            <!-- Listing Item -->
            <div class="carousel-item">
              @foreach ($chunks as $popular)
                <a href="/events/{{ $popular->slug }}" class="listing-item-container">
                  <div class="listing-item">
                    <img src="{{ $popular->picture }}" alt="">

                    @if ($popular->status == 'sold-out')
                      <div class="listing-badge now-closed">@lang("Sold Out!")</div>
                    @endif

                    <div class="listing-item-content">
                      <span class="tag">{{ $popular->category->name_translated[LaravelLocalization::getCurrentLocale()] }}</span>
                      <h3>{{ $popular->name }}</h3>
                      <span>{{ $popular->location }}</span>
                    </div>
                    {{-- <span class="like-icon"></span> --}}
                  </div>
                  <div class="star-rating" data-rating="{{ $popular->averageReview() }}">
                    <div class="rating-counter">({{ $popular->reviews->count() }} @lang("reviews"))</div>
                    <div class="price">USD${{ $popular->type_entrances->first()->price }}</div>
                  </div>
                </a>
              @endforeach
            </div>
            <!-- Listing Item / End -->
          @endforeach
        </div>

      </div>

  	</div>
  </div>
@endsection
