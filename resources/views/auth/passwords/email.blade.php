@extends('layouts.app')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h2>@lang("Reset Password")</h2>

      </div>
    </div>
  </div>
</div>

<div class="container margin-bottom-100">
  <div class="row">
    <div class="col-md-6 col-md-offset-3 margin-bottom-90">
      @if (session('status'))
        <div class="alert alert-success">
          {{ session('status') }}
        </div>
      @endif

      <form class="form-vertical" method="POST" action="{{ route('password.email') }} ">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <div class="col-md-12">
            <label for="email" class="control-label">@lang("E-Mail Address")</label>
            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

            @if ($errors->has('email'))
              <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
              </span>
            @endif
          </div>
        </div>

        <div class="form-group">
            <div class="col-md-12">
                <button type="submit" class="button fullwidth">
                    @lang("Send Password Reset Link")
                </button>
            </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
