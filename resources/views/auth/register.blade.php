@extends('layouts.app')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h2>@lang("Register")</h2>

      </div>
    </div>
  </div>
</div>

<div class="container margin-bottom-35">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">

      <div class="row margin-bottom-20">
        <div class="col-md-12">
          <div class="form-row">
            <p>@lang("Connect with:")</p>

            <ul class="social-icons rounded">
              {{-- <li><a class="twitter" href="/auth/twitter"><i class="icon-twitter"></i></a></li> --}}
              <li><a class="facebook" href="/auth/facebook"><i class="icon-facebook"></i></a></li>
              <li><a class="gplus" href="/auth/google"><i class="icon-gplus"></i></a></li>
            </ul>
          </div>
        </div>
      </div>

      <form class="" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}

        <div class="row">
          <div class="col-md-6">
            <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
              <label for="first_name" class="control-label">@lang("First Name")</label>
              <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>

              @if ($errors->has('first_name'))
                <span class="help-block">
                  <strong>{{ $errors->first('first_name') }}</strong>
                </span>
              @endif
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
              <label for="last_name" class="control-label">@lang("Last Name")</label>
              <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>

              @if ($errors->has('last_name'))
                <span class="help-block">
                  <strong>{{ $errors->first('last_name') }}</strong>
                </span>
              @endif
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
              <label for="username" class="control-label">@lang("Username")</label>
              <input id="username" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

              @if ($errors->has('username'))
                <span class="help-block">
                  <strong>{{ $errors->first('username') }}</strong>
                </span>
              @endif
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email" class="control-label">@lang("E-Mail Address")</label>
              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

              @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
              @endif
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <label for="phone_number" class="control-label">@lang("Phone Number")</label>
              <input id="phone_number" type="tel" class="form-control" name="phone_number" value="{{ old('phone_number') }}">
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="password" class="control-label">@lang("Password")</label>
              <input id="password" type="password" class="form-control" name="password" required>
              @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
              @endif
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="password-confirm" class="control-label">@lang("Confirm Password")</label>
              <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="gender" class="control-label">@lang("Gender")</label>
              <select id="gender" name="gender" class="form-control chosen-select">
                <option value="male">@lang("Male")</option>
                <option value="female">@lang("Female")</option>
              </select>
            </div>
          </div>

          <div class="col-md-6">
            <div class="form-group">
              <label for="birth_date" class="control-label">@lang("Birth Date")</label>
              <input id="birth_date" type="date" name="birth_date" required>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <button type="submit" class="button fullwidth">@lang("Register")</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
