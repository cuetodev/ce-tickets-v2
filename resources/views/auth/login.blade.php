@extends('layouts.app')

@section('content')

<!-- Titlebar
================================================== -->
<div id="titlebar" class="gradient">
  <div class="container">
    <div class="row">
      <div class="col-md-12">

        <h2>@lang("Login")</h2>

      </div>
    </div>
  </div>
</div>

<div class="container margin-bottom-35">
  <div class="row">
    <div class="col-md-6 col-md-offset-3">
      <div class="row margin-bottom-20">
        <div class="col-md-12">
          <div class="form-row">
            <p>@lang("Connect with:")</p>

            <ul class="social-icons rounded">
              {{-- <li><a class="twitter" href="/auth/twitter"><i class="icon-twitter"></i></a></li> --}}
              <li><a class="facebook" href="/auth/facebook"><i class="icon-facebook"></i></a></li>
              <li><a class="gplus" href="/auth/google"><i class="icon-gplus"></i></a></li>
            </ul>
          </div>
        </div>
      </div>

      <form class="login" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="row">

          <div class="col-md-12">
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              <label for="email" class="control-label">@lang("E-Mail Address")</label>
              <input id="email" type="email" class="input-text" name="email" placeholder="@lang("Your Email")" value="{{ old('email') }}" required autofocus>
              @if ($errors->has('email'))
                <span class="help-block">
                  <strong>{{ $errors->first('email') }}</strong>
                </span>
              @endif
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              <label for="password" class="control-label">@lang("Password")</label>
              <input id="password" type="password" placeholder="@lang("Password")" class="input-text" name="password" required>

              @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
              @endif
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <div class="checkboxes margin-top-10">
                <input id="remenber-me-log" type="checkbox" checked name="remember">
                <label for="remember-me-log">@lang("Remember Me")</label>
              </div>
            </div>
          </div>

          <div class="col-md-12">
            <div class="form-group">
              <button type="submit" class="submit button">@lang("Login")</button>
              <span><a class="" href="{{ route('password.request') }}">@lang("Forgot Your Password?")</a></span>
            </div>
          </div>

        </div>

      </form>

    </div>

  </div>

</div>

@endsection
