@extends('layouts.app')

@section('content')
  <div id="titlebar">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12">

  				<h2>404 Not Found</h2>

  				<!-- Breadcrumbs -->
  				<nav id="breadcrumbs">
  					<ul>
  						<li><a href="{{url('/')}}">@lang("Home")</a></li>
  						<li>404 Not Found</li>
  					</ul>
  				</nav>

  			</div>
  		</div>
  	</div>
  </div>

  <div class="container">

	<div class="row">
		<div class="col-md-12">

			<section id="not-found" class="center">
				<h2>404 <i class="fa fa-question-circle"></i></h2>
				<p>@lang("We're sorry, but the page you were looking for doesn't exist.")</p>

				<!-- Search -->
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
            <form class="" action="/search" method="GET">
  					   <div class="main-search-input">
                <div class="main-search-input-item">
                  <input type="text" placeholder="@lang("What are you looking for?")" value="" name="keyword"/>
                </div>

                <div class="main-search-input-item location">
                  <input type="text" placeholder="@lang("Location")" value="" name="location"/>
                </div>

                <div class="main-search-input-item">
                  <select data-placeholder="@lang("All Dates")" class="chosen-select" name="date">
                    <option value="all day">@lang("All Dates")</option>
                    <option value="today">@lang("Today")</option>
                    <option value="tomorrow">@lang("Tomorrow")</option>
                    <option value="this-week">@lang("This Week")</option>
                    <option value="next-week">@lang("Next Week")</option>
                    <option value="this-month">@lang("This Month")</option>
                    <option value="next-month">@lang("Next Month")</option>
                  </select>
                </div>

                <button class="button" type="submit">@lang("Search")</button>
  					</div>
          </form>
					</div>
				</div>
				<!-- Search Section / End -->


			</section>

		</div>
	</div>

</div>


@endsection
