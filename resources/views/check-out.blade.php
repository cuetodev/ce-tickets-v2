@extends('layouts.app')

@section('content')

    <!-- Titlebar
  ================================================== -->
  <div id="titlebar" class="gradient">
    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <h2>@lang("Check Out")</h2>
          <!-- Breadcrumbs -->
          <nav id="breadcrumbs">
            <ul>
              <li><a href="/">@lang("Home")</a></li>
              <li>@lang("Check Out")</li>
            </ul>
          </nav>

        </div>
      </div>
    </div>
  </div>

  <div class="container padding-bottom-70">

    <div class="row">
      <div class="col-md-8">
        <!-- Sorting / Layout Switcher -->
  			<div class="row margin-bottom-25">

  				<div class="col-md-6 col-xs-6">
  					<!-- Layout Switcher -->
            <div class="alert alert-danger">
              <span> @lang("You have 20 minutes to complete the payment or your order will be cancelled.")</span>
            </div>
  				</div>

  				<div class="col-md-6 col-xs-6">
  					<!-- Sort by -->
  					<div class="sort-by">
  						<div class="sort-by-select">
  							{{-- <a href="{{ url('cart') }}/clear" class="button border"><i class="sl sl-icon-trash"></i> Clear the cart</a> --}}
  						</div>
  					</div>
  				</div>
  			</div>
  			<!-- Sorting / Layout Switcher / End -->
        <table class="table table-striped cart-list">
          <thead>
            <tr>
              <th>@lang("ITEM")</th>
              <th>@lang("QUANTITY")</th>
              <th>@lang("TAX")</th>
              <th>@lang("SUBTOTAL")</th>
              {{-- <th>TOTAL</th> --}}
              {{-- <th>ACTIONS</th> --}}
            </tr>
          </thead>
          <tbody>
            @foreach ( $order->items as $item)
              <tr>
                <td style="vertical-align: middle;">
                  <span class="">
                    {{ $item->event_name }}
                  </span> -
                  <strong> {{ $item->entrance_type }} </strong>
                </td>
                <td style="vertical-align: middle;">
                  {{ $item->quantity }}
                </td>
                <td style="vertical-align: middle;"> ${{ $item->tax }} </td>
                <td style="vertical-align: middle;"> ${{ $item->subtotal }} </td>
                {{-- <td style="vertical-align: middle;"> ${{ $item->total() }} </td> --}}
                {{-- <td style="vertical-align: middle;">
                  <a href="{{ url('cart') }}/remove/{{ $item->rowId }}" data-toggle="tooltip" data-placement="bottom" title="Remove Item">
                    <i class="sl sl-icon-trash"></i>
                  </a>
                  <a href="#" onclick="event.preventDefault(); document.getElementById('update-{{ $item->rowId }}').submit();">
                    <i class="sl sl-icon-arrow-up-circle" style="margin-left: 9px;"></i>
                  </a>
                </td> --}}
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>

      <div class="col-md-4 col-lg-4 sticky">
        <div class="boxed-widget">

          <div class="box_style_1 expose">
            <h3 class="inner">- @lang("Summary") -</h3>

            <table class="table table_summary">
              <tbody>
                @foreach ( $order->items as $item)
                  <tr>
                    <td>
                      {{ $item->event_name }} - {{ $item->entrance_type }}
                    </td>
                    <td class="text-right">
                      {{ $item->quantity }}x ${{ $item->subtotal }}
                    </td>
                  </tr>
                @endforeach
                {{-- <tr>
                  <td>
                    Total amount
                  </td>
                  <td class="text-right">
                    {{ $total }}
                  </td>
                </tr> --}}
                <tr class="total">
                  <td>
                    @lang("Total cost")
                  </td>
                  <td class="text-right">
                    ${{ $total }}
                  </td>
                </tr>
              </tbody>
            </table>
            @if ($total != 0)
              <a href="{{ url('order/payment') }}" class="button fullwidth margin-top-5"><i class="im im-icon-Paypal" ></i><span>@lang("Pay with Paypal")</span></a>

              <form action="{{ url('/order/stripe/payment') }}" id="stripeForm" method="POST">
                  {{ csrf_field() }}

                <input type="hidden" name="stripeToken" id="stripeToken">
                <input type="hidden" name="stripeEmail" id="stripeEmail">

                <button id="stripeCheckoutBtn" data-amount="{{ $total }}" type="submit" class="button fullwidth margin-top-5"><i class="fa fa-cc-stripe" ></i><span>@lang("Pay with Stripe")</span></button>
              </form>

            @else
              <a href="{{ url('order/payment') }}" class="button fullwidth margin-top-5"><i class="im im-icon-Ticket" ></i><span>@lang("Get Tickets")</span></a>
            @endif
            <a href="{{ url('order/cancel') }}" class="button border fullwidth margin-top-5"><i class="sl sl-icon-close" ></i><span>@lang("Cancel Order")</span></a>

          </div>
          <!--/box_style_1 -->
        </div>
      </div>
    </div>
  </div>

@endsection
