@if ( $paginator->hasPages() )
  <div class="row">
    <div class="col-md-12">
      <div class="pagination-container margin-top-20 margin-bottom-40">
        <nav class="pagination">
          <ul>
            @if ($paginator->onFirstPage())
              <li class="disabled"><a href="#"><i class="sl sl-icon-arrow-left"></i></a></li>
            @else
              <li class=""><a href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="sl sl-icon-arrow-left"></i></a></li>
            @endif

            @foreach ($elements as $element)
              @if (is_string($element))
                <li><a href="#" class="current-page">{{ $element }}</a></li>
              @endif

              @if (is_array($element))
                @foreach ($element as $page => $url)
                  @if ($page == $paginator->currentPage())
                    <li><a href="#" class="current-page">{{ $page }}</a></li>
                  @else
                    <li><a href="{{ $url }}" class="">{{ $page }}</a></li>
                  @endif
                @endforeach
              @endif
            @endforeach

            @if ($paginator->hasMorePages())
              <li><a href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="sl sl-icon-arrow-right"></i></a></li>
            @endif
          </ul>
        </nav>
      </div>
    </div>
  </div>
@endif
