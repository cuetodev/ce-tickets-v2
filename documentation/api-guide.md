﻿# Tickets Api V1 Guide.
_____________________________________

This is a simple guide of how to use the api of `CE-TICKETS`

## Getting started
__________________________________________________________

### Authentication

The most end-points in the api require authentication, for every request you have to use a `api_token`, for get it you must authenticate with this end-point:

`POST: /api/auth/`

You must send a **POST** Request with those values:

```
email: test@example.com
password: Your-password
```

this will return something like this:

```
{" api_token": "XXXXXXXXXXXxxxXXXXXxxxXXXXxxxxXXXXxxxXXXxxxXXX" }
```

This is your **api_token**, so in every request, you must send it in the header of request, example:

```
Authorization: Bearer XXXXXXXXXXXxxxXXXXXxxxXXXXxxxxXXXXxxxXXXxxxXXX
```

Or you can send it as a *Query String* :
```
?api_token=XXXXXXXXXXXxxxXXXXXxxxXXXXxxxxXXXXxxxXXXxxxXXX
```
But I don't recommend it, due to security reasons.

### Social Authentication

The social Authentication is an attempt of provide to the api the capability of handle social login/register

`POST: /api/auth/social/{provider}`

where provider can be `google`, `facebook` or `twitter` that is important.

#### Parameters

| Name | Type | Description |
|----|----|
| email | string | the email provided by the social account of the user |
| uid | string | the id provided by the social account of the user |
| first_name | string | the the first name provided by the social account of the user |
| last_name | string | the last name provided by the social account of the user |
| avatar | string | the url of avatar img provided by the social account of the user |

### Register new users

`POST: /api/auth/register`

#### Parameters

| Name | Type |
|----|----|
| first_name | string |
| last_name | string |
| username | string |
| phone_number | string |
| gender | enum ('male', 'female') |
| birth_date | date |
| email | string |
| password | string |
| password_confirmation | string |

### Return others parameters

You can return some parameters in a api. For example, you can return the user who bought a specific ticket when you call the api tickets.

The form to call the the params is:

```
?include[]=param0&include[]=param1&include[]=param2
```

the code below include with the api call the param0, param1 and param2.

Note: In every specification of api, there are a specification about the params that accept it.

#### Example

Example in **Jquery Ajax**

```
var form = new FormData();
form.append("email", "test@example.com");
form.append("password", "Your-password");

var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/auth/",
  "method": "POST",
  "headers": {
    "accept": "application/json",
    "cache-control": "no-cache"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

After that you can store that credential for use later, you can store in a **cookie**, or a **LocaStorage**.

## User Api
____________________________________________

### End-points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/users/  |Show all Users | ✔️ |
|  `GET` |  api/users/{user}  | Show one User | ✔️ |
|  `PUT` |  api/users/{user}  | Update a User |  ✔️ |
|  `DELETE` |  api/users/{user} | Delete a User | ✔️ |
|  `GET` |  api/users/{user}/assign_role/{role} | Assign a Role to a User | ✔️  |
|  `GET` |  api/users/{user}/remove_role/{role}  | Remove a Role to a User | ✔️  |

### Parameters

| Name | Type |
|----|----|
| first_name | string |
| last_name | string |
| gender | string (male \ female) |
| birth_date | string |
| phone_number | string |
| username | string |
| email| string |
| avatar | string |
| activated |bool ( 0/1 )|

### Include Parameters

```
?include[]=roles
```

| Parameter | Description |
|-----------|-------------|
| roles | include the roles of user |
| organization | include the organization of the user |
| country | include the country |
| city | include the city |

### Example

Example with **Jquery Ajax**

```
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/users/4",
  "method": "PUT",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer LOIARUHLhKeyVTgZ8K9AdYtcYE1b8uZwYtuZtbPJx9jYyKrYYPRyXJdoow38",
    "content-type": "application/x-www-form-urlencoded",
  },
  "data": {
    "last_name": "Guzman",
    "first_name": "Wendy",
    "birth_date": "2017-12-17"
  }
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

The code on top, update the name and the birth date.

## Roles Api
_________________________________________________

### End-Points


| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/roles/  |Show all roles | ✔️ |
|  `GET` |  api/roles/{role}  | Show one Role | ✔️ |
|  `PUT` |  api/roles/{role}  | Update a Role |  ✔️ |
|  `DELETE` |  api/roles/{Role} | Delete a Role | ✔️ |
|  `POST` |  api/roles/ | Create a New Role | ✔️  |

### Parameters

| Name | Type |
|----|----|
| name | string |
| description | string |


### Example

Example with **Jquery Ajax**

```
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/roles",
  "method": "GET",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer LOIARUHLhKeyVTgZ8K9AdYtcYE1b8uZwYtuZtbPJx9jYyKrYYPRyXJdoow38",
    "cache-control": "no-cache",
  },
  "data": {}
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

The code on top, call all roles


## Events Api

_______________________________________________________________

### End-Points



| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/events/  |Show all Events | ❌ |
|  `GET` |  api/events/{event}  | Show one Event | ❌  |
| `POST` | api/events/ | Create a new Event | ✔️ |
|  `PUT` |  api/events/{event}  | Update a User | ✔️ |
|  `DELETE` |  api/events/{event} | Delete a Event | ✔️ |
|  `GET` |  api/events/{event}/assign/category/{category} | Assign a Category to a Event | ✔️  |
|  `GET` |  api/events/{event}/assign_entrance/{entrance} | Assign a Type 0f Entrance to a Event | ✔️  |
|  `GET` |  api/events/{event}/remove_entrance/{entrance}  | Remove a Type 0f Entrance to a Event | ✔️  |

### Parameters Optional

```
?status=sold-out
```

values: 'sold-out, available, ended'

### About Privacy Events

You can have private events by especify send the POST parameter `privacy` with a value `private`, if you want to share the event you must to generate a url with the parameter `share_code`. More info Read: "How to build and share a URL" to the end of this doc.

`share_code` change if you make a call to update the event.

### About slug or friendly Url

If you create a Event the system will generate a friendly title especific for url. If you Change the title the Friendly Title will be updated to the changed title. You don't need to specify a slug or Friendly title, beacause the system generate it automatically.

### Parameters

| Name | Type | Note |
|----|----|
| name | string |
| description | string |
| category_id | integer | this parameters assign a category |
| picture | string |
| organization_id | integer |
| facebook_link | string |
| twitter_link | string |
| privacy | string |
| location | string |
| latitude | string |
| longitude | string |
| start_date | date |
| status | string | return: {available, sold-out or ended} default: available |
| end_date | date |
| start_time | time |
| end_time | time |
| stage_map | string | picture of stage map |
| is_recurrent | boolean | Is a recurrent event |
| paid | boolean | the event was paid? |
| paid_date | date | the paid date |
| reference | string | the number of reference of the invoice for the productor |
| week_days | array | The days of the week the event will happen. {'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'} |

### Include Parameters

```
?include[]=category
```

| Parameter | Description |
|-----------|-------------|
| category | include the category of event |
| reviews | include the review of users |
| entrances | include the entrances or type tickets |
| organization | include the organization |
| city | include the city |
| country | include the country |

### Example

Example with **Jquery Ajax**

```
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/events",
  "method": "GET",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer LOIARUHLhKeyVTgZ8K9AdYtcYE1b8uZwYtuZtbPJx9jYyKrYYPRyXJdoow38",
    "cache-control": "no-cache",
  },
  "data": {}
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

The code on top, call all events


## Category Api

____________________________________________________________________________

### End-Points



| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/categories/  |Show all categories | ❌ |
|  `GET` |  api/categories/{category}  | Show one category | ❌  |
| `POST` | api/categories/ | Create a new category | ✔️ |
|  `PUT` |  api/categories/{category}  | Update a Category | ✔️ |
|  `DELETE` |  api/categories/{category} | Delete a Category | ✔️ |

### Parameters

| Name | Type |
|----|----|
| name | string |
| description | string |
| picture | string |
| name_translated | array |
| description_translated | array |

**Picture**: *need a link*

**name_translated** and **description_translated**: is a array that accept any languaje code, for example 'es' and 'en'

### Example

Example with **Jquery Ajax**

```
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/categories",
  "method": "GET",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer LOIARUHLhKeyVTgZ8K9AdYtcYE1b8uZwYtuZtbPJx9jYyKrYYPRyXJdoow38",
    "cache-control": "no-cache",
    "postman-token": "f7c824bb-50fc-de1b-ec20-a3230ec3d406"
  },
  "data": {}
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

The code on top, call all categories

Example of JSON structure of Query:

```
{
	"name": "Well",
	"description": "This is a well description for an example",
	"picture": "https://eventstocity.com/uploads/bthzIT_1547785039.jpeg",
	"name_translated": {
		"es": "Bueno 2",
		"en": "Well 2"
	},
	"description_translated": {
		"es": "Esto es una buena descripción para un ejemplo",
		"en": "This is a well description for an example"
	}
}
```

## Reviews Api
__________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/reviews/  |Show all reviews | ❌ |
|  `GET` |  api/reviews/{review}  | Show one review | ❌  |
| `POST` | api/reviews/ | Create a new review | ✔️ |
|  `PUT` |  api/reviews/{review}  | Update a review | ✔️ |
|  `DELETE` |  api/reviews/{review} | Delete a review | ✔️ |

### Parameters

| Name | Type |
|----|----|
| comment | string |
| rating | integer - string (0 - 5) |
| event_id | integer |
| user_id | integer |
| picture | string |

### Include Parameters

```
?include[]=event
```

| Parameter | Description |
|-----------|-------------|
| event | include the event of review |
| user | include the user who review |

### Example

Example with **Jquery Ajax**

```
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/reviews",
  "method": "GET",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer LOIARUHLhKeyVTgZ8K9AdYtcYE1b8uZwYtuZtbPJx9jYyKrYYPRyXJdoow38",
    "cache-control": "no-cache",
  },
  "data": {}
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

The code on top, call all reviews


## Entrance Api
____________________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/entrances/  |Show all entrances | ✔️ |
|  `GET` |  api/entrances/{entrance}  | Show one entrance | ✔️ |
| `POST` | api/entrances/ | Create a new entrance | ✔️ |
|  `PUT` |  api/entrances/{entrance}  | Update a entrance | ✔️ |
|  `DELETE` |  api/entrances/{entrance} | Delete a entrance | ✔️ |

### Parameters

| Name | Type |
|----|----|
| name | string |
| description | string |
| quantity_availables | integer |
| price | float |
| event_id | integer |
| original_qty | integer |

### Example

Example with **Jquery Ajax**

```
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/entrances",
  "method": "GET",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer LOIARUHLhKeyVTgZ8K9AdYtcYE1b8uZwYtuZtbPJx9jYyKrYYPRyXJdoow38",
    "cache-control": "no-cache",
  },
  "data": {}
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

The code on top, call all entrances.

## Organizations Api
__________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/organizations/  |Show all organizations | ✔️ |
|  `GET` |  api/organizations/{organization}  | Show one organization | ✔️ |
| `POST` | api/organizations/ | Create a new organization | ✔️ |
|  `PUT` |  api/organizations/{organization}  | Update a organization | ✔️ |
|  `DELETE` |  api/organizations/{organization} | Delete a organization | ✔️ |
|  `GET` |  api/organizations/request/all |Show all organizationsRequest | ✔️ |
|  `GET` |  api/organization/request/{organizationRequest}/aprove | Aprove a organization Request and notify the user by email | ✔️ |
|  `GET` |  api/organization/request/{organizationRequest}/reject | Reject a organization Request and notify the user by email | ✔️ |

### Parameters

| Name | Type |
|----|----|
| name | string |
| description | string |
| phone | string |
| facebook_link | string |
| twitter_link | string |
| email | string |
| website | string |
|activated| bool |
| picture | string |
| user_id | integer |


### Example

Example with **Jquery Ajax**

```
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/organizations",
  "method": "GET",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer LOIARUHLhKeyVTgZ8K9AdYtcYE1b8uZwYtuZtbPJx9jYyKrYYPRyXJdoow38",
    "cache-control": "no-cache",
  },
  "data": {}
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

The code on top, call all organizations.


## Tickets Api
______________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/tickets/  |Show all tickets | ✔️ |
| `POST` | api/tickets/check | Check if a ticket is used or not, if the ticket is not used yet, change it to `1` | ✔️ |
| `POST` | api/tickets/verify | Verify a Ticket with a code parameter | ✔️ |
| `GET` | api/tickets/event/{event} | Return all tickets of a specific event | ✔️ |


### Parameters for /check

| Name | Type |
|----|----|
| code | string |
| staff_id | integer |

**Note**: This end-point is for the app.

**Warning**: This may change in the future.

### Parameters for /verify

| Name | Type |
|----|----|
| code | string |

### Include Parameters

```
?include[]=checked_by
```

| Parameter | Description |
|-----------|-------------|
| user | include the user who bought the ticket |
| event | include the event of the ticket |
| type_entrance | include the type of entrance of ticket |
| checked_by | include the include the staff who checked the ticket |


### Example

Example with **Jquery Ajax**

```
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/tickets/check_ticket",
  "method": "POST",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer LOIARUHLhKeyVTgZ8K9AdYtcYE1b8uZwYtuZtbPJx9jYyKrYYPRyXJdoow38",
    "content-type": "application/x-www-form-urlencoded",
    "cache-control": "no-cache",
  },
  "data": {
    "code": "fkdfkdfdkfkdfdfkdfkkdkfd"
  }
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

The code on top, check a ticket.

## Search and Filter Api
______________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/search  |Search Events | ❌ |

### Parameters or filters

| Name | Type |
|----|----|
| keyword | string |
| category | string |
| location | string |
| stars | integer (1-5) |
| price | float |
| date | string ('today', 'tomorrow', 'this-week', 'next-week', 'this-month', 'next-month') |
| dayOfWeek | string ( 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday' ) |
| source | string ('app')
| privacy | string ('public' or 'private') it is optional |
| exclude | string ('sold-out', 'ended', 'available') it is optional |

**Note 1:** All parameters are optionals, if you don't specify any parameters the search will return all events

**Note 2:** The parameter privacy is optional, if you don't specific that parameter, will return all Events, the only function of this is to filter the events by privacy.

**Note 3:** The parameter exclude is optional, if you don't especific that parameter, will return all Events, the only function of this is to filter the events excluding some events

### Example

Example with **Jquery Ajax**

```
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/search?keyword=mundo",
  "method": "GET",
  "headers": {
    "cache-control": "no-cache",
  }
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

The example return all events with the keyword: mundo

## Most Visited Api
______________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/events/most-visited  | Get the upcoming events order by most visits | ❌ |


## Uploads Api
______________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/uploads  | Show all File uploaded | ❌ |
|  `POST` |  api/uploads  | Upload a file | ❌ |

### Parameters or filters

| Name | Type |
|----|----|
| file |  |

### Example

Example with **Jquery Ajax**

```
var form = new FormData();
form.append("file", "fleet-02.png");

var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/uploads",
  "method": "POST",
  "headers": {
    "cache-control": "no-cache"
  },
  "processData": false,
  "contentType": false,
  "mimeType": "multipart/form-data",
  "data": form
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

This will return a url of a file uploaded.

## Staff Api
______________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/staff  | Show all Staff user | ✔️ |
|  `GET` |  api/staff/by/event/{event}  | Show all staff of a specific event | ✔️ |
|  `GET` |  api/staff/{staff}/events  | Show all events assigned to a specific staff | ✔️ |
|  `GET` |  api/staff/{staff}/assign/event/{event}  | Assign a specific event to a specific staff | ✔️ |
|  `GET` |  api/staff/{staff}/remove/event/{event}  | Remove a specific event to a specific staff | ✔️ |
|  `GET` |  api/staff  | Show all Staff user | ✔️ |
|  `POST` |  api/staff  | Create a new Staff User and send by email the access code | ✔️ |
|  `PUT` | api/staff/{staff}/reset_access | Reset the access code and send by email the access code | ✔️ |
|  `DELETE` |  api/staff/{staff}  | Destroy a Staff User | ✔️ |

### Parameters or filters

| Name | Type |
|----|----|
| name | string  |
| email | string |
| organization_id | number |

### Include Parameters

```
?include[]=organization
```

| Parameter | Description |
|-----------|-------------|
| organization | include the organization who belong the staff |

## Auth Staff Api
______________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `POST` |  api/staff/auth  | Auth a Staff User and return a api_token relate to Organizer user | ❌  |

### Parameters or filters

| Name | Type |
|----|----|
| code | string  |


## Finances Api

______________________________________________________________________________________________

### End-Points

| Method   | Url                                        | Action                                 | Need Authentication?|
|----------|--------------------------------------------|----------------------------------------|---------------------|
|  `GET`   | finances/organization/all                  |Returns all the finances by organization| ✔                   ️|   
|  `GET`   | finances/organization/{organization}/events|Returns all the finances by event       | ✔️                   |
|  `POST`   | finances/pay_event/                       |Mark event as payed                     | ✔️  |
|  `POST`   | finances/by_organization_paid/{organization_id} |Returns all the payed events by organization      | ✔️  |

### Extra query parameters
You cant pass either {type} date range specifying if your going to query a week a month or a year, below the example

/api/finances/organization/all?type=month
/api/finances/organization/all?type=year
/api/finances/organization/all?type=year

/api/finances/organization/2/events?type=month
/api/finances/organization/2/events?type=year
/api/finances/organization/2/events?type=year

If the parameter type isn't passed then the endpoint will use the current date as range, additionaly to this you can send custom from and to dates

/api/finances/organization/all?from=2018-01-01&to=2018-02-12
/api/finances/organization/2/events?from=2018-01-01&to=2018-02-12

### Pay event post parameters

The pay_event endpoint marks an event as payed and takes as parameters the folllowing
event: the event id
reference: the transaction reference number
paid_date: the paid date specified by the user

api/finances/pay_event/   


## Gifts Api
______________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/gifts/to/me  | Show all my presents or gifts | ✔️ |
|  `GET` |  api/gifts/given  | Show all gifts that I've given to the others users | ✔️ |
|  `POST` |  api/gifts/generate/{ticket}  | Generate a ticket with a especific event ID | ✔️ |
|  `POST` |  api/gifts/accept/{gift}  | Accept a especific gift: need to send the parameter accept with a aleatory value, may be a encriptedCode | ✔️ |
|  `POST` |  api/gifts/reject/{gift}  | Reject a especific gift: need to send the parameter reject with a aleatory value, may be a encriptedCode | ✔️ |


### Parameters or filters

| Name | Type | Description |
|----|----|----|
| email_receiver | text | The email of the people who you whant to give, works on the gifts/generate endpoint |
| accept | text | need that parameter to work, and need to be an aleatory value or EncriptedCode, works on the gifts/accept endpoint |
| reject | text | need that parameter to work, and need to be an aleatory value or EncriptedCode, works on the gifts/reject endpoint |


## Cancel Order Api
______________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `GET` |  api/orders/{order_id}/cancel  |cancel a order that not has been paid | ✔️ |

## make order Api
______________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `POST` |  api/orders/make/order  |receive cart items and create an order | ✔️ |

### Parameters for /make/order

| Name | Type |
|----|----|
| user_id | int |
| cart_content | array | {event_id,entrance_id,event_name,entrance_name,qty,price,tax}

**Note**: This end-point is for the app.

**Warning**: This may change in the future.


### Example

Example with **Jquery Ajax**

```
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/orders/make/order",
  "method": "POST",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer LOIARUHLhKeyVTgZ8K9AdYtcYE1b8uZwYtuZtbPJx9jYyKrYYPRyXJdoow38",
    "content-type": "application/x-www-form-urlencoded",
    "cache-control": "no-cache",
  },
  "data": {
    "user_id": "1",
    "cart_content": {"1":{"event_id":"1","entrance_id":"1","event_name":"none","entrance_name":"test","qty":"8","price":"120","tax":"4"},"2":{"event_id":"2","entrance_id":"2","event_name":"testing","entrance_name":"test","qty":"3","price":"150","tax":"0.03"}}
  }
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

## pay confirmation Api
______________________________________________________________________________________________

### End-Points

| Method | Url | Action | Need Authentication? |
|----------|----------|-------------|----------|
|  `POST` |  api/orders/pay/confirmation  |receive order_id change status to paid if payment status is success and assign tickets to the owner | ✔️ |

### Parameters for /pay_confirmation

| Name | Type |
|----|----|
| ACK | string |
| order_id | int |

**Note**: This end-point if for the app.

**Warning**: This may change in the future.


### Example

Example with **Jquery Ajax**

```
var settings = {
  "async": true,
  "crossDomain": true,
  "url": "http://ce-tickets.dev/api/orders/pay/confirmation",
  "method": "POST",
  "headers": {
    "accept": "application/json",
    "authorization": "Bearer LOIARUHLhKeyVTgZ8K9AdYtcYE1b8uZwYtuZtbPJx9jYyKrYYPRyXJdoow38",
    "content-type": "application/x-www-form-urlencoded",
    "cache-control": "no-cache",
  },
  "data": {
    "ACK": "SUCCESS",
    "order_id": "1"
  }
}

$.ajax(settings).done(function (response) {
  console.log(response);
});
```

The code on top, create an order and return order id.

## How to build and share url

The simplest way to build a url to share is using the actual domain + the slug (friendly url)

```
https://ce-tickets.dev/events/friendly-url/
```

to build a url of a private event, you need to specify a `share_code`, a code of 8 digit that is unique and can change in aleatory way if you update the actual event.

```
https://ce-tickets.dev/events/friendly-url?share_code=34rD45w2
```
