<?php

namespace App\Console;

use App\Order;
use App\Event;
use App\Entrance;
use App\Ticket;

use Carbon\Carbon;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        //Destroy All Orders that not has been paid in 20 minutes

        $schedule->call(function () {

            // Check if a current event data pass and has new dates in dates table
//            $events = Event::with('dates')->where('status', '!=', 'ended')
//                ->where('start_date', '<=', Carbon::now()->format('Y-m-d'))
//                ->get();
//
//            foreach ($events as $event) {
//                $nextEvent = $event
//                    ->dates
//                    ->where('start_date', '>', Carbon::now()->format('Y-m-d'))
//                    ->sortBy('start_date')->first();
//
//                if ($nextEvent) {
//                    $event->start_date = $nextEvent->start_date;
//                    $event->start_time = $nextEvent->start_time;
//                    $event->quantity_availables = $nextEvent->quantity_availables;
//                    $event->save();
//                }
//            }
            //End check

            // Create the next recurrent event
            $events = Event::where('is_recurrent', 1)
                ->where('end_date', '<', \Carbon\Carbon::now()->format('Y-m-d'))
                ->get();

            foreach ($events as $event) {
                $dates = [];
                foreach ($event->days as $day) {
                    array_push($dates, \Carbon\Carbon::parse('next ' . $day->day)->format('Y-m-d'));
                }

                sort($dates);

                $nextEvent = $event->replicate();
                $nextEvent->start_date = $dates[0];
                $nextEvent->end_date = $dates[0];
                $nextEvent->share_code = str_random(8);
                $nextEvent->slug = str_slug($event->name) . '-' . \Carbon\Carbon::now()->format('Y-m-d');
                $nextEvent->status = 'available';
                $nextEvent->push();

                foreach ($event->type_entrances as $entranceType) {
                    $entrance = $entranceType->replicate();
                    $entrance->quantity_availables = $entrance->original_qty;
                    $nextEvent->type_entrances()->save($entrance);
                }

                $event->is_recurrent = 0;
                $event->status = 'ended';
                $event->save();
            }
            // End the next recurrent event


          $orders = Order::where('status', 'pending')
          ->where('updated_at', '<=', Carbon::now()
          ->subMinutes(20)
          ->toDateTimeString());

          if (!is_null($orders->get())) {

            foreach ($orders->get() as $order) {
              $items = $order->items();

              foreach ($items->get() as $item) {

                $type_ticket = Entrance::find($item->entrance_id);

                $type_ticket->update([
                  'quantity_availables' => ($type_ticket->quantity_availables + $item->quantity)
                ]);

              }

              $items->delete();
            }

            $orders->delete();
          }

            //Update Events status time-out
            $date = Carbon::now();
            Event::where('end_date', '<', $date->format('Y-m-d'))
                    ->where('end_time', '<', $date->toTimeString())
                    ->update(['status' => 'ended']);



            // verify if event has run out of entrances and set sold-out
            $events = Event::where('status','available')->get();
            foreach ($events as $event) {
                $entrances = Entrance::where('event_id',$event->id)->get();
                foreach ($entrances as $entrance) {
                    if ($entrance->quantity_availables == 0) {
                        $event_soldout = Event::find($event->id);
                        $event_soldout->status = 'sold-out';
                        $event_soldout->save();
                    }
                }
            }

        })->everyMinute()->name('events-related-action')
            ->withoutOverlapping();

    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');
        require base_path('routes/console.php');
    }
}
