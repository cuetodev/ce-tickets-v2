<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  protected $guarded = [];

  protected $casts = [
    'name_translated' => 'array',
    'description_translated' => 'array'
  ];

  public function events()
  {
    return $this->hasMany('App\Event');
  }

  public function setNameTranslatedAttribute($value) {
    $this->attributes['name_translated'] = json_encode($value);
  }

  public function setDescriptionTranslatedAttribute($value) {
    $this->attributes['description_translated'] = json_encode($value);
  }

}
