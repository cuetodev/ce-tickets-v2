<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{
    protected $guarded = [];
    public $timestamps = false;
    protected $dates = ['created_at', 'updated_at', 'start_date','end_date'];
    protected $fillable = ['event_id', 'start_date','end_date', 'start_time', 'end_time', 'quantity_availables'];

    public function events()
    {
      return $this->belongsTo('App\Event');
    }
}
