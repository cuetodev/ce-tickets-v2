<?php

namespace App\Mail;

use App\User;
use App\Gift;
use App\Event;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GiftInvitation extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $gift;
    public $event;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Gift $gift)
    {
        $this->user = User::where('email', $gift->giver)->first();
        $this->gift = $gift;

        $this->event = Event::find($gift->event_id);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      $receiver = User::where('email', $this->gift->receiver)->first();

      if (is_null($receiver)) {

        return $this->view('emails.gift-invitation-signup');

      } else {

        return $this->view('emails.gift-invitation');

      }
    }
}
