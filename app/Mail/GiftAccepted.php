<?php

namespace App\Mail;

use App\Gift;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class GiftAccepted extends Mailable
{
    use Queueable, SerializesModels;

    public $gift;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Gift $gift)
    {
        $this->gift = $gift;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.gift-accepted');
    }
}
