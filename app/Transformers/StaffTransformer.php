<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Staff;

class StaffTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
      'organization',
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var \App\Staff $resource
     * @return array
     */
    public function transform(Staff $resource)
    {
        return [
          'id' => (int) $resource->id,
          'name' => $resource->name,
          'email' => $resource->email,
          'code' => $resource->code,
          'organization_id' => (int) $resource->organization_id,
          'created_at' => $resource->created_at,
          'updated_at' => $resource->updated_at,
        ];
    }

    public function includeOrganization(Staff $resource)
    {
      $organization = $resource->organization;

      if (null != $organization) {
        return $this->item($organization, new OrganizationTransformer);
      }
    }
}
