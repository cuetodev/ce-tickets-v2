<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Event;

use Carbon\Carbon;

class EventTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
      'category',
      'reviews',
      'entrances',
      'dates',
      'organization',
      'city',
      'country',
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var \App\Event $resource
     * @return array
     */
    public function transform(Event $resource)
    {
        return [

            'id' => (int) $resource->id,
            'name' => $resource->name,
            'description' => $resource->description,
            'picture' => $resource->picture,
            'privacy' => $resource->privacy,
            'status' => $resource->status,
            'stage_map' => $resource->stage_map,
            'average_review' => $resource->averageReview(),
            'location' => $resource->location,
            'latitude' => $resource->latitude,
            'longitude' => $resource->longitude,
            'start_date' => Carbon::parse($resource->start_date)->toDateString(),
            'end_date' => Carbon::parse($resource->end_date)->toDateString(),
            'start_time' => $resource->start_time,
            'end_time' => $resource->end_time,
            'share_code' => $resource->share_code,
            'slug' => $resource->slug,
            'facebook_link' => $resource->facebook_link,
            'twitter_link' => $resource->twitter_link,
            'visits' => $resource->visits,
            'is_recurrent' => $resource->is_recurrent,
            'paid' => $resource->paid,
            'paid_date' => $resource->paid_date,
            'reference' => $resource->reference,
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at,

        ];
    }

    public function includeCategory(Event $resource)
    {
      $category = $resource->category;

      return $this->item($category, new CategoryTransformer);
    }

    public function includeReviews(Event $resource)
    {
      $reviews = $resource->reviews;

      return $this->collection($reviews, new ReviewTransformer);
    }

    public function includeEntrances(Event $resource)
    {
      $entrances = $resource->type_entrances;

      return $this->collection($entrances, new EntranceTransformer);
    }

    public function includeDates(Event $resource)
    {
      $dates = $resource->dates();

      return $this->collection($dates, new DateTransformer);
    }

    public function includeOrganization(Event $resource)
    {
      $organization = $resource->organization;

      return $this->item($organization, new OrganizationTransformer);
    }

    public function includeCountry(Event $resource)
    {
      $country = $resource->country;

      return $this->item($country, new CountryTransformer);
    }

    public function includeCity(Event $resource)
    {
      $city = $resource->city;

      return $this->item($city, new CityTransformer);
    }
}
