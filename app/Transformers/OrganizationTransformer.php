<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Organization;
// use App\User;

class OrganizationTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
      // 'owner',
    ];

    /**
     * Transform object into a generic array
     *
     * @var \App\Organization $resource
     * @return array
     */
    public function transform(Organization $resource)
    {
        return [

            'id' => (int) $resource->id,
            'name' => $resource->name,
            'description' => $resource->description,
            'phone' => $resource->phone,
            'facebook_link' => $resource->facebook_link,
            'twitter_link' => $resource->twitter_link,
            'instagram_link' => $resource->instagram_link,
            'user_id' => $resource->user_id,
            'picture' => $resource->picture,
            'activated' => $resource->activated,
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at,
        ];
    }

    // public function includeOwner(Organization $resource)
    // {
    //   $owner = $resource->organizationOwner;
    //
    //   return $this->item($owner, new UserTransformer);
    // }
}
