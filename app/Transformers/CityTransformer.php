<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use App\City;
use App\Country;

class CityTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
      // 'country',
    ];

    /**
     * Transform object into a generic array
     *
     * @var \App\City $resource
     * @return array
     */
    public function transform(City $resource)
    {
        return [

            'id' => (int) $resource->id,
            'name' => $resource->name,
            'slug' => $resource->slug,
            'picture' => $resource->picture,
            'country_id' => (int) $resource->country_id,
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at,

        ];
    }

    // public function includeCountry(City $resource)
    // {
    //   $country = $resource->country;
    //
    //   if (null != $country) {
    //     return $this->item($country, new CountryTransformer);
    //   }
    // }
}
