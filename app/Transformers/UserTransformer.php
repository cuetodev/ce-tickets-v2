<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\User;

class UserTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
      'roles',
      'organization',
      'country',
      'city',
      'tickets',
      'orders'
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var \App\User $resource
     * @return array
     */
    public function transform(User $resource)
    {
        return [
          'id' => (int) $resource->id,
          'first_name' => $resource->first_name,
          'last_name' => $resource->last_name,
          'gender' => $resource->gender,
          'birth_date' => $resource->birth_date,
          'phone_number' => $resource->phone_number,
          'username' => $resource->username,
          'email' => $resource->email,
          'avatar' => $resource->avatar,
          'activated' => $resource->activated,
          'created_at' => $resource->created_at,
          'updated_at' => $resource->updated_at,
        ];
    }

    public function includeRoles(User $resource)
    {
      $roles = $resource->roles;

      return $this->collection($roles, new RoleTransformer);
    }

    // public function includemyOrganization(User $resource)
    // {
    //   $myOrganization = $resource->myOrganization();
    //
    //   return $this->item($myOrganization, new OrganizationTransformer);
    // }

    public function includeOrganization(User $resource)
    {
      $organization = $resource->organization;

      if (null != $organization) {
        return $this->item($organization, new OrganizationTransformer);
      }

    }

    public function includeCountry(User $resource)
    {
      $country = $resource->country;

      if (null != $country) {
        return $this->item($country, new CountryTransformer);
      }
    }

    public function includeCity(User $resource)
    {
      $city = $resource->city;

      if (null != $city) {
        return $this->item($city, new CityTransformer);
      }
    }

    public function includeTickets(User $resource)
    {
      $tickets = $resource->tickets;

      if (null != $tickets) {
        return $this->collection($tickets, new TicketTransformer());
      }
    }

    public function includeOrders(User $resource)
    {
      $orders = $resource->orders;

      if (null != $orders) {
        return $this->collection($orders, new OrderTransformer());
      }
    }
}
