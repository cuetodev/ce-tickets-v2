<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Entrance;

class EntranceTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var \App\Entrance $resource
     * @return array
     */
    public function transform(Entrance $resource)
    {
        return [

            'id' => (int) $resource->id,
            'event_id' => (int) $resource->event_id,
            'name' => $resource->name,
            'description' => $resource->description,
            'quantity_availables' => $resource->quantity_availables,
            'price' => (float) $resource->price,
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at,

        ];
    }
}
