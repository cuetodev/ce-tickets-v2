<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Category;

class CategoryTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var \App\Category $resource
     * @return array
     */
    public function transform(Category $resource)
    {
        return [

            'id' => (int) $resource->id,
            'name' => $resource->name,
            'name_translated' => $resource->name_translated,
            'slug' => $resource->slug,
            'picture' => $resource->picture,
            'description' => $resource->description,
            'description_translated' => $resource->description_translated,
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at,

        ];
    }
}
