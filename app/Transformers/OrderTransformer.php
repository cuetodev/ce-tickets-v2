<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;

use App\Order;
use App\OrderItem;

class OrderTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
      'items',
    ];

    /**
     * Transform object into a generic array
     *
     * @var \App\Order $resource
     * @return array
     */
    public function transform(Order $resource)
    {
        return [

            'id' => (int) $resource->id,
            'user_id' => (int) $resource->user_id,
            'status' => $resource->status,
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at,

        ];
    }

    public function includeItems(Order $resource)
    {
      $items = $resource->items;

      if (null != $items) {
        return $this->collection($items, new OrderItemTransformer);
      }
    }
}
