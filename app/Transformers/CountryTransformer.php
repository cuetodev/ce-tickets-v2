<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Country;
use App\City;

class CountryTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [
      'cities'
    ];

    /**
     * Transform object into a generic array
     *
     * @var \App\Country $resource
     * @return array
     */
    public function transform(Country $resource)
    {
        return [
            'id' => (int) $resource->id,
            'name' => $resource->name,
            'slug' => $resource->slug,
            'picture' => $resource->picture,
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at,
        ];
    }

    public function includeCities(Country $resource)
    {
      $cities = $resource->cities;

      if (null != $cities) {
        return $this->collection($cities, new CityTransformer);
      }
    }
}
