<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Date;

class DateTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var \App\Date $resource
     * @return array
     */
    public function transform(Date $resource)
    {
        return [
            'id' => (int) $resource->id,
            'event_id' => (int) $resource->event_id,
            'start_date' => $resource->start_date,
            'end_date' => $resource->end_date,
            'start_time' => $resource->start_time,
            'end_time' => $resource->end_time,
        ];
    }
}
