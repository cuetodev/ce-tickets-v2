<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Ticket;
use App\Staff;

class TicketTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [
      'user',
      'event',
      'type_entrance',
      'checked_by',
    ];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var \App\Ticket $resource
     * @return array
     */
    public function transform(Ticket $resource)
    {
        return [

            'id' => (int) $resource->id,
            'user_id' => (int) $resource->user_id,
            'event_id' => (int) $resource->event_id,
            'code' => $resource->code,
            'price' => (float) $resource->price,
            'checked' => $resource->checked,
            'entrance_id' => (int) $resource->entrance_id,
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at,

        ];
    }

    public function includeUser(Ticket $resource)
    {
      $user = $resource->user;

      return $this->item($user, new UserTransformer());
    }

    public function includeEvent(Ticket $resource)
    {
      $event = $resource->event;

      return $this->item($event, new EventTransformer());
    }

    public function includeTypeEntrance(Ticket $resource)
    {
      $entrance = $resource->entrance;

      return $this->item($entrance, new EntranceTransformer());
    }

    public function includeCheckedBy(Ticket $resource)
    {
      $checked_by = Staff::find($resource->checked_by);

      if (null != $checked_by) {
        return $this->item($checked_by, new StaffTransformer());
      }
    }
}
