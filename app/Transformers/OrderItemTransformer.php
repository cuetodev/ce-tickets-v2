<?php

namespace App\Transformers;

use League\Fractal;
use League\Fractal\TransformerAbstract;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\OrderItem;

class OrderItemTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = [];

    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * Transform object into a generic array
     *
     * @var \App\OrderItem $resource
     * @return array
     */
    public function transform(OrderItem $resource)
    {
        return [

            'id' => (int) $resource->id,
            'order_id' => (int) $resource->order_id,
            'event_id' => (int) $resource->event_id,
            'entrance_id' => (int) $resource->entrance_id,
            'event_name' => $resource->event_name,
            'entrance_type' => $resource->entrance_type,
            'quantity' => (int) $resource->quantity,
            'price' => (double) $resource->price,
            'tax' => (double) $resource->tax,
            'subtotal' => (double) $resource->subtotal,
            'created_at' => $resource->created_at,
            'updated_at' => $resource->updated_at,
        ];
    }
}
