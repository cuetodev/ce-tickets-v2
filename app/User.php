<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'password',
        'avatar',
        'phone_number',
        'gender',
        'birth_date',
        'activated',
        'api_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    public function roles()
    {
      return $this->belongsToMany('App\Role');
    }

    public function authorizeRoles($roles)
    {
      if ( $this->hasAnyRole($roles) ) {
        return true;
      }
      abort(401, 'This action is unauthorized.');
    }

    public function hasAnyRole($roles)
    {
      if ( is_array($roles) ) {
        foreach ( $roles as $role ) {
          if ( $this->hasRole($role) ) {
            return true;
          }
        }
      } else {
        if ( $this->hasRole($roles) ) {
          return true;
        }
      }

      return false;
    }

    public function hasRole($role)
    {
      if ( $this->roles()->where('name', $role)->first() ) {
        return true;
      }

      return false;
    }

    public function assignRole($role)
    {
      return $this->roles()->attach($role);
    }

    public function removeRole($role)
    {
      return $this->roles()->detach($role);
    }

    public function social()
    {
      return $this->hasOne('App\Social');
    }

    public function organization()
    {
      return $this->hasOne('App\Organization');
    }

    public function reviews()
    {
      return $this->hasMany('App\Review');
    }

    public function tickets()
    {
      return $this->hasMany('App\Ticket');
    }

    public function orders()
    {
      return $this->hasMany('App\Order');
    }

    public function country()
    {
      return $this->belongsTo('App\Country');
    }

    public function city()
    {
      return $this->belongsTo('App\City');
    }
}
