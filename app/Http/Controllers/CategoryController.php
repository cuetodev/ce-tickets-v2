<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
      $categories = Category::all();

      return view('categories-list', compact('categories'));
    }

    public function show($slug)
    {
      $category = Category::where('slug', '=', $slug)->firstOrFail();

      return view('category-single', compact('category'));
    }
}
