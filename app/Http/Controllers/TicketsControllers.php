<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Ticket;

class TicketsControllers extends Controller
{
    public function all()
    {
      $tickets =  Ticket::where('user_id', auth()->user()->id)->where('checked', 0)->get();

      return view('print.tickets', compact('tickets'));
    }

    public function single($ticket_id)
    {
      $ticket = Ticket::where('id', $ticket_id)->where('checked', 0)->where('user_id', auth()->user()->id)->first();

      return view('print.single_ticket', compact('ticket'));
    }
}
