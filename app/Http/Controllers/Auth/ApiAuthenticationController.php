<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Social;
use App\Role;

use App\Mail\Welcome;

class ApiAuthenticationController extends Controller
{
    public function login(Request $request)
    {
      $email = $request->input('email');
      $password = $request->input('password');

      if (Auth::attempt(['email' => $email, 'password' => $password])) {

        return response()->json(['api_token' => $request->user()->api_token], 200);

      }

      return response()->json(['error' => 'please check your credentials'], 404);
    }

    public function register(Request $request)
    {

      $validation = Validator::make($request->all(), [
      'first_name' => 'required|string|max:255',
      'last_name' => 'required|string|max:255',
      'username' => 'required|string|max:30|unique:users,username',

      'phone_number' => 'required|string|max:20',
      'gender' => 'nullable|string|max:20',
      'birth_date' => 'nullable|date',

      'email' => 'required|string|email|max:255|unique:users,email',
      'password' => 'required|string|min:8|confirmed',
      ]);

      if ($validation->fails()) {

        return response()->json($validation->messages(), 200);

      } else {
        $user = User::create([
          'first_name' => $request->input('first_name'),
          'last_name' => $request->input('last_name'),
          'username' => $request->input('username'),

          'phone_number' => $request->input('phone_number'),
          'gender' => $request->input('gender'),
          'birth_date' => $request->input('birth_date'),

          'email' => $request->input('email'),
          'password' => bcrypt($request->input('password')),
          'api_token' => str_random(60),
        ]);

        $user->assignRole(Role::where('name', 'client')->first());

        \Mail::to($user)->send(new Welcome($user));

        return $user;

        return response()->json($user, 201);
      }
    }

    public function socialAuth( Request $request, $provider )
    {

      $socialUser = null;

      $userCheck = User::where('email', '=', $request->input('email'))->first();

      $email = $request->input('email');

      if (!empty($userCheck)) {
        $socialUser = $userCheck;
      } else {

        $sameSocialId = Social::where('provider_id', '=', $request->input('uid'))->where('provider', '=', $provider)->first();

        if (empty($sameSocialId)) {

          $newSocialUser = new User;

          $newSocialUser->email = $email;

          $newSocialUser->first_name = $request->input('first_name');

          $newSocialUser->last_name = $request->input('last_name');

          $newSocialUser->password = bcrypt(str_random(20));
          $newSocialUser->avatar = $request->input('avatar');

          $newSocialUser->api_token = str_random(60);

          $newSocialUser->save();

          $socialData = new Social;

          $socialData->provider_id = $request->input('uid');
          $socialData->provider = $provider;

          $newSocialUser->social()->save($socialData);

          $newSocialUser->assignRole(Role::where('name', 'client')->first());

          $socialUser = $newSocialUser;

        } else {

          $socialUser = $sameSocialId->user;

        }

      }

      // auth()->login($socialUser, true);

      return response()->json(['api_token' => $socialUser->api_token], 200);

    }


}
