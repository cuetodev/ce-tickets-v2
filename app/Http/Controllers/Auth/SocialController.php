<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Config;

use App\Http\Controllers\Controller;

use App\Social;
use App\User;
use App\Role;

class SocialController extends Controller
{
    public function getSocialRedirect($provider )
    {

      $providerKey = Config::get( 'services.' . $provider );

      if ( empty($providerKey) ) {

        return view('welcome')->with('error', 'No such provider');

      }

      return Socialite::driver( $provider )->redirect();

    }

    public function getSocialHandle(Request $request, $provider )
    {
      if ( $request->input('denied') != '' ) {

        return redirect('/login')->with('status', 'danger')->with('message', 'You did not share your profile data with our social app.');

      }

      $user = Socialite::driver( $provider )->user();

      $socialUser = null;

      $userCheck = User::where('email', '=', $user->email)->first();

      $email = $user->email;

      if (!$user->email) {
        $email = 'missing' . str_random(10);
      }

      if (!empty($userCheck)) {
        $socialUser = $userCheck;
      } else {

        $sameSocialId = Social::where('provider_id', '=', $user->id)->where('provider', '=', $provider)->first();

        if (empty($sameSocialId)) {

          $newSocialUser = new User;

          $newSocialUser->email = $email;

          $name = explode(' ', $user->name);

          if (count($name) >= 1) {
            $newSocialUser->first_name = $name[0];
          }

          if (count($name) >= 2) {
            $newSocialUser->last_name = $name[1];
          }

          $newSocialUser->password = bcrypt(str_random(20));
          $newSocialUser->avatar = $user->avatar;

          $newSocialUser->api_token = str_random(60);

          $newSocialUser->save();

          $socialData = new Social;

          $socialData->provider_id = $user->id;
          $socialData->provider = $provider;

          $newSocialUser->social()->save($socialData);

          $newSocialUser->assignRole(Role::where('name', 'client')->first());

          $socialUser = $newSocialUser;

        } else {

          $socialUser = $sameSocialId->user;

        }

      }

      auth()->login($socialUser, true);

      if ( auth()->user()->hasRole('client') ) {
        return redirect('/dashboard#settings');
      }

      if ( auth()->user()->hasRole('manager') ) {
        return redirect('/dashboard');
      }

      if ( auth()->user()->hasRole('organizer') ) {
        return redirect('/dashboard');
      }

    }
}
