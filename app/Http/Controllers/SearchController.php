<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Event;

class SearchController extends Controller
{
    public function index(Request $request, Event $events)
    {
      $events = $events->newQuery();

      if ($request->has('keyword')) {
        $events->search($request->input('keyword'));

      }

      if ($request->has('category')) {
        $events->join('categories','categories.id','=','events.category_id')->where('categories.name', $request->input('category'));
        // $events->whereHas('category', function ($query) use ($request) {
        //   return $query->where('name', $request->input('category') );
        // } );
      }

      if ($request->has('location')) {
        $events->where( 'location', 'like', '%'.$request->input('location').'%' );
      }

      if ($request->has('stars')) {
        $events->whereHas('reviews', function ($query) use ($request) {
          return $query->where( 'rating', $request->input('stars') );
        });
      }

      if ($request->has('price')) {
        $events->join('entrances','entrances.event_id','=','events.id')->where( 'price', '>=', (float)$request->input('price'))->where('quantity_availables', '!=', (int)0);

        // $events->whereHas('type_entrances', function ($query) use ($request) {
        //   return $query->where( 'price', '>=', (float)$request->input('price'))->where('quantity_availables', '!=', (int)0);
        // });

      }

      if ($request->has('date') && $request->input('date') != 'all day' ) {

        if ($request->input('date') == 'today') {
          $events->where('start_date', Carbon::parse('today'))
          ->orWhere('end_date', Carbon::parse('today'));

        }

        if ($request->input('date') == 'tomorrow') {
          $events = $events->where('start_date', Carbon::parse('tomorrow')
          )->orWhere('end_date', Carbon::parse('tomorrow'));
        }

        if ($request->input('date') == 'this-week') {
          $events->whereBetween('start_date', array(Carbon::parse('this week'),Carbon::parse('next week')))
            ->orWhere('end_date','<', Carbon::parse('next week')
          );
        }

        if ($request->input('date') == 'next-week') {
          $events->whereDate('start_date','<', Carbon::parse('next week')
          )->orWhere('end_date','>', Carbon::parse('next week'));
        }

        if ($request->input('date') == 'next-month') {
          $events->whereBetween('start_date',array(Carbon::parse('next month'),Carbon::parse('next month')->addMonths(01))
          )->orWhere('end_date','>', Carbon::parse('next month')->addMonths(01));
        }

        if ($request->input('date') == 'this-month') {
          $events->whereBetween('start_date', array(Carbon::parse('this month'),Carbon::parse('next month'))
          )->orWhere('end_date','<', Carbon::parse('next month')
          );
        }

      }

      if ($request->has('dayOfWeek')) {

        $dates = Carbon::parse($request->input('dayOfWeek'));

        $events->where('start_date', $dates)
        ->orWhere('end_date', $dates);
      }

      $searched = $events->where('status', '!=', 'ended')->where('privacy', 'public')->select('events.*')->orderBy('events.created_at', 'DESC')->paginate(6);
      // var_dump($events);exit();ß
      return view('search', compact('searched'));

    }
}
