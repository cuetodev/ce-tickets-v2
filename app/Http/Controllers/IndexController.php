<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;

use App\Category;

use App\City;
use App\Country;

use Carbon\Carbon;

class IndexController extends Controller
{
    public function index()
    {

      $populars = Event::where('privacy', 'public')
                          ->where('status', '!=', 'ended')
                          ->orderByDesc('visits')->get();

      $categories = Category::withCount('events')->orderBy('events_count', 'DESC')->get();

      $todays = Event::where('start_date', Carbon::parse('today'))
      ->orWhere('end_date', Carbon::parse('today'))
      ->where('status', '!=', 'ended')
      ->take(4)->get();

      $popularCities = City::withCount('events')->orderBy('events_count', 'DESC')->take(4)->get();

      return view('index', compact(['todays', 'categories', 'populars', 'popularCities']));
    }
}
