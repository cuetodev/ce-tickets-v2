<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;
use App\Review;

class ReviewController extends Controller
{
    public function store(Event $event, Request $request)
    {

      // $path = null;

      // if ($request->hasFile('picture')) {
      //
      //   $path = $request->file('picture')->move(
      //     'uploads',
      //     time().str_random(5).'.'.$request->file('picture')->getClientOriginalExtension()
      //   );
      //
      // }

      Review::create([
        'comment' => "I am a review",
        'rating' => $request->input('rating'),
        // 'picture' => url($path),
        'user_id' => auth()->user()->id,
        'event_id' => $event->id,
      ]);

      return back();
    }
}
