<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;

use App\Notifications\InvoicePaid;

use Srmklive\PayPal\Services\ExpressCheckout;

use App\Order;
use App\OrderItem;
use App\Entrance;

use App\Ticket;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class OrderController extends Controller
{
    public function makeOrder()
    {
      $order = auth()->user()->orders()->where('status', 'pending')->first();

      if (!is_null($order)) {

        return redirect('check-out/pay');

      } else {

        if (!is_null(Cart::content())) {

          $order = new Order();
          $order->user_id = auth()->user()->id;
          $order->save();

          foreach (Cart::content() as $item) {

            OrderItem::create([
              'order_id' => $order->id,
              'event_id' => Entrance::find($item->id)->event_id,
              'entrance_id' => (int)$item->options->type_ticket['id'],
              'event_name' => $item->name,
              'entrance_type' => $item->options->type_ticket['name'],
              'quantity' => $item->qty,
              'price' => $item->price,
              'tax' => $item->tax(),
              'subtotal' => $item->total(2, '.', ''),
            ]);

            $type_ticket = Entrance::find((int)$item->options->type_ticket['id']);

            //Updating Quantity Availables
            $type_ticket->update([
              'quantity_availables' => ($type_ticket->quantity_availables - (int)$item->qty)
            ]);

          }

          return redirect('check-out/pay');

        } else {

          return back();

        }

      }
    }

    public function checkOut()
    {
      $order = auth()->user()->orders()->where('status', 'pending')->first();

      $total = 0;
      foreach ($order->items as $item) {
        $total = $item->subtotal + $total;
      }


      return view('check-out', compact(['order', 'total']));
    }

    public function cancel()
    {
      $order = auth()->user()->orders()->where('status', 'pending')->first();

      $items = OrderItem::where('order_id', $order->id);

      foreach ($items->get() as $item) {

        $type_ticket = Entrance::find($item->entrance_id);

        //Updating Quantity Availables
        $type_ticket->update([
          'quantity_availables' => ($type_ticket->quantity_availables + $item->quantity)
        ]);

      }

      $items->delete();

      $order->delete();

      Cart::destroy();

      return redirect('/');
    }

    public function success(Request $request)
    {
      $provider = new ExpressCheckout();

      $token = $request->get('token');
      $payerId = $request->get('PayerID');

      $order = auth()->user()->orders()->where('status', 'pending')->first();

      $data = [];

      $data['items'] = collect([]);

      foreach ($order->items as $item) {
        $data['items']->push([
          'name' => $item->event_name.'-'.$item->entrance_type,
          'price' => ($item->price + $item->tax),
          'qty' => $item->quantity,
        ]);
      }

      $data['invoice_id'] = 'CETICKETS'.'_'.$order->id;
      $data['invoice_description'] = "Order #{$order->id} Invoice";

      $data['return_url'] = url('/payment/success');
      $data['cancel_url'] = url('/order/cancel');

      $total = 0;

      foreach ($data['items'] as $item) {
        $total += $item['price'] * $item['qty'];
      }

      $data['total'] = $total;

      //Verify Express Checkout token
      $response = $provider->getExpressCheckoutDetails($token);

      if (in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {

        $payment_status = $provider->doExpressCheckoutPayment($data, $token, $payerId);
        $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];

        // $order = auth()->user()->orders()->where('status', 'pending')->first();

        $order->update([
          'status' => 'paid'
        ]);

        $items = $order->items;

        foreach ($items as $item) {

          //Generating the new tickets
          $n = 1;

          while ($n <= $item->quantity) {

            //Fix: generate tickets with the quantity...
            $ticket = new Ticket;

            $ticket->user_id = auth()->user()->id;
            $ticket->event_id = $item->event_id;
            $ticket->generateCode();
            $ticket->price = $item->price;
            $ticket->entrance_id = $item->entrance_id;

            $ticket->save();

            $n++;
          }
        }

        Cart::destroy();

        auth()->user()->notify(new InvoicePaid($order));

        // return dd($request);

        return view('success');

      }
    }

    public function successAlt(Request $request)
    {
      // $provider = new ExpressCheckout();

      // $token = $request->get('token');
      // $payerId = $request->get('PayerID');

      $order = auth()->user()->orders()->where('status', 'pending')->first();

      $data = [];

      $data['items'] = collect([]);

      foreach ($order->items as $item) {
        $data['items']->push([
          'name' => $item->event_name.'-'.$item->entrance_type,
          'price' => ($item->price + $item->tax),
          'qty' => $item->quantity,
        ]);
      }

      $data['invoice_id'] = 'CETICKETS'.'_'.$order->id;
      $data['invoice_description'] = "Order #{$order->id} Invoice";

      $data['return_url'] = url('/payment/success');
      $data['cancel_url'] = url('/order/cancel');

      $total = 0;

      foreach ($data['items'] as $item) {
        $total += $item['price'] * $item['qty'];
      }

      $data['total'] = $total;

      //Verify Express Checkout token
      // $response = $provider->getExpressCheckoutDetails($token);

      if ($data['total'] == (float)0) {

        // $payment_status = $provider->doExpressCheckoutPayment($data, $token, $payerId);
        // $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];

        // $order = auth()->user()->orders()->where('status', 'pending')->first();

        $order->update([
          'status' => 'paid'
        ]);

        $items = $order->items;

        foreach ($items as $item) {

          //Generating the new tickets
          $n = 1;

          while ($n <= $item->quantity) {

            //Fix: generate tickets with the quantity...
            $ticket = new Ticket;

            $ticket->user_id = auth()->user()->id;
            $ticket->event_id = $item->event_id;
            $ticket->generateCode();
            $ticket->price = $item->price;
            $ticket->entrance_id = $item->entrance_id;

            $ticket->save();

            $n++;
          }
        }

        Cart::destroy();

        auth()->user()->notify(new InvoicePaid($order));

        // return dd($request);

        return view('success');

      }
    }

    public function payment()
    {
      $order = auth()->user()->orders()->where('status', 'pending')->first();

      if ($order->items->sum('price') == 0) {
         return redirect('/payment/f/success');
      }


      return (new PaypalController())->pay($order);

      $provider = new ExpressCheckout();
      $data = [];

      $data['items'] = collect([]);

      foreach ($order->items as $item) {
        $data['items']->push([
          'name' => $item->event_name.'-'.$item->entrance_type,
          'price' => ($item->price + $item->tax),
          'qty' => $item->quantity,
        ]);
      }

      $data['invoice_id'] = 'CETICKETS'.'_'.$order->id;
      $data['invoice_description'] = "Order #{$order->id} Invoice";

      $data['return_url'] = url('/payment/success');
      $data['cancel_url'] = url('/order/cancel');

      $total = 0;

      foreach ($data['items'] as $item) {
        $total += $item['price'] * $item['qty'];
      }

      $data['total'] = $total;

      $response = $provider->setExpressCheckout($data);

      if ($data['total'] == (float)0) {
        return redirect('/payment/f/success');
      }

      // return redirect($response['paypal_link']);

      return dd($response);
    }

    public function invoice($invoice_id)
    {
      $order = Order::where('user_id', auth()->user()->id)->where('id', $invoice_id)->first();

      return view('print.invoice', compact('order'));
    }

    public function stripePayment()
    {
        $order = auth()->user()->orders()->where('status', 'pending')->first();
        $total = 0;
        foreach ($order->items as $item) {
            $total += $item->quantity * ($item->price + $item->tax);
        }

        Stripe::setApiKey(config('services.stripe.secret'));

        $customer = Customer::create([
            "email" => request('stripeEmail'),
            "source" => request('stripeToken')
        ]);

        $charge = Charge::create([
            "customer" => $customer->id,
            "amount" => $total * 100,
            "description" => 'CETICKETS'.'_'.$order->id,
            "currency" => "usd"
        ]);


        Cart::destroy();

        $order->update([
            'status' => 'paid',
            'transaction_id' => $charge->id
        ]);

        $items = $order->items;

        foreach ($items as $item) {

            //Generating the new tickets
            $n = 1;

            while ($n <= $item->quantity) {

                //Fix: generate tickets with the quantity...
                $ticket = new Ticket;

                $ticket->user_id = auth()->user()->id;
                $ticket->event_id = $item->event_id;
                $ticket->generateCode();
                $ticket->price = $item->price;
                $ticket->entrance_id = $item->entrance_id;

                $ticket->save();

                $n++;
            }
        }

        auth()->user()->notify(new InvoicePaid($order));

        return redirect('/order/stripe/success');
    }

    public function stripeSuccess(){
        return view('success');
    }
}
