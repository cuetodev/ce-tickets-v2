<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Country;

class CountryController extends Controller
{
  public function index()
  {
    $countries = Country::all();

    return view('countries-list', compact('countries'));
  }

  public function show($slug)
  {
    $country = Country::where('slug', '=', $slug)->firstOrFail();

    return view('country-single', compact('country'));
    // return dd($country->cities);
  }
}
