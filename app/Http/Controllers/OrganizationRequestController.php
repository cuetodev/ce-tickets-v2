<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\OrganizationRequest;
use App\Organization;

use App\Mail\OrganizationRequestReceived;

class OrganizationRequestController extends Controller
{
    public function index(Request $request)
    {
      return view('organization-form');
    }

    public function makeRequest(Request $request)
    {
      $this->validate($request, [
        'name_organization' => 'required|string|max:255',
        'email_organization' => 'required|string|max:255|unique:organization_requests,email|email',
        'email_organization' => 'required|string|max:255|unique:organizations,email|email',
        'description_organization' => 'required|string|max:2000',
        'phone_organization' => 'required|numeric',
        'facebook_link' => 'url|nullable',
        'twitter_link' => 'url|nullable',
        'instagram_link' => 'url|nullable',
        'website' => 'url',
        'picture_organization' => 'required|file|image'
      ]);

      $organizationRequest = new OrganizationRequest;

      if ($request->has('name_organization')) {
        $organizationRequest->name = $request->input('name_organization');
      }

      if ($request->has('email_organization')) {
        $organizationRequest->email = $request->input('email_organization');
      }

      if ($request->has('description_organization')) {
        $organizationRequest->description = $request->input('description_organization');
      }

      if ($request->has('phone_organization')) {
        $organizationRequest->phone = $request->input('phone_organization');
      }

      if ($request->has('facebook_link')) {
        $organizationRequest->facebook_link = $request->input('facebook_link');
      }

      if ($request->has('twitter_link')) {
        $organizationRequest->twitter_link = $request->input('twitter_link');
      }

      if ($request->has('instagram_link')) {
        $organizationRequest->instagram_link = $request->input('instagram_link');
      }

      if ($request->has('website')) {
        $organizationRequest->website = $request->input('website');
      }

      if ($request->hasFile('picture_organization')) {

        $file_name = str_random(9).'_'.time().'.'.$request->file('picture_organization')->getClientOriginalExtension();

        $file_path = $request->file('picture_organization')->move(
          'picturesorg',
          $file_name
        );

        $organizationRequest->picture = url($file_path);

      }

      $organizationRequest->user_id = auth()->user()->id;

      $organizationRequest->save();

      \Mail::to($organizationRequest->email)->send(new OrganizationRequestReceived);

      return redirect('dashboard');
    }

    public function is_organizer()
    {
      // verify if is organizer
      $is_organizer = Organization::where('user_id',auth()->user()->id)->count('id');
      if ($is_organizer > 0) {
        // true
          // return you are already an organizer
        // link
        return redirect('dashboard');

      } else {
        // verify if has a request
        $organizer_request = OrganizationRequest::where('user_id',auth()->user()->id)->count('id');
        if ($organizer_request > 0) {

          return redirect('dashboard')->with('organizer_request', 'You have an organization request in proccess!');

        } else {
          // return form to make request
          return redirect('organization/become');
        }
      }
    }
}
