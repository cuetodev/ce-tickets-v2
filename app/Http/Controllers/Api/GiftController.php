<?php

namespace App\Http\Controllers\Api;

use App\Gift;
use App\User;
use App\Ticket;
use App\Event;
use App\Entrance;

use App\Mail\GiftInvitation;
use App\Mail\GiftAccepted;
use App\Mail\GiftRejected;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GiftController extends Controller
{
    public function getGiftsReceived()
    {
      $gift_to_me = Gift::where('receiver', auth()->user()->email)->where('accepted', 0)->get();

      return response()->json($gift_to_me, 200);
    }

    public function getGiftsGifted()
    {
      $gifted = Gift::where('giver', auth()->user()->email)->get();

      return response()->json($gifted, 200);
    }

    public function generateGift(Request $request, Ticket $ticket)
    {
      $gift = new Gift;

      $gift->accepted = 0;

      $gift->giver = auth()->user()->email;
      $gift->receiver = $request->input('email_receiver');
      $gift->event_id = $ticket->event_id;
      $gift->entrance_id = $ticket->entrance_id;

      $gift->save();

      //We'll going to notify user by email now

      \Mail::to($gift->receiver)->send(new GiftInvitation($gift));

      // Destroy the ticket

      $ticket->delete();

      return response()->json([
        'message' => 'Gift Created and Email has been sended'
      ], 201);

    }

    public function acceptGift(Gift $gift, Request $request)
    {
      if (auth()->user()->email == $gift->receiver) {
        if ($request->has("accept")) {
          $gift->accepted = 1;
          $gift->save();
        }

        // Generate the ticket

        $ticket = new Ticket();

        $ticket->user_id = auth()->user()->id;
        $ticket->event_id = $gift->event_id;
        $ticket->entrance_id = $gift->entrance_id;

        $ticket->price = 0.00;
        $ticket->generateCode();

        $ticket->save();

        // Send a notification to email.

        \Mail::to($gift->giver)->send(new GiftAccepted($gift));

      }

      return response()->json([
        'message' => 'Gift Accepted and Email has been sended'
      ], 201);

    }

    public function rejectGift(Gift $gift, Request $request)
    {
      if (auth()->user()->email == $gift->receiver) {
        if ($request->has("reject")) {
          $gift->accepted = 0;
          $gift->save();
        }

        // Generate the ticket to original user back.

        $ticket = new Ticket();

        $ticket->user_id = User::where('email', $gift->giver)->first()->id;
        $ticket->event_id = $gift->event_id;
        $ticket->entrance_id = $gift->entrance_id;

        $ticket->price = 0.00;
        $ticket->generateCode();

        $ticket->save();

        // Send a notification to email

        \Mail::to($gift->giver)->send(new GiftRejected($ticket));

        $gift->delete();

      }

      return response()->json([
        'message' => 'Gift Rejected and Email has been sended'
      ], 201);
    }
}
