<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\OrganizationRequest;
use App\Organization;
use App\User;
use App\Role;

use App\Mail\OrganizationRequestAproved;
use App\Mail\OrganizationRequestRejected;
use App\Mail\OrganizationNotifyToAdmin;

class OrganizationRequestController extends Controller
{
    public function aprove(OrganizationRequest $organizationRequest)
    {
      if (auth()->user()->hasRole('manager')) {
        $organization = new Organization;

        $organization->name = $organizationRequest->name;
        $organization->description = $organizationRequest->description;
        $organization->phone = $organizationRequest->phone;
        $organization->facebook_link = $organizationRequest->facebook_link;
        $organization->twitter_link = $organizationRequest->twitter_link;
        $organization->instagram_link = $organizationRequest->instagram_link;
        $organization->email = $organizationRequest->email;
        $organization->website = $organizationRequest->website;
        $organization->picture = $organizationRequest->picture;
        $organization->user_id = $organizationRequest->user_id;

        $organization->activated = 1;

        $organization->save();

        $user = User::find($organization->user_id);

        $user->assignRole(Role::where('name', 'organizer')->first());

        $user->removeRole(Role::where('name', 'client')->first());

        $organizationRequest->delete();

        \Mail::to($organization->email)->send(new OrganizationRequestAproved);

        \Mail::to( Role::find(3)->users->first->get()->email )->send(new OrganizationNotifyToAdmin);

        return response()->json(['message' => "Approved"], 200);
      }

      return response()->json(['error' => "You not are authorized for this action"], 401);
    }

    public function reject(OrganizationRequest $organizationRequest)
    {
      if (auth()->user()->hasRole('manager')) {

        \Mail::to($organizationRequest->email)->send(new OrganizationRequestRejected);

        $organizationRequest->delete();

        return response()->json(['message' => "Rejected and Deleted"], 200);
      }

      return response()->json(['error' => "You not are authorized for this action"], 401);
    }

    public function index()
    {
      return response()->json(OrganizationRequest::all(), 200);
    }
}
