<?php

namespace App\Http\Controllers\Api;

use App\User;

use Fractal;
use App\Transformers\UserTransformer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return User::all();

        return Fractal::collection(User::all(), new UserTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return Fractal::item($user, new UserTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->update($request->all());

        return response()->json($user, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(['No Content'], 204);
    }

    /**
     * Assign a Role to User
     *
     * @param int $user
     * @param int $role
     */
     public function assignRole(User $user, $role)
     {
       $user->assignRole($role);

       return Fractal::item($user, new UserTransformer);
     }

     /**
      * Remove a Role to User
      *
      * @param int $user
      * @param int $role
      */
      public function removeRole(User $user, $role)
      {
        $user->removeRole($role);

        return Fractal::item($user, new UserTransformer);
      }
}
