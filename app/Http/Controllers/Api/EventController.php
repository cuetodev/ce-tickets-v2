<?php

namespace App\Http\Controllers\Api;

use App\Date;
use App\Event;
use App\Category;

use App\EventDay;
use Carbon\Carbon;
use Fractal;

use App\Transformers\EventTransformer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     /**
     * status: ended, available, sold-out
     *
     */

    public function index(Request $request)
    {
      if ($request->has('status')) {
        return Fractal::collection(Event::where('status', $request->input('status'))->get(), new EventTransformer);
      } else {
        return Fractal::collection(Event::all(), new EventTransformer);
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = Event::create($request->except(['week_days']));

        $event->slug = str_slug($event->name.' '.$event->id, '-');

        $event->share_code = str_random(8);

        if ($request->has('is_recurrent') && $request->has('week_days')) {

            $event->is_recurrent = 1;
            foreach ($request->get('week_days') as $day) {
                $event->days()->create(['day' => $day]);
            }
        }


        $event->save();

        return Fractal::item($event, new EventTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        return Fractal::item($event, new EventTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
      $event->update($request->all());



      if ($request->has('auto')) {
        $event->slug = str_slug($event->name.' '.$event->id, '-');

        $event->share_code = str_random(8);
        
        $event->save();
      }

      return Fractal::item($event, new EventTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $event->delete();

        return response()->json(null, 204);
    }

    public function assignCategory(Event $event, Category $category)
    {
      $event->category()->associate($category);

      $event->save();

      return Fractal::item($event, new EventTransformer);
    }

    public function assignEntrance(Event $event, $entrance)
    {
      $event->assignEntrance($entrance);

      return Fractal::item($event, new EventTransformer);
    }

    public function removeEntrance(Event $event, $entrance)
    {
      $event->removeEntrance($entrance);

      return Fractal::item($event, new EventTransformer);
    }

    public function mostVisited()
    {

        $events = Event::where('privacy', 'public')
                        ->where('status', '!=', 'ended')
                        ->where('end_date', '>=', \Carbon\Carbon::now()->format('Y-m-d'))
                        ->orderByDesc('visits')->get();

        return Fractal::collection($events, new EventTransformer);
    }

    public function recurrentEvents()
    {
        $first_date = request('first_event_date');
        $first_time = request('first_event_time');
        $end_time = request('end_time');
        $first_time_part = explode(':', $first_time);
        $period = request('period');
        $period_times = request('period_times');
        $week_days = request('week_days');

        $starting_date = Carbon::parse($first_date);
        $dates = array();
        array_push($dates, $starting_date->format('Y-m-d'));

        for ($i = 0; $i < $period_times; $i++) {
            if ($period == 1) {
                foreach ($week_days as $day) {
                    array_push($dates, $starting_date
                        ->modify('next ' . $day)->format('Y-m-d')
                    );
                }
                $starting_date->addWeek()
                    ->startOfWeek();

            } elseif ($period == 2) {
                $starting_date->addMonth();
                array_push($dates, $starting_date->format('Y-m-d'));
            }
        }

        $event_id = 3; // TODO testing purpose
        foreach ($dates as $date) {
            Date::create([
                'event_id' => $event_id,
                'start_date' => $date,
                'start_time' => Carbon::parse($first_time),
                'end_date' => $date,
                'end_time' => Carbon::parse($end_time),
                'quantity_availables' => 30
            ]);
        }

        return $dates;
    }
}
