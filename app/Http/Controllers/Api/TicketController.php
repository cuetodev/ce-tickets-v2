<?php

namespace App\Http\Controllers\Api;

use App\Ticket;

use App\Event;

use Fractal;

use App\Transformers\TicketTransformer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TicketController extends Controller
{
    public function index()
    {
      return Fractal::collection(Ticket::all(), new TicketTransformer);
    }

    public function ticketsByEvent(Event $event)
    {
      return Fractal::collection(Ticket::where('event_id', $event->id)->get(), new TicketTransformer);
    }

    public function checkTicket(Request $request)
    {
      $ticket = Ticket::where('code', '=', $request->input('code'))->first();

      if(is_null($ticket)) {

        return response()->json(null, 204);

      } else {
        if ($ticket->checked == 0) {
          $ticket->checked = 1;

          if ($request->has('staff_id')) {

            $ticket->checked_by = $request->input('staff_id');
            $ticket->save();

          }

          return response()->json(['status' => 'verified', 'data' => $ticket], 200);
        }
        return response()->json(['status' => 'used', 'data' => $ticket], 200);

      }
    }

    public function verifyTicket(Request $request)
    {
      return Fractal::item(Ticket::where('code', '=', $request->input('code'))->first(), new TicketTransformer);
    }
}
