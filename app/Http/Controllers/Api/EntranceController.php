<?php

namespace App\Http\Controllers\Api;

use App\Entrance;

use Fractal;

use App\Transformers\EntranceTransformer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EntranceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Fractal::collection(Entrance::all(), new EntranceTransformer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $entrance = Entrance::create(
            array_merge(
                $request->all(),
                ['original_qty' => $request->get('quantity_availables')])
        );

        return Fractal::item($entrance, new EntranceTransformer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Entrance $entrance)
    {
        return Fractal::item($entrance, new EntranceTransformer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Entrance $entrance)
    {
        $entrance->update($request->all());

        return Fractal::item($entrance, new EntranceTransformer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Entrance $entrance)
    {
        $entrance->delete();

        return response()->json(null, 204);
    }
}
