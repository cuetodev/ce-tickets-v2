<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Fractal;

use App\Staff;
use App\Transformers\StaffTransformer;

use App\Organization;

use App\Event;
use App\Transformers\EventTransformer;

use App\Mail\NewStaff;
use App\Mail\ResetCodeStaff;

class StaffController extends Controller
{
    public function index()
    {
      return Fractal::collection( Staff::all(), new StaffTransformer );
    }

    public function byEvent(Event $event)
    {
      return Fractal::collection( $event->staff, new StaffTransformer );
    }

    public function allEvents(Staff $staff)
    {
      return Fractal::collection( $staff->events, new EventTransformer );
    }

    public function store(Request $request)
    {
      $staff = new Staff();

      if ($request->has('name')) {
        $staff->name = $request->input('name');
      }

      if ($request->has('email')) {
        $staff->email = $request->input('email');
      }

      if ($request->has('organization_id')) {
        $staff->organization_id = $request->input('organization_id');
      }

      $staff->generateCode();

      $staff->save();

      \Mail::to($staff->email)->send(new NewStaff($staff));

      return Fractal::item($staff, new StaffTransformer);
    }

    public function regenerateCode(Staff $staff)
    {
      $staff->generateCode();

      $staff->save();

      \Mail::to($staff->email)->send(new ResetCodeStaff($staff));

      return Fractal::item($staff, new StaffTransformer);
    }

    public function destroy(Staff $staff)
    {
      $staff->delete();

      return response()->json(null, 204);
    }

    public function auth(Request $request)
    {
      if ($request->has('code')) {
        $staff = Staff::where('code', $request->input('code'))->first();

        $api_token = Organization::find($staff->organization_id)->user->api_token;

        return response()->json([
          'api_token' => $api_token,
          'staff' => $staff
        ], 200);
      }

      return response()->json(null, 204);
    }

    public function assignEvent(Staff $staff, $event)
    {
      $staff->assignEvent($event);

      return Fractal::item($staff, new StaffTransformer);
    }

     public function removeEvent(Staff $staff, $event)
     {
       $staff->removeEvent($event);

       return Fractal::item($staff, new StaffTransformer);
     }
}
