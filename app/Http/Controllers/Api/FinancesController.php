<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Event;
use App\Entrance;
use App\Ticket;
use App\Organization;
use Validator;

class FinancesController extends Controller
{
    public function general()
    {
      // $tickets = Ticket::all();
      // Query all the tickets with active events
      $tickets = Ticket::join('events', 'events.id', '=', 'tickets.event_id')->where('events.status','available')->get();

      (float)$money = 0.00;

      foreach ($tickets as $ticket) {
        $money += $ticket->price;
      }

      $collectedMoney = $money;

      $ticketsSelled = $tickets->count();

      $availablesTickets = 0;

      foreach (Entrance::all() as $type_ticket) {
        $availablesTickets += $type_ticket->quantity_availables;
      }

      $percentOfTicketsSelled = ($ticketsSelled * 100) / $availablesTickets;

      return response()->json(
        [
          'collectedMoney' => $collectedMoney,
          'selledTickets' => $ticketsSelled,
          'availablesTickets' => $availablesTickets,
          'percentOfTicketsSelled' => round($percentOfTicketsSelled, 2). '%',
        ]
      );

    }

    public function byEventsGeneral()
    {
      $events = Event::where('events.status','available')->get();

      $statisticData = collect([]);

      foreach ($events as $event) {

        (float)$money = 0.00;

        $ticketsSelled = 0;

        $availablesTickets = 0;

        foreach ($event->tickets as $ticket) {
          $money += $ticket->price;

          // $availablesTickets += $ticket->quantity_availables;
        }

        $ticketsSelled += $event->tickets->count();

        foreach ($event->type_entrances as $entrance) {
          $availablesTickets += $entrance->quantity_availables;
        }

        $collectedMoney = $money;

        $percentOfTicketsSelled = $availablesTickets == 0 ? 0 : ($ticketsSelled * 100) / $availablesTickets;

        $statisticData->push([
          'event_id' => $event->id,
          'collectedMoney' => $collectedMoney,
          'selledTickets' => $ticketsSelled,
          'availablesTickets' => $availablesTickets,
          'percentOfTicketsSelled' => round($percentOfTicketsSelled, 2). '%',
        ]);

      }

      return $statisticData;
    }

    public function byOrganizationAll(Request $request)
    {
      $type = $request->input('type');

      // Getting range type
      if($type != null)
      {
        switch ($type) {
          case 'week':
            $from = date("Y-m-d",strtotime('last monday'));
            $to   = date("Y-m-d",strtotime('next Sunday'));
            break;

          case 'month':
            $from = date("Y-m-01");
            $to   = date("Y-m-t");
            break;

          case 'year':
            $from = date("Y-01-01");
            $to   = date("Y-12-31");
            break;
        }
      }
      else
      {
        // Setting the dates to custom  dates if passed as parameters
        $from = ($request->input('from') != null)  ? $request->input('from') : date("Y-m-d");
        $to   = ($request->input('to') != null)  ? $request->input('to') : date("Y-m-d");

      }

      $events = DB::table('events')
                    ->select(DB::raw('events.organization_id as organization_id,SUM(tickets.price) as collectedMoney,COUNT(*) as selledTickets,entrances.quantity_availables as availablesTickets'))
                    ->join('tickets', 'tickets.event_id', '=', 'events.id')
                    ->join('entrances', 'entrances.event_id', '=', 'events.id')
                    ->where('tickets.created_at','>=',$from)
                    ->where('tickets.created_at','<=',$to)
                    ->groupBy('events.organization_id')
                    ->groupBy('events.id')
                    ->groupBy('quantity_availables')
                    ->get();

      $events_total = array();
      foreach ($events as $key => $row) 
      {
        $events_total[$row->organization_id]['organization_id'] = $row->organization_id;

        if(isset($events_total[$row->organization_id]['collectedMoney']))
        {
          $events_total[$row->organization_id]['collectedMoney'] += $row->collectedMoney;
        }
        else
        {
          $events_total[$row->organization_id]['collectedMoney'] = $row->collectedMoney;
        }

        if(isset($events_total[$row->organization_id]['selledTickets']))
        {
          $events_total[$row->organization_id]['selledTickets'] += $row->selledTickets;
        }
        else
        {
          $events_total[$row->organization_id]['selledTickets'] = $row->selledTickets;
        }

        if(isset($events_total[$row->organization_id]['availablesTickets']))
        {
          $events_total[$row->organization_id]['availablesTickets'] += $row->availablesTickets;
        }
        else
        {
          $events_total[$row->organization_id]['availablesTickets'] = $row->availablesTickets;
        }
        
      }

      $events_total = array_values($events_total);

      foreach ($events_total as $key => $row) 
      {
        $events_total[$key]['percentOfTicketsSelled'] = number_format(($row['selledTickets'] * 100) / $row['availablesTickets'],2);
      }

      return response()->json($events_total);
    }

    public function byEventsAll($organization,Request $request)
    {
      $type = $request->input('type');
      // Getting range type
      if($type != null)
      {
        switch ($type) {
          case 'week':
            $from = date("Y-m-d",strtotime('last monday'));
            $to   = date("Y-m-d",strtotime('next Sunday'));
            break;

          case 'month':
            $from = date("Y-m-01");
            $to   = date("Y-m-t");
            break;

          case 'year':
            $from = date("Y-01-01");
            $to   = date("Y-12-31");
            break;
        }
      }
      else
      {
        // Setting the dates to custom  dates if passed as parameters
        $from = ($request->input('from') != null)  ? $request->input('from') : date("Y-m-d");
        $to   = ($request->input('to') != null)  ? $request->input('to') : date("Y-m-d");

      }

      $events = DB::table('events')
                    ->select(DB::raw('events.id as event_id,SUM(tickets.price) as collectedMoney,COUNT(*) as selledTickets,entrances.quantity_availables as availablesTickets'))
                    ->join('tickets', 'tickets.event_id', '=', 'events.id')
                    ->join('entrances', 'entrances.event_id', '=', 'events.id')
                    ->where('tickets.created_at','>=',$from)
                    ->where('tickets.created_at','<=',$to)
                    ->where('events.organization_id',$organization)
                    ->groupBy('events.id')
                    ->groupBy('quantity_availables')
                    ->get();

      return response()->json($events);
    }

    public function byOrganization($organization_id)
    {
      $events = Event::where('organization_id', $organization_id)->where('status','available')->get();

      (float)$money = 0.00;

      $ticketsSelled = 0;

      $availablesTickets = 0;

      foreach ($events as $event) {
        foreach ($event->tickets as $ticket) {
          $money += $ticket->price;

          // $availablesTickets += $ticket->quantity_availables;
        }

        $ticketsSelled += $event->tickets->count();

        foreach ($event->type_entrances as $entrance) {
          $availablesTickets += $entrance->quantity_availables;
        }
      }

      $collectedMoney = $money;

      $percentOfTicketsSelled = ($ticketsSelled * 100) / $availablesTickets;

      return response()->json(
        [
          'collectedMoney' => $collectedMoney,
          'selledTickets' => $ticketsSelled,
          'availablesTickets' => $availablesTickets,
          'percentOfTicketsSelled' => round($percentOfTicketsSelled, 2). '%',
        ]
      );
    }

    public function byEventsofOrganizator($organization_id)
    {
      $events = Event::where('organization_id', $organization_id)->where('status','available')->get();

      $statisticData = collect([]);

      foreach ($events as $event) {

        (float)$money = 0.00;

        $ticketsSelled = 0;

        $availablesTickets = 0;

        foreach ($event->tickets as $ticket) {
          $money += $ticket->price;

          // $availablesTickets += $ticket->quantity_availables;
        }

        $ticketsSelled += $event->tickets->count();

        foreach ($event->type_entrances as $entrance) {
          $availablesTickets += $entrance->quantity_availables;
        }

        $collectedMoney = $money;

        $percentOfTicketsSelled = $availablesTickets == 0 ? 0 : ($ticketsSelled * 100) / $availablesTickets;

        $statisticData->push([
          'event_id' => $event->id,
          'collectedMoney' => $collectedMoney,
          'selledTickets' => $ticketsSelled,
          'availablesTickets' => $availablesTickets,
          'percentOfTicketsSelled' => round($percentOfTicketsSelled, 2). '%',
        ]);

      }

      return $statisticData;
    }

    public function payEvent(Request $request) 
    {
      $e         = $request->input('event');
      $event     = Event::find($e);


       $validator = Validator::make($request->all(), [
            'reference' => 'required',
            'paid_date' => 'required|date',
        ]);


      if($event != null && !$validator->fails() && $event->status == 'ended') 
      {
          $reference = $request->input('reference');
          $paid_date = $request->input('paid_date');
          // Updating the event data
          $event->reference = $reference;
          $event->paid_date = $paid_date;
          $event->paid      = true;
          $event->save();
          return response()->json([
            'message'=> 'Event updated'
          ]);
      }
      else {
          $error = 'invalid params';
      }

      if($event->status != 'ended') {
          $error = 'event can not be payed';

      }

      return response()->json([
            'erros'=> $error
      ]);

    }

    public function byOrganizationPaid($organization_id)
    {
    
      $events = DB::table('events')
                    ->select(DB::raw('events.id,events.organization_id as organization_id,events.paid,events.paid_date,events.reference   '))
                    // ->join('tickets', 'tickets.event_id', '=', 'events.id')
                    // ->join('entrances', 'entrances.event_id', '=', 'events.id')
                    ->where('events.paid','=',1)
                    ->where('events.organization_id','=',$organization_id)
                    ->get();

   


      return response()->json($events);
    }

    public function byEvent(Event $event)
    {
      (float)$money = 0.00;

      $ticketsSelled = 0;

      $availablesTickets = 0;

      foreach ($event->tickets as $ticket) {
        $money += $ticket->price;

        // $availablesTickets += $ticket->quantity_availables;
      }

      $ticketsSelled += $event->tickets->count();

      foreach ($event->type_entrances as $entrance) {
        $availablesTickets += $entrance->quantity_availables;
      }

      $collectedMoney = $money;

      $percentOfTicketsSelled = $availablesTickets == 0 ? 0 : ($ticketsSelled * 100) / $availablesTickets;

      return response()->json(
        [
          'collectedMoney' => $collectedMoney,
          'selledTickets' => $ticketsSelled,
          'availablesTickets' => $availablesTickets,
          'percentOfTicketsSelled' => round($percentOfTicketsSelled, 2). '%',
        ]
      );
    }
}
