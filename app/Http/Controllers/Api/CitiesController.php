<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\City;

use Fractal;

use App\Transformers\CityTransformer;

class CitiesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      return Fractal::collection( City::all(), new CityTransformer );
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $city = City::create($request->all());

      $city->slug = str_slug($city->name.' '.$city->id, '-');

      $city->save();

      return Fractal::item($city, new CityTransformer);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(City $city)
  {
      return Fractal::item($city, new CityTransformer);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, City $city)
  {
      $city->update($request->all());

      $city->slug = str_slug($city->name.' '.$city->id, '-');

      $city->save();

      return Fractal::item($city, new CityTransformer);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(City $city)
  {
      $city->delete();

      return response()->json(null, 204);

  }
}
