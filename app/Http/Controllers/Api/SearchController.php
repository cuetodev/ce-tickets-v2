<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Fractal;

use Carbon\Carbon;

use App\Event;

use App\Transformers\EventTransformer;

class SearchController extends Controller
{
  public function index(Request $request, Event $events)
  {
    $events = $events->newQuery();

    if ($request->has('keyword')) {
      $events->search($request->input('keyword'));
    }

    if ($request->has('category')) {
      $events->whereHas('category', function ($query) use ($request) {
        return $query->where( 'name', $request->input('category') );
      } );
    }

    if ($request->has('location')) {
      $events->where( 'location', 'like', '%'.$request->input('location').'%' );
    }

    if ($request->has('stars')) {
      $events->whereHas('reviews', function ($query) use ($request) {
        return $query->where( 'rating', $request->input('stars') );
      });
    }

    if ($request->has('price')) {
      $events->whereHas('type_entrances', function ($query) use ($request) {
        return $query->where( 'price', '>=', (float)$request->input('price'))->where('quantity_availables', '!=', (int)0);
      });
    }

    if ($request->has('date')) {

      if ($request->input('date') == 'today') {
        $events->where('start_date', Carbon::parse('today'))
        ->orWhere('end_date', Carbon::parse('today'))
        ->orWhereHas('dates', function ($query) {
          $query->where( 'start_date', Carbon::parse('today') )
          ->orWhere('end_date', Carbon::parse('today')
        );
        });
      }

      if ($request->input('date') == 'tomorrow') {
        $events->where('start_date', Carbon::parse('tomorrow')
        )->orWhere('end_date', Carbon::parse('tomorrow')
        )->orWhereHas('dates', function ($query) {
          $query->where( 'start_date', Carbon::parse('tomorrow')
        )->orWhere('end_date', Carbon::parse('tomorrow') );
        });
      }

      if ($request->input('date') == 'this-week') {
          $date = Carbon::now();

          $events = $this->applyDateFilters($events,
                                              $date->format('Y-m-d'),
                                              $date->endOfWeek()->format('Y-m-d'));
      }

      if ($request->input('date') == 'next-week') {
          $date = Carbon::parse('next week');

          $events = $this->applyDateFilters($events,
                                              $date->format('Y-m-d'),
                                              $date->endOfWeek()->format('Y-m-d'));
      }

        if ($request->input('date') == 'this-month') {
            $date = Carbon::now();

            $events = $this->applyDateFilters($events,
                                                $date->format('Y-m-d'),
                                                $date->endOfMonth()->format('Y-m-d'));
        }

        if ($request->input('date') == 'next-month') {
            $date = Carbon::parse('next month')->startOfMonth();

            $events = $this->applyDateFilters($events,
                                                $date->format('Y-m-d'),
                                                $date->endOfMonth()->format('Y-m-d'));
        }
    }

      if ($request->has('source') && !$request->has('date')) {

          $dates = Carbon::now()->format('Y-m-d');

          $events->where(function($query) use ($dates){
              $query->where('end_date', '>=', $dates);
          });
      }

    if ($request->has('dayOfWeek')) {

      $dates = Carbon::parse($request->input('dayOfWeek'));

      $events->where('start_date', $dates)
      ->orWhere('end_date', $dates)
      ->orWhereHas('dates', function ($query) use ($dates) {

        $query->where('start_date', $dates)
        ->orWhere('end_date', $dates);
      });
    }

    if ($request->has('exclude')) {
      $events->where('status', '!=', $request->input('exclude'))->get();
    }

    if ($request->has('privacy')) {
      $events->where('privacy', '=', $request->input('privacy'))->get();
    }

    $searched = $events->latest()->get();

    return Fractal::collection( $searched, new EventTransformer );

  }

  function applyDateFilters($events, $startDate, $endDate){
      $events->where(function($query) use ($startDate, $endDate){
          $query->where('start_date', '>=', $startDate)
          ->where('start_date', '<=', $endDate);

      })->orWhere(function($query) use ($startDate, $endDate){
          $query->where( 'end_date', '>=', $startDate
          )->where( 'end_date', '<=', $endDate);

      })->orWhereHas('dates', function ($query) use ($startDate, $endDate) {
          $query->where( 'start_date', '>=', $startDate
          )->where('start_date', '<=', $endDate)
              ->where(function($query) use ($endDate){
                  $query->orWhere('end_date', '<=', $endDate);
              });
      });

      return $events;
  }
}
