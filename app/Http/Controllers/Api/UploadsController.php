<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Upload;

class UploadsController extends Controller
{
    public function index()
    {
      return Upload::all();
    }

    public function load(Request $request)
    {
      if ($request->hasFile('file')) {

        $file_name = str_random(6).'_'.time().'.'.$request->file('file')->getClientOriginalExtension();

        $file_path = $request->file('file')->move(
          'uploads',
          $file_name
        );

        $upload = Upload::create([
          'file' => $file_name,
          'url' => url($file_path),
        ]);

        return response()->json([
          'url' => $upload->url
        ]);

      }

      return response()->json([
        'error' => 'Not uploads'
      ], 204);
    }
}
