<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Country;

use Fractal;

use App\Transformers\CountryTransformer;

class CountriesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
      return Fractal::collection( Country::all(), new CountryTransformer );
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
      $country = Country::create($request->all());

      $country->slug = str_slug($country->name.' '.$country->id, '-');

      $country->save();

      return Fractal::item($country, new CountryTransformer);
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show(Country $country)
  {
      return Fractal::item($country, new CountryTransformer);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, Country $country)
  {
      $country->update($request->all());

      $country->slug = str_slug($country->name.' '.$country->id, '-');

      $country->save();

      return Fractal::item($country, new CountryTransformer);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Country $country)
  {
      $country->delete();

      return response()->json(null, 204);

  }
}
