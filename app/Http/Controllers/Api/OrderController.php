<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Fractal;

use App\Order;
use App\OrderItem;
use App\Entrance;

use App\Ticket;

use App\Notifications\InvoicePaid;

use App\Transformers\OrderTransformer;

class OrderController extends Controller
{
    public function index()
    {
      return Fractal::collection(Order::all(), new OrderTransformer);
    }

    /*
		Api to assign ticket if payment is success
		 'Api\OrderController@pay_confirmation'
		arguments :
		{ACK,order_id}
		}
	*/
    public function pay_confirmation(Request $request)
    {
    	// receive order id
    	// if status paid search order items
    	// for each order items check quantity
    	// for each quantity create a ticket

    	$order = Order::find($request->order_id);

    	if (in_array(strtoupper($request->ACK), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
    		$order->status = "paid";
    		$order->save();

       		$items = OrderItem::where('order_id',$order->id)->get();
		    	foreach ($items as $item) {
					//Generating the new tickets
					$n = 1;

					while ($n <= $item->quantity) {

						//Fix: generate tickets with the quantity...
						$ticket = new Ticket;

						$ticket->user_id = $order->user_id;
						$ticket->event_id = $item->event_id;
						$ticket->generateCode();
						$ticket->price = $item->price;
						$ticket->entrance_id = $item->entrance_id;

						$ticket->save();

						$n++;
					}
			    }

          auth()->user()->notify(new InvoicePaid($order));

			    return response()->json(
			        [
			          'status' => 'success',
			          'message' => 'tickets assigned successfully'
			        ]
			     );
    	}else{
    		return response()->json(
			        [
			          'status' => 'fail',
			          'message' => 'pay not completed'
			        ]
			     );
    	}

    }


	/*
	Api to create orders
	 'Api\OrderController@make_order'
	arguments :
	{user_id,cart_content[{event_id,entrance_id,event_name,entrance_name,qty,price,tax}]}
	}
	*/
    public function make_order(Request $request)
    {
    	if (!is_null($request->cart_content)) {

    		$order = new Order();
			$order->user_id = $request->user_id;
			$order->save();

	    	foreach ($request->cart_content as $item) {

	            OrderItem::create([
	              'order_id' => $order->id,
	              'event_id' => $item['event_id'],
	              'entrance_id' => (int)$item['entrance_id'],
	              'event_name' => $item['event_name'],
	              'entrance_type' => $item['entrance_name'],
	              'quantity' => $item['qty'],
	              'price' => $item['price'],
	              'tax' => intval($item['qty']) * floatval($item['tax']) ,
	              'subtotal' => round($item['price'] * $item['qty'],2)
	            ]);

	            $type_ticket = Entrance::find((int)$item['entrance_id']);

	            //Updating Quantity Availables
	            $type_ticket->update([
	              'quantity_availables' => ($type_ticket->quantity_availables - (int)$item['qty'])
	            ]);

	        }

	        return response()->json(
		        [
		          'status' => 'success',
		          'message' => 'order created successfully!',
		          'order_id' =>  $order->id
		        ]
		    );

    	} else {
    		return response()->json(
		        [
		          'status' => 'fail',
		          'message' => 'Cart Empty'
		        ]
		     );
    	}
    }


    public function cancel(Order $order)
    {
      $items = OrderItem::where('order_id', $order->id);

      foreach ($items->get() as $item) {

        $type_ticket = Entrance::find($item->entrance_id);

        //Updating Quantity Availables
        $type_ticket->update([
          'quantity_availables' => ($type_ticket->quantity_availables + $item->quantity)
        ]);

      }

      $items->delete();

      $order->delete();

      Cart::destroy();

      return response()->json(
        [
          'status' => 'success',
          'message' => 'order was cancelled!'
        ]);
    }
}
