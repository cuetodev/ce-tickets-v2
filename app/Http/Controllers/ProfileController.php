<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use App\OrganizationRequest;
use App\Organization;
use App\Event;

class ProfileController extends Controller
{
    public function index()
    {
      $is_organizer = Organization::where('user_id',auth()->user()->id)->count('id');
      $organizer_request = OrganizationRequest::where('user_id',auth()->user()->id)->count('id');

      return view('profile')->with('is_organizer',$is_organizer)->with('organizer_request',$organizer_request);
    }

    public function basicInfo(Request $request)
    {
      if ($request->has('first_name')) {
        auth()->user()->first_name = $request->input('first_name');
      }

      if ($request->has('last_name')) {
        auth()->user()->last_name = $request->input('last_name');
      }

      if ($request->has('phone_number')) {
        auth()->user()->phone_number = $request->input('phone_number');
      }

      if ($request->has('email')) {
        auth()->user()->email = $request->input('email');
      }

      if ($request->has('gender')) {
        auth()->user()->gender = $request->input('gender');
      }

      if ($request->has('birth_date')) {
        auth()->user()->birth_date = $request->input('birth_date');
      }

      auth()->user()->save();

      return back();
    }

    public function changePassword(Request $request)
    {
      $this->validate($request, [
        'current_password' => 'required|current_password',
        'new_password' => 'required|string|min:6|confirmed',
      ]);

      $request->user()->fill([
        'password' => Hash::make($request->input('new_password')),
      ])->save();

      $request->session()->flash('success', 'Password Changed!');

      return back();

    }

    public function updateAvatar(Request $request)
    {
      $this->validate($request, [
        'avatar' => 'required|dimensions:min_width=100,min_height=200',
      ]);

      if ($request->hasFile('avatar')) {

        $file_name = str_random(7).'_'.time().'.'.$request->file('avatar')->getClientOriginalExtension();

        $file_path = $request->file('avatar')->move(
          'avatars',
          $file_name
        );

        auth()->user()->avatar = url($file_path);

        auth()->user()->save();

        return back();
      }
    }
}
