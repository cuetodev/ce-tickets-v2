<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\City;

class CityController extends Controller
{

  public function show($slug)
  {
    $city = City::where('slug', '=', $slug)->firstOrFail();

    return view('city-single', compact('city'));

    // return dd($city);
  }
}
