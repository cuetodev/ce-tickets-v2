<?php

namespace App\Http\Controllers;

use App\Notifications\InvoicePaid;
use App\Order;
use App\Ticket;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use JWTAuth;
use Auth;
use App\Http\Requests;
use Illuminate\Http\Request;
use PayPal\Exception\PayPalConnectionException;
use PayPal\Exception\PPConnectionException;
use Validator;
use URL;
use Session;
/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use App\Http\Controllers\Controller;
use App\APIRequest;

class PaypalController
{
    private $_api_context;

    public function __construct()
    {
        $paypal_conf = \Config::get('paypal');

        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret']
        ));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function pay(Order $order)
    {

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $itemList = new ItemList();
        $total = 0;
        $tax = 0;

        foreach ($order->items as $orderItem) {

            $total += $orderItem->subtotal;
//            $tax += $orderItem->tax;

            $item = new Item();
            $item->setName($orderItem->event_name.'-'.$orderItem->entrance_type)
            ->setCurrency('USD')
                ->setQuantity($orderItem->quantity)
                ->setPrice($orderItem->price + $orderItem->tax);

            $itemList->addItem($item);
        }

        $details = new Details();
        $details->setTax(0)->setShipping(0)->setSubtotal($total + $tax);

        $amount = new Amount();
        $amount->setCurrency('USD')->setTotal($total + $tax)->setDetails($details);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription('Your transaction description');

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(url('/payment/paypal/result')) /** Specify return URL **/
        ->setCancelUrl(url('/payment/paypal/result'));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

//        dd($payment);
        try {
            $payment->create($this->_api_context);
        } catch (PayPalConnectionException $ex) {

            dd('There is an error contact the web support team.');
            if (\Config::get('app.debug')) {
                return Redirect::route('addmoney.paywithpaypal');
                /** echo "Exception: " . $ex->getMessage() . PHP_EOL; **/
                /** $err_data = json_decode($ex->getData(), true); **/
                /** exit; **/
            } else {
                return Redirect::route('addmoney.paywithpaypal');
                /** die('Some error occur, sorry for inconvenient'); **/
            }
        }

        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }

        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        if(isset($redirect_url)) {
            /** redirect to paypal **/
            return Redirect::away($redirect_url);
        }
    }

    public function getPaymentStatus()
    {
        /** Get the payment ID before session clear **/
        $payment_id = Session::get('paypal_payment_id');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            dd('Payment Fail');
            return Redirect::route('addmoney.paywithpaypal');
        }

        $payment = Payment::get($payment_id, $this->_api_context);

        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        /** dd($result);exit; /** DEBUG RESULT, remove it later **/
        if ($result->getState() == 'approved') {
            $order = auth()->user()->orders()->where('status', 'pending')->first();
            $order->update([
                'status' => 'paid',
                'transaction_id' => $result->getId()
            ]);

            $items = $order->items;

            foreach ($items as $item) {

                //Generating the new tickets
                $n = 1;

                while ($n <= $item->quantity) {

                    //Fix: generate tickets with the quantity...
                    $ticket = new Ticket;

                    $ticket->user_id = auth()->user()->id;
                    $ticket->event_id = $item->event_id;
                    $ticket->generateCode();
                    $ticket->price = $item->price;
                    $ticket->entrance_id = $item->entrance_id;

                    $ticket->save();

                    $n++;
                }
            }

            Cart::destroy();

            auth()->user()->notify(new InvoicePaid($order));


            return view('success');
        }

        \Toastr::error('Payment failed', 'Error');
        return Redirect::route('addmoney.paywithpaypal');
    }
}
