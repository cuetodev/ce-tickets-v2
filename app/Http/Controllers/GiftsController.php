<?php

namespace App\Http\Controllers;

use App\Gift;
use App\User;
use App\Ticket;
use App\Event;
use App\Entrance;

use App\Mail\GiftInvitation;
use App\Mail\GiftAccepted;
use App\Mail\GiftRejected;

use Illuminate\Http\Request;

class GiftsController extends Controller
{
    public function index()
    {
      $gift_to_me = Gift::where('receiver', auth()->user()->email)->where('accepted', 0)->get();

      $gifted = Gift::where('giver', auth()->user()->email)->get();

      return view('gift_index', compact(['gift_to_me', 'gifted']));
    }

    public function generateGift(Request $request, Ticket $ticket)
    {
      $gift = new Gift;

      $gift->accepted = 0;

      $gift->giver = auth()->user()->email;
      $gift->receiver = $request->input('email_receiver');
      $gift->event_id = $ticket->event_id;
      $gift->entrance_id = $ticket->entrance_id;

      $gift->save();

      //We'll going to notify user by email now

      \Mail::to($gift->receiver)->send(new GiftInvitation($gift));

      // Destroy the ticket

      $ticket->delete();

      return back();

    }

    public function acceptGift(Gift $gift, Request $request)
    {
      if (auth()->user()->email == $gift->receiver) {
        if ($request->has("accept")) {
          $gift->accepted = 1;
          $gift->save();
        }

        // Generate the ticket

        $ticket = new Ticket();

        $ticket->user_id = auth()->user()->id;
        $ticket->event_id = $gift->event_id;
        $ticket->entrance_id = $gift->entrance_id;

        $ticket->price = 0.00;
        $ticket->generateCode();

        $ticket->save();

        // Send a notification to email.

        \Mail::to($gift->giver)->send(new GiftAccepted($gift));

      }

      return redirect('dashboard/#my-tickets');

    }

    public function rejectGift(Gift $gift, Request $request)
    {
      if (auth()->user()->email == $gift->receiver) {
        if ($request->has("reject")) {
          $gift->accepted = 0;
          $gift->save();
        }

        // Generate the ticket to original user back.

        $ticket = new Ticket();

        $ticket->user_id = User::where('email', $gift->giver)->first()->id;
        $ticket->event_id = $gift->event_id;
        $ticket->entrance_id = $gift->entrance_id;

        $ticket->price = 0.00;
        $ticket->generateCode();

        $ticket->save();

        // Send a notification to email

        \Mail::to($gift->giver)->send(new GiftRejected($ticket));

        $gift->delete();

      }

      return back();
    }

}
