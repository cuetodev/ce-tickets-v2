<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \Cart;

use App\Event;

class CartController extends Controller
{
    public function index()
    {
      return view('cart');
    }

    public function addItem(Request $request, Event $event)
    {
      $product_id = $event->id;
      $product_name = $event->name;

      if ($request->has('quantity')) {
        $product_qty = (int)$request->input('quantity');
      }

      if ($request->has('id_type')) {

        $type_ticket = $event->type_entrances()->find((int)$request->input('id_type'))->id;

        $type_ticket_name = $event->type_entrances()->find((int)$request->input('id_type'))->name;

        $type_ticket_price = $event->type_entrances()->find((int)$request->input('id_type'))->price;
      }

      Cart::add($type_ticket, $product_name, $product_qty, $type_ticket_price,

      ['type_ticket' => array('id' => $type_ticket, 'name' => $type_ticket_name)]);

      return redirect('cart');

    }

    public function addMultipleItems(Request $request, Event $event)
    {
      $product_id = $event->id;
      $product_name = $event->name;

      $input = $request->all();

      foreach ($input['ticket'] as $item) {

        if ( $item['quantity'] > 0 ) {

          $entrance = $event->type_entrances()->find((int)$item['id_type']);

          $product_qty = $item['quantity'];

          $type_ticket = $entrance->id;
          $type_ticket_name = $entrance->name;
          $type_ticket_price = $entrance->price;

          Cart::add($type_ticket, $product_name, $product_qty, $type_ticket_price, ['type_ticket' => array('id' => $type_ticket, 'name' => $type_ticket_name)]);
        }
      }

      return redirect('cart');
    }

    public function removeItem($item)
    {
      Cart::remove($item);

      return back();
    }

    public function updatePrice(Request $request, $item)
    {
      if ($request->has('item_quantity')) {

        Cart::update($item, $request->input('item_quantity'));

      }

      return back();
    }

    public function clearCart()
    {
      Cart::destroy();

      return back();
    }
}
