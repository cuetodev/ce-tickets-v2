<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Event;

class EventController extends Controller
{
    public function index()
    {
      $events = Event::where('status', '!=', 'ended')->where('privacy', '!=', 'private')->latest()->paginate(6);

      return view('event-list', compact('events'));

    }

    public function show($slug, Request $request)
    {
      if ($request->has('share_code')) {
        $event = Event::where('slug', '=', $slug)->where('status', '!=', 'ended')->where('share_code', $request->input('share_code'))->where('privacy', '=', 'private')->firstOrFail();
      } else {
        $event = Event::where('slug', '=', $slug)->where('status', '!=', 'ended')->where('privacy', '=', 'public')->firstOrFail();
      }

      $event->update([
         'visits' =>  $event->visits + 1
      ]);

      return view('event-single', compact('event'));
    }
}
