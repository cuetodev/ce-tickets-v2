<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $guarded = [];

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function events()
    {
      return $this->hasMany('App\Event');
    }

    public function staff()
    {
      return $this->hasMany('App\Staff');
    }

}
