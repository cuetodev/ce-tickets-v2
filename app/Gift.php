<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gift extends Model
{
  protected $guarded = [];

  public function event()
  {
    return $this->belongsTo('App\Event');
  }

  public function entrance()
  {
    return $this->belongsTo('App\Entrance');
  }

}
