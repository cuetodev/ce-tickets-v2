<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  protected $guarded = [];

  protected $dates = ['created_at', 'updated_at', 'start_date', 'end_date', ];

  public function scopeSearch($query, $search)
  {
    return $query->where('events.name', 'like', '%' .$search. '%')->orWhere('events.description', 'like', '%' .$search. '%');
  }

  public function category()
  {
    return $this->belongsTo('App\Category');
  }

  public function organization()
  {
    return $this->belongsTo('App\Organization');
  }

  public function generateCode()
  {
    $string_code = str_random(30);

    if (!is_null($this->where('share_code', '=', $string_code)->first())) {
      $this->generateCode();
    }

    $this->share_code = $string_code;
  }

  public function reviews()
  {
    return $this->hasMany('App\Review');
  }

  public function averageReview()
  {
    $review_1 = $this->reviews()->where('rating', '1')->count() * 1;
    $review_2 = $this->reviews()->where('rating', '2')->count() * 2;
    $review_3 = $this->reviews()->where('rating', '3')->count() * 3;
    $review_4 = $this->reviews()->where('rating', '4')->count() * 4;
    $review_5 = $this->reviews()->where('rating', '5')->count() * 5;

    $total_review_x_start = $review_1 + $review_2 + $review_3 + $review_4 + $review_5;

    return $total_review_x_start == 0 ? 0 : round($total_review_x_start / $this->reviews()->count(), 1);
  }

  public function dates()
  {
    return $this->hasMany('App\Date');
  }

    public function days()
    {
        return $this->hasMany('App\EventDay');
    }

  public function type_entrances()
  {
    return $this->hasMany('App\Entrance');
  }

  public function assignEntrance($entrance)
  {
    return $this->type_entrances()->attach($entrance);
  }

  public function removeEntrance($entrance)
  {
    return $this->type_entrances()->detach($entrance);
  }

  public function tickets()
  {
    return $this->hasMany('App\Ticket');
  }

  public function country()
  {
    return $this->belongsTo('App\Country');
  }

  public function city()
  {
    return $this->belongsTo('App\City');
  }

  public function staff()
  {
    return $this->belongsToMany('App\Staff', 'staff_event');
  }

}
