<?php

namespace App;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $guarded = [];

    public function entrance()
    {
      return $this->belongsTo('App\Entrance');
    }

    public function user()
    {
      return $this->belongsTo('App\User');
    }

    public function event()
    {
      return $this->belongsTo('App\Event');
    }

    public function generateCode()
    {
      $string_code = strtoupper(str_random(6).'-'.Carbon::now()->timestamp);

      if (!is_null($this->where('code', '=', $string_code)->first())) {
        $this->generateCode();
      }

      $this->code = $string_code;
    }


}
