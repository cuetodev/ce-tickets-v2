<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $guarded = [];

    public function generateCode()
    {
      $string_code = strtoupper(str_random(15));

      if (!is_null($this->where('code', '=', $string_code)->first())) {
        $this->generateCode();
      }

      $this->code = $string_code;
    }

    public function organization()
    {
      return $this->belongsTo('App\Organization');
    }

    public function events()
    {
      return $this->belongsToMany('App\Event', 'staff_event');
    }

    public function assignEvent($event)
    {
      return $this->events()->attach($event);
    }

    public function removeEvent($event)
    {
      return $this->events()->detach($event);
    }
}
