/**
 * Created by Alvade on 18/11/2017.
 */
DashBoardA.config(['$translateProvider', function($translateProvider) {

    // Adding a translation table for the English language
    $translateProvider.translations('en_US', {
        "URL_DATATABLE": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/English.json",
        "PAGE_BAR_INIT" : "Home",
        "TRANSACTION_SUCCESS": "Operation Performed",
        "TRANSACTION_ERROR": "Error in Operation",
        "PAGE_BAR_DASH" : "Dashboard",
        "PAGE_BAR_ORG": "Organizers",
        "EVENTS": "Events",
        "FINANCES": "Finances",
        "USERS": "Users",
        "SELECT_IMAGE": "Select Image",
        "CHANGE_IMAGE": "Change",
        "DELETE": "Delete",
        "FACEBOOK": "Facebook",
        "TWITTER": "Twitter",
        "CANCEL": "Cancel",
        "CLOSE": "Close",
        "ACEPT": "Accept",
        "SURE": "Are you sure?",
        "PROFILE": "Profile",
        "ORGANIZATION": "Organization",
        "EVENT": "Event",
        "CONFIGURATIONS": "Configurations",
        "ADD_NEW": "Add New",
        "SELECT": "Choose One",
        "CATEGORY": "Category",
        "CATEGORIES": "Categories",
        "COUNTRIES": "Countries",
        "CITIES": "Cities",
        "SEND_NOTIFICATION": "Send Notification",
        "SEND": "Send",
        "NOTIFICATION": "Notification",
        "ASSISTANTS": "Assistants",
        "DASHBOARD": {
            "PAGE_TITLE": "Dashboard",
            "USERS_ACTIVE": "Active Users",
            "ORGANIZERS": "Organizer Requests",
            "SEE_MORE": "See more",
            "WELCOME": "Welcome!",
            "WATCH_PROFILE": "View profile"
        },
        "HEADER": {
            "MY_PROFILE": "My Profile",
            "LOGOUT": "Logout"
        },
        "USER": {
            "FIRST_NAME": "First Name",
            "LAST_NAME": "Last Name",
            "EMAIL": "Email",
            "DESCRIPTION": "Description",
            "PHONE": "Phone",
            "NAME": "Name",
            "ACTIONS": "Actions",
            "TERM_USERS" : "Terminated Users",
            "ROLES": "Roles",
            "USER_INFO": "User Data",
            "STATUS": "Status",
            "SELECT": "Select",
            "BIRTHDATE": "Birthdate",
            "PLACEHOLDERS": {
                "NAME": "User First Name",
                "LAST_NAME": "Last Name",
                "EMAIL": "Email"
            },
            "SUCCESS_ADD": "The user has been created!",
            "SUCCESS_EDIT": "The user has been modified!",
            "TITLE_ADD": "Add User",
            "TITLE_EDIT": "Edit User",
            "ADD": "Save",
            "EDIT": "Save",
            "INACTIVE": "Inactive",
            "ACTIVE": "Active",
            "TERMINATE": "Terminate"

        },
        "ORGANIZER": {
            "ORG_ACTIV": "Active organizers",
            "NAME": "Name",
            "WEB_SITE": "Web Site",
            "PHONE": "Phone Number",
            "ACTIONS": "Actions",
            "CONTACT": "Contact's Name",
            "PHONE_CONT": "Contact's Phone",
            "CONFIRM_DISAPPROVE": "Are you sure do you want to reject organizer?",
            "CONFIRM_APPROVE": "Are you sure do you want to approve organizer?",
            "DESCRIPTION": "Description",
            "EMAIL": "Email",
            "TITLE": {
                "DISAPPROVE": "Reject organizer",
                "APPROVE": "Approve organizer",
                "ADD_ORGANIZATION": "Add Organization",
                "EDIT_ORGANIZER": "Edit Organization"
            },
            "PLACEHOLDERS": {
                "NAME_ORG":"Organization's Name",
                "PHONE_ORG":"Organization's Phone",
                "WEB_SITE": "Web Site",
                "EMAIL": "Email"
            },
            "WITH_OUT_ORGANIZATION": "Producers without Organization",
            "ORGANIZERS_REQUEST": "Organizers Request",
            "CREATE_ORGANIZATION": "Create Organization",
            "SUCCESS_ADD": "The organizer has been added!",
            "SUCCESS_EDIT": "The organizer has been modified!",
            "TITLE_ADD": "Add Organization",
            "TITLE_EDIT": "Edit Organization",
            "ADD": "Save",
            "EDIT": "Save",
            "PROFILE": "Organizer Profile",
            "EDIT_INFO": "Edit Information",
            "REPRE_DATA": "Representative Data",
            "CONTACT_PROFILE": "Media",
            "ORG_DATA": "Organization Data",
            "ORG_DATA_REQ": "Organization Request Data"
        },
        "FINANCE": {
            "BY_EVENT": "By Event",
            "PAY_EVENT": "Pay Event",
            "SOLD": "Tickets Sold",
            "SUCCESS_SAVED_PAY": "The information has been saved!",
            "ERROR_SAVED_PAY": "The information couldn't been saved!",
            "SOLD_PERCENT": "Tickets Sold Percent",
            "MONEY_COLLECT": "Money Collected",
            "AVAILABLE": "Tickets Available",
            "BY_ORGANIZATION": "By Organization",
            "EARNINGS": "Earnings",
            "SOLD2": "Sold",
            "ALL_TICKETS": "All Tickets",
            "PAYMENT_INFORMATION": "Payment Information",
            "SALE": "Sale",
            "REFERENCE" : "Reference"
        },
        "ERROR": {
            "USER_ERR_SELECT": "User Status Required.",
            "EVENT_ERR_SELECT": "Event Status Required.",
            "NAME_ERR_REQ": "Name Required.",
            "NAME_ERR_MINL": "Name too short.",
            "NAME_ERR_MAXL": "Name too long.",
            "LAST_NAME_REQ": "Last Name Required.",
            "LAST_NAME_MINL": "Last Name too short.",
            "LAST_NAME_MAXL": "Last Name too long.",
            "EMAIL_ERR_EMAIL": "Invalid Email.",
            "EMAIL_ERR_REQ": "Invalid Email.",
            "EMAIL_ERR_MINL": "Email too short.",
            "EMAIL_ERR_MAXL": "Email too long.",
            "BIRTH_ERR_REQ": "Invalid Birth Date.",
            "PHONE_REQ": "Phone Number Required.",
            "PHONE_MINL": "Phone Number too short.",
            "PHONE_MAXL": "Phone Number too long.",
            "ORG_ERR_NAME_ORG": "Organization Name Required",
            "WEB_SITE_MINL": "Web Site too short.",
            "WEB_SITE_MAXL": "Web Site too long.",
            "FACEBOOK_MINL": "Facebook Link too short.",
            "FACEBOOK_MAXL": "Facebook Link too long.",
            "TWITTER_MINL": "Twitter Link  too short.",
            "TWITTER_MAXL": "Twitter Link  too long.",
            "DESCRIP_REQ": "Description Required.",
            "DESCRIP_MINL": "Description too short.",
            "DESCRIP_MAXL": "Description too long.",
            "START_DATE_REQ": "Start Date Required.",
            "DATE_REQ": "Date Required.",
            "END_DATE_REQ": "End Date Required.",
            "START_TIME_REQ": "Start Date Required.",
            "END_TIME_REQ": "End Date Required.",
            "PRIVACY_REQ": "Privacy Required.",
            "ADDRESS_REQ": "Address Required.",
            "LATITUDE_REQ": "Latitude Required.",
            "LONGITUDE_REQ": "Longitude Required.",
            "PRICE_REQ": "Price Required.",
            "PRICE_MINL": "Price too short.",
            "PRICE_MAXL": "Price too long.",
            "NUMBER_REQ": "Number Required.",
            "NUMBER_MINL": "Number too short.",
            "NUMBER_MAXL": "Number too long.",
            "IMAGE_REQ": "Image Required.",
            "ADD_ANY_TICKET": "Add at least one ticket.",
            "IMAGE_CATEGORY": "Error uploading the image to category",
            "SAVE_CATEGORY": "Error adding category",
            "EDIT_CATEGORY": "Error when editing category",
            "IMAGE_COUNTRY": "Error uploading the image to country",
            "SAVE_COUNTRY": "Error adding country",
            "EDIT_COUNTRY": "Error when editing country",
            "SAVE_STAFF": "Error adding staff",
            "EDIT_STAFF": "Error editing staff",
            "IMAGE_CITY": "Error uploading the image to city",
            "SAVE_CITY": "Error adding city",
            "EDIT_CITY": "Error when editing city",
            "DAYS_REQUIRED": "Error the recurrent days must be selected",
            "REFERENCE_REQ": "Please, add the transaction reference number",
            "REFERENCE_MINL": "Reference number too short.",
            "REFERENCE_MAXL": "Reference number too long."
        },
        "EVENT_OBJECT": {
            "NAME": "Name",
            "SHARE_CODE": "Share Code",
            "CODE": "Code",
            "DATE": "Date",
            "USER": "User",
            "STATUS": "Status",
             "URL": "URL",
            "DATE_EVENT": "Event Date",
            "EVENT_CATEGORY": "Event Category",
            "PRIVACY": "Privacy",
            "EVENT_LINK": "Event Link",
            "LOCATION": "Location",
            "ACTIONS": "Actions",
            "EVENT_DATA": "Event Data",
            "DESCRIPTION": "Description",
            "START_DATE": "Start Date",
            "END_DATE": "End Date",
            "START_TIME": "Start Time",
            "RECURRENT" : "Recurrent",
            "END_TIME": "End Time",
            "PUBLIC": "Public",
            "PRIVATE": "Private",
            "COUNTRY": "Country",
            "CITY": "City",
            "SUSPENDED": "Suspended",
            "AVAILABLE": "Available",
            "ENDED": "Ended",
            "SOLD-OUT": "Sold-out",
            "ADDRESS": "Address",
            "LONGITUDE": "Longitude",
            "LATITUDE": "Latitude",
            "TICKETS": "Tickets",
            "PRICE": "Price",
            "DELETE_TICKET_CONFIRM": "Are you sure to delete tickets?",
            "DELETE_STAFF_CONFIRM": "Are you sure to delete staff?",
            "UPDATE_STAFF_CONFIRM": "Are you sure to reset the access?",
            "STAFF": "Staff",
            "EVENT_IMAGE": "Event Image",
            "UPLOAD_IMAGE": "Upload Image",
            "UPLOAD_STAGE_IMAGE": "Upload Stage Image",
            "TICKETS_DATA": "Tickets Data",
            "TICKETS_SOLD": "Tickets Sold",
            "TICKETS_NUMBER": "Tickets Number",
            "FREE": "Free",
            "STAFF_DATA": "Staff Data",
            "ADD_EVENT": "Add Event",
            "EDIT_EVENT": "Edit Event",
            "UPDATE_EVENT": "Update Event",
            "PUBLISH_EVENT": "Publish Event",
            "SUCCESS_EDIT":"The event has been edited!",
            "SUCCESS_ADD":"The event has been added!",
            "ERROR_EDIT": "Error updating event",
            "ERROR_ADD": "Error when adding event",
            "TITLE": {
                "ENTRANCE": "Delete Ticket",
                "STAFF": "Delete Staff",
                "RESET_ACCESS_STAFF": "Reset the access code"
            },
            "PLACEHOLDERS": {
                "NAME": "Event name",
                "DESCRIPTION": "Description",
                "COUNTRY": "Select a Country",
                "CITY": "Select a City",
                "CATEGORY": "Select a Category",
                "PRICE": "Ticket Price"
            }
        },
        "CATEGORY_OBJECT": {
            "ADD_CATEGORY": "Add Category",
            "EDIT_CATEGORY": "Edit Category",
            "ADD": "Save",
            "EDIT": "Save",
            "SUCCESS_ADD": "The category has been added!",
            "SUCCESS_EDIT": "The category has been Edited!",
            "CATEGORY_DATA": "Category Data",
            "NAME": "Category Name",
            "DESCRIPTION": "Category Description"
        },
        "COUNTRY": {
            "ADD_COUNTRY": "Add Country",
            "EDIT_COUNTRY": "Edit Country",
            "ADD": "Save",
            "EDIT": "Save",
            "SUCCESS_ADD": "The country has been added!",
            "SUCCESS_EDIT": "The country has been Edited!",
            "COUNTRY_DATA": "Country Data",
            "NAME": "Country Name",
            "PLACEHOLDERS": {
                "NAME": ""
            }
        },
        "CITY": {
            "ADD_CITY": "Add City",
            "EDIT_CITY": "Edit City",
            "ADD": "Save",
            "EDIT": "Save",
            "SUCCESS_ADD": "The city has been added!",
            "SUCCESS_EDIT": "The city has been Edited!",
            "CITY_DATA": "City Data",
            "NAME": "City Name",
            "PLACEHOLDERS": {
                "COUNTRY": "Select Country"
            }
        },
        "LANGUAGE": {
            "ES": "Spanish",
            "EN": "English"
        }
    });

    // Adding a translation table for the Russian language
    $translateProvider.translations('es_MX', {
        "URL_DATATABLE": "http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json",
        "PAGE_BAR_INIT" : "Inicio",
        "TRANSACTION_SUCCESS": "Operación Realizada",
        "TRANSACTION_ERROR": "Error en Operación",
        "PAGE_BAR_DASH" : "Panel de Administración",
        "PAGE_BAR_ORG": "Organizadores",
        "EVENTS": "Eventos",
        "FINANCES": "Finanzas",
        "USERS": "Usuarios",
        "SELECT_IMAGE": "Seleccionar imagen",
        "CHANGE_IMAGE": "Cambiar",
        "DELETE": "Eliminar",
        "FACEBOOK": "Facebook",
        "TWITTER": "Twitter",
        "CANCEL": "Cancelar",
        "CLOSE": "Cerrar",
        "ACEPT": "Aceptar",
        "SURE": "¿Esta seguro?",

        "PROFILE": "Perfil",
        "ORGANIZATION": "Organización",
        "EVENT": "Evento",
        "CONFIGURATIONS": "Configuraciones",
        "ADD_NEW": "Agregar Nuevo",
        "SELECT": "Seleccione",
        "CATEGORY": "Categoria",
        "CATEGORIES": "Categorias",
        "COUNTRIES": "Paises",
        "CITIES": "Ciudades",
        "SEND_NOTIFICATION": "Enviar Notificación",
        "SEND": "Enviar",
        "NOTIFICATION": "Notificación",
        "ASSISTANTS": "Assistants",
        "DASHBOARD": {
            "PAGE_TITLE": "Panel de Administración",

            "USERS_ACTIVE": "Usuarios Activos",
            "ORGANIZERS": "Solicitudes de Organizador",

            "SEE_MORE": "Ver más",
            "WELCOME": "Bienvenido",

            "WATCH_PROFILE": "Ver Perfil"
        },
        "HEADER": {
            "MY_PROFILE": "Mi perfil",
            "LOGOUT": "Cerrar Sesión"

        },
        "USER": {
            "FIRST_NAME": "Nombre",
            "LAST_NAME": "Apellidos",
            "EMAIL": "Correo",
            "NAME": "Nombre",
            "DESCRIPTION": "Descripcion",
            "PHONE": "Telefono",



            "ACTIONS": "Acciones",
            "TERM_USERS" : "Usuarios Dados de Baja",

            "ROLES": "Roles",
            "USER_INFO": "Datos de usuario",
            "STATUS": "Estatus",
            "SELECT": "Seleccione",
            "BIRTHDATE": "Fecha de nacimiento",

            "PLACEHOLDERS": {
                "NAME": "Nombre de Usuario",

                "LAST_NAME": "Apellidos",
                "EMAIL": "Email"
            },
            "SUCCESS_ADD": "¡El usuario ha sido creado correctamente!",
            "SUCCESS_EDIT": "El usuario ha sido editado correctamente!",


            "TITLE_ADD": "Agregar usuario",
            "TITLE_EDIT": "Editar usuario",
            "ADD": "Agregar",

            "EDIT": "Editar",
            "INACTIVE": "Inactivo",
            "ACTIVE": "Activo",
            "TERMINATE": "Baja"

        },
        "ORGANIZER": {
            "ORG_ACTIV": "Organizadores Activos",

            "NAME": "Nombre",
            "WEB_SITE": "Sitio Web",
            "PHONE": "Telefono",
            "ACTIONS": "Acciones",
            "CONTACT": "Nombre de Contacto",
            "PHONE_CONT": "Telefono de Contacto",
            "CONFIRM_DISAPPROVE": "¿Esta seguro de desaprobar organizador?",
            "CONFIRM_APPROVE": "¿Esta seguro de aprobar organizador?",




            "DESCRIPTION": "Descripcion",
            "EMAIL": "Correo",
            "TITLE": {
                "DISAPPROVE": "Desaprobar organizador",
                "APPROVE": "Aprobar organizador",


                "ADD_ORGANIZATION": "Agregar Organizacion",
                "EDIT_ORGANIZER": "Editar Organizacion"
            },
            "PLACEHOLDERS": {
                "NAME_ORG":"Nombre de Organización",
                "PHONE_ORG":"Nombre de Organización",
                "NAME": "Nombre de Usuario",
                "LAST_NAME": "Apellidos",


                "WEB_SITE": "Sitio Web",
                "EMAIL": "Email"
            },
            "WITH_OUT_ORGANIZATION": "Productores sin Organización",
            "ORGANIZERS_REQUEST": "Solicitud de Organizadores",
            "CREATE_ORGANIZATION": "Crear organización",
            "SUCCESS_ADD": "¡La organización ha sido creada!",
            "SUCCESS_EDIT": "¡El organizador ha sido modificado!",
            "TITLE_ADD": "Agregar Organización",
            "TITLE_EDIT": "Editar Organización",
            "ADD": "Agregar",





            "EDIT": "Editar",
            "PROFILE": "Perfil Organizador",
            "EDIT_INFO": "Editar Información",
            "REPRE_DATA": "Datos Representante",
            "ORG_DATA": "Datos Organización",
            "CONTACT_PROFILE": "Contacto",
            "ORG_DATA_REQ": "Datos de Solicitud de Organización"






        },
        "FINANCE": {
            "BY_EVENT": "Por Evento",
            "PAY_EVENT": "Pagar Evento",
            "SOLD": "Tickets Vendidos",
            "SUCCESS_SAVED_PAY": "La información ha sido guardada",
            "ERROR_SAVED_PAY": "La información no pudo ser guardada",

            "SOLD_PERCENT": "Tickets vendidos porciento",
            "MONEY_COLLECT": "Dinero Recolectado",
            "AVAILABLE": "Tickets Disponibles",
            "BY_ORGANIZATION": "Por Organización",
            "EARNINGS": "Ganancias",
            "SOLD2": "Vendidos",
            "ALL_TICKETS": "Totales",
            "PAYMENT_INFORMATION": "Información del pago",
            "SALE": "Venta",
            "REFERENCE" : "Reference"
        },
        "ERROR": {
            "USER_ERR_SELECT": "Usuario Activo / Inactvio requerido.",

            "EVENT_ERR_SELECT": "Estatus de Evento Requerido.",
            "NAME_ERR_REQ": "Nombre requerido.",
            "NAME_ERR_MINL": "Nombre demasiado corto.",
            "NAME_ERR_MAXL": "Nombre demasiado largo.",
            "LAST_NAME_REQ": "Apellidos requeridos.",
            "LAST_NAME_MINL": "Apellidos demasiado corto.",
            "LAST_NAME_MAXL": "Apellidos demasiado largo.",
            "EMAIL_ERR_EMAIL": "Email inválido.",
            "EMAIL_ERR_REQ": "Email inválido.",
            "EMAIL_ERR_MINL": "Email demasiado corto.",
            "EMAIL_ERR_MAXL": "Email demasiado largo.",
            "BIRTH_ERR_REQ": "Fecha de nacimiento invalida.",
            "PHONE_REQ": "Telefono requerido",
            "PHONE_MINL": "Telefono demasiado corto.",
            "PHONE_MAXL": "Apellidos demasiado largo.",
            "ORG_ERR_NAME_ORG": "Especifique nombre de organizacion",





            "WEB_SITE_MINL": "Sitio Web demasiado corto.",
            "WEB_SITE_MAXL": "Sitio Web demasiado largo.",
            "FACEBOOK_MINL": "Link de Facebook demasiado corto.",
            "FACEBOOK_MAXL": "Link de Facebook demasiado largo.",
            "TWITTER_MINL": "Link de Twitter demasiado corto.",
            "TWITTER_MAXL": "Link de Twitter demasiado largo.",
            "DESCRIP_REQ": "Descripción requerida",
            "DESCRIP_MINL": "Descripción demasiado corta.",
            "DESCRIP_MAXL": "Descripción demasiado larga.",



            "START_DATE_REQ": "Fecha de Inicio Requerida.",
            "DATE_REQ": "Fecha Requerida.",
            "END_DATE_REQ": "Fecha Fin Requerida.",
            "START_TIME_REQ": "Hora de Inicio Requerida.",
            "END_TIME_REQ": "Hora de Fin Requerida.",
            "PRIVACY_REQ": "Privacidad Requerida.",
            "ADDRESS_REQ": "Dirección Requerida.",
            "LATITUDE_REQ": "Latitud Requerdia.",
            "LONGITUDE_REQ": "Longitud Requerida.",
            "PRICE_REQ": "Precio Requerido.",
            "PRICE_MINL": "Precio Demasiado Corto.",
            "PRICE_MAXL": "Precio Demasiado Largo.",


            "NUMBER_REQ": "Numero Requerido.",
            "NUMBER_MINL": "Numero Demasiado Corto.",
            "NUMBER_MAXL": "Numero Demasiado Largo.",
            "IMAGE_REQ": "Imagen Requerida.",
            "ADD_ANY_TICKET": "Agregar al Menos un Ticket.",
            "IMAGE_CATEGORY": "Error al Subir la Imagen de Categoria.",

            "SAVE_CATEGORY": "Error al Agregar la Categoria",
            "EDIT_CATEGORY": "Error al Editar la Información de la Categoria",

            "IMAGE_COUNTRY": "Error al Subir la Imagen de Pais.",
            "SAVE_COUNTRY": "Error al Agregar el País",
            "EDIT_COUNTRY": "Error al Editar la Información del País",

            "SAVE_STAFF": "Error al Agregar staff",
            "EDIT_STAFF": "Error al Editar staff",
            "IMAGE_CITY": "Error al Subir la Imagen de la Ciudad",

            "SAVE_CITY": "Error al Agregar la Ciudad",
            "EDIT_CITY": "Error al Editar la Ciudad",
            "DAYS_REQUIRED": "Error los Dias Recurrentes Deben ser Seleccionados",
            "REFERENCE_REQ": "Por Favor, Agregue el Número de Referencia de la Transacción",
            "REFERENCE_MINL": "Numero de Referencia Demasiado Corto.",
            "REFERENCE_MAXL": "Numero de Referencia Demasiado Largo."




        },
        "EVENT_OBJECT": {
            "NAME": "Nombre",
            "SHARE_CODE": "Compartir Código",
            "CODE": "Código",

            "DATE": "Fecha",
            "USER": "Usuario",

            "STATUS": "Estatus",
            "URL": "URL",
            "DATE_EVENT": "Fecha de Evento",
            "EVENT_CATEGORY": "Categoria de Evento",
            "PRIVACY": "Privacidad",
            "EVENT_LINK": "Link de Evento",
            "LOCATION": "Ubicación",
            "ACTIONS": "Acciones",
            "EVENT_DATA": "Datos de Evento",
            "DESCRIPTION": "Descripción",
            "START_DATE": "Fecha de Inicio",
            "END_DATE": "Fecha de Termino",
            "START_TIME": "Hora de Inicio de Evento",


            "RECURRENT" : "Recurrente",
            "END_TIME": "Hora de Finalización.",

            "PUBLIC": "Publico",
            "PRIVATE": "Privado",
            "COUNTRY": "País",
            "CITY": "Ciudad",
            "SUSPENDED": "Suspendido",
            "AVAILABLE": "Disponible",
            "ENDED": "Finalizado",

            "SOLD-OUT": "Agotado",
            "ADDRESS": "Dirección",
            "LONGITUDE": "Longitud",
            "LATITUDE": "Latitud",
            "TICKETS": "Tickets",
            "PRICE": "Precio",
            "DELETE_TICKET_CONFIRM": "¿Estas Seguro de eliminar el tipo de entrada?",
            "DELETE_STAFF_CONFIRM": "¿Estas Seguro de eliminar al personal?",


            "UPDATE_STAFF_CONFIRM": "¿Estas Seguro de reiniciar el acceso?",
            "STAFF": "Staff",
            "EVENT_IMAGE": "Imagen de Evento",
            "UPLOAD_IMAGE": "Subir Imagen",
            "UPLOAD_STAGE_IMAGE": "Subir Imagen de Escenario",
            "TICKETS_DATA": "Datos de Tipo de Entrada",

            "TICKETS_SOLD": "Tickets Agotados",
            "TICKETS_NUMBER": "Numero de Tickets",
            "FREE": "Gratis",
            "STAFF_DATA": "Datos de Personal",
            "ADD_EVENT": "Agregar Evento",
            "EDIT_EVENT": "Editar Evento",
            "UPDATE_EVENT": "Actualizar Evento",
            "PUBLISH_EVENT": "Publicar Evento",
            "SUCCESS_EDIT":"El evento ha sido actualizado!",
            "SUCCESS_ADD":"El eventa ha sido publicado!",
            "ERROR_EDIT": "Error al Actualizar el Evento.",
            "ERROR_ADD": "Error al Publicar el Evento.",




            "TITLE": {
                "ENTRANCE": "Eliminar Tipo de entrada",
                "STAFF": "Eliminar Personal",
                "RESET_ACCESS_STAFF": "Restablecer el código de acceso"



            },
            "PLACEHOLDERS": {
                "NAME": "Nombre de Evento",

                "DESCRIPTION": "Descripción",
                "COUNTRY": "Seleccione un País",
                "CITY": "Seleccione una Ciudad",
                "CATEGORY": "Seleccione una Categoria",
                "PRICE": "Precio de Entrada"
            }





        },
        "CATEGORY_OBJECT": {
            "ADD_CATEGORY": "Agregar Categoria",
            "EDIT_CATEGORY": "Editar Categoria",
            "ADD": "Guardar",
            "EDIT": "Guardar",
            "SUCCESS_ADD": "La Categoria ha sido Agregada",
            "SUCCESS_EDIT": "La Categoria ha sido Editada",
            "CATEGORY_DATA": "Información de Categoria",
            "NAME": "Nombre de Categoria",
            "DESCRIPTION": "Descripcion de Categoria"
        },
        "COUNTRY": {
            "ADD_COUNTRY": "Agregar País",
            "EDIT_COUNTRY": "Editar País",
            "ADD": "Guardar",
            "EDIT": "Guardar",
            "SUCCESS_ADD": "The País ha sido Agregada",
            "SUCCESS_EDIT": "The País ha sido Editada",
            "COUNTRY_DATA": "Información de País",
            "NAME": "Nombre de País",
            "PLACEHOLDERS": {
                "NAME": ""
            }
        },
        "CITY": {
            "ADD_CITY": "Agregar Ciudad",
            "EDIT_CITY": "Editar Ciudad",
            "ADD": "Guardar",
            "EDIT": "Guardar",
            "SUCCESS_ADD": "The Ciudad ha sido Agregada",
            "SUCCESS_EDIT": "The Ciudad ha sido Editada",
            "CITY_DATA": "Información de Ciudad",
            "NAME": "Nombre de Ciudad",
            "PLACEHOLDERS": {
                "COUNTRY": "Seleccione una Ciudad"
            }
        },
        "LANGUAGE": {
            "ES": "Español",
            "EN": "Inglés"
        }
    });

    // Tell the module what language to use by default
    if(!localStorage.getItem('prefLang')) {
        $translateProvider.preferredLanguage('en_US');
    } else {
        console.log(localStorage.getItem('prefLang'));
        var lang = JSON.parse(localStorage.getItem('prefLang'));
        $translateProvider.preferredLanguage(lang.modulo);
    }

}]);