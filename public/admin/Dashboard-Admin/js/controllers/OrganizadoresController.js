/**
 * Created by Cesar on 20/07/2017.
 */
angular.module('DashBoardA').controller('OrganizadoresController', function ($rootScope, $scope, $translate, OrganizationsRequest, Servicio, DTOptionsBuilder, DTColumnDefBuilder, Users, $http, $timeout) {
    $scope.$on('$viewContentLoaded', function () {

        $scope.organizadoresEspera = [];
        $scope.peticionOrganizador = {};
        $scope.usuario = {};
        //Obtiene los registros que se encuentran en la tabla de usuarios y que existan en organizadores

        Users.get(function (data) {
            $scope.organizadores = data.data;
            $scope.organizadoresAct = [];
            $scope.organizadoresSnOrg = [];
            $scope.users = [];
            $scope.organizadores.forEach(function (item, index, array) {
                //Extrae los usuarios activos y que sean de tipo organizador
                var temp = item.roles.data.length - 1;

                $scope.users.push(item);
                if (item.roles.data.length > 0) {
                    if (item.roles.data[temp].name == 'organizer' && item.organization) {
                        $scope.organizadoresAct.push(item);
                    }
                    if (item.roles.data[temp].name == 'organizer' && !item.organization) {
                        $scope.organizadoresSnOrg.push(item);
                    }

                    //Extrae los usuarios inactivos y que sean de tipo organizador
                    // if (item.roles.data[temp].name == 'client' || item.activated == 0) {
                    //     $scope.organizadoresEspera.push(item);
                    //
                    // }
                }
            });
        });

        var action = $http({
            method: 'GET',
            url: $rootScope.settings.pathAPI + 'api/organizations/request/all',
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            }
        }).then(function successCallback(response) {
            console.log(response);
            $scope.organizadoresEspera = response.data;
        }, function errorCallback(response) {
            console.log(response);
            $scope.organizadoresEspera = [];
        });
        // OrganizationsRequest.get(function (data) {
        //     console.log(data);
        //     $scope.organizadoresEspera = data.data;
        // });

        //El motivo del do-while es para evitar que se llame mas de una vez la definicion de la tabla
        do {

            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para
            //la tabla de productores activos
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource($translate.instant("URL_DATATABLE"))
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para
            //la tabla de solicitudes de productores activos
            $scope.vm2 = this;
            $scope.vm2.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource($translate.instant("URL_DATATABLE"))
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();

            $scope.vm3 = this;
            $scope.vm3.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource($translate.instant("URL_DATATABLE"))
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();

        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });

    $scope.$watch('organizadores', function (newVal) {
    });
    $scope.$watch('organizadoresAct', function (newVal) {
    });
    $scope.$watch('organizadoresEspera', function (newVal) {
    });
    $scope.$watch('organizadoresSnOrg', function (newVal) {
    });

    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.setOrganizator = function (data) {
        $rootScope.organizator = data;
    };

    $scope.UpdateRecords = function () {
        Users.get(function (data) {
            $scope.organizadores = data.data;
            $scope.organizadoresAct = [];
            $scope.organizadoresSnOrg = [];
            $scope.users = [];
            $scope.organizadores.forEach(function (item, index, array) {
                //Extrae los usuarios activos y que sean de tipo organizador
                var temp = item.roles.data.length - 1;
                $scope.users.push(item);
                if (item.roles.data.length > 0) {
                    if (item.roles.data[temp].name == 'organizer' && item.organization) {
                        $scope.organizadoresAct.push(item);
                    }
                    if (item.roles.data[temp].name == 'organizer' && !item.organization) {
                        $scope.organizadoresSnOrg.push(item);
                    }

                    //Extrae los usuarios inactivos y que sean de tipo organizador
                    // if (item.roles.data[temp].name == 'client' || item.activated == 0) {
                    //     $scope.organizadoresEspera.push(item);
                    //
                    // }
                }
            });
        });

        var action = $http({
            method: 'GET',
            url: $rootScope.settings.pathAPI + 'api/organizations/request/all',
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            }
        }).then(function successCallback(response) {
            console.log(response);
            $scope.organizadoresEspera = response.data;
        }, function errorCallback(response) {
            console.log(response);
            $scope.organizadoresEspera = [];
        });
    };

    //Funcion utilizada para actualizar un tipo de usuario
    $scope.updateUser = function (organizador, index) {
        var idRol = 0;
        for (var i = 0; i < organizador.roles.data.length; i++) {
            if (organizador.roles.data[i].name === 'organizer') {
                idRol = organizador.roles.data[i].id;
            }
        }
        if (idRol != 0) {

            var action = $http({
                method: 'GET',
                url: $rootScope.settings.pathAPI + 'api/users/' + organizador.id + '/remove_role/' + idRol,
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                    "Authorization": 'Bearer ' + $rootScope.settings.token
                },

            });

            console.log($('#sample_1').DataTable());
        } else {

            var action = $http({
                method: 'GET',
                url: $rootScope.settings.pathAPI + 'api/users/' + organizador.id + '/assign_role/2',
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                    "Authorization": 'Bearer ' + $rootScope.settings.token
                }

            }).then(function successCallback(response) {
                $scope.settings.success = $translate.instant('ORGANIZER.SUCCESS_EDIT');

            }, function errorCallback(response) {
                //$scope.settings.success = "error en modificar organizador!";
                console.log(response, $scope.settings.success)
            });
            console.log($('#sample_2').DataTable());
        }
        $scope.UpdateRecords();
    };

    $scope.aproveRejectOrganizer = function (id, isApprovee) {
        if (isApprovee) {
            var path = $rootScope.settings.pathAPI + 'api/organization/request/' + id + '/aprove';
        } else {
            var path = $rootScope.settings.pathAPI + 'api/organization/request/' + id + '/reject';
        }
        var action = $http({
            method: 'GET',
            url: path,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            }

        }).then(function successCallback(response) {
            $scope.settings.success = $translate.instant('ORGANIZER.SUCCESS_EDIT');

        }, function errorCallback(response) {
            console.log(response, $scope.settings.success)
        });
        $scope.UpdateRecords();
    };

    $scope.selectOrganizerRequest = function(data){
        $scope.peticionOrganizador = data;
        $scope.users.forEach(function (item,index,array) {
           if(item.id == data.user_id){
               $scope.usuario = item;
           }
        });
    }
});

angular.module('DashBoardA').directive('ngConfirmClick', [
    function () {
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick;
                var clickAction = attr.confirmedClick;
                element.bind('click', function (event) {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
    }]);

angular.module('DashBoardA').controller('AddOrganization', function ($rootScope, $scope, Servicio, Organizations, $stateParams,
                                                                     $http, $timeout, $window, $translate) {
    $scope.settings = {
        pageTitle: $translate.instant("ORGANIZER.TITLE_ADD"),
        action: $translate.instant("ORGANIZER.ADD")
    };
    $scope.file = null;
    $scope.$watch('file', function (newVal) {
        if (newVal)
            console.log('new: ', newVal);
    });
    $scope.description = null;
    $scope.$watch('description', function (newVal) {
        if (newVal) {

            $scope.organization.description = newVal;
        }
    });

    $scope.uploadFile = function () {
        $scope.file;
    };

    var id = $stateParams.id;

    $scope.organization = {
        name: "",
        description: "",
        phone: "",
        facebook_link: "",
        twitter_link: "",
        email: "",
        website: "",
        picture: "",
        user_id: "",
        activated: 1
    };


    //Metodo encargado de guardar el registro
    //main.js se encuentra el factory bajo el nombre Organizators
    $scope.submit = function () {
        var action = $http({
            method: 'POST',
            url: $rootScope.settings.pathAPI + 'api/organizations',
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
                name: $scope.organization.name,
                description: $scope.organization.description,
                phone: $scope.organization.phone,
                facebook_link: $scope.organization.facebook_link,
                twitter_link: $scope.organization.twitter_link,
                email: $scope.organization.email,
                website: $scope.organization.website,
                user_id: $rootScope.organizator.id,
                activated: $scope.organization.activated
            }
        }).then(function successCallback(response) {
            $scope.settings.success = response;
            $scope.organization = response.data;
            if ($scope.file) {
                $scope.savePictureOrganization();
            } else {
                $scope.transactionSuccess();
            }
        }, function errorCallback(response) {
            // $scope.settings.success = " error al crear y asignar organizacion!";
            console.log("Error", response)
        });

    };

    $scope.savePictureOrganization = function () {


        var data = data = new FormData();
        data.append('file', $scope.file);
        jQuery.ajax({
            url: $rootScope.settings.pathAPI + 'api/uploads',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                var action = $http({
                    method: 'PUT',
                    url: $rootScope.settings.pathAPI + 'api/organizations/' + $scope.organization.id,
                    headers: {
                        "Content-Type": "application/json;charset=UTF-8",
                        "Authorization": 'Bearer ' + $rootScope.settings.token
                    },
                    data: {
                        picture: data.url,
                    }
                }).then(function successCallback(response) {
                    $scope.settings.success = response;
                    console.log($scope.settings.success);
                    $scope.transactionSuccess();
                }, function errorCallback(response) {
                    // $scope.settings.success = " error asignar imagen!";
                    console.log(response);
                });
            }
        });
    };

    $scope.transactionSuccess = function () {
        $window.location.reload();
    }


});

angular.module('DashBoardA').controller('EditOrganization', function ($rootScope, $scope, $translate, Organizations, $http, $timeout, $stateParams) {
    $scope.settings = {
        pageTitle: $translate.instant("ORGANIZER.TITLE_EDIT"),
        action: $translate.instant("ORGANIZER.EDIT")
    };
    $scope.eleccion = null;
    var id = $stateParams.id;



    Organizations.get({id: id}, function (data) {

        $scope.organization = data;

    });

    $scope.submit = function () {

        var action = $http({
            method: 'PUT',
            url: $rootScope.settings.pathAPI + 'api/organizations/' + $scope.organization.id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
                name: $scope.organization.name,
                description: $scope.organization.description,
            }
        });
        $scope.settings.success = $translate.instant("ORGANIZER.SUCCESS_EDIT");
    }
})


//Controlador encargado de subir las imagenes al servidor
angular.module('DashBoardA').controller('FileUploadCtrl', function ($scope, FileUploader, Servicio) {
    //PHP encargado de subir el archivo
    var uploader = $scope.uploader = new FileUploader(
        {
            url: '../Dashboard-Admin/php/upload.php'
        });
    // Filtros para limitar la subida de archivos
    uploader.filters.push(
        {
            name: 'customFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });
    // Metodos llamados en cada operacion relacionada al archivo
    uploader.uonWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
        Servicio.data.nombre = fileItem.file.name;
    };
    uploader.onAfterAddingAll = function (addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function (item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function (fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);

    };
    uploader.onCompleteAll = function () {
        console.info('onCompleteAll');
    };
    console.info('uploader', uploader);
});


//Servicio usado para compartir variables en el scope de cada controlador
angular.module('DashBoardA').factory("Servicio", function () {
    return {
        data: {}
    };
});








