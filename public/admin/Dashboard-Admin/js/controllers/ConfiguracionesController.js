/**
 * Created by Alvade on 07/11/2017.
 */
angular.module('DashBoardA').controller('ConfiguracionesController', function ($rootScope, $translate, $scope, DTOptionsBuilder, Servicio, Staffs, Country, City, Entrance, Category, $http, $state, $window) {

    $scope.$on('$viewContentLoaded', function () {
        //Obtiene los registros que se encuentran en la tabla de usuarios
        $scope.countries = [];
        $scope.cities = [];
        $scope.staves = [];
        $scope.staff = {};
        $scope.cont = 0;
        $scope.$watch('staff', function (newVal) {
        });
        $scope.$watch('staves', function (newVal) {
        });
        if ($rootScope.update) {
            $rootScope.update = false;
            $window.location.reload();
        }
        if (!$rootScope.isAdmin) {
            Staffs.get(function (data) {
                $scope.staves = [];
                data.data.forEach(function (item) {
                    if (item.organization_id == $scope.organizador.organization.id) {
                        $scope.staves.push(item);
                    }
                });
            });
            var data = localStorage.getItem('userLogged');
            $scope.organizador = JSON.parse(data);
            console.log($scope.organizador);
        }
            Category.get(function (data) {
                $scope.categorys = data.data;
            });
            Country.get(function (data) {
                $scope.countries = data.data;
            });
            City.get(function (data) {
                $scope.cities = data.data;
            });

        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para cada tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap().withOption("responsive", true);

            $scope.vm2 = this;
            $scope.vm2.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap().withOption("responsive", true);

            $scope.vm3 = this;
            $scope.vm3.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap().withOption("responsive", true);


        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });
    //$rootScope.settings.pageTitle = settings.pageTitle;
    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    $scope.$watch('categorys', function (newVal) {
    });


    $scope.reload = function () {
        $window.location.reload();
    };
    //Categorias
    $scope.removeCategoria = function (id) {
        var action = $http({
            method: 'DELETE',
            url: $rootScope.settings.pathAPI + 'api/categories/' + id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {}
        });
        Category.get(function (data) {
            $scope.categorys = data.data;
        });
    };

    $scope.resetStaff = function () {
        $scope.staff = {
            id: 0,
            name: "",
            email: "",
            organization_id: 0
        }
    };

    $scope.addStaff = function (edit) {
        console.log('sfdsfs sdfsd sfd');
        if (edit) {
            var action = $http({
                method: 'PUT',
                url: $rootScope.settings.pathAPI + 'api/staff/' + edit.id + '/reset_access',
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                    "Authorization": 'Bearer ' + $rootScope.settings.token
                },
                data: {}
            }).then(function successCallback(response) {
                console.log(response);
                Staffs.get(function (data) {
                    $scope.staves = [];
                    data.data.forEach(function (item) {
                        if (item.organization_id == $scope.organizador.organization.id) {
                            $scope.staves.push(item);
                        }
                    });
                });
            }, function errorCallback(response) {
                $scope.settings.error = $translate.instant("ERROR.EDIT_STAFF");
                console.log(response, $scope.settings.error)
            });
        } else {
            var action = $http({
                method: 'POST',
                url: $rootScope.settings.pathAPI + 'api/staff',
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                    "Authorization": 'Bearer ' + $rootScope.settings.token
                },
                data: {
                    name: $scope.staff.name,
                    email: $scope.staff.email,
                    organization_id: $scope.organizador.organization.id
                }
            }).then(function successCallback(response) {
                Staffs.get(function (data) {
                    $scope.staves = [];
                    data.data.forEach(function (item) {
                        if (item.organization_id == $scope.organizador.organization.id) {
                            $scope.staves.push(item);
                        }
                    });
                });
            }, function errorCallback(response) {
                $scope.settings.error = $translate.instant("ERROR.SAVE_STAFF");
                console.log(response, $scope.settings.error)
            });
        }
    };

    $scope.deleteStaff = function (data, index) {
        var position = index;
        $scope.staves.splice(position, 1);
        if (data.id != 0) {
            var action = $http({
                method: 'DELETE',
                url: $rootScope.settings.pathAPI + 'api/staff/' + data.id,
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                    "Authorization": 'Bearer ' + $rootScope.settings.token
                },
                data: {}
            })
        }
    };

});


angular.module('DashBoardA').controller('AddCategoria', function ($rootScope, $translate, $scope, DTOptionsBuilder, Servicio, Events, $http, $state, $window, $stateParams) {
    $scope.settings = {
        pageTitle: $translate.instant("CATEGORY_OBJECT.ADD_CATEGORY"),
        action: $translate.instant("CATEGORY_OBJECT.ADD")
    };
    //$scope.category ={name:"",description:""};
    $scope.$on('$viewContentLoaded', function () {
        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();

        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });
    //$rootScope.settings.pageTitle = settings.pageTitle;
    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    var count = 0;

    //Inicia la variable categoria con los datos correspondientes
    $scope.category = {
        name: "",
        description: "",
        picture: null,
        name_translated: {
            es: ""
        },
        description_translated: {
            es: ""
        }
    };
    $scope.file = null;
    $scope.$watch('file', function (newVal) {
    });

    $scope.submit = function () {
        var action = $http({
            method: 'POST',
            url: $rootScope.settings.pathAPI + 'api/categories',
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
                name: $scope.category.name,
                description: $scope.category.description,
                name_translated: {
                    es: $scope.category.name_translated.es,
                    en: $scope.category.name
                },
                description_translated: {
                    es: $scope.category.description_translated.es,
                    en: $scope.category.description
                }
            }
        }).then(function successCallback(response) {
            if ($scope.file) {
                var data = data = new FormData();
                data.append('file', $scope.file);
                //Sube el archivo al servidor
                jQuery.ajax({
                    url: $rootScope.settings.pathAPI + 'api/uploads',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        var action = $http({
                            method: 'PUT',
                            url: $rootScope.settings.pathAPI + 'api/categories/' + response.data.id,
                            headers: {
                                "Content-Type": "application/json;charset=UTF-8",
                                "Authorization": 'Bearer ' + $rootScope.settings.token
                            },
                            data: {
                                picture: data.url
                            }
                        }).then(function successCallback(response) {
                            $rootScope.update = true;
                            $window.history.back();
                        }, function errorCallback(response) {
                            $scope.settings.error = $translate.instant("ERROR.IMAGE_CATEGORY");
                            console.log(response, $scope.settings.error)
                        });
                    }
                });
            } else {
                $rootScope.update = true;
                $window.history.back();
            }
        }, function errorCallback(response) {
            $scope.settings.error = $translate.instant("ERROR.SAVE_CATEGORY");
            console.log(response, $scope.settings.error)
        });
        $scope.settings.success = $translate.instant("CATEGORY_OBJECT.SUCCESS_ADD");


    };


    $scope.reload = function () {
        $window.location.reload();
    }

});

angular.module('DashBoardA').controller('edCategoria', function ($rootScope, $translate, $scope, DTOptionsBuilder, Servicio, Events, Category, $http, $state, $window, $stateParams) {
    $scope.settings = {
        pageTitle: $translate.instant("CATEGORY_OBJECT.EDIT_CATEGORY"),
        action: $translate.instant("CATEGORY_OBJECT.EDIT")
    };
    //$scope.category ={name:"",description:""};
    $scope.$on('$viewContentLoaded', function () {
        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();

        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });
    //$rootScope.settings.pageTitle = settings.pageTitle;
    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    var id = $stateParams.id;

    Category.get({id: id}, function (data) {
        $scope.category = data;

    });
    $scope.file = null;
    $scope.$watch('file', function (newVal) {

    });

    //Obtiene los registros que se encuentran en la tabla de usuarios

    $scope.submit = function () {
        var action = $http({
            method: 'PUT',
            url: $rootScope.settings.pathAPI + 'api/categories/' + $scope.category.id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
                name: $scope.category.name,
                description: $scope.category.description,
                name_translated: {
                    es: $scope.category.name_translated.es,
                    en: $scope.category.name
                },
                description_translated: {
                    es: $scope.category.description_translated.es,
                    en: $scope.category.description
                }
            }
        }).then(function successCallback(response) {
            if ($scope.file) {
                var data = data = new FormData();
                data.append('file', $scope.file);
                //Sube el archivo al servidor
                jQuery.ajax({
                    url: $rootScope.settings.pathAPI + 'api/uploads',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        var action = $http({
                            method: 'PUT',
                            url: $rootScope.settings.pathAPI + 'api/categories/' + $scope.category.id,
                            headers: {
                                "Content-Type": "application/json;charset=UTF-8",
                                "Authorization": 'Bearer ' + $rootScope.settings.token
                            },
                            data: {
                                picture: data.url
                            }
                        }).then(function successCallback(response) {
                            $rootScope.update = true;
                            $window.history.back();
                        }, function errorCallback(response) {
                            $scope.settings.error = $translate.instant("ERROR.IMAGE_CATEGORY");
                            console.log(response, $scope.settings.error)
                        });
                    }
                });
            } else {
                $rootScope.update = true;
                $window.history.back();
            }
        }, function errorCallback(response) {
            $scope.settings.error = $translate.instant("ERROR.EDIT_CATEGORY");
            console.log(response, $scope.settings.error)
        });
        $scope.settings.success = $translate.instant("CATEGORY_OBJECT.SUCCESS_EDIT");

    };


    $scope.reload = function () {
        $window.location.reload();
    }

});


angular.module('DashBoardA').controller('AddPais', function ($rootScope, $translate, $scope, DTOptionsBuilder, Servicio, Events, $http, $state, $window, $stateParams) {
    $scope.settings = {
        pageTitle: $translate.instant("COUNTRY.ADD_COUNTRY"),
        action: $translate.instant("COUNTRY.ADD")
    };
    //$scope.category ={name:"",description:""};
    $scope.$on('$viewContentLoaded', function () {
        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();

        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });
    //$rootScope.settings.pageTitle = settings.pageTitle;
    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    var count = 0;

    //Inicia la variable categoria con los datos correspondientes
    $scope.country = {
        name: "",
        picture: null
    };
    $scope.file = null;
    $scope.$watch('file', function (newVal) {
    });

    $scope.submit = function () {
        var action = $http({
            method: 'POST',
            url: $rootScope.settings.pathAPI + 'api/countries',
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
                name: $scope.country.name
            }
        }).then(function successCallback(response) {
            if ($scope.file) {
                var data = data = new FormData();
                data.append('file', $scope.file);
                //Sube el archivo al servidor
                jQuery.ajax({
                    url: $rootScope.settings.pathAPI + 'api/uploads',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        var action = $http({
                            method: 'PUT',
                            url: $rootScope.settings.pathAPI + 'api/countries/' + response.data.id,
                            headers: {
                                "Content-Type": "application/json;charset=UTF-8",
                                "Authorization": 'Bearer ' + $rootScope.settings.token
                            },
                            data: {
                                picture: data.url
                            }
                        }).then(function successCallback(response) {
                            $rootScope.update = true;
                            $window.history.back();
                        }, function errorCallback(response) {
                            $scope.settings.error = $translate.instant("ERROR.IMAGE_COUNTRY");
                            console.log(response, $scope.settings.error)
                        });
                    }
                });
            } else {
                $rootScope.update = true;
                $window.history.back();
            }
        }, function errorCallback(response) {
            $scope.settings.error = $translate.instant("ERROR.SAVE_COUNTRY");
            console.log(response, $scope.settings.error)
        });
        $scope.settings.success = $translate.instant("COUNTRY.SUCCESS_ADD");


    };


    $scope.reload = function () {
        $window.location.reload();
    }

});

angular.module('DashBoardA').controller('edPais', function ($rootScope, $translate, $scope, DTOptionsBuilder, Country, Servicio, Events, Category, $http, $state, $window, $stateParams) {
    $scope.settings = {
        pageTitle: $translate.instant("COUNTRY.EDIT_COUNTRY"),
        action: $translate.instant("COUNTRY.EDIT")
    };
    //$scope.category ={name:"",description:""};
    $scope.$on('$viewContentLoaded', function () {
        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();

        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });
    //$rootScope.settings.pageTitle = settings.pageTitle;
    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    var id = $stateParams.id;

    Country.get({id: id}, function (data) {
        $scope.country = data;
        console.log($scope.country);

    });
    $scope.file = null;
    $scope.$watch('file', function (newVal) {

    });

    //Obtiene los registros que se encuentran en la tabla de usuarios

    $scope.submit = function () {
        var action = $http({
            method: 'PUT',
            url: $rootScope.settings.pathAPI + 'api/countries/' + $scope.country.id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
                name: $scope.country.name,
                description: $scope.country.description
            }
        }).then(function successCallback(response) {
            if ($scope.file) {
                var data = data = new FormData();
                data.append('file', $scope.file);
                //Sube el archivo al servidor
                jQuery.ajax({
                    url: $rootScope.settings.pathAPI + 'api/uploads',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        var action = $http({
                            method: 'PUT',
                            url: $rootScope.settings.pathAPI + 'api/countries/' + $scope.country.id,
                            headers: {
                                "Content-Type": "application/json;charset=UTF-8",
                                "Authorization": 'Bearer ' + $rootScope.settings.token
                            },
                            data: {
                                picture: data.url
                            }
                        }).then(function successCallback(response) {
                            $rootScope.update = true;
                            $window.history.back();
                        }, function errorCallback(response) {
                            $scope.settings.error = $translate.instant("ERROR.IMAGE_COUNTRY");
                            console.log(response, $scope.settings.error)
                        });
                    }
                });
            } else {
                $rootScope.update = true;
                $window.history.back();
            }
        }, function errorCallback(response) {
            $scope.settings.error = $translate.instant("ERROR.EDIT_COUNTRY");
            console.log(response, $scope.settings.error)
        });
        $scope.settings.success = $translate.instant("COUNTRY.SUCCESS_EDIT");

    };


    $scope.reload = function () {
        $window.location.reload();
    }

});

angular.module('DashBoardA').controller('AddCiudad', function ($rootScope, $translate, $scope, DTOptionsBuilder, Servicio, Events, Country, $http, $state, $window, $stateParams) {
    $scope.settings = {
        pageTitle: $translate.instant("CITY.ADD_CITY"),
        action: $translate.instant("CITY.ADD")
    };
    //$scope.category ={name:"",description:""};
    $scope.$on('$viewContentLoaded', function () {
        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();

        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });
    //$rootScope.settings.pageTitle = settings.pageTitle;
    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    $scope.country = {};
    Country.get({}, function (data) {
        $scope.countries = data.data;
        console.log($scope.countries);

    });

    $scope.file = null;
    $scope.$watch('file', function (newVal) {
    });


    //Obtiene los registros que se encuentran en la tabla de usuarios
    $scope.city = {
        name: "",
        country_id: 0,
    };

    $scope.submit = function () {
        var action = $http({
            method: 'POST',
            url: $rootScope.settings.pathAPI + 'api/cities',
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
                name: $scope.city.name,
                country_id: $scope.country.value.id,
            }
        }).then(function successCallback(response) {
            if ($scope.file) {
                var data = data = new FormData();
                data.append('file', $scope.file);
                //Sube el archivo al servidor
                jQuery.ajax({
                    url: $rootScope.settings.pathAPI + 'api/uploads',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        var action = $http({
                            method: 'PUT',
                            url: $rootScope.settings.pathAPI + 'api/cities/' + response.data.id,
                            headers: {
                                "Content-Type": "application/json;charset=UTF-8",
                                "Authorization": 'Bearer ' + $rootScope.settings.token
                            },
                            data: {
                                picture: data.url
                            }
                        }).then(function successCallback(response) {
                            $rootScope.update = true;
                            $window.history.back();
                        }, function errorCallback(response) {
                            $scope.settings.error = $translate.instant("ERROR.IMAGE_CITY");
                            console.log(response, $scope.settings.error)
                        });
                    }
                });
            } else {
                $rootScope.update = true;
                $window.history.back();
            }
        }, function errorCallback(response) {
            $scope.settings.error = $translate.instant("ERROR.SAVE_CITY");
            console.log(response, $scope.settings.error)
        });
        $scope.settings.success = $translate.instant("CITY.SUCCESS_ADD");
    };


    $scope.reload = function () {
        $window.location.reload();
    }

});

angular.module('DashBoardA').controller('edCiudad', function ($rootScope, $translate, $scope, DTOptionsBuilder, Servicio, Events, City, Country, $http, $state, $window, $stateParams) {
    $scope.settings = {
        pageTitle: $translate.instant("CITY.EDIT_CITY"),
        action: $translate.instant("CITY.EDIT")
    };
    //$scope.category ={name:"",description:""};
    $scope.$on('$viewContentLoaded', function () {
        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();

        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });
    //$rootScope.settings.pageTitle = settings.pageTitle;
    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;

    var id = $stateParams.id;
    $scope.country = {};
    $scope.city = {};
    City.get({id: id}, function (data) {
        $scope.city = data;
        console.log($scope.city);
        Country.get({id: data.country_id}, function (data) {
            $scope.country.value = data;
            console.log($scope.country.value);

        });
    });
    Country.get({}, function (data) {
        $scope.countries = data.data;
        console.log($scope.countries);

    });
    $scope.file = null;
    $scope.$watch('file', function (newVal) {

    });

    //Obtiene los registros que se encuentran en la tabla de usuarios

    $scope.submit = function () {
        var action = $http({
            method: 'PUT',
            url: $rootScope.settings.pathAPI + 'api/cities/' + $scope.city.id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
                name: $scope.city.name,
                country_id: $scope.country.value.id
            }
        }).then(function successCallback(response) {
            if ($scope.file) {
                var data = data = new FormData();
                data.append('file', $scope.file);
                //Sube el archivo al servidor
                jQuery.ajax({
                    url: $rootScope.settings.pathAPI + 'api/uploads',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function (data) {
                        var action = $http({
                            method: 'PUT',
                            url: $rootScope.settings.pathAPI + 'api/cities/' + $scope.city.id,
                            headers: {
                                "Content-Type": "application/json;charset=UTF-8",
                                "Authorization": 'Bearer ' + $rootScope.settings.token
                            },
                            data: {
                                picture: data.url
                            }
                        }).then(function successCallback(response) {
                            $rootScope.update = true;
                            $window.history.back();
                        }, function errorCallback(response) {
                            $scope.settings.error = $translate.instant("ERROR.IMAGE_CITY");
                            console.log(response, $scope.settings.error)
                        });
                    }
                });
            } else {
                $rootScope.update = true;
                $window.history.back();
            }
        }, function errorCallback(response) {
            $scope.settings.error = $translate.instant("ERROR.EDIT_CITY");
            console.log(response, $scope.settings.error)
        });
        $scope.settings.success = $translate.instant("CITY.SUCCESS_EDIT");

    };


    $scope.reload = function () {
        $window.location.reload();
    }

});

//Servicio usado para compartir variables en el scope de cada controlador
angular.module('DashBoardA').factory("Servicio", function () {
    return {
        data: {}
    };
})
;

angular.module('DashBoardA').directive('ngConfirmClick', [
    function () {
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick;
                var clickAction = attr.confirmedClick;
                element.bind('click', function (event) {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction);
                    }
                });
            }
        };
    }]);

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
angular.module('DashBoardA').filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

angular.module('DashBoardA').controller('SendNotification', function ($rootScope, $translate, $scope, DTOptionsBuilder, Servicio, Events, $http, $state, $window, $stateParams) {
    $scope.settings = {
        pageTitle: $translate.instant("SEND_NOTIFICATION"),
        action: $translate.instant("SEND")
    };
    //$scope.category ={name:"",description:""};
    $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();
    });
    //$rootScope.settings.pageTitle = settings.pageTitle;
    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    var count = 0;

    //Inicia la variable mensaje con los datos requeridos
    $scope.mensaje = {
        description: "",
    };
    $scope.$watch('mensaje', function (newVal) {
    });

    $scope.submit = function () {
        var action = $http({
            method: 'POST',
            url: $rootScope.settings.pathNotifications + 'notificaciones.php',
            headers: {
                "Content-Type": "application/json;charset=UTF-8"
            },
            data: {
                mensaje: $scope.mensaje.description
            }
        }).then(function successCallback(response) {
            $scope.settings.success = response;
            $scope.mensaje = {
                description: "",
            };
        }, function errorCallback(response) {
            $scope.settings.error = response;
            console.log(response, $scope.settings.error)
        });
    };


    $scope.reload = function () {
        $window.location.reload();
    }

});