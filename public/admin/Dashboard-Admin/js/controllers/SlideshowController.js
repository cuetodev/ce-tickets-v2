/**
 * Created by Cesar on 15/08/2017.
 */
angular.module('DashBoardA').controller('SlideshowController', function($rootScope, $scope, Users, $http, $timeout) {
	$scope.$on('$viewContentLoaded', function() {
		// initialize core components
		App.initAjax();
	});

	// set sidebar closed and body solid layout mode
	$rootScope.settings.layout.pageContentWhite = true;
	$rootScope.settings.layout.pageBodySolid = false;
	$rootScope.settings.layout.pageSidebarClosed = false;

	var count = 0;

	Users.get(function (data) {
		$scope.users = data.response;
		var length = $scope.users.length;
		for (i = 0; i < length; i++) {
			if($scope.users[i].usua_activo == 'A'){
				count = count + 1;
				console.log(count);
			}
		}
		$scope.usrsActivos = count;
	})

});
