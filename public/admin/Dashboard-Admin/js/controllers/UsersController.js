/**
 * Created by Cesar on 17/07/2017.
 */
angular.module('DashBoardA').controller('UsersController', function ($rootScope, $scope, $translate, DTOptionsBuilder, Servicio, Rol, Users, $http, $timeout, $window) {
    $scope.$on('$viewContentLoaded', function () {
        //Obtiene los registros que se encuentran en la tabla de usuarios
        Users.get(function (data) {
            $scope.users = data.data;
            console.log($scope.users);
            var length = $scope.users.length;
            for (i = 0; i < length; i++) {
                if ($scope.users[i].activated == 0) {
                    count = count + 1;
                    console.log(count);
                }
            }
        });

        Rol.get(function (data) {
            $scope.roles = data.data;
            console.log($scope.roles);
        });
        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();

            $scope.vm2 = this;
            $scope.vm2.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource($translate.instant("URL_DATATABLE"))
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();

            $scope.vm3 = this;
            $scope.vm3.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource($translate.instant("URL_DATATABLE"))
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap();

        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });

    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    var count = 0;

    $scope.$watch('users', function (newVal) {
    });
    $scope.updateUsers = function () {
        Users.get(function (data) {
            $scope.users = data.data;
            console.log($scope.users);
            var length = $scope.users.length;
            for (i = 0; i < length; i++) {
                if ($scope.users[i].activated == 0) {
                    count = count + 1;
                    console.log(count);
                }
            }
        });
    };

    //Funcion utilizada para eliminar un registro de la base
    $scope.remove = function (id) {
        Users.delete({id: id}).$promise.then(function (data) {
            if (data.response) {
                $route.reload();
            }
        })
    };

    $scope.activarUsuario = function (id) {
        var action = $http({
            method: 'PUT',
            url: $rootScope.settings.pathAPI + 'api/users/' + id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {activated: 1}
        });
        $scope.updateUsers();
    };
    $scope.bajaUsuario = function (id) {
        var action = $http({
            method: 'PUT',
            url: $rootScope.settings.pathAPI + 'api/users/' + id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {activated: 2}
        });
        $scope.updateUsers();
    }
});

angular.module('DashBoardA').controller('AddUser', function ($rootScope, $translate, $scope, Servicio, Users, $http, $timeout) {
    $scope.settings = {
        pageTitle: $translate.instant('USER.TITLE_ADD'),
        action: $translate.instant('USER.ADD')
    };

    $scope.user = {
        id: "",
        first_name: "",
        last_name: "",
        gender: "",
        birth_date: "",
        phone_number: "",
        username: "",
        email: "",
        avatar: "",
        password: "",
        activated: "",
        remember_token: "",
        created_at: "",
        updated_at: "",
        api_token: ""
    };


    //Metodo encargado de guardar el registro en
    //main.js se encuentra el factory bajo el nombre Users
    $scope.submit = function () {

        if (Servicio.data.nombre) {
            var path = '../Dashboard-Admin/appData/img/users/';
            console.log(path.concat(Servicio.data.nombre));
            $scope.user.usua_path_imagen = path.concat(Servicio.data.nombre);

        }
        Users.save({user: $scope.user}).$promise.then(function (data) {
            if (data.response) {
                angular.copy({}, $scope.user);
                $scope.settings.success = $translate.instant('USER.SUCCESS_ADD');
            }
        })
    }


});

angular.module('DashBoardA').controller('EditUser', function ($rootScope, $scope, $translate, User, $http, $timeout, $stateParams, $window) {
    $scope.settings = {
        pageTitle: $translate.instant('USER.TITLE_EDIT'),
        action: $translate.instant('USER.EDIT')
    };

    var id = $stateParams.id;

    if (id) {
        User.get({id: id}, function (data) {
            $scope.user = data;
            console.log($scope.user);
        });
    }

    $scope.submit = function () {
        /*User.update({id: $scope.user.id},{email: $scope.user.email, first_name: $scope.user.first_name,
         last_name: $scope.user.last_name,birth_date: $scope.user.birth_date}, function (data) {
         console.log(data,' data update');
         $scope.settings.success = "El usuario ha sido editado correctamente!";
         });*/
        var action = $http({
            method: 'PUT',
            url: $rootScope.settings.pathAPI + 'api/users/' + $scope.user.id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
                first_name: $scope.user.first_name,
                last_name: $scope.user.last_name,
                birth_date: $scope.user.birth_date,
                email: $scope.user.email,
                activated: $scope.user.activated
            }
        });
        $scope.settings.success = $translate.instant('USER.SUCCESS_EDIT');
    };

    $scope.reload = function () {
        $window.location.reload();
        //$scope.reload();
    }

});


//Controlador encargado de subir las imagenes al servidor
angular.module('DashBoardA').controller('FileUploadCtrl', function ($scope, FileUploader, Servicio) {
    //PHP encargado de subir el archivo
    var uploader = $scope.uploader = new FileUploader(
        {
            url: '../Dashboard-Admin/php/upload.php'
        });
    // Filtros para limitar la subida de archivos
    uploader.filters.push(
        {
            name: 'customFilter',
            fn: function (item /*{File|FileLikeObject}*/, options) {
                return this.queue.length < 10;
            }
        });
    // Metodos llamados en cada operacion relacionada al archivo
    uploader.uonWhenAddingFileFailed = function (item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function (fileItem) {
        console.info('onAfterAddingFile', fileItem);
        Servicio.data.nombre = fileItem.file.name;
        console.log(Servicio.data.nombre);
    };
    uploader.onAfterAddingAll = function (addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function (item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function (fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function (progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function (fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function (fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function (fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function (fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);

    };
    uploader.onCompleteAll = function () {
        console.info('onCompleteAll');
    };
    console.info('uploader', uploader);
});

//Servicio usado para compartir variables en el scope de cada controlador
angular.module('DashBoardA').factory("Servicio", function () {
    return {
        data: {}
    };
});






