/**
 * Created by Alvade on 26/06/2018.
 */

angular.module('DashBoardA').controller('TicketsController', function ($rootScope, $scope, Tickets, $translate, DTOptionsBuilder, Servicio, Events, Event, Entrance, $http, $state, $window, $stateParams) {

    $scope.$on('$viewContentLoaded', function () {
        //Obtiene los registros que se encuentran en la tabla de usuarios
        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap().withOption("responsive", true);


        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });
    $scope.tickets = [];
    $scope.evento = [];
    $scope.entrances = [];
    //$rootScope.settings.pageTitle = settings.pageTitle;
    // oculta el sidebar y pone el body solido en blanco
    var id = $stateParams.id;
    if (id) {

        Event.get({id: id}, function (data) {
            $scope.evento = data;
            for (var i = 0; i < $scope.evento.entrances.data.length; i++) {
                $scope.evento.entrances.data[i].price = $scope.numberFormat($scope.evento.entrances.data[i].price);
                if ($scope.evento.entrances.data[i].price <= 0) {
                    $scope.evento.entrances.data[i].isGratis = true;
                } else {
                    $scope.evento.entrances.data[i].isGratis = false;
                }
                $scope.entrances.push($scope.evento.entrances.data[i]);
            }
            Tickets.get({id: id}, function (data) {
                data.data.forEach(function (item) {
                    var pos = item.code.indexOf('-');
                    item.codeSmall = item.code.substr(pos - 2, 5);
                    item.date = new Date(item.created_at.date);
                    $scope.entrances.forEach(function (entrance) {
                        if (item.entrance_id == entrance.id) {
                            item.entrance = entrance.name;
                        }
                    });
                    $scope.tickets.push(item);
                });
            });
        });
    }

    $scope.numberFormat = function (price) {
        if (price.toString().indexOf('.') == -1) {
            aux = price.toString().concat(".00");
            price = parseFloat(aux);

        }

        return price;
    };
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;


});