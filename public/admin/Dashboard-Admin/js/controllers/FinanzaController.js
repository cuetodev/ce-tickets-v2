angular.module('DashBoardA').controller('FinanzaController', function ($rootScope, $scope, $translate, $interval, Users, $http, $timeout, DTOptionsBuilder, Organizations, FinancesByOrganization, Event, Events, Finances) {
    $scope.financesOrganizer = [];
    $scope.financesEvent = [];
    $scope.finances = [{selledTickets: 0, percentOfTicketsSelled: 0, collectedMoney: 0, availablesTickets: 0}];
    $scope.events = [];
    $scope.filterTimeEvents = 'week';
    $scope.filterTimeEventsAdmin = 'week';
    $scope.filterTimeOrganization = 'week';

    $scope.loadData = function (count) {
        if ($rootScope.isAdmin) {
            Organizations.get(
                function (data) {
                    $scope.organizations = data.data;
                    var action = $http({
                        method: 'GET',
                        url: $rootScope.settings.pathAPI + 'api/finances/organization/all?type=' + $scope.filterTimeOrganization,
                        headers: {
                            "Content-Type": "application/json;charset=UTF-8",
                            "Authorization": 'Bearer ' + $rootScope.settings.token
                        },
                        data: {}
                    }).then(function successCallback(response) {
                        var length = $scope.organizations.length;
                        var lengthResponse = response.data.length;
                        $scope.financesOrganizer = [];
                        for (i = 0; i < length; i++) {
                            for (j = 0; j < lengthResponse; j++) {
                                if (response.data[j].organization_id == $scope.organizations[i].id) {

                                    var object = response.data[j];
                                    object.name = $scope.organizations[i].name;
                                    object.picture = $scope.organizations[i].picture;
                                    if (count == 1 && $scope.financesOrganizer.indexOf(object) < 0 || count == 0) {

                                        $scope.financesOrganizer.push(object);
                                        break;
                                    }
                                }
                                // FinancesByFilter.get({
                                //     filter: 'by_organization',
                                //     id: $scope.organizations[i].id
                                // }, function (data) {
                                //
                                //     if (count == 1 && !$scope.financesOrganizer.indexOf(data) || count == 0) {
                                //         $scope.financesOrganizer.push(data);
                                //     }
                                // })
                            }
                        }
                    });

                    $scope.financesEvent = [];
                    for (var pos = 0; pos < $scope.organizations.length; pos++) {
                        var action = $http({
                            method: 'GET',
                            url: $rootScope.settings.pathAPI + 'api/finances/organization/' + $scope.organizations[pos].id + '/events?type=' + $scope.filterTimeEventsAdmin,
                            headers: {
                                "Content-Type": "application/json;charset=UTF-8",
                                "Authorization": 'Bearer ' + $rootScope.settings.token
                            },
                            data: {}
                        }).then(function successCallback(response) {
                            if (response) {
                                if (count == 0) {
                                    // $scope.financesEvent = response.data;
                                    Events.get(
                                        function (data) {
                                            for (var j = 0; j < response.data.length; j++) {
                                                for (var i = 0; i < data.data.length; i++) {
                                                    if (response.data[j].event_id == data.data[i].id) {
                                                        var eventFinance = {
                                                            eventName: data.data[i].name,
                                                            collectedMoney: response.data[j].collectedMoney,
                                                            selledTickets: response.data[j].selledTickets,
                                                            availablesTickets: response.data[j].availablesTickets,
                                                            percentOfTicketsSelled: response.data[j].percentOfTicketsSelled,
                                                            event_id: response.data[j].event_id,
                                                            status: data.data[i].status,
                                                            paid: data.data[i].paid,
                                                            paid_date: data.data[i].paid_date,
                                                            reference: data.data[i].reference,
                                                            org_name: data.data[i].organization.name
                                                        };
                                                        console.log(data.data[i]);
                                                        $scope.financesEvent.push(eventFinance);
                                                        // $scope.events.push(data.data[i]);
                                                        break;
                                                    }
                                                }
                                            }

                                        });

                                } else {
                                    Events.get(
                                        function (data) {

                                            for (var j = 0; j < response.data.length; j++) {
                                                for (var i = 0; i < data.data.length; i++) {
                                                    if (response.data[j].event_id == data.data[i].id) {
                                                        var eventFinance = {
                                                            eventName: data.data[i].name,
                                                            collectedMoney: response.data[j].collectedMoney,
                                                            selledTickets: response.data[j].selledTickets,
                                                            availablesTickets: response.data[j].availablesTickets,
                                                            percentOfTicketsSelled: response.data[j].percentOfTicketsSelled,
                                                            event_id: response.data[j].event_id,
                                                            status: data.data[i].status,
                                                            paid: data.data[i].paid,
                                                            paid_date: data.data[i].paid_date,
                                                            reference: data.data[i].reference,
                                                            org_name: data.data[i].organization.name
                                                        };
                                                        console.log(data.data[i]);
                                                        console.log(eventFinance);
                                                        if (count == 1 && $scope.financesEvent.indexOf(eventFinance) < 0) {
                                                            $scope.financesEvent.push(eventFinance);
                                                            // $scope.events.push(data.data[i]);
                                                        }
                                                        break;
                                                    }
                                                }
                                            }
                                        });
                                    // for (var i = 0; i < response.data.length; i++) {
                                    //     if (count == 1 && !$scope.financesOrganizer.indexOf(response.data[i])) {
                                    //         Event.get({id: response.data[i].event_id}, function (data) {
                                    //             var eventFinance = {
                                    //                 eventName: data.name,
                                    //                 collectedMoney: response.data[i].collectedMoney,
                                    //                 selledTickets: response.data[i].selledTickets,
                                    //                 availablesTickets: response.data[i].availablesTickets,
                                    //                 percentOfTicketsSelled: response.data[i].percentOfTicketsSelled,
                                    //                 event_id: response.data[i].event_id
                                    //             };
                                    //             $scope.financesEvent.push(eventFinance);
                                    //         });
                                    //         break;
                                    //     }
                                    // }
                                }
                            }
                        });
                    }
                });


            var action = $http({
                method: 'GET',
                url: $rootScope.settings.pathAPI + 'api/finances/all',
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                    "Authorization": 'Bearer ' + $rootScope.settings.token
                },
                data: {}
            }).then(function successCallback(response) {
                console.log(response);
                if (response) {
                    $scope.finances = response.data;
                }

            });

        } else {
            $scope.loadOrganizationFinances(count);
        }
    };

    $scope.loadOrganizationFinances = function (count) {
        var data = localStorage.getItem('userLogged');
        $scope.organizador = JSON.parse(data);
        var action = $http({
            method: 'GET',
            url: $rootScope.settings.pathAPI + 'api/finances/organization/' + $scope.organizador.organization.id + '/events?type=' + $scope.filterTimeEvents,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {}
        }).then(function successCallback(response) {
            if (response.data.length > 0) {
                if (count == 0) {
                    // $scope.financesEvent = response.data;
                    Events.get(
                        function (data) {
                            for (var j = 0; j < response.data.length; j++) {
                                for (var i = 0; i < data.data.length; i++) {

                                    if (response.data[j].event_id == data.data[i].id) {
                                        var eventFinance = {
                                            eventName: data.data[i].name,
                                            collectedMoney: response.data[j].collectedMoney,
                                            selledTickets: response.data[j].selledTickets,
                                            availablesTickets: response.data[j].availablesTickets,
                                            percentOfTicketsSelled: response.data[j].percentOfTicketsSelled,
                                            event_id: response.data[j].event_id,
                                            status: data.data[i].status,
                                            paid: data.data[i].paid,
                                            paid_date: data.data[i].paid_date,
                                            reference: data.data[i].reference,

                                        };
                                        $scope.financesEvent.push(eventFinance);
                                        // $scope.events.push(data.data[i]);
                                        break;
                                    }
                                }
                            }

                        });

                } else {
                    Events.get(
                        function (data) {
                            $scope.financesEvent = [];
                            for (var j = 0; j < response.data.length; j++) {
                                for (var i = 0; i < data.data.length; i++) {
                                    if (response.data[j].event_id == data.data[i].id) {
                                        var eventFinance = {
                                            eventName: data.data[i].name,
                                            collectedMoney: response.data[j].collectedMoney,
                                            selledTickets: response.data[j].selledTickets,
                                            availablesTickets: response.data[j].availablesTickets,
                                            percentOfTicketsSelled: response.data[j].percentOfTicketsSelled,
                                            event_id: response.data[j].event_id,
                                            status: data.data[i].status,
                                            paid: data.data[i].paid,
                                            paid_date: data.data[i].paid_date,
                                            reference: data.data[i].reference,

                                        };
                                        if (count == 1 && $scope.financesEvent.indexOf(eventFinance) < 0) {
                                            $scope.financesEvent.push(eventFinance);
                                            // $scope.events.push(data.data[i]);
                                        }
                                        break;
                                    }
                                }
                            }
                        });
                }
                // if (count == 0) {
                //     Events.get(
                //         function (data) {
                //             var j = 0;
                //             for (var i = 0; i < data.data.length; i++) {
                //                 if ($rootScope.userLogged.organization != undefined)
                //                     if ($rootScope.userLogged.organization.id == data.data[i].organization.id) {
                //                         if (count == 1 && !$scope.financesOrganizer.indexOf(data.data[i]) || count == 0) {
                //                             // $scope.events.push(data.data[i]);
                //                             if (response.data[j].event_id == data.data[i].id)
                //                                 var eventFinance = {
                //                                     eventName: data.data[i].name,
                //                                     collectedMoney: response.data[j].collectedMoney,
                //                                     selledTickets: response.data[j].selledTickets,
                //                                     availablesTickets: response.data[j].availablesTickets,
                //                                     percentOfTicketsSelled: response.data[j].percentOfTicketsSelled
                //                                 };
                //                             $scope.financesEvent.push(eventFinance);
                //                         }
                //                         j++;
                //                     }
                //             }
                //
                //             for (var i = 0; i < $scope.financesEvent.length; i++) {
                //                 console.log($scope.financesEvent[i]);
                //             }
                //         });
                // } else {
                //     for (var i = 0; i < response.data.length; i++) {
                //         if (count == 1 && !$scope.financesOrganizer.indexOf(response.data[i])) {
                //             // $scope.financesEvent.push(response.data[i]);
                //             Event.get({id: response.data[i].event_id}, function (data) {
                //                 var eventFinance = {
                //                     eventName: data.name,
                //                     collectedMoney: response.data[i].collectedMoney,
                //                     selledTickets: response.data[i].selledTickets,
                //                     availablesTickets: response.data[i].availablesTickets,
                //                     percentOfTicketsSelled: response.data[i].percentOfTicketsSelled,
                //                     event_id: response.data[i].event_id
                //                 };
                //                 $scope.financesEvent.push(eventFinance);
                //             });
                //             break;
                //         }
                //     }
                // }
            }
        });

        var action = $http({
            method: 'GET',
            url: $rootScope.settings.pathAPI + 'api/finances/organization/all',
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {}
        }).then(function successCallback(response) {
            console.log(response);
            if (response.data > 0) {
                $scope.finances = response.data;
            }

        });

        // Finances.get(function (data) {
        //
        //     $scope.finances = data;
        //     console.log(data);
        // });
    };

    $scope.updateFilter = function (filter, kind) {
        switch (kind) {
            case "E":
                $scope.filterTimeEvents = filter;
                break;
            case "EA":
                $scope.filterTimeEventsAdmin = filter;
                break;
            case "O":
                $scope.filterTimeOrganization = filter;
                break;
        }
        $scope.loadData(1);
    };


    $scope.loadData(0);
    var promise = $interval(function () {
        $scope.loadData(1);
    }, 300000);

    $scope.$watch('events', function (newVal) {
    });
    $scope.$watch('financesEvent', function (newVal) {
    });
    $scope.$watch('finances', function (newVal) {
    });
    $scope.$watch('financesOrganizer', function (newVal) {
    });

    $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        $scope.vm2 = this;
        $scope.vm2.dtOptions = DTOptionsBuilder.newOptions()
            .withLanguageSource(
                $translate.instant("URL_DATATABLE")
            )
            .withPaginationType('full_numbers')
            .withButtons([

                    {
                        extend: 'csv',
                        text: '<i class="fa fa-file-text-o"></i> Excel',
                        charset: 'UTF-8',
                        bom: true,

                    }

                ]
            )
            .withDisplayLength(10)
            .withBootstrap().withOption("responsive", true);
        $scope.vm = this;
        $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
            .withLanguageSource(
                $translate.instant("URL_DATATABLE")
            )
            .withPaginationType('full_numbers')
            .withButtons([

                    {
                        extend: 'csv',
                        text: '<i class="fa fa-file-text-o"></i> Excel',
                        charset: 'UTF-8',
                        bom: true,

                    }

                ]
            )
            .withDisplayLength(10)
            .withBootstrap().withOption("responsive", true);
        App.initAjax();
    });

    $scope.$on('$destroy', function () {
        $interval.cancel(promise);
    });
    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;


});

angular.module('DashBoardA').controller('payEventosController', function ($rootScope, $location, $translate, Staffs, $scope, DTOptionsBuilder, Country, City, Event, Events, $http, Category, $state, $window, $stateParams) {
    $scope.settings = {

    };
    $scope.settings.error = null;
    $scope.settings.success = null;
    var data = localStorage.getItem('userLogged');
    $scope.organizador = JSON.parse(data);
    $scope.referencia = null;
    $scope.fecha = null;
        //Definicion del evento
        $scope.event = {
            name: "",
            description: "",
            facebook_link: "",
            twitter_link: "",
            quantity_tickets: 0,
            location: "",
            latitude: 0,
            longitude: 0,
            start_date: "",
            end_date: "",
            start_time: "",
            end_time: "",
            privacy: "",
            picture: "",
            status: "available",
            country_id: 0,
            organization_id: 0
        };
    var id = $stateParams.id;
    if (id) {
        Event.get({id: id}, function (data) {
            $scope.event = data;

        });
    }
        //Metodo ejecutado al cargar el contenido de la pagina
        $scope.$on('$viewContentLoaded', function () {
            // do {
            //     Servicio.data.count = 0;
            //     //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            //     $scope.vm = this;
            //     $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
            //         .withLanguageSource(
            //             $translate.instant("URL_DATATABLE")
            //         )
            //         .withPaginationType('full_numbers')
            //         .withDisplayLength(10)
            //         .withBootstrap().withOption("responsive", true);
            //
            // } while (Servicio.data.count != 0);
            // initialize core components
            App.initAjax();
        });

        // oculta el sidebar y pone el body solido en blanco
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = false;
        var count = 0;

    $scope.submit = function () {
        var data = {
            event: id,
            reference: $scope.referencia,
            paid_date: $scope.fecha
        };


        // if(!data){
        //     $scope.settings.success = $translate.instant("FINANCE.SUCCESS_SAVED_PAY");
        //     openModal();
        // } else {
        //
        // }
        var action = $http({
            method: 'POST',
            url: $rootScope.settings.pathAPI + 'api/finances/pay_event',
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: data
        }).then(function successCallback(response) {
            console.log(response);
            $scope.settings.success = $translate.instant("FINANCE.SUCCESS_SAVED_PAY");
            openModal();

        }, function errorCallback(response) {
            $scope.settings.error = $translate.instant("FINANCE.ERROR_SAVED_PAY");
            console.log(response);
            openModal();
        });

    };

    });
