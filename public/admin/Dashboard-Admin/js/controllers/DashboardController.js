angular.module('DashBoardA').controller('DashboardController', function ($rootScope, $translate, OrganizationsRequest, $interval, $scope, Users, Events, $http, $timeout) {
    $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

    });
    $scope.loadData = function () {
        var countAux = 0;
        $scope.organizadoresEsperaAux = 0;
        var action = $http({
            method: 'GET',
            url: $rootScope.settings.pathAPI + 'api/users' + $rootScope.settings.parametersUser,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
            }
        }).then(function successCallback(response) {
            // console.log(response.data.data);
            for (i = 0; i < response.data.data.length; i++) {
                // console.log(response.data.data[i]);
                if (response.data.data[i].activated == 1) {
                    countAux = countAux + 1;

                }
            }
            var count = countAux;
            $scope.usrsActivos = count;

        }, function errorCallback(response) {
            console.log('Error: ' + response);
        });
        var action = $http({
            method: 'GET',
            url: $rootScope.settings.pathAPI + 'api/organizations/request/all',
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            }
        }).then(function successCallback(response) {
            // console.log(response);
            $scope.organizadoresEspera = response.data.length;
        }, function errorCallback(response) {
            $scope.organizadoresEspera = 0;
        });
        // OrganizationsRequest.get(function (data) {
        //     if (data) {
        //         console.log(data);
        //         $scope.organizadoresEspera = data.data.length;
        //     } else {
        //         $scope.organizadoresEspera = 0;
        //     }
        // });

        $scope.eventsCountAux = 0;
        var data = localStorage.getItem('userLogged');
        $scope.organizador = JSON.parse(data);
        Events.get(function (data) {
            if ($rootScope.isAdmin || !$scope.organizador.organization) {
                $scope.eventsCountAux = data.data.length;
            } else {
                for (var i = 0; i < data.data.length; i++) {

                    if ($scope.organizador.organization)
                        if ($scope.organizador.organization.id == data.data[i].organization.id) {
                            $scope.eventsCountAux++;
                        }
                }
            }
            $scope.eventsCount = $scope.eventsCountAux;
        });

    };
    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    $scope.loadData();
    var promise = $interval(function () {
        $scope.loadData();
    }, 300000);


    $scope.$on('$destroy', function () {
        $interval.cancel(promise);
    });
});

