/**
 * Created by Alvade on 03/08/2017.
 */

angular.module('DashBoardA').controller('EventosController', function ($rootScope, $scope, $translate, DTOptionsBuilder, Servicio, Country, City, Events, Event, Entrance, Category, $http, $state, $window) {

    $scope.$on('$viewContentLoaded', function () {
        //Obtiene los registros que se encuentran en la tabla de usuarios
        $scope.countries = [];
        $scope.cities = [];
        if ($rootScope.update) {
            $rootScope.update = false;
            $window.location.reload();
        }
        Events.get(function (data) {
            if ($rootScope.isAdmin) {
                $scope.events = data.data;
            } else {
                var userLogged = localStorage.getItem('userLogged');
                $scope.organizador = JSON.parse(userLogged);
                $scope.events = [];
                for (var i = 0; i < data.data.length; i++) {
                    if ($scope.organizador.organization != undefined)
                        if ($scope.organizador.organization.id == data.data[i].organization.id) {
                            $scope.events.push(data.data[i]);
                        }
                }
            }
        });
        Category.get(function (data) {
            $scope.categorys = data.data;
        });
        Country.get(function (data) {
            $scope.countries = data.data;
        });
        City.get(function (data) {
            $scope.cities = data.data;
        });
        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap().withOption("responsive", true);

            $scope.vm2 = this;
            $scope.vm2.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap().withOption("responsive", true);

            $scope.vm3 = this;
            $scope.vm3.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap().withOption("responsive", true);


        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });
    //$rootScope.settings.pageTitle = settings.pageTitle;
    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    $scope.$watch('categorys', function (newVal) {
    });


    $scope.reload = function () {
        $window.location.reload();
    };
    //Categorias
    $scope.removeCategoria = function (id) {
        var action = $http({
            method: 'DELETE',
            url: $rootScope.settings.pathAPI + 'api/categories/' + id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {}
        });
        Category.get(function (data) {
            $scope.categorys = data.data;
        });
    }

});

angular.module('DashBoardA').controller('edEventosController', function ($rootScope, $translate, Staffs, $scope, DTOptionsBuilder, Country, City, Servicio, Event, Events, $http, Category, $state, $window, $stateParams) {
    $scope.settings = {
        pageTitle: $translate.instant("EVENT_OBJECT.EDIT_EVENT"),
        action: $translate.instant("EVENT_OBJECT.UPDATE_EVENT")
    };
    if (!$rootScope.isAdmin) {
        var data = localStorage.getItem('userLogged');
        $scope.organizador = JSON.parse(data);
    }
    $scope.$on('$viewContentLoaded', function () {
        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap().withOption("responsive", true);

        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });

    //$rootScope.settings.pageTitle = settings.pageTitle;
    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    $scope.entrances = [];
    $scope.staves = [];
    $scope.stavesAdded = [];
    $scope.stavesOriginal = [];
    $scope.countries = [];
    $scope.countryName = "";
    $scope.country = {};
    $scope.city = {};
    $scope.showDays = false;
    $scope.editStatus = false;
    $scope.entrance = {
        name: "",
        description: "",
        price: 0.00,
        quantity_availables: 100,
        isGratis: false
    };
    $scope.staff = {
        name: "",
        email: "",
        event_id: 0
    };

    $scope.isEddit = false;
    $scope.entranceAux = {
        name: "",
        description: "",
        price: 0.00,
        quantity_availables: 100,
        isGratis: false
    };
    $scope.event = {};
    $scope.category = {};
    $scope.file = null;
    $scope.stageFile = null;
    $scope.countries = [];
    $scope.$watch('entrances', function (newVal) {
    });
    $scope.$watch('source', function (newVal) {
    });
    $scope.$watch('stageSource', function (newVal) {
    });

    $scope.$watch('staves', function (newVal) {
    });
    $scope.$watch('stavesAdded', function (newVal) {
    });
    $scope.$watch('entrance', function (newVal) {
    });
    $scope.$watch('file', function (newVal) {
    });
    $scope.$watch('stageFile', function (newVal) {

    });
    $scope.$watch('countryName', function (newVal) {

    });
    $scope.$watch('entrance.isGratis', function (newVal) {
        if (newVal) {
            $scope.entrance.price = 000;

        } else {
            $scope.entrance.price = null;

        }
    });
    $scope.$watch('country.value', function (newVal) {
        if (newVal) {
            if ($scope.event.location == undefined && $scope.event.location == "" || !$scope.event.location.includes(newVal.name)) {
                $scope.event.location = newVal.name;
                if ($scope.city) {
                    $scope.city = {};
                }
            }
            if ($scope.event.location && $scope.event.location != "") {
                changeLocation($scope.event.location);
                $scope.inciaCities($scope.country.value, undefined);
            }
        }

    });
    $scope.$watch('city.value', function (newVal) {
        if (newVal) {
            if ($scope.event.location && $scope.event.location != "" && !$scope.event.location.includes(newVal.name)) {
                $scope.event.location = newVal.name.concat(', ', $scope.country.value.name);

                if ($scope.event.location && $scope.event.location != "") {
                    changeLocation($scope.event.location);
                }
            }
        }
    });

    $scope.inciaCities = function (data2, localidad) {
        $scope.cities = [];

        City.get({}, function (data) {
            for (var i = 0; i < data.data.length; i++) {
                if (data.data[i].country_id == data2.id) {
                    $scope.cities.push(data.data[i]);
                }
            }
        });
        $scope.changeCity(localidad);
        return true;
    };

    $scope.change = function (value) {
        if ($scope.countries) {
            for (var i = 0; i < $scope.countries.length; i++) {
                if (value.includes($scope.countries[i].name)) {
                    $scope.country.value = $scope.countries[i];
                }
            }
        }

        if (!$scope.country.value) {

            $scope.event.latitude = 0;
            $scope.event.longitude = 0;
            $scope.event.location = "";
            changeValue();
        }
        $scope.inciaCities($scope.country.value, value);


    };
    $scope.changeCity = function (value) {
        if (value != undefined) {

            $scope.city = {};
            if ($scope.cities.length > 0) {
                for (var i = 0; i < $scope.cities.length; i++) {

                    if (value.includes($scope.cities[i].name.toUpperCase())) {
                        $scope.city.value = $scope.cities[i];
                    }
                }
            } else {
                City.get({}, function (data) {
                    if (data.data.length > 0) {
                        for (var i = 0; i < data.data.length; i++) {
                            if (value.includes(data.data[i].name) && $scope.country.value.id == data.data[i].country_id) {
                                $scope.city.value = data.data[i];
                            }
                        }
                    }
                });
            }


            if (!$scope.city.value && $scope.cities.length > 0) {

                $scope.event.latitude = 0;
                $scope.event.longitude = 0;
                $scope.event.location = $scope.country.value.name;
                changeValue();
                changeLocation($scope.event.location);
            }
        }
    };

    var vm = this;
    vm.disabled = undefined;
    vm.searchEnabled = undefined;
    vm.setInputFocus = function () {
        $scope.$broadcast('UiSelectDemo1');
    };
    vm.enable = function () {
        vm.disabled = false;
    };
    vm.disable = function () {
        vm.disabled = true;
    };
    vm.enableSearch = function () {
        vm.searchEnabled = true;
    };
    vm.disableSearch = function () {
        vm.searchEnabled = false;
    };

    Category.get({}, function (data) {
        $scope.categories = data.data;
    });
    Country.get({}, function (data) {
        $scope.countries = data.data;
    });

    $scope.numberFormat = function (price) {
        if (price.toString().indexOf('.') == -1) {
            aux = price.toString().concat(".00");
            price = parseFloat(aux);

        }

        return price;
    };
    var id = $stateParams.id;
    //Obtiene los registros que se encuentran en la tabla de eventos
    if (id) {
        Event.get({id: id}, function (data) {
                $scope.event = data;
                $scope.event.location = data.location;
                $scope.editStatus = ($scope.event.status == 'available' || $scope.event.status == 'suspended');
                $scope.source = $scope.event.picture;
                $scope.stageSource = $scope.event.stage_map;

                for (var i = 0; i < $scope.event.entrances.data.length; i++) {
                    $scope.event.entrances.data[i].price = $scope.numberFormat($scope.event.entrances.data[i].price);
                    if ($scope.event.entrances.data[i].price <= 0) {
                        $scope.event.entrances.data[i].isGratis = true;
                    } else {
                        $scope.event.entrances.data[i].isGratis = false;
                    }
                    $scope.entrances.push($scope.event.entrances.data[i]);
                }
                Staffs.get(function (data) {

                    $scope.staves = [];
                    if (!$rootScope.isAdmin) {
                        data.data.forEach(function (item) {
                            if (item.organization_id == $scope.organizador.organization.id) {
                                $scope.staves.push(item);
                            }
                        });
                    }
                });

                var action = $http({
                    method: 'GET',
                    url: $rootScope.settings.pathAPI + 'api/staff/by/event/' + $scope.event.id,
                    headers: {
                        "Content-Type": "application/json;charset=UTF-8",
                        "Authorization": 'Bearer ' + $rootScope.settings.token
                    }
                }).then(function successCallback(response) {


                    $scope.staves.forEach(function (item, index, array) {
                        response.data.data.forEach(function (item2, ind, array2) {
                            if (item.id == item2.id) {
                                $scope.stavesAdded.push(item);
                            }
                        });
                    });
                    $scope.stavesOriginal = $scope.stavesAdded;

                }, function errorCallback(response) {
                  console.log(response);
                });

                changeValue();
                $scope.country.value = $scope.event.country;
                // $scope.inciaCities($scope.country.value,undefined);
                $scope.city.value = $scope.event.city;

                $scope.category.value = $scope.event.category;
            }
        );
    }

    $scope.changeStaff = function (data) {
        $scope.stavesAdded = data;
    };

    $scope.resetEntrance = function (data) {
        if (data) {
            $scope.entrance = {
                id: data.id,
                event_id: data.event_id,
                name: data.name,
                description: data.description,
                price: data.price,
                quantity_availables: data.quantity_availables,
                isGratis: data.isGratis,
                created_at: data.created_at,
                updated_at: data.updated_at
            };
            $scope.entranceAux = {
                id: data.id,
                event_id: data.event_id,
                name: data.name,
                description: data.description,
                price: data.price,
                quantity_availables: data.quantity_availables,
                isGratis: data.isGratis,
                created_at: data.created_at,
                updated_at: data.updated_at
            };
            $scope.isEddit = true;
        } else {
            $scope.entrance = {
                name: "",
                description: "",
                price: 0.00,
                quantity_availables: 100,
                isGratis: false
            };
            $scope.isEddit = false;
        }
    };

    $scope.resetStaff = function () {
        $scope.staff = {
            name: "",
            email: "",
            event_id: ""
        };
    };

    //añade el tipo de entrada al evento
    $scope.add = function () {

        if ($scope.entrance != null) {
            if ($scope.isEddit) {
                $scope.entrances.forEach(function (item, index, array) {
                    if (item.name == $scope.entranceAux.name && item.description == $scope.entranceAux.description) {

                        item.name = $scope.entrance.name;
                        item.description = $scope.entrance.description;
                        item.price = $scope.entrance.price;
                        item.quantity_availables = $scope.entrance.quantity_availables;
                        item.isGratis = $scope.entrance.isGratis;

                    }
                });
            } else {
                $scope.entrances.push({
                    name: $scope.entrance.name,
                    description: $scope.entrance.description,
                    price: $scope.entrance.price,
                    quantity_availables: $scope.entrance.quantity_availables,
                    isGratis: $scope.entrance.isGratis
                });
            }
        }

    };

    $scope.addStaff = function () {

        if ($scope.entrance != null) {
            $scope.entrances.push({
                name: $scope.entrance.name,
                description: $scope.entrance.description,
                price: $scope.entrance.price,
                quantity_availables: $scope.entrance.quantity_availables,
                isGratis: $scope.entrance.isGratis
            });
        }

    };

    $scope.uploadFile = function () {

        // var data = new FormData($('#formImagen').)
        $scope.file = document.getElementById('imagen').files[0];

    };
    $scope.uploadStageFile = function () {

        // var data = new FormData($('#formImagen').)
        $scope.Stagefile = document.getElementById('stageImage').files[0];

    };

    $scope.deleteEntrance = function (data, index) {

        var position = index;
        $scope.entrances.splice(position, 1);
        if (data.id) {
            var action = $http({
                method: 'DELETE',
                url: $rootScope.settings.pathAPI + 'api/entrances/' + data.id,
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                    "Authorization": 'Bearer ' + $rootScope.settings.token
                },
                data: {}
            })
        }
    };

    $scope.deleteStaff = function (data, index) {
        var position = index;
        $scope.entrances.splice(position, 1);
        // if (data.id) {
        //     var action = $http({
        //         method: 'DELETE',
        //         url: $rootScope.settings.pathAPI + 'api/staff/' + data.id,
        //         headers: {
        //             "Content-Type": "application/json;charset=UTF-8",
        //             "Authorization": 'Bearer ' + $rootScope.settings.token
        //         },
        //         data: {}
        //     })
        // }
    };


    $scope.submit = function () {
        if ($scope.file) {
            var data = data = new FormData();
            data.append('file', $scope.file);
            jQuery.ajax({
                url: $rootScope.settings.pathAPI + 'api/uploads',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (data) {
                    var action = $http({
                        method: 'PUT',
                        url: $rootScope.settings.pathAPI + 'api/events/' + $scope.event.id,
                        headers: {
                            "Content-Type": "application/json;charset=UTF-8",
                            "Authorization": 'Bearer ' + $rootScope.settings.token
                        },
                        data: {
                            picture: data.url,
                        }
                    });
                }
            });
        }
        if ($scope.stageFile) {
            var data = data = new FormData();
            data.append('file', $scope.stageFile);
            jQuery.ajax({
                url: $rootScope.settings.pathAPI + 'api/uploads',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (data) {
                    var action = $http({
                        method: 'PUT',
                        url: $rootScope.settings.pathAPI + 'api/events/' + $scope.event.id,
                        headers: {
                            "Content-Type": "application/json;charset=UTF-8",
                            "Authorization": 'Bearer ' + $rootScope.settings.token
                        },
                        data: {
                            stage_map: data.url,
                        }
                    }).then(function successCallback(response) {
                        $scope.editEvent();
                    });
                }
            });
        } else {
            $scope.editEvent();
        }

    };

    $scope.convertTime = function (value) {
        var time = value;
        if (time.indexOf('AM') >= 0 || time.indexOf('PM') >= 0) {
            var hours = Number(time.match(/^(\d+)/)[1]);
            var minutes = Number(time.match(/:(\d+)/)[1]);
            var AMPM = time.match(/\s(.*)$/)[1];
            if (AMPM == "PM" && hours < 12) hours = hours + 12;
            if (AMPM == "AM" && hours == 12) hours = hours - 12;
            var sHours = hours.toString();
            var sMinutes = minutes.toString();
            if (hours < 10) sHours = "0" + sHours;
            if (minutes < 10) sMinutes = "0" + sMinutes;
            return sHours + ":" + sMinutes + ":00";
        } else {
            return time;
        }
    };

    $scope.editEvent = function () {
        $scope.event.start_time = $scope.convertTime($scope.event.start_time);
        $scope.event.end_time = $scope.convertTime($scope.event.end_time);
        var updateEvent = {
            name: $scope.event.name,
            description: $scope.event.description,
            facebook_link: $scope.event.facebook_link,
            twitter_link: $scope.event.twitter_link,
            location: $scope.event.location,
            latitude: $scope.event.latitude,
            longitude: $scope.event.longitude,
            start_date: $scope.event.start_date,
            end_date: $scope.event.end_date,
            start_time: $scope.event.start_time,
            end_time: $scope.event.end_time,
            privacy: $scope.event.privacy,
            category_id: $scope.category.value.id,
            organization_id: $scope.event.organization_id,
            country_id: $scope.country.value.id,
            city_id: $scope.city.value.id,
            status:  $scope.event.status
        };
        if ($scope.event.privacy == 'private' && !$scope.event.share_code) {
            var timeInMills = new Date().getTime().toString();
            var long = timeInMills.length;
            var halfPosition = Math.round(long / 2);
            var code = timeInMills.substr(halfPosition - 2, 6);
            code = code.concat('-', $scope.event.name.charAt(0).toUpperCase());

            updateEvent.share_code = code;
        } else if ($scope.event.privacy == 'public' && $scope.event.share_code) {
            updateEvent.share_code = undefined;
        }
        var action = $http({
            method: 'PUT',
            url: $rootScope.settings.pathAPI + 'api/events/' + $scope.event.id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: updateEvent
        }).then(function successCallback(response) {
            $scope.settings.success = $translate.instant("EVENT_OBJECT.SUCCESS_EDIT");

            $scope.event = response.data;
            var joinedStaves = [];
            $scope.stavesAdded.forEach(function (item, index, array) {

                if ($scope.stavesOriginal.indexOf(item) < 0) {
                    joinedStaves.push(item);
                }
            });
            $scope.stavesOriginal.forEach(function (item, index, array) {

                if ($scope.stavesAdded.indexOf(item) < 0) {
                    joinedStaves.push(item);
                }
            });
            var joinedCont = 0;
            if (joinedStaves.length > 0) {
                joinedStaves.forEach(function (item, index, array) {

                    if ($scope.stavesAdded.indexOf(item) >= 0) {
                        var action = $http({
                            method: 'GET',
                            url: $rootScope.settings.pathAPI + 'api/staff/' + item.id + '/assign/event/' + response.data.id,
                            headers: {
                                "Content-Type": "application/json;charset=UTF-8",
                                "Authorization": 'Bearer ' + $rootScope.settings.token
                            }

                        }).then(function successCallback(response) {
                            if (joinedCont == joinedStaves.length) {
                                $scope.updateEntrances();
                            }
                        }, function errorCallback(response) {
                            console.log(response);
                        });
                    } else if ($scope.stavesOriginal.indexOf(item) >= 0) {
                        var action = $http({
                            method: 'GET',
                            url: $rootScope.settings.pathAPI + 'api/staff/' + item.id + '/remove/event/' + response.data.id,
                            headers: {
                                "Content-Type": "application/json;charset=UTF-8",
                                "Authorization": 'Bearer ' + $rootScope.settings.token
                            }

                        }).then(function successCallback(response) {
                            if (joinedCont == joinedStaves.length) {
                                $scope.updateEntrances();
                            }
                        }, function errorCallback(response) {
                            console.log(response);
                        });
                    }
                    joinedCont = joinedCont + 1;
                });
            } else {
                $scope.updateEntrances();
            }

        }, function errorCallback(response) {
            $scope.settings.error = $translate.instant("EVENT_OBJECT.ERROR_EDIT");

            $scope.settings.share_code = null;
            $scope.settings.url = null;
            openModal();
        });

    };

    $scope.updateEntrances = function () {
        $scope.entrances.forEach(function (item, index, array) {
            if (item.id) {
                var action = $http({
                    method: 'PUT',
                    url: $rootScope.settings.pathAPI + 'api/entrances/' + item.id,
                    headers: {
                        "Content-Type": "application/json;charset=UTF-8",
                        "Authorization": 'Bearer ' + $rootScope.settings.token
                    },
                    data: {
                        event_id: $scope.event.id,
                        name: item.name,
                        description: item.description,
                        price: item.price,
                        quantity_availables: item.quantity_availables
                    }
                }).then(function successCallBack(response) {
                    if (index == $scope.entrances.length - 1) {
                        $scope.reload();
                    }
                });
            } else {
                var action2 = $http({
                    method: 'POST',
                    url: $rootScope.settings.pathAPI + 'api/entrances',
                    headers: {
                        "Content-Type": "application/json;charset=UTF-8",
                        "Authorization": 'Bearer ' + $rootScope.settings.token
                    },
                    data: {
                        event_id: $scope.event.id,
                        name: item.name,
                        description: item.description,
                        quantity_availables: item.quantity_availables,
                        price: item.price
                    }
                }).then(function successCallBack(response) {
                    if (index == $scope.entrances.length - 1) {
                        $scope.reload();
                    }
                });
            }
        });
    };
    $scope.reload = function () {
        $rootScope.update = true;
        if ($scope.event.share_code) {

            $scope.settings.success = 'Private event successfully updated!';
            $scope.settings.share_code = $scope.event.share_code;
            $scope.settings.url = $rootScope.settings.pathAPI + 'events/' + $scope.event.slug;
        }
        $scope.settings.success = 'Event successfully updated!';
        openModal();
    };

    $scope.changePage = function () {
        $('#messageModal').modal('toggle');

        //$location.path("/events.html");
    };
});


angular.module('DashBoardA').controller('AddEventoController', function ($rootScope, $location, $translate, Staffs, $scope, DTOptionsBuilder, Country, City, Servicio, Events, $http, Category, $state, $window, $stateParams) {
    $scope.settings = {
        pageTitle: $translate.instant("EVENT_OBJECT.ADD_EVENT"),
        action: $translate.instant("EVENT_OBJECT.PUBLISH_EVENT")
    };
    var data = localStorage.getItem('userLogged');
    $scope.organizador = JSON.parse(data);
    //metodo encargado de actualizar los valores del mapa
    changeValue();
    // seccion de metodos watch (vigilan cualquier cambio en los datos y los actualizan)
    $scope.$watch('file', function (newVal) {
    });
    $scope.$watch('stageFile', function (newVal) {
    });
    $scope.$watch('source', function (newVal) {
    });
    $scope.$watch('entrances', function (newVal) {
    });
    $scope.$watch('staves', function (newVal) {
    });
    $scope.$watch('event.longitude', function (newVal) {
    });
    $scope.$watch('event.latitude', function (newVal) {
    });
    $scope.$watch('event.location', function (newVal) {

    });
    $scope.$watch('entrance', function (newVal) {
    });
    $scope.$watch('isRecurrent', function (newVal) {

        $scope.isRecurrent = newVal;
    });
    $scope.$watch('entrance.price', function (newVal) {
    });
    $scope.$watch('entrance.isGratis', function (newVal) {
        if (newVal) {
            $scope.entrance.price = 000;

        } else {
            $scope.entrance.price = null;

        }
    });
    $scope.$watch('settings.error', function (newVal) {
    });
    $scope.$watch('country.value', function (newVal) {
        if (newVal) {

            if ($scope.event.location == undefined && $scope.event.location == "" || !$scope.event.location.includes(newVal.name)) {
                $scope.event.location = newVal.name;

                if ($scope.city) {
                    $scope.city = {};
                }
            }
            if ($scope.event.location && $scope.event.location != "") {
                changeLocation($scope.event.location);
                $scope.inciaCities($scope.country.value, undefined);
            }
        }

    });
    $scope.$watch('city.value', function (newVal) {
        if (newVal) {
            if ($scope.event.location && $scope.event.location != "" && !$scope.event.location.includes(newVal.name)) {
                $scope.event.location = newVal.name.concat(', ', $scope.country.value.name);
                if ($scope.event.location && $scope.event.location != "") {
                    changeLocation($scope.event.location);
                }
            }
        }
    });


    $scope.inciaCities = function (data2, localidad) {
        $scope.cities = [];

        City.get({}, function (data) {
            for (var i = 0; i < data.data.length; i++) {
                if (data.data[i].country_id == data2.id) {
                    $scope.cities.push(data.data[i]);
                }
            }
        });
        $scope.changeCity(localidad);
        return true;
    };

    $scope.change = function (value) {
        if ($scope.countries) {
            for (var i = 0; i < $scope.countries.length; i++) {
                if (value.includes($scope.countries[i].name)) {
                    $scope.country.value = $scope.countries[i];
                }
            }
        }
        if (!$scope.country.value) {
            $scope.event.latitude = 0;
            $scope.event.longitude = 0;
            $scope.event.location = "";
            changeValue();
        }
        $scope.inciaCities($scope.country.value, value);


    };
    $scope.changeCity = function (value) {
        if (value != undefined) {
            $scope.city = {};
            if ($scope.cities.length > 0) {
                for (var i = 0; i < $scope.cities.length; i++) {
                    if (value.includes($scope.cities[i].name.toUpperCase())) {
                        $scope.city.value = $scope.cities[i];
                    }
                }
            } else {
                City.get({}, function (data) {
                    if (data.data.length > 0) {
                        for (var i = 0; i < data.data.length; i++) {
                            if (value.includes(data.data[i].name) && $scope.country.value.id == data.data[i].country_id) {
                                $scope.city.value = data.data[i];
                            }
                        }
                    }
                });
            }

            if (!$scope.city.value && $scope.cities.length > 0) {
                $scope.event.latitude = 0;
                $scope.event.longitude = 0;
                $scope.event.location = $scope.country.value.name;
                changeValue();
                changeLocation($scope.event.location);
            }
        }
    };
    //Definicion de Variables usadas en la vista
    //URL de la imagen
    //$scope.isGratis = false;
    $scope.source = null;
    //imagen
    $scope.file = null;
    //imagen
    $scope.stageFile = null;
    //pais
    $scope.country = {};
    //ciudad
    $scope.city = {};
    $scope.showDays = true;

    $scope.editStatus = true;
    //Nombre Pais
    $scope.countryName = null;
    // array de entradas
    $scope.entrances = [];
    // array de paises
    $scope.countries = [];
    // array de ciudades
    $scope.cities = [];
    //Definicion de una entrada
    $scope.entrance = {
        name: "",
        description: "",
        price: 000,
        quantity_availables: 100,
        isGratis: false
    };
    //Definicion de una persona tipo staff
    $scope.staff = {
        name: "",
        email: "",
        event_id: 0
    };
    //Array del personal tipo staff
    $scope.staves = [];
    //Array del personal tipo staff
    $scope.stavesAdded = [];
    //Array de dias recurrentes
    $scope.week_days = [];
    //Array de dias recurrentes agregados
    $scope.weekDaysAdded = [];
    //categoria de evento
    $scope.category = {};
    //variable que ayuda a determinar si se editara una entrada
    $scope.isEddit = false;
    //determina si es recurrente o no el evento
    $scope.isRecurrent = false;
    //variable auxiliar usada al momento de editar
    $scope.entranceAux = {
        name: "",
        description: "",
        price: 0,
        quantity_availables: 100,
        isGratis: false
    };
    //Es para los componentes select de bootstrap
    var vm = this;
    vm.disabled = undefined;
    vm.searchEnabled = undefined;
    vm.setInputFocus = function () {
        $scope.$broadcast('UiSelectDemo1');
    };
    vm.enable = function () {
        vm.disabled = false;
    };
    vm.disable = function () {
        vm.disabled = true;
    };
    vm.enableSearch = function () {
        vm.searchEnabled = true;
    };
    vm.disableSearch = function () {
        vm.searchEnabled = false;
    };
    // FIN de para los componentes select de bootstrap

    //Extrae Staff
    Staffs.get(function (data) {

        $scope.staves = [];
        data.data.forEach(function (item) {
            if (item.organization_id == $scope.organizador.organization.id) {
                $scope.staves.push(item);
            }
        });
    });

    //Se definen los dias
    for (var i = 0; i < 7; i++) {
        var day = {
            display: "",
            back: ""
        };
        switch (i) {
            case 0:
                day.display = "Monday";
                day.back = "monday";
                break;
            case 1:
                day.display = "Tuesday";
                day.back = "tuesday";
                break;
            case 2:
                day.display = "Wednesday";
                day.back = "wednesday";
                break;
            case 3:
                day.display = "Thursday";
                day.back = "thursday";
                break;
            case 4:
                day.display = "Friday";
                day.back = "friday";
                break;
            case 5:
                day.display = "Saturday";
                day.back = "saturday";
                break;
            case 6:
                day.display = "Sunday";
                day.back = "sunday";
                break;
        }
        $scope.week_days.push(day);
    }


    //Extraer categorias
    Category.get({}, function (data) {
        $scope.categories = data.data;
    });

    //Consulta paises
    Country.get({}, function (data) {
        $scope.countries = data.data;
    });


    //Metodo ejecutado al cargar el contenido de la pagina
    $scope.$on('$viewContentLoaded', function () {
        do {
            Servicio.data.count = 0;
            //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
            $scope.vm = this;
            $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
                .withLanguageSource(
                    $translate.instant("URL_DATATABLE")
                )
                .withPaginationType('full_numbers')
                .withDisplayLength(10)
                .withBootstrap().withOption("responsive", true);

        } while (Servicio.data.count != 0);
        // initialize core components
        App.initAjax();
    });

    // oculta el sidebar y pone el body solido en blanco
    $rootScope.settings.layout.pageContentWhite = true;
    $rootScope.settings.layout.pageBodySolid = false;
    $rootScope.settings.layout.pageSidebarClosed = false;
    var count = 0;

    //Definicion del evento
    $scope.event = {
        name: "",
        description: "",
        facebook_link: "",
        twitter_link: "",
        quantity_tickets: 0,
        location: "",
        latitude: 0,
        longitude: 0,
        start_date: "",
        end_date: "",
        start_time: "",
        end_time: "",
        privacy: "",
        picture: "",
        status: "available",
        country_id: 0,
        organization_id: $scope.organizador.organization.id
    };

    //Obtiene la imagen seleccionada
    $scope.uploadFile = function () {
        $scope.file = document.getElementById('imagen').files[0];
    };
    //Obtiene la imagen seleccionada
    $scope.uploadStageFile = function () {
        $scope.stageFile = document.getElementById('stageImage').files[0];
    };

    //Ayuda a inicializar la entrada, y define si es edicion o agregar
    $scope.resetEntrance = function (data) {
        if (data) {
            $scope.entrance = {
                name: data.name,
                description: data.description,
                price: data.price,
                quantity_availables: data.quantity_availables,
                original_qty: data.quantity_availables,
                isGratis: data.isGratis,
            };
            $scope.entranceAux = {
                name: data.name,
                description: data.description,
                price: data.price,
                quantity_availables: data.quantity_availables,
                original_qty: data.quantity_availables,
                isGratis: data.isGratis,
            };
            $scope.isEddit = true;
        } else {
            $scope.entrance = {
                name: "",
                description: "",
                price: 000,
                quantity_availables: 100,
                isGratis: false
            };
            $scope.isEddit = false;
        }
    };

    //Reinicia el Staff cada vez que se quiera agregar uno nuevo
    $scope.resetStaff = function () {
        $scope.staff = {
            name: "",
            email: "",
            event_id: 0
        };
    };

    //añade o actualiza el tipo de entrada a la lista de entradas a guardar en el evento
    $scope.add = function () {

        if ($scope.entrance != null) {
            if ($scope.isEddit) {
                $scope.entrances.forEach(function (item, index, array) {
                    if (item.name == $scope.entranceAux.name && item.description == $scope.entranceAux.description) {

                        item.name = $scope.entrance.name;
                        item.description = $scope.entrance.description;
                        item.price = $scope.entrance.price;
                        item.quantity_availables = $scope.entrance.quantity_availables;
                        item.isGratis = $scope.entrance.isGratis;
                        item.original_qty = $scope.entrance.quantity_availables;

                    }
                    // }
                });
            } else {
                $scope.entrances.push({
                    name: $scope.entrance.name,
                    description: $scope.entrance.description,
                    price: $scope.entrance.price,
                    quantity_availables: $scope.entrance.quantity_availables,
                    original_qty: $scope.entrance.quantity_availables,
                    isGratis: $scope.entrance.isGratis
                });
            }
        }
        // $scope.entrance = null;

    };

    //Añade el staff a la lista
    $scope.addStaff = function () {
        $scope.staves.push({
            name: $scope.staff.name,
            email: $scope.staff.email,
            event_id: 0
        });
    };

    //Elimina la entrada de la lista de entradas
    $scope.deleteEntrance = function (data, index) {

        var position = index;
        $scope.entrances.splice(position, 1);
    };

    //Elimina el staff de la lista
    $scope.deleteStaff = function (data, index) {

        var position = index;
        $scope.stavesAdded.splice(position, 1);
    };

    $scope.changeStaff = function (data) {

        $scope.stavesAdded = data;
    };
    $scope.changedays = function (data) {

        $scope.weekDaysAdded = data;
    };

    $scope.convertTime = function (value) {
        var time = value;
        if (time.indexOf('AM') >= 0 || time.indexOf('PM') >= 0) {
            var hours = Number(time.match(/^(\d+)/)[1]);
            var minutes = Number(time.match(/:(\d+)/)[1]);
            var AMPM = time.match(/\s(.*)$/)[1];
            if (AMPM == "PM" && hours < 12) hours = hours + 12;
            if (AMPM == "AM" && hours == 12) hours = hours - 12;
            var sHours = hours.toString();
            var sMinutes = minutes.toString();
            if (hours < 10) sHours = "0" + sHours;
            if (minutes < 10) sMinutes = "0" + sMinutes;
            return sHours + ":" + sMinutes + ":00";
        } else {
            return time;
        }
    };

    //Guarda todos los datos relacionados al evento
    $scope.submit = function () {
        $scope.settings.error = null;
        if ($scope.entrances.length > 0 && $scope.file) {
            var data1 = data = new FormData();
            data.append('file', $scope.file);
            //Sube el archivo al servidor
            jQuery.ajax({
                url: $rootScope.settings.pathAPI + 'api/uploads',
                data: data1,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (response) {
                    if ($scope.stageFile) {
                        var data = data = new FormData();
                        data.append('file', $scope.stageFile);
                        //Sube el archivo al servidor
                        jQuery.ajax({
                            url: $rootScope.settings.pathAPI + 'api/uploads',
                            data: data,
                            cache: false,
                            contentType: false,
                            processData: false,
                            type: 'POST',
                            success: function (data) {
                                $scope.saveEvent(response, data);
                            }
                        });
                    } else {
                        $scope.saveEvent(response, null);
                    }
                }
            });
        } else {
            if (!$scope.file) {
                $scope.settings.error = $translate.instant("ERROR.IMAGE_REQ");
                openModal();
            } else {
                $scope.settings.error = $translate.instant("ERROR.ADD_ANY_TICKET");
                openModal();
            }
        }
    };

    $scope.saveEvent = function (picture, stageImage) {

        //Si el archivo se subio satisfactoriamente, se guarda el evento

        if ($scope.isRecurrent && $scope.weekDaysAdded.length == 0) {
            $scope.settings.error = $translate.instant("ERROR.DAYS_REQUIRED");
            openModal();
        } else {
            //Convierte las horas a un formato correcto
            $scope.event.start_time = $scope.convertTime($scope.event.start_time);
            $scope.event.end_time = $scope.convertTime($scope.event.end_time);

            var finalEvent = {
                name: $scope.event.name,
                description: $scope.event.description,
                category_id: $scope.category.value.id,
                picture: picture.url,
                organization_id: $scope.organizador.organization.id,
                facebook_link: $scope.event.facebook_link,
                twitter_link: $scope.event.twitter_link,
                privacy: $scope.event.privacy,
                location: $scope.event.location,
                latitude: $scope.event.latitude,
                longitude: $scope.event.longitude,
                start_date: $scope.event.start_date,
                end_date: $scope.event.end_date,
                start_time: $scope.event.start_time,
                end_time: $scope.event.end_time,
                status: "available",
                country_id: $scope.country.value.id,
                city_id: $scope.city.value.id,
                stage_map: (stageImage == null ? null : stageImage.url),
                is_recurrent: $scope.isRecurrent
            };

            if ($scope.isRecurrent) {
                var selectedDays = [];
                $scope.weekDaysAdded.forEach(function (item, index, array) {
                    selectedDays.push(item.back);
                });
                finalEvent.week_days = selectedDays;
            }
            if ($scope.event.privacy == 'private') {
                var timeInMills = new Date().getTime().toString();
                var long = timeInMills.length;
                var halfPosition = Math.round(long / 2);
                var code = timeInMills.substr(halfPosition - 2, 6);
                code = code.concat('-', $scope.event.name.charAt(0).toUpperCase());

                finalEvent.share_code = code;
            }

            //guarda el evento
            var action = $http({
                method: 'POST',
                url: $rootScope.settings.pathAPI + 'api/events',
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                    "Authorization": 'Bearer ' + $rootScope.settings.token
                },
                data: finalEvent
            }).then(function successCallback(response) {
                //Si el evento se guardo correctamente empieza a guardar el tipo de entradas relacionadas al evento
                $scope.settings.success = $translate.instant("EVENT_OBJECT.SUCCESS_ADD");
                $scope.event = response.data;


                //guarda el staff asignado al evento
                if ($scope.stavesAdded.length > 0) {
                    var cont = 1;
                    $scope.stavesAdded.forEach(function (item, index, array) {


                        var action = $http({
                            method: 'GET',
                            url: $rootScope.settings.pathAPI + 'api/staff/' + item.id + '/assign/event/' + response.data.id,
                            headers: {
                                "Content-Type": "application/json;charset=UTF-8",
                                "Authorization": 'Bearer ' + $rootScope.settings.token
                            }

                        }).then(function successCallback(response2) {
                            if (cont == $scope.stavesAdded.length) {
                                $scope.saveEntrances(response);
                            }
                            cont = cont + 1;
                        }, function errorCallback(response) {
                            console.log(response);
                        });

                    });
                } else {
                    $scope.saveEntrances(response);
                }
            }, function errorCallback(response) {
                $scope.settings.error = $translate.instant("EVENT_OBJECT.ERROR_ADD");
                console.log(response);
                $scope.settings.share_code = null;
                $scope.settings.url = null;
                openModal();
            });
        }
    };

    $scope.saveEntrances = function (event) {
        $scope.entrances.forEach(function (item2, index2, array) {
            var action2 = $http({
                method: 'POST',
                url: $rootScope.settings.pathAPI + 'api/entrances',
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                    "Authorization": 'Bearer ' + $rootScope.settings.token
                },
                data: {
                    event_id: event.data.id,
                    name: item2.name,
                    description: item2.description,
                    quantity_availables: item2.quantity_availables,
                    price: item2.price,
                    original_qty: item2.original_qty
                }
            }).then(function successCallBack(response) {

                if (index2 == $scope.entrances.length - 1) {
                    $rootScope.update = true;
                    if ($scope.event.share_code) {

                        $scope.settings.success = 'Private event successfully publicated!';
                        $scope.settings.share_code = $scope.event.share_code;
                        $scope.settings.url = $rootScope.settings.pathAPI + 'events/' + $scope.event.slug;
                    }
                    $scope.settings.success = 'Event successfully publicated!';
                    openModal();
                    //$window.history.back();
                }
            });
        });
    };


    $scope.changePage = function () {
        $('#messageModal').modal('toggle');

        //$location.path("/events.html");
    };
    // $scope.redirect = function () {
    //     closeModal();
    // }

});

// angular.module('DashBoardA').controller('AddCategoria', function ($rootScope, $scope, DTOptionsBuilder, Servicio, Events, $http, $state, $window, $stateParams) {
//     $scope.settings = {
//         pageTitle: "Agregar una Categoria",
//         action: "Guardar"
//     };
//     //$scope.category ={name:"",description:""};
//     $scope.$on('$viewContentLoaded', function () {
//         do {
//             Servicio.data.count = 0;
//             //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
//             $scope.vm = this;
//             $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
//                 .withLanguageSource(
//                     'http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
//                 )
//                 .withPaginationType('full_numbers')
//                 .withDisplayLength(10)
//                 .withBootstrap();
//
//         } while (Servicio.data.count != 0);
//         // initialize core components
//         App.initAjax();
//     });
//     //$rootScope.settings.pageTitle = settings.pageTitle;
//     // oculta el sidebar y pone el body solido en blanco
//     $rootScope.settings.layout.pageContentWhite = true;
//     $rootScope.settings.layout.pageBodySolid = false;
//     $rootScope.settings.layout.pageSidebarClosed = false;
//     var count = 0;
//
//     //Inicia la variable categoria con los datos correspondientes
//     $scope.category = {
//         name: "",
//         description: "",
//         picture: null
//     };
//     $scope.file = null;
//     $scope.$watch('file', function (newVal) {
//     });
//
//     $scope.submit = function () {
//         var action = $http({
//             method: 'POST',
//             url: $rootScope.settings.pathAPI + 'api/categories',
//             headers: {
//                 "Content-Type": "application/json;charset=UTF-8",
//                 "Authorization": 'Bearer ' + $rootScope.settings.token
//             },
//             data: {
//                 name: $scope.category.name,
//                 description: $scope.category.description
//             }
//         }).then(function successCallback(response) {
//             if($scope.file){
//                 var data = data = new FormData();
//                 data.append('file', $scope.file);
//                 //Sube el archivo al servidor
//                 jQuery.ajax({
//                     url: $rootScope.settings.pathAPI + 'api/uploads',
//                     data: data,
//                     cache: false,
//                     contentType: false,
//                     processData: false,
//                     type: 'POST',
//                     success: function (data) {
//                         var action = $http({
//                             method: 'PUT',
//                             url: $rootScope.settings.pathAPI + 'api/categories/' + response.data.id,
//                             headers: {
//                                 "Content-Type": "application/json;charset=UTF-8",
//                                 "Authorization": 'Bearer ' + $rootScope.settings.token
//                             },
//                             data: {
//                                 picture: data.url
//                             }
//                         }).then(function successCallback(response) {
//                             $rootScope.update = true;
//                             $window.history.back();
//                         },function errorCallback(response) {
//                             $scope.settings.error = "¡Error al adjuntar la imagen a la categoria!";
//                             console.log(response, $scope.settings.error)
//                         });
//                     }});
//             }else{
//                 $rootScope.update = true;
//                 $window.history.back();
//             }
//         }, function errorCallback(response) {
//             $scope.settings.error = "¡Error al crear categoria!";
//             console.log(response, $scope.settings.error)
//         });
//         $scope.settings.success = "La categoria ha sido creada correctamente!";
//
//
//     };
//
//
//     $scope.reload = function () {
//         $window.location.reload();
//     }
//
// });
//
// angular.module('DashBoardA').controller('edCategoria', function ($rootScope, $scope, DTOptionsBuilder, Servicio, Events, Category, $http, $state, $window, $stateParams) {
//     $scope.settings = {
//         pageTitle: "Editar una Categoria",
//         action: "Editar"
//     };
//     //$scope.category ={name:"",description:""};
//     $scope.$on('$viewContentLoaded', function () {
//         do {
//             Servicio.data.count = 0;
//             //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
//             $scope.vm = this;
//             $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
//                 .withLanguageSource(
//                     'http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
//                 )
//                 .withPaginationType('full_numbers')
//                 .withDisplayLength(10)
//                 .withBootstrap();
//
//         } while (Servicio.data.count != 0);
//         // initialize core components
//         App.initAjax();
//     });
//     //$rootScope.settings.pageTitle = settings.pageTitle;
//     // oculta el sidebar y pone el body solido en blanco
//     $rootScope.settings.layout.pageContentWhite = true;
//     $rootScope.settings.layout.pageBodySolid = false;
//     $rootScope.settings.layout.pageSidebarClosed = false;
//
//     var id = $stateParams.id;
//
//     Category.get({id: id}, function (data) {
//         $scope.category = data;
//
//     });
//     $scope.file = null;
//     $scope.$watch('file', function (newVal) {
//
//     });
//
//     //Obtiene los registros que se encuentran en la tabla de usuarios
//
//     $scope.submit = function () {
//         var action = $http({
//             method: 'PUT',
//             url: $rootScope.settings.pathAPI + 'api/categories/' + $scope.category.id,
//             headers: {
//                 "Content-Type": "application/json;charset=UTF-8",
//                 "Authorization": 'Bearer ' + $rootScope.settings.token
//             },
//             data: {
//                 name: $scope.category.name,
//                 description: $scope.category.description
//             }
//         }).then(function successCallback(response) {
//             if($scope.file){
//                 var data = data = new FormData();
//                 data.append('file', $scope.file);
//                 //Sube el archivo al servidor
//                 jQuery.ajax({
//                     url: $rootScope.settings.pathAPI + 'api/uploads',
//                     data: data,
//                     cache: false,
//                     contentType: false,
//                     processData: false,
//                     type: 'POST',
//                     success: function (data) {
//                         var action = $http({
//                             method: 'PUT',
//                             url: $rootScope.settings.pathAPI + 'api/categories/' + $scope.category.id,
//                             headers: {
//                                 "Content-Type": "application/json;charset=UTF-8",
//                                 "Authorization": 'Bearer ' + $rootScope.settings.token
//                             },
//                             data: {
//                                 picture: data.url
//                             }
//                         }).then(function successCallback(response) {
//                             $rootScope.update = true;
//                             $window.history.back();
//                         },function errorCallback(response) {
//                             $scope.settings.error = "¡Error al adjuntar la imagen a la categoria!";
//                             console.log(response, $scope.settings.error)
//                         });
//                     }});
//             }else{
//                 $rootScope.update = true;
//                 $window.history.back();
//             }
//         }, function errorCallback(response) {
//             $scope.settings.error = "¡Error al crear categoria!";
//             console.log(response, $scope.settings.error)
//         });
//         $scope.settings.success = "La categoria ha sido editada correctamente!";
//
//     };
//
//
//     $scope.reload = function () {
//         $window.location.reload();
//     }
//
// });
//
//
//
// angular.module('DashBoardA').controller('AddPais', function ($rootScope, $scope, DTOptionsBuilder, Servicio, Events, $http, $state, $window, $stateParams) {
//     $scope.settings = {
//         pageTitle: "Agregar un País",
//         action: "Guardar"
//     };
//     //$scope.category ={name:"",description:""};
//     $scope.$on('$viewContentLoaded', function () {
//         do {
//             Servicio.data.count = 0;
//             //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
//             $scope.vm = this;
//             $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
//                 .withLanguageSource(
//                     'http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
//                 )
//                 .withPaginationType('full_numbers')
//                 .withDisplayLength(10)
//                 .withBootstrap();
//
//         } while (Servicio.data.count != 0);
//         // initialize core components
//         App.initAjax();
//     });
//     //$rootScope.settings.pageTitle = settings.pageTitle;
//     // oculta el sidebar y pone el body solido en blanco
//     $rootScope.settings.layout.pageContentWhite = true;
//     $rootScope.settings.layout.pageBodySolid = false;
//     $rootScope.settings.layout.pageSidebarClosed = false;
//     var count = 0;
//
//     //Inicia la variable categoria con los datos correspondientes
//     $scope.country = {
//         name: "",
//         picture: null
//     };
//     $scope.file = null;
//     $scope.$watch('file', function (newVal) {
//     });
//
//     $scope.submit = function () {
//         var action = $http({
//             method: 'POST',
//             url: $rootScope.settings.pathAPI + 'api/countries',
//             headers: {
//                 "Content-Type": "application/json;charset=UTF-8",
//                 "Authorization": 'Bearer ' + $rootScope.settings.token
//             },
//             data: {
//                 name: $scope.country.name
//             }
//         }).then(function successCallback(response) {
//             if($scope.file){
//                 var data = data = new FormData();
//                 data.append('file', $scope.file);
//                 //Sube el archivo al servidor
//                 jQuery.ajax({
//                     url: $rootScope.settings.pathAPI + 'api/uploads',
//                     data: data,
//                     cache: false,
//                     contentType: false,
//                     processData: false,
//                     type: 'POST',
//                     success: function (data) {
//                         var action = $http({
//                             method: 'PUT',
//                             url: $rootScope.settings.pathAPI + 'api/countries/' + response.data.id,
//                             headers: {
//                                 "Content-Type": "application/json;charset=UTF-8",
//                                 "Authorization": 'Bearer ' + $rootScope.settings.token
//                             },
//                             data: {
//                                 picture: data.url
//                             }
//                         }).then(function successCallback(response) {
//                             $rootScope.update = true;
//                             $window.history.back();
//                         },function errorCallback(response) {
//                             $scope.settings.error = "¡Error al adjuntar la imagen al País!";
//                             console.log(response, $scope.settings.error)
//                         });
//                     }});
//             }else{
//                 $rootScope.update = true;
//                 $window.history.back();
//             }
//         }, function errorCallback(response) {
//             $scope.settings.error = "¡Error al crear País!";
//             console.log(response, $scope.settings.error)
//         });
//         $scope.settings.success = "El País ha sido creada correctamente!";
//
//
//     };
//
//
//     $scope.reload = function () {
//         $window.location.reload();
//     }
//
// });
//
// angular.module('DashBoardA').controller('edPais', function ($rootScope, $scope, DTOptionsBuilder, Country, Servicio, Events, Category, $http, $state, $window, $stateParams) {
//     $scope.settings = {
//         pageTitle: "Editar País",
//         action: "Editar"
//     };
//     //$scope.category ={name:"",description:""};
//     $scope.$on('$viewContentLoaded', function () {
//         do {
//             Servicio.data.count = 0;
//             //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
//             $scope.vm = this;
//             $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
//                 .withLanguageSource(
//                     'http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
//                 )
//                 .withPaginationType('full_numbers')
//                 .withDisplayLength(10)
//                 .withBootstrap();
//
//         } while (Servicio.data.count != 0);
//         // initialize core components
//         App.initAjax();
//     });
//     //$rootScope.settings.pageTitle = settings.pageTitle;
//     // oculta el sidebar y pone el body solido en blanco
//     $rootScope.settings.layout.pageContentWhite = true;
//     $rootScope.settings.layout.pageBodySolid = false;
//     $rootScope.settings.layout.pageSidebarClosed = false;
//
//     var id = $stateParams.id;
//
//     Country.get({id: id}, function (data) {
//         $scope.country = data;
//         console.log($scope.country);
//
//     });
//     $scope.file = null;
//     $scope.$watch('file', function (newVal) {
//
//     });
//
//     //Obtiene los registros que se encuentran en la tabla de usuarios
//
//     $scope.submit = function () {
//         var action = $http({
//             method: 'PUT',
//             url: $rootScope.settings.pathAPI + 'api/countries/' + $scope.country.id,
//             headers: {
//                 "Content-Type": "application/json;charset=UTF-8",
//                 "Authorization": 'Bearer ' + $rootScope.settings.token
//             },
//             data: {
//                 name: $scope.country.name,
//                 description: $scope.country.description
//             }
//         }).then(function successCallback(response) {
//             if($scope.file){
//                 var data = data = new FormData();
//                 data.append('file', $scope.file);
//                 //Sube el archivo al servidor
//                 jQuery.ajax({
//                     url: $rootScope.settings.pathAPI + 'api/uploads',
//                     data: data,
//                     cache: false,
//                     contentType: false,
//                     processData: false,
//                     type: 'POST',
//                     success: function (data) {
//                         var action = $http({
//                             method: 'PUT',
//                             url: $rootScope.settings.pathAPI + 'api/countries/' + $scope.country.id,
//                             headers: {
//                                 "Content-Type": "application/json;charset=UTF-8",
//                                 "Authorization": 'Bearer ' + $rootScope.settings.token
//                             },
//                             data: {
//                                 picture: data.url
//                             }
//                         }).then(function successCallback(response) {
//                             $rootScope.update = true;
//                             $window.history.back();
//                         },function errorCallback(response) {
//                             $scope.settings.error = "¡Error al adjuntar la imagen al País!";
//                             console.log(response, $scope.settings.error)
//                         });
//                     }});
//             }else{
//                 $rootScope.update = true;
//                 $window.history.back();
//             }
//         }, function errorCallback(response) {
//             $scope.settings.error = "¡Error al editar País!";
//             console.log(response, $scope.settings.error)
//         });
//         $scope.settings.success = "El País ha sido editado correctamente!";
//
//     };
//
//
//     $scope.reload = function () {
//         $window.location.reload();
//     }
//
// });
//
// angular.module('DashBoardA').controller('AddCiudad', function ($rootScope, $scope, DTOptionsBuilder, Servicio, Events, Country, $http, $state, $window, $stateParams) {
//     $scope.settings = {
//         pageTitle: "Agregar Ciudad",
//         action: "Guardar"
//     };
//     //$scope.category ={name:"",description:""};
//     $scope.$on('$viewContentLoaded', function () {
//         do {
//             Servicio.data.count = 0;
//             //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
//             $scope.vm = this;
//             $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
//                 .withLanguageSource(
//                     'http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
//                 )
//                 .withPaginationType('full_numbers')
//                 .withDisplayLength(10)
//                 .withBootstrap();
//
//         } while (Servicio.data.count != 0);
//         // initialize core components
//         App.initAjax();
//     });
//     //$rootScope.settings.pageTitle = settings.pageTitle;
//     // oculta el sidebar y pone el body solido en blanco
//     $rootScope.settings.layout.pageContentWhite = true;
//     $rootScope.settings.layout.pageBodySolid = false;
//     $rootScope.settings.layout.pageSidebarClosed = false;
//
//     $scope.country = {};
//     Country.get({}, function (data) {
//         $scope.countries = data.data;
//         console.log($scope.countries);
//
//     });
//
//     $scope.file = null;
//     $scope.$watch('file', function (newVal) {
//     });
//
//
//
//     //Obtiene los registros que se encuentran en la tabla de usuarios
//     $scope.city = {
//         name: "",
//         country_id: 0,
//     };
//
//     $scope.submit = function () {
//         var action = $http({
//             method: 'POST',
//             url: $rootScope.settings.pathAPI + 'api/cities',
//             headers: {
//                 "Content-Type": "application/json;charset=UTF-8",
//                 "Authorization": 'Bearer ' + $rootScope.settings.token
//             },
//             data: {
//                 name: $scope.city.name,
//                 country_id: $scope.country.value.id,
//             }
//         }).then(function successCallback(response) {
//             if($scope.file){
//                 var data = data = new FormData();
//                 data.append('file', $scope.file);
//                 //Sube el archivo al servidor
//                 jQuery.ajax({
//                     url: $rootScope.settings.pathAPI + 'api/uploads',
//                     data: data,
//                     cache: false,
//                     contentType: false,
//                     processData: false,
//                     type: 'POST',
//                     success: function (data) {
//                         var action = $http({
//                             method: 'PUT',
//                             url: $rootScope.settings.pathAPI + 'api/cities/' + response.data.id,
//                             headers: {
//                                 "Content-Type": "application/json;charset=UTF-8",
//                                 "Authorization": 'Bearer ' + $rootScope.settings.token
//                             },
//                             data: {
//                                 picture: data.url
//                             }
//                         }).then(function successCallback(response) {
//                             $rootScope.update = true;
//                             $window.history.back();
//                         },function errorCallback(response) {
//                             $scope.settings.error = "¡Error al adjuntar la imagen al País!";
//                             console.log(response, $scope.settings.error)
//                         });
//                     }});
//             }else{
//                 $rootScope.update = true;
//                 $window.history.back();
//             }
//         }, function errorCallback(response) {
//             $scope.settings.error = "¡Error al crear País!";
//             console.log(response, $scope.settings.error)
//         });
//         $scope.settings.success = "El País ha sido creada correctamente!";
//     };
//
//
//     $scope.reload = function () {
//         $window.location.reload();
//     }
//
// });
//
// angular.module('DashBoardA').controller('edCiudad', function ($rootScope, $scope, DTOptionsBuilder, Servicio, Events, City, Country, $http, $state, $window, $stateParams) {
//     $scope.settings = {
//         pageTitle: "Editar Ciudad",
//         action: "Editar"
//     };
//     //$scope.category ={name:"",description:""};
//     $scope.$on('$viewContentLoaded', function () {
//         do {
//             Servicio.data.count = 0;
//             //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
//             $scope.vm = this;
//             $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
//                 .withLanguageSource(
//                     'http://cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json'
//                 )
//                 .withPaginationType('full_numbers')
//                 .withDisplayLength(10)
//                 .withBootstrap();
//
//         } while (Servicio.data.count != 0);
//         // initialize core components
//         App.initAjax();
//     });
//     //$rootScope.settings.pageTitle = settings.pageTitle;
//     // oculta el sidebar y pone el body solido en blanco
//     $rootScope.settings.layout.pageContentWhite = true;
//     $rootScope.settings.layout.pageBodySolid = false;
//     $rootScope.settings.layout.pageSidebarClosed = false;
//
//     var id = $stateParams.id;
//     $scope.country = {};
//     $scope.city = {};
//     City.get({id: id}, function (data) {
//         $scope.city = data;
//         console.log($scope.city);
//         Country.get({id: data.country_id}, function (data) {
//             $scope.country.value = data;
//             console.log($scope.country.value);
//
//         });
//     });
//     Country.get({}, function (data) {
//         $scope.countries = data.data;
//         console.log($scope.countries);
//
//     });
//     $scope.file = null;
//     $scope.$watch('file', function (newVal) {
//
//     });
//
//     //Obtiene los registros que se encuentran en la tabla de usuarios
//
//     $scope.submit = function () {
//         var action = $http({
//             method: 'PUT',
//             url: $rootScope.settings.pathAPI + 'api/cities/' + $scope.city.id,
//             headers: {
//                 "Content-Type": "application/json;charset=UTF-8",
//                 "Authorization": 'Bearer ' + $rootScope.settings.token
//             },
//             data: {
//                 name: $scope.city.name,
//                 country_id: $scope.country.value.id
//             }
//         }).then(function successCallback(response) {
//             if($scope.file){
//                 var data = data = new FormData();
//                 data.append('file', $scope.file);
//                 //Sube el archivo al servidor
//                 jQuery.ajax({
//                     url: $rootScope.settings.pathAPI + 'api/uploads',
//                     data: data,
//                     cache: false,
//                     contentType: false,
//                     processData: false,
//                     type: 'POST',
//                     success: function (data) {
//                         var action = $http({
//                             method: 'PUT',
//                             url: $rootScope.settings.pathAPI + 'api/cities/' + $scope.city.id,
//                             headers: {
//                                 "Content-Type": "application/json;charset=UTF-8",
//                                 "Authorization": 'Bearer ' + $rootScope.settings.token
//                             },
//                             data: {
//                                 picture: data.url
//                             }
//                         }).then(function successCallback(response) {
//                             $rootScope.update = true;
//                             $window.history.back();
//                         },function errorCallback(response) {
//                             $scope.settings.error = "¡Error al adjuntar la imagen a la Ciudad!";
//                             console.log(response, $scope.settings.error)
//                         });
//                     }});
//             }else{
//                 $rootScope.update = true;
//                 $window.history.back();
//             }
//         }, function errorCallback(response) {
//             $scope.settings.error = "¡Error al editar Ciudad!";
//             console.log(response, $scope.settings.error)
//         });
//         $scope.settings.success = "¡La Ciudad ha sido editada correctamente!";
//
//     };
//
//
//     $scope.reload = function () {
//         $window.location.reload();
//     }
//
// });

//Servicio usado para compartir variables en el scope de cada controlador
angular.module('DashBoardA').factory("Servicio", function () {
    return {
        data: {}
    };
})
;

angular.module('DashBoardA').directive('ngConfirmClick', [
    function () {
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick;
                var clickAction = attr.confirmedClick;
                element.bind('click', function (event) {
                    if (window.confirm(msg)) {
                        scope.$eval(clickAction);
                    }
                });
            }
        };
    }]);

/**
 * AngularJS default filter with the following expression:
 * "person in people | filter: {name: $select.search, age: $select.search}"
 * performs a AND between 'name: $select.search' and 'age: $select.search'.
 * We want to perform a OR.
 */
angular.module('DashBoardA').filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function (item) {
                var itemMatches = false;

                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    };
});

// angular.module('DashBoardA').controller('payEventosController', function ($rootScope, $location, $translate, Staffs, $scope, DTOptionsBuilder, Country, City, Servicio, Events, $http, Category, $state, $window, $stateParams) {
//     $scope.settings = {
//         pageTitle: $translate.instant("EVENT_OBJECT.ADD_EVENT"),
//         action: $translate.instant("EVENT_OBJECT.PUBLISH_EVENT")
//     };
//     var data = localStorage.getItem('userLogged');
//     $scope.organizador = JSON.parse(data);
//
//
//     //Extraer categorias
//     Category.get({}, function (data) {
//         $scope.categories = data.data;
//     });
//
//     //Consulta paises
//     Country.get({}, function (data) {
//         $scope.countries = data.data;
//     });
//
//
//     //Metodo ejecutado al cargar el contenido de la pagina
//     $scope.$on('$viewContentLoaded', function () {
//         do {
//             Servicio.data.count = 0;
//             //Se crea una nueva instancia de DTOptionsBuilder con las configuraciones necesarias para la tabla
//             $scope.vm = this;
//             $scope.vm.dtOptions = DTOptionsBuilder.newOptions()
//                 .withLanguageSource(
//                     $translate.instant("URL_DATATABLE")
//                 )
//                 .withPaginationType('full_numbers')
//                 .withDisplayLength(10)
//                 .withBootstrap().withOption("responsive", true);
//
//         } while (Servicio.data.count != 0);
//         // initialize core components
//         App.initAjax();
//     });
//
//     // oculta el sidebar y pone el body solido en blanco
//     $rootScope.settings.layout.pageContentWhite = true;
//     $rootScope.settings.layout.pageBodySolid = false;
//     $rootScope.settings.layout.pageSidebarClosed = false;
//     var count = 0;
//
//     //Definicion del evento
//     $scope.event = {
//         name: "",
//         description: "",
//         facebook_link: "",
//         twitter_link: "",
//         quantity_tickets: 0,
//         location: "",
//         latitude: 0,
//         longitude: 0,
//         start_date: "",
//         end_date: "",
//         start_time: "",
//         end_time: "",
//         privacy: "",
//         picture: "",
//         status: "available",
//         country_id: 0,
//         organization_id: $scope.organizador.organization.id
//     };
//
// });