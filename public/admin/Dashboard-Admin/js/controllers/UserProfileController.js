angular.module('DashBoardA').controller('UserProfileController', function ($rootScope, $scope, $translate, User,Users, $stateParams, $http, $window, $timeout, $state) {
    $scope.$on('$viewContentLoaded', function () {
        var id = $stateParams.id;
        if ($rootScope.isEdit) {
            var notify = {
                type: 'success',
                title: 'Data Updated Successful!',
                content: 'The information has already updated.',
                timeout: 5000 //time in ms
            };
            $rootScope.$emit('notify', notify);
            $rootScope.isEdit = false;
            $window.location.reload();
        }

        if (id == 0 && !$rootScope.isAdmin) {

            if (!$rootScope.userLogged) {
                var data = localStorage.getItem('userLogged');
                $scope.organizador = JSON.parse(data);
                $rootScope.userLogged = JSON.parse(data);
            } else {
                $scope.organizador = $rootScope.userLogged;
            }
            User.get({id: $scope.organizador.id}, function (data) {
                $scope.organizador = data;
            });

        } else {
            if (id) {
                User.get({id: id}, function (data) {
                    $scope.organizador = data;
                    $scope.sourceOrg = $scope.organizador.organization.picture;
                });
            }
        }
        App.initAjax(); // initialize core components
        Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu
    });
    $scope.$watch('organizador', function (newVal) {
    });
    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = false;


});

angular.module('DashBoardA').controller('EditUserProfileController', function ($rootScope, $scope, $translate, User, Users, $stateParams, $http, $timeout, $state, $window) {
    $scope.$on('$viewContentLoaded', function () {
        App.initAjax(); // initialize core components
        Layout.setAngularJsSidebarMenuActiveLink('set', $('#sidebar_menu_link_profile'), $state); // set profile link active in sidebar menu


    });

    $scope.myImage = '';
    $scope.myCroppedImage = '';
    $scope.$watch('myCroppedImage', function (newVal) {
        console.log($scope.myCroppedImage);
    });
    $scope.$watch('myImage', function (newVal) {

    });
    var handleFileSelect = function (evt) {
        var file = evt.currentTarget.files[0];
        var reader = new FileReader();
        reader.onload = function (evt) {
            $scope.$apply(function ($scope) {
                $scope.myImage = evt.target.result;
            });
        };
        if (file) {
            reader.readAsDataURL(file);
        } else {
            $scope.myImage = '';
        }
    };
    angular.element(document.querySelector('#fileUser')).on('change', handleFileSelect);

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = false;
    $scope.fileOrg = null;
    $scope.file = null;

    $scope.$watch('fileOrg', function (newVal) {
        if (newVal) {

            var data = data = new FormData();
            data.append('file', $scope.fileOrg);
            jQuery.ajax({
                url: $rootScope.settings.pathAPI + 'api/uploads',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success: function (data) {

                    var action = $http({
                        method: 'PUT',
                        url: $rootScope.settings.pathAPI + 'api/organizations/' + $scope.organizador.organization.id,
                        headers: {
                            "Content-Type": "application/json;charset=UTF-8",
                            "Authorization": 'Bearer ' + $rootScope.settings.token
                        },
                        data: {
                            picture: data.url
                        }
                    }).then(function successCallback(response) {


                    }, function errorCallback(response) {
                        //$scope.success = " error asignar imagen!";
                        console.log(response);
                    });
                }
            });
        }

    });

    $scope.$watch('file', function (newVal) {

        if (newVal) {

        }
    });
    var id = $stateParams.id;

    $scope.sourceOrg = null;
    if (id == 0 && !$rootScope.isAdmin) {
        $scope.organizador = $rootScope.userLogged;

    } else {
        if (id) {
            User.get({id: id}, function (data) {
                $scope.organizador = data;
                $scope.sourceOrg = $scope.organizador.organization.picture;
            });
        }
    }

    $scope.fileAux = null;
    $scope.changeFile = function (data) {
        if (data) {
            var contentType = 'image/jpeg';
            var sliceSize = 1024;
            var byteCharacters = atob(data.split(',')[1]);
            var bytesLength = byteCharacters.length;
            var slicesCount = Math.ceil(bytesLength / sliceSize);
            var byteArrays = new Array(slicesCount);

            for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
                var begin = sliceIndex * sliceSize;
                var end = Math.min(begin + sliceSize, bytesLength);

                var bytes = new Array(end - begin);
                for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                    bytes[i] = byteCharacters[offset].charCodeAt(0);
                }
                byteArrays[sliceIndex] = new Uint8Array(bytes);
            }
            $scope.file = document.getElementById('fileUser').files[0];
            $rootScope.fileAux = new File(byteArrays, $scope.file.name, {type: contentType});
        }
    };

    $scope.submit = function () {

        // console.log($scope.file);
        var action = $http({
            method: 'PUT',
            url: $rootScope.settings.pathAPI + 'api/users/' + $scope.organizador.id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
                name: $scope.organizador.name,
                last_name: $scope.organizador.last_name,
                email: $scope.organizador.email,
                phone_number: $scope.organizador.phone_number,
                birth_date: $scope.organizador.birth_date
            }
        }).then(function successCallback(response) {

        }, function errorCallback(response) {
            // $scope.settings.success = " error al modificar organizador!";
            console.log(response);
        });

        var action2 = $http({
            method: 'PUT',
            url: $rootScope.settings.pathAPI + 'api/organizations/' + $scope.organizador.organization.id,
            headers: {
                "Content-Type": "application/json;charset=UTF-8",
                "Authorization": 'Bearer ' + $rootScope.settings.token
            },
            data: {
                name: $scope.organizador.organization.name,
                description: $scope.organizador.organization.description,
            }
        }).then(function successCallback(response) {
            $scope.success = "La organizacion ha sido editada correctamente!";
            $rootScope.isEdit = true;
            if ($rootScope.fileAux) {
                $scope.savePicture();
            } else {
                $window.history.back();
            }

        }, function errorCallback(response) {
            //$scope.settings.success = " error al modificar organizacion!";
            console.log(response);
        });


    };

    $scope.savePicture = function () {

        var data = data = new FormData();
        data.append('file', $rootScope.fileAux);
        jQuery.ajax({
            url: $rootScope.settings.pathAPI + 'api/uploads',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function (data) {
                var action = $http({
                    method: 'PUT',
                    url: $rootScope.settings.pathAPI + 'api/users/' + $scope.organizador.id,
                    headers: {
                        "Content-Type": "application/json;charset=UTF-8",
                        "Authorization": 'Bearer ' + $rootScope.settings.token
                    },
                    data: {
                        avatar: data.url,
                    }
                }).then(function successCallback(response) {
                    $scope.success = response;
                    $rootScope.fileAux = null;
                    $window.history.back();


                }, function errorCallback(response) {
                    //$scope.success = " error asignar imagen!";
                    console.log(response);
                });
            }
        });
    };


})
;
