/***
 Metronic AngularJS App Main Script
 ***/

/* Metronic App */
var DashBoardA = angular.module("DashBoardA", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize",
    "ngResource",
    "ngMessages",
    "datatables",
    "ngCookies",
    "datatables.bootstrap",
    "file-model",
    "ngImgCrop",
    "datatables.buttons",
    "pascalprecht.translate",
    "btorfs.multiselect",
    "angularNotify"
]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
DashBoardA.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // global configs go here
    });
}]);


/* Setup global settings */
DashBoardA.factory('settings', ['$rootScope', function ($rootScope) {
    // supported languages
   var lang = [{
        clave: 'EN',
        pathBandera: '../assets/global/img/flags/us.png',
        modulo: 'en_US',
        nombre: 'English',
        active: false
    },{
        clave: 'ES',
        pathBandera: '../assets/global/img/flags/es.png',
        modulo: 'es_MX',
        nombre: 'Spanish',
        active: false
    }];
    if(!localStorage.getItem('lang')) {
        localStorage.setItem('lang', JSON.stringify(lang));
    }
    if(!localStorage.getItem('prefLang')) {
        lang[0].active = true;
        localStorage.setItem('prefLang', JSON.stringify(lang[0]));
    }
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load

        },
        assetsPath: '../assets',
        globalPath: '../assets/global',
        layoutPath: '../assets/layouts/layout',
        token: localStorage.getItem('token'),
        idUser: localStorage.getItem('id'),
        //pathAPI: 'http://ec2-54-244-44-74.us-west-2.compute.amazonaws.com/',
        //pathRedirect: 'http://localhost/final/public/admin/',
		pathAPI: 'https://eventstocity.com/',
        pathRedirect: 'https://eventstocity.com/',
        parametersUser: '?include[]=roles&include[]=organization&include[]=country&include[]=city',
        parametersEvent: '?include[]=category&include[]=reviews&include[]=entrances&include[]=organization&include[]=country&include[]=city',
        pathNotifications: 'https://eventstocity.com/admin/Dashboard-Admin/php/'
    };

    $rootScope.settings = settings;
//console.log(localStorage.getItem('token'));
    return settings;
}]);

DashBoardA.controller('UserSessionController', function ($rootScope, $scope, $translate, $filter, User, $http, $timeout, $cookieStore) {

    $scope.$watch(function () {
            return $filter('translate')('HEADER.LOGOUT');
        },
        function (newval) {
            console.log(newval);
        }
    );
    $scope.lang = JSON.parse(localStorage.getItem('prefLang'));
    $scope.langs = JSON.parse(localStorage.getItem('lang'));
    $scope.jsTrSimple = $translate.instant('SERVICE');
    $scope.jsTrParams = $translate.instant('SERVICE_PARAMS');
    $scope.ok = $translate.instant('HEADER.LOGOUT');
    console.log($scope.ok);
    $translate('HEADER.LOGOUT').then(function (headline) {
        $scope.trad = headline;
    });
    console.log($scope.jsTrParams);
    console.log($scope.jsTrSimple);
    User.get({id: $rootScope.settings.idUser}, function (data) {
        localStorage.setItem('userLogged', JSON.stringify(data));
        $rootScope.userLogged = data;
        $rootScope.isAdmin = false;
        for (var i = 0; i < $rootScope.userLogged.roles.data.length; i++) {
            if ($rootScope.userLogged.roles.data[i].name == 'manager') {
                $rootScope.isAdmin = true;
            }
        }

        console.log($rootScope.isAdmin);

        $scope.loggOut = function () {
            localStorage.clear();
            window.location = $rootScope.settings.pathAPI + "admin/";
        }
    });
    $scope.changeLanguage = function (lang) {
        console.log(lang);
        lang.active = true;
        localStorage.setItem('prefLang', JSON.stringify(lang));
        $scope.lang.active = false;
        $scope.langs.forEach(function(item) {
            if(item.clave == lang.clave) {
                item.active = true;
            }
            if(item.clave == $scope.lang.clave) {
                item.active = false;
            }
        });
        $scope.lang = lang;
        localStorage.setItem('lang', JSON.stringify($scope.langs));
        $translate.use(lang.modulo);
    };

});

DashBoardA.factory('cacheFactory', ['$cacheFactory', function ($cacheFactory) {
    return $cacheFactory("userData");
}]);


DashBoardA.factory('User', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/users/:id' + $rootScope.settings.parametersUser, {id: '@_id'}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}},
        update: {
            method: "PUT",
            data: '@data',
            headers: {'Authorization': "Bearer " + $rootScope.settings.token},
            params: {id: '@id'}
        }
    });
    return values;
}]);

DashBoardA.factory('Users', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/users' + $rootScope.settings.parametersUser, {}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}},
        update: {
            method: "PUT",
            data: '@data',
            headers: {'Authorization': "Bearer " + $rootScope.settings.token},
            params: {id: '@id'}
        }
    });
    return values;
}]);

DashBoardA.factory('Rol', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/roles/:id', {id: '@_id'}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}},
        update: {
            method: "PUT",
            data: '@data',
            headers: {'Authorization': "Bearer " + $rootScope.settings.token},
            params: {id: '@id'}
        }
    });
    return values;
}]);

DashBoardA.factory('Category', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/categories/:id', {id: '@_id'}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}},
        update: {
            method: "PUT",
            data: '@data',
            headers: {'Authorization': "Bearer " + $rootScope.settings.token},
            params: {id: '@id'}
        }
    });
    return values;
}]);

DashBoardA.factory('Country', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/countries/:id', {id: '@_id'}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}}
    });

    return values;
}]);

DashBoardA.factory('Staffs', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/staff/:id', {id: '@_id'}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}}
    });

    return values;
}]);

DashBoardA.factory('Tickets', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/tickets/event/:id?include[]=user', {id: '@_id'}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}}
    });

    return values;
}]);

DashBoardA.factory('City', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/cities/:id', {id: '@_id'}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}}
    });

    return values;
}]);

DashBoardA.factory('Entrance', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/entrances/:id', {id: '@_id'}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}},
        update: {
            method: "PUT",
            data: '@data',
            headers: {'Authorization': "Bearer " + $rootScope.settings.token},
            params: {id: '@id'}
        }
    });
    return values;
}]);


// DashBoardA.factory('Users', ['$resource', '$rootScope', function ($resource, $rootScope) {
//     var values = $resource($rootScope.settings.pathAPI + 'api/users/', {}, {
//         get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}}
//     });
//
//     return values;
// }]);

DashBoardA.factory('OrganizationsRequest', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/organizations/request', {}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}}
    });

    return values;
}]);

DashBoardA.factory('Organizations', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/organizations/:id', {id: '@id'}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}}
    });

    return values;
}]);

DashBoardA.factory('Finances', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/finances/organization/all:filter', {filter: '@filter'}, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}}
    });

    return values;
}]);

// DashBoardA.factory('FinancesByFilter', ['$resource', '$rootScope', function ($resource, $rootScope) {
//     var values = $resource($rootScope.settings.pathAPI + 'api/finances/:filter/:id/:events', {filter: '@filter', id: '@id',events: '@events'}, {
//         get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}}
//     });
//
//     return values;
// }]);

DashBoardA.factory('FinancesByOrganization', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/finances/organization/:id/events:filter', {
        id: '@id',
        filter: '@filter'
    }, {
        get: {method: "GET", headers: {'Authorization': "Bearer " + $rootScope.settings.token}}
    });

    return values;
}]);

DashBoardA.factory('Events', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/events' + $rootScope.settings.parametersEvent, {
        get: {method: "GET"}
    });

    return values;
}]);

DashBoardA.factory('Event', ['$resource', '$rootScope', function ($resource, $rootScope) {
    var values = $resource($rootScope.settings.pathAPI + 'api/events/:id' + $rootScope.settings.parametersEvent, {id: '@id'}, {
        get: {method: "GET"}
    });

    return values;
}]);


/* Setup App Main Controller */
DashBoardA.controller('AppController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function () {
        //App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive
    });
}]);


/***
 Layout Partials.
 By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial
 initialization can be disabled and Layout.init() should be called on page load complete as explained above.
 ***/

/* Setup Layout Part - Header */
DashBoardA.controller('HeaderController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initHeader(); // init header
    });
}]);

/* Setup Layout Part - Sidebar */
DashBoardA.controller('SidebarController', ['$state', '$scope', function ($state, $scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initSidebar($state); // init sidebar
    });
}]);

/* Setup Layout Part - Quick Sidebar */
DashBoardA.controller('QuickSidebarController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        setTimeout(function () {
            QuickSidebar.init(); // init quick sidebar
        }, 2000)
    });
}]);

/* Setup Layout Part - Theme Panel */
DashBoardA.controller('ThemePanelController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Footer */
DashBoardA.controller('FooterController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initFooter(); // init footer
    });
}]);

/* Redireccionamiento a todas las paginas */
DashBoardA.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

    // Redirecciona en caso de que no exista la URL
    $urlRouterProvider.otherwise("/dashboard.html");

    $stateProvider

    // Dashboard
        .state('dashboard', {
            url: "/dashboard.html",
            templateUrl: "views/dashboard.html",
            data: {pageTitle: 'Dashboard'},
            controller: "DashboardController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',

                            '../assets/pages/scripts/dashboard.min.js',
                            'js/controllers/DashboardController.js',

                        ]
                    });
                }]
            }
        })



        // Users
        .state('users', {
            url: "/users.html",
            templateUrl: "views/users.html",
            data: {pageTitle: 'Users'},
            controller: "UsersController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',


                            'js/controllers/UsersController.js',
                        ]
                    });
                }]
            }
        })

        // add user
        .state('user', {
            url: "/user/",
            templateUrl: "views/user.html",
            data: {pageTitle: 'User'},
            controller: "AddUser",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'angularFileUpload',
                        files: [
                            '../assets/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                        ]
                    }, {
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',


                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',

                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',


                            'js/controllers/UsersController.js',


                        ]
                    }]);
                }]
            }
        })

        // edit user
        .state('edUser', {
            url: "/user/:id",
            templateUrl: "views/user.html",
            data: {pageTitle: 'User'},
            controller: "EditUser",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'angularFileUpload',
                        files: [
                            '../assets/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                        ]
                    }, {
                        name: 'DashBoardA',
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',


                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',

                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',


                            'js/controllers/UsersController.js',

                        ]
                    }]);
                }]
            }
        })
        .state('edEvento', {
            url: "/event/edit/:id",
            templateUrl: "views/evento.html",
            data: {pageTitle: 'Event'},
            controller: "edEventosController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'ui.select',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    }, {
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
                            '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',


                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/global/plugins/typeahead-addresspicker/dist/typeahead.bundle.min.js',
                            '../assets/global/plugins/typeahead-addresspicker/dist/typeahead-addresspicker.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',


                            'js/controllers/EventosController.js',
                        ]
                    }]);
                }]
            }
        })
        .state('payEvent', {
            url: "/event/pay/:id",
            templateUrl: "views/pay_event.html",
            data: {pageTitle: 'Pay Event'},
            controller: "payEventosController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'ui.select',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    }, {
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
                            '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',


                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/global/plugins/typeahead-addresspicker/dist/typeahead.bundle.min.js',
                            '../assets/global/plugins/typeahead-addresspicker/dist/typeahead-addresspicker.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',

                            'js/controllers/FinanzaController.js'
                        ]
                    }]);
                }]
            }
        })

        // Organizadores
        .state('organizadores', {
            url: "/organizers.html",
            templateUrl: "views/organizadores.html",
            data: {pageTitle: 'Organizers'},
            controller: "OrganizadoresController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',
                            '../assets/pages/css/profile.css',
                            '../assets/pages/scripts/profile.min.js',

                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',

                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',


                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                            '../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                            'js/controllers/OrganizadoresController.js',


                        ]
                    });
                }]
            }
        })



        // edit organizador
        .state('edOrganizador', {
            url: "/organizer/edit/:id",
            templateUrl: "views/organizador.html",
            data: {pageTitle: 'Organizer'},
            controller: "EditOrganization",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'angularFileUpload',
                        files: [
                            '../assets/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                        ]
                    }, {
                        name: 'DashBoardA',
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',


                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',


                            'js/controllers/OrganizadoresController.js',

                        ]
                    }]);
                }]
            }
        })
        // Asistentes por evento
        .state('tickets', {
            url: "/tickets/:id",
            templateUrl: "views/tickets.html",
            data: {pageTitle: 'Events'},
            controller: "TicketsController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',


                            'js/controllers/TicketsController.js',
                        ]
                    });
                }]
            }
        })
        // Eventos
        .state('eventos', {
            url: "/events.html",
            templateUrl: "views/eventos.html",
            data: {pageTitle: 'Events'},
            controller: "EventosController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',


                            'js/controllers/EventosController.js',
                        ]
                    });
                }]
            }
        })
        .state('eventosT', {
            url: "/events_tickets.html",
            templateUrl: "views/eventosT.html",
            data: {pageTitle: 'Events'},
            controller: "EventosController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',


                            'js/controllers/EventosController.js',
                        ]
                    });
                }]
            }
        })

        .state('evento', {
            url: "/event/",
            templateUrl: "views/evento.html",
            data: {pageTitle: 'Event'},
            controller: "AddEventoController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        [{
                            name: 'ui.select',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                            files: [
                                '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                            ]
                        }, {
                            name: 'DashBoardA',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                '../assets/global/plugins/morris/morris.css',
                                '../assets/global/plugins/morris/morris.min.js',
                                '../assets/global/plugins/morris/raphael-min.js',
                                '../assets/global/plugins/jquery.sparkline.min.js',
                                '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                                '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
                                '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',
                                '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',


                                '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                                '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                                '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                                '../assets/global/plugins/typeahead-addresspicker/dist/typeahead.bundle.min.js',
                                // '../assets/global/plugins/typeahead-addresspicker/dist/typeahead.jquery.js',
                                '../assets/global/plugins/typeahead-addresspicker/dist/typeahead-addresspicker.js',


                                '../assets/pages/scripts/components-date-time-pickers.min.js',


                                'js/controllers/EventosController.js',
                            ]
                        }]
                    );
                }]
            }
        })

        // Configuraciones
        .state('configuracion', {
            url: "/configurations.html",
            templateUrl: "views/configuraciones.html",
            data: {pageTitle: 'Configuration'},
            controller: "ConfiguracionesController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',


                            'js/controllers/ConfiguracionesController.js',
                        ]
                    });
                }]
            }
        })


        // categoria
        .state('categoria', {
            url: "/category/",
            templateUrl: "views/categoria.html",
            data: {pageTitle: 'Category'},
            controller: "AddCategoria",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',

                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',

                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                            'js/controllers/ConfiguracionesController.js',
                        ]
                    });
                }]
            }
        })

        // categoria
        .state('edCategory', {
            url: "/category/edit/:id",
            templateUrl: "views/categoria.html",
            data: {pageTitle: 'Category'},
            controller: "edCategoria",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        [{
                            name: 'ui.select',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                            files: [
                                '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                            ]
                        }, {
                            name: 'DashBoardA',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                '../assets/global/plugins/morris/morris.css',
                                '../assets/global/plugins/morris/morris.min.js',
                                '../assets/global/plugins/morris/raphael-min.js',
                                '../assets/global/plugins/jquery.sparkline.min.js',


                                '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                                '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                                '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                                '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                                '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                                '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                                '../assets/pages/scripts/components-date-time-pickers.min.js',
                                'js/controllers/ConfiguracionesController.js',
                            ]
                        }]);
                }]
            }
        })

        // categoria
        .state('entrada', {
            url: "/entrance/",
            templateUrl: "views/entrada.html",
            data: {pageTitle: 'Entrance'},
            controller: "AddEntrada",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',


                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                            'js/controllers/EventosController.js',
                        ]
                    });
                }]
            }
        })

        // categoria
        .state('edEentrance', {
            url: "/entrance/edit/:id",
            templateUrl: "views/entrada.html",
            data: {pageTitle: 'Entrance'},
            controller: "edEentrada",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',


                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                            'js/controllers/EventosController.js',
                        ]
                    });
                }]
            }
        })


        // pais
        .state('pais', {
            url: "/country/",
            templateUrl: "views/pais.html",
            data: {pageTitle: 'Country'},
            controller: "AddPais",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'DashBoardA',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                '../assets/global/plugins/morris/morris.css',
                                '../assets/global/plugins/morris/morris.min.js',
                                '../assets/global/plugins/morris/raphael-min.js',
                                '../assets/global/plugins/jquery.sparkline.min.js',

                                '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',

                                '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                                '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                                '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                                '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                                '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                                '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                                '../assets/pages/scripts/components-date-time-pickers.min.js',
                                'js/controllers/ConfiguracionesController.js',

                            ]
                        });
                }]
            }
        })

        // categoria
        .state('edPais', {
            url: "/country/edit/:id",
            templateUrl: "views/pais.html",
            data: {pageTitle: 'Country'},
            controller: "edPais",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',

                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',

                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                            'js/controllers/ConfiguracionesController.js',
                        ]
                    });
                }]
            }
        })


        // ciudad
        .state('ciudad', {
            url: "/city/",
            templateUrl: "views/ciudad.html",
            data: {pageTitle: 'City'},
            controller: "AddCiudad",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'ui.select',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    }, {
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',

                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',

                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                            'js/controllers/ConfiguracionesController.js',
                        ]
                    }]);
                }]
            }
        })

        // ciudad
        .state('edCiudad', {
            url: "/city/edit/:id",
            templateUrl: "views/ciudad.html",
            data: {pageTitle: 'City'},
            controller: "edCiudad",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'ui.select',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    }, {
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',

                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',

                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                            'js/controllers/ConfiguracionesController.js',
                        ]
                    }]);
                }]
            }
        })


        // categoria
        .state('localidad', {
            url: "/location/",
            templateUrl: "views/localidad.html",
            data: {pageTitle: 'Location'},
            controller: "AddCategoria",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',


                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                            'js/controllers/EventosController.js',
                        ]
                    });
                }]
            }
        })

        // Finanzas
        .state('finanzas', {
            url: "/finances.html",
            templateUrl: "views/finanzas.html",
            data: {pageTitle: 'Finances'},
            controller: "FinanzaController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',

                            '../assets/pages/scripts/dashboard.min.js',
                            'js/controllers/FinanzaController.js',

                        ]
                    });
                }]
            }
        })


        // Finanzas
        .state('slideshow', {
            url: "/slideshow.html",
            templateUrl: "views/slideshow.html",
            data: {pageTitle: 'Slideshow'},
            controller: "SlideshowController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '../assets/global/plugins/morris/morris.css',
                            '../assets/global/plugins/morris/morris.min.js',
                            '../assets/global/plugins/morris/raphael-min.js',
                            '../assets/global/plugins/jquery.sparkline.min.js',

                            '../assets/pages/scripts/dashboard.min.js',
                            'js/controllers/SlideshowController.js',

                        ]
                    });
                }]
            }
        })

        .state('notificacion', {
            url: "/sendNotification.html",
            templateUrl: "views/sendNotification.html",
            data: {pageTitle: 'Notifications'},
            controller: "SendNotification",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(
                        {
                            name: 'DashBoardA',
                            insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                            files: [
                                '../assets/global/plugins/morris/morris.css',
                                '../assets/global/plugins/morris/morris.min.js',
                                '../assets/global/plugins/morris/raphael-min.js',
                                '../assets/global/plugins/jquery.sparkline.min.js',

                                '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',

                                '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                                '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                                '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                                '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                                '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                                '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                                '../assets/pages/scripts/components-date-time-pickers.min.js',
                                'js/controllers/ConfiguracionesController.js',

                            ]
                        });
                }]
            }
        })

        // Blank Page
        .state('blank', {
            url: "/blank",
            templateUrl: "views/blank.html",
            data: {pageTitle: 'Blank Page Template'},
            controller: "BlankController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'js/controllers/BlankController.js'
                        ]
                    });
                }]
            }
        })

        // AngularJS plugins
        .state('fileupload', {
            url: "/file_upload.html",
            templateUrl: "views/file_upload.html",
            data: {pageTitle: 'AngularJS File Upload'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'angularFileUpload',
                        files: [
                            '../assets/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                        ]
                    }, {
                        name: 'DashBoardA',
                        files: [
                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // UI Select
        .state('uiselect', {
            url: "/ui_select.html",
            templateUrl: "views/ui_select.html",
            data: {pageTitle: 'AngularJS Ui Select'},
            controller: "UISelectController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'ui.select',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '../assets/global/plugins/angularjs/plugins/ui-select/select.min.js'
                        ]
                    }, {
                        name: 'DashBoardA',
                        files: [
                            'js/controllers/UISelectController.js'
                        ]
                    }]);
                }]
            }
        })

        // UI Bootstrap
        .state('uibootstrap', {
            url: "/ui_bootstrap.html",
            templateUrl: "views/ui_bootstrap.html",
            data: {pageTitle: 'AngularJS UI Bootstrap'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'DashBoardA',
                        files: [
                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // Tree View
        .state('tree', {
            url: "/tree",
            templateUrl: "views/tree.html",
            data: {pageTitle: 'jQuery Tree View'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/jstree/dist/themes/default/style.min.css',

                            '../assets/global/plugins/jstree/dist/jstree.min.js',
                            '../assets/pages/scripts/ui-tree.min.js',
                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // Form Tools
        .state('formtools', {
            url: "/form-tools",
            templateUrl: "views/form_tools.html",
            data: {pageTitle: 'Form Tools'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                            '../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
                            '../assets/global/plugins/typeahead/typeahead.css',

                            '../assets/global/plugins/fuelux/js/spinner.min.js',
                            '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                            '../assets/global/plugins/jquery.input-ip-address-control-1.0.min.js',
                            '../assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',
                            '../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                            '../assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                            '../assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js',
                            '../assets/global/plugins/typeahead/handlebars.min.js',
                            '../assets/global/plugins/typeahead/typeahead.bundle.min.js',
                            '../assets/pages/scripts/components-form-tools-2.min.js',

                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // Date & Time Pickers
        .state('pickers', {
            url: "/pickers",
            templateUrl: "views/pickers.html",
            data: {pageTitle: 'Date & Time Pickers'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [

                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css',

                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
                            '../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',

                            '../assets/pages/scripts/components-date-time-pickers.min.js',

                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // Custom Dropdowns
        .state('dropdowns', {
            url: "/dropdowns",
            templateUrl: "views/dropdowns.html",
            data: {pageTitle: 'Custom Dropdowns'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load([{
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            '../assets/global/plugins/select2/css/select2.min.css',
                            '../assets/global/plugins/select2/css/select2-bootstrap.min.css',

                            '../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            '../assets/global/plugins/select2/js/select2.full.min.js',

                            '../assets/pages/scripts/components-bootstrap-select.min.js',
                            '../assets/pages/scripts/components-select2.min.js',

                            'js/controllers/GeneralPageController.js'
                        ]
                    }]);
                }]
            }
        })

        // Advanced Datatables
        .state('datatablesmanaged', {
            url: "/datatables/managed.html",
            templateUrl: "views/datatables/managed.html",
            data: {pageTitle: 'Advanced Datatables'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/datatables/datatables.min.css',
                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',

                            '../assets/global/plugins/datatables/datatables.all.min.js',

                            '../assets/pages/scripts/table-datatables-managed.min.js',

                            'js/controllers/GeneralPageController.js'
                        ]
                    });
                }]
            }
        })

        // Ajax Datatables
        .state('datatablesajax', {
            url: "/datatables/ajax.html",
            templateUrl: "views/datatables/ajax.html",
            data: {pageTitle: 'Ajax Datatables'},
            controller: "GeneralPageController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/datatables/datatables.min.css',
                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',

                            '../assets/global/plugins/datatables/datatables.all.min.js',
                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/scripts/datatable.js',

                            'js/scripts/table-ajax.js',
                            'js/controllers/GeneralPageController.js'
                        ]
                    });
                }]
            }
        })

        // User Profile
        .state("profile", {
            url: "/producer",
            templateUrl: "views/profile/main.html",
            data: {pageTitle: 'User Profile'},
            controller: "UserProfileController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            '../assets/pages/css/profile.css',

                            '../assets/global/plugins/jquery.sparkline.min.js',
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',

                            '../assets/pages/scripts/profile.min.js',

                            'js/controllers/UserProfileController.js',

                        ]
                    });
                }]
            }
        })

        // User Profile Dashboard
        .state("profile.dashboard", {
            url: "/profile/:id",
            templateUrl: "views/profile/dashboard.html",
            data: {pageTitle: 'Organizer Profile'}
        })

        // Edit User Profile
        .state("profile-edit", {
            url: "/producer/editar",
            templateUrl: "views/profile/main-edit.html",
            data: {pageTitle: 'User Profile Edit'},
            controller: "UserProfileController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                            '../assets/pages/css/profile.css',

                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',

                            '../assets/global/plugins/jquery.sparkline.min.js',
                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',


                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                            '../assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',


                            '../assets/pages/scripts/profile.min.js',

                            'js/controllers/UserProfileController.js',

                        ]
                    });
                }]
            }
        })

        // User Profile Dashboard
        .state("profile-edit.main", {
            url: "/profile/:id",
            templateUrl: "views/profile/dashboard-edit.html",
            data: {pageTitle: 'Organizer Profile'}
        })

        // User Profile Account
        .state("profile.account", {
            url: "/account",
            templateUrl: "views/profile/account.html",
            data: {pageTitle: 'User Account'}
        })

        .state("profile.approve", {
            url: "/autorizar/:id",
            templateUrl: "views/profile/approve.html",
            data: {pageTitle: 'Aprobar Organizador'}
        })

        // User Profile Help
        .state("profile.help", {
            url: "/help",
            templateUrl: "views/profile/help.html",
            data: {pageTitle: 'User Help'}
        })

        // Todo
        .state('todo', {
            url: "/todo",
            templateUrl: "views/todo.html",
            data: {pageTitle: 'Todo'},
            controller: "TodoController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashBoardA',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
                            '../assets/apps/css/todo-2.css',
                            '../assets/global/plugins/select2/css/select2.min.css',
                            '../assets/global/plugins/select2/css/select2-bootstrap.min.css',

                            '../assets/global/plugins/select2/js/select2.full.min.js',

                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',

                            '../assets/apps/scripts/todo-2.min.js',

                            'js/controllers/TodoController.js'
                        ]
                    });
                }]
            }
        })

}]);

/* Init global settings and run the app */
DashBoardA.run(["$rootScope", "$location", "settings", "$cookieStore", "$state", "$templateCache", function ($rootScope, $location, settings, $cookieStore, $state, $templateCache) {
    // $rootScope.$on('$locationChangeSuccess', function() {
    //     $templateCache.removeAll();
    // });

    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
    // keep user logged in after page refresh
    $rootScope.globals = $cookieStore.get('globals') || {};
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
    }

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
        var loggedIn = localStorage.getItem('id');
        if (restrictedPage && !loggedIn) {
            window.location = $rootScope.$settings.pathRedirect + "admin/";
        }
    });
}]);


