<?php

function sendMessage(){
	    //$mensaje  = $_POST['mensaje'];
        $mensaje  = file_get_contents('php://input');
        $mensaje  = json_decode($mensaje, true);
		$content = array(
			"en" => $mensaje['mensaje']
		);
		
		$fields = array(
			'app_id' => "5b207345-0a44-4aaa-938e-e73112b16b73",
			'included_segments' => array('All'),
            'data' => array(),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
        print("\nJSON sent:\n");
        print($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
													'Authorization: Basic ZmRhODNjZmQtYjQ2Yy00NmU4LTljNDUtNjVkZGFjYzM3NzNl'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}

	$response = sendMessage();
	$return["allresponses"] = $response;
	$return = json_encode( $return);
	
	print("\n\nJSON received:\n");
	print($return);
	print("\n");
?>