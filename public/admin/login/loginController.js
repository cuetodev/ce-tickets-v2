(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'AuthenticationService', 'FlashService','$window'];
    function LoginController($location, AuthenticationService, FlashService, $window) {
        var vm = this;
        vm.login = login;

        (function initController() {
            if(localStorage.getItem("token")){
                $window.location.href = 'Dashboard-Admin/index.html';
            }
        })();

        function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    $window.location.href = 'Dashboard-Admin/index.html';
                    console.log("acceso correcto");
                } else {
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        };
    }
	

})();