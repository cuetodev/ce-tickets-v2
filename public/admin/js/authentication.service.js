(function () {
    'use strict';

    angular
        .module('app')
        .factory('AuthenticationService', AuthenticationService);

    AuthenticationService.$inject = ['$http', '$rootScope','$location'];
    function AuthenticationService($http, $rootScope, $location) {
        var service = {};
        service.Login = Login;

        return service;

        function Login(username, password, callback) {
            $http.post('/api/auth', { email: username, password: password })
            .then(function successCallback(response){
                var token = response.data.api_token;
                var id= "";
                var rol="";
                $http.defaults.headers.common['Authorization'] = 'Bearer ' +token;
                $http.get('/api/user')
                    .then(function successCallback(response){
                        id = response.data.id;
                        $http.defaults.headers.common['Authorization'] = 'Bearer ' +token;
                        $http.get( '/api/users/'+id+'?include[]=roles&include[]=organization&include[]=country&include[]=city')
                            .then(function successCallback(response){
                                var temp = response.data.roles.data.length;
								var isValidRol = false;
						        for(var i = 0 ; i < temp ; i++){
									if(response.data.roles.data[i].name == "organizer" || response.data.roles.data[i].name == "manager" ){
										isValidRol = true;
									}
								}
								if(isValidRol) {
									localStorage.setItem('token', token);
									localStorage.setItem('id', id);
									callback({ success: true, message: 'DATOS CORRECTOS' });
                                } else {
                                    callback({ success: false, message: 'You can`t access to this area' });
									}
                            }, function errorCallback(response){
                        });
                    }, function errorCallback(response){
                });
            }, function errorCallback(response){
                callback({ success: false, message: response.data.error });
                localStorage.clear();
            });
        }

    }
})();